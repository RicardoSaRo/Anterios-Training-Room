varying vec2 v_vTexcoord;
varying vec4 v_vColour;
void main()
{
    vec4 col = texture2D(gm_BaseTexture, v_vTexcoord)*v_vColour;
    vec3 lum = vec3(0.299, 0.587, 0.114);
    float gray = dot(col.rgb, lum);
    gl_FragColor = vec4(gray, gray, gray, col.a);
    gl_FragColor.rgb *= vec3(1.15,1.1,0.8);
}