varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform sampler2D tex;

void main()
{
    vec4 texture = texture2D(tex,v_vTexcoord);
    gl_FragColor = texture2D( gm_BaseTexture,v_vTexcoord-vec2(texture.r/5.0-0.1,texture.g/5.0-0.1)) * v_vColour;
} 