varying vec2 v_vTexcoord;
varying vec4 v_vColour;
uniform vec3 size;// width, height, intensity
vec3 getvalue(vec3 norm,vec3 offset,float amount)
{
    return (abs(norm.r-offset.r)+abs(norm.g-offset.g)+abs(norm.b-offset.b))*norm*amount;
}

void main()
{
    vec3 left = v_vColour.rgb * texture2D( gm_BaseTexture, v_vTexcoord + vec2(-2./size.x,0.)).rgb;
    vec3 up = v_vColour.rgb * texture2D( gm_BaseTexture, v_vTexcoord + vec2(0.,-2./size.y)).rgb;
    vec3 right = v_vColour.rgb * texture2D( gm_BaseTexture, v_vTexcoord + vec2(2./size.x,0.)).rgb;
    vec3 down = v_vColour.rgb * texture2D( gm_BaseTexture, v_vTexcoord + vec2(0.,2./size.y)).rgb;
    vec4 norm = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
    gl_FragColor = vec4(vec3(getvalue(norm.rgb,left,size.z)+getvalue(norm.rgb,up,size.z)+getvalue(norm.rgb,right,size.z)+getvalue(norm.rgb,down,size.z))/4.0,norm.a);
} 