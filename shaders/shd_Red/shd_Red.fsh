//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 OriginalColor = texture2D( gm_BaseTexture, v_vTexcoord );
	
	float Red = OriginalColor.r;
	float Green = 0.0;
	float Blue = 0.0;
	float Alfa = 1.0;
	
	vec4 Color = vec4( Red, Green, Blue, Alfa );
	
	gl_FragColor = Color;
}
