varying vec2 v_vTexcoord;
varying vec2 v_vPosition;

uniform vec3 col_light;		// RGB
//uniform vec3 col_dark;	// RGB
uniform vec2 light_pos;		// Room coords
uniform vec2 texel_size;
uniform float steps;		// or effect distance. Pass in as integer > 0

#define MAX_BRIGHTNESS 0.3
#define BRIGHTNESS_RANGE 256.0
#define BRIGHTNESS_STEP 16.0

void main()
{
    
	float dis = length(v_vPosition - light_pos);
	vec2 light_vec = (v_vPosition - light_pos) / dis * texel_size;
	
	vec4 base_col = texture2D( gm_BaseTexture, v_vTexcoord );
	
	// VERSION SMOOTH SHADE:
	//----------------------------------------------------------------------------------------------------
	/*
	float neighbor_light_alpha = 1.0; float neighbor_dark_alpha  = 1.0;
	for (float i = 1.0; i <= steps; i++) {
		neighbor_light_alpha	+= texture2D( gm_BaseTexture, v_vTexcoord - i * light_vec).a;
		//neighbor_dark_alpha		+= texture2D( gm_BaseTexture, v_vTexcoord + i * light_vec).a;
	}
	neighbor_light_alpha /= steps;
	//neighbor_dark_alpha /= steps;
	
	
	//VERSION HARD SHADE - SLOW (often but not always better):
	//----------------------------------------------------------------------------------------------------
	float neighbor_light_alpha = 1.0; float neighbor_dark_alpha  = 1.0;
	for (float i = 1.0; i <= steps; i++) {
		neighbor_light_alpha	*= texture2D( gm_BaseTexture, v_vTexcoord - i * light_vec).a;
		//neighbor_dark_alpha		*= texture2D( gm_BaseTexture, v_vTexcoord + i * light_vec).a;
	}
	*/
	//VERSION HARD SHADE - FAST (often but not always worse):
	//----------------------------------------------------------------------------------------------------
	float neighbor_light_alpha = texture2D( gm_BaseTexture, v_vTexcoord - steps * light_vec).a;
	//dark colour doesn't work well in this faster version

	// KEEP FOR ALL VERSIONS:
	//----------------------------------------------------------------------------------------------------
	//base_col.rgb = mix(col_dark,  base_col.rgb, neighbor_dark_alpha);
	//float brightness = floor(max(MAX_BRIGHTNESS - dis/BRIGHTNESS_RANGE, 0.0) * BRIGHTNESS_STEP) / BRIGHTNESS_STEP;
	float brightness = max(MAX_BRIGHTNESS - dis/BRIGHTNESS_RANGE, 0.0);
	base_col.rgb = mix(col_light + brightness, base_col.rgb, neighbor_light_alpha);

	gl_FragColor = base_col;
}