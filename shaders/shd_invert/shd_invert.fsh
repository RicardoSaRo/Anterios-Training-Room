varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    gl_FragColor = texture2D( gm_BaseTexture,v_vTexcoord) * v_vColour;
    gl_FragColor.rgb = 1.0-gl_FragColor.rgb;
} 