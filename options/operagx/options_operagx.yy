{
  "option_operagx_version": "0.0.0.5",
  "option_operagx_next_version": "0.0.0.6",
  "option_operagx_game_name": "Anterios - Training Room-",
  "option_operagx_interpolate_pixels": false,
  "option_operagx_scale": 1,
  "option_operagx_texture_page": "2048x2048",
  "option_operagx_display_cursor": false,
  "option_operagx_guid": "75f4e267-d948-4a41-9c15-9deff8947d17",
  "option_operagx_team_name": "rsrstudios studio",
  "option_operagx_team_id": "0bcbbe8c-1985-4176-9995-23f83b53f96a",
  "option_operagx_editUrl": "https://dc.gxc.gg/game/75f4e267-d948-4a41-9c15-9deff8947d17/edit",
  "option_operagx_internalShareUrl": "https://gxc.gg/games/nprzob/anterios-training-room-/tracks/1a223985-725d-43d9-8833-d24689cd7eb0/",
  "option_operagx_publicShareUrl": "https://gxc.gg/games/nprzob/anterios-training-room-/",
  "resourceVersion": "1.0",
  "name": "operagx",
  "tags": [],
  "resourceType": "GMOperaGXOptions",
}