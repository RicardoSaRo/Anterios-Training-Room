1.0.0←ed6a955d-5826-4f98-a450-10b414266c27←ed6a955d-5826-4f98-a450-10b414266c27|{
    "option_gameguid": "faadba7a-0401-49e0-94d9-edbeaa8130a8",
    "option_lastchanged": "15 July 2019 5:37:42",
    "option_game_speed": 60
}←be5f1418-b31b-48af-a023-f04cdf6e5121|{
    "textureGroups": {
        "Additions": [
            {
                "Key": 1,
                "Value": {
                    "id": "2db640fe-0f9e-4840-b64b-36e1adb8b37d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Werewolf_Gold",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 2,
                "Value": {
                    "id": "b73dcf6f-aa6c-4d8d-b08b-b0d663ec3c09",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "GGolem1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 3,
                "Value": {
                    "id": "41d54423-2951-49ae-8ac8-3f75dbb9c124",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BGolem1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 4,
                "Value": {
                    "id": "809ca826-85ff-4999-b032-55fca7803051",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Manticore",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 5,
                "Value": {
                    "id": "d97d8029-1163-401a-8420-09e8762a3153",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Manticore_Hitboxes",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 6,
                "Value": {
                    "id": "79f3badd-634f-496b-bbbb-9e165f3b6250",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Throwing_Weapons",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 7,
                "Value": {
                    "id": "3b7bc802-c0ba-4c02-9850-e47987924ac6",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Enemy_Bullets_1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 8,
                "Value": {
                    "id": "5828e03a-a9ff-424d-a1e9-ed0aefa19a7b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Werewolf_Brown",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 9,
                "Value": {
                    "id": "2ab1f80c-fa3b-4991-bdd5-e5f44e0ad387",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Werewolf_Pink",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 10,
                "Value": {
                    "id": "d0a694d8-fcaa-449e-a4cd-eb5e1056e17e",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Werewolf_Gray",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 11,
                "Value": {
                    "id": "59d73f29-919f-4e63-8830-7ff7263a2950",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Werewolf_Foxy",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 12,
                "Value": {
                    "id": "01619a90-531d-4fd6-861e-255e63dd1e7c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Golem_Cave",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 13,
                "Value": {
                    "id": "ac788274-a66e-4237-878d-bb0b3030a6db",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Title_Screen",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 14,
                "Value": {
                    "id": "dd4e37b0-538a-4526-bfb5-8c4bdd72ba60",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Map",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 15,
                "Value": {
                    "id": "33f502d6-24c8-4426-a5c0-73ceeea03e79",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "OCrab1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 16,
                "Value": {
                    "id": "6e0c7c97-564b-4528-a89a-626cef8d91e4",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "ONeon1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 17,
                "Value": {
                    "id": "2d7920e5-2d6d-432e-9219-0c661174b118",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Graveyards",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 18,
                "Value": {
                    "id": "db8a44d2-a21f-4b87-b1cc-29dd37fcb988",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Beach1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 19,
                "Value": {
                    "id": "482c3076-4801-4a91-933a-85fedad20475",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Dragonkin1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 20,
                "Value": {
                    "id": "7452092a-0e75-4480-992c-fb69281edc66",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Wasteland",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 21,
                "Value": {
                    "id": "f4e2215b-06eb-4531-9098-76ca9527bba6",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Wastipede1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 22,
                "Value": {
                    "id": "feaa64fb-3633-4d6e-96fa-048ad652146d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Meadows",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 23,
                "Value": {
                    "id": "9f5d986f-8a1d-43c4-b2ff-374449ae8197",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Defeated",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 24,
                "Value": {
                    "id": "c8e90c6a-1e2f-483f-9e56-5b723b4d8429",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Meetonak",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 25,
                "Value": {
                    "id": "d8724ece-5f33-49a9-9ed7-fc9e2cf728d8",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "FX",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 26,
                "Value": {
                    "id": "a14286b1-ed81-476f-aa09-de669e931471",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordF1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 27,
                "Value": {
                    "id": "484e5e45-51a1-4cec-a25f-e462babe3ea5",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordI1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 28,
                "Value": {
                    "id": "cdbdef8f-a2a4-4cc4-acda-9e9f74bc5a54",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "GUI",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 29,
                "Value": {
                    "id": "7cc170f3-ffc0-46c0-b213-80fa5005bb6b",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Enemy_Bullets_2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 30,
                "Value": {
                    "id": "556026be-be13-491a-a8f8-c17771b5032e",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Enemy_Bullets_DR",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 31,
                "Value": {
                    "id": "2a35175d-dfa6-43be-ba0c-49d2b302823a",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Watchtowers",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 32,
                "Value": {
                    "id": "afeda952-beb3-4aff-9dfe-b2de88449e1f",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Weapons_Menu",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 33,
                "Value": {
                    "id": "84b4cf27-db96-47ae-8fd7-88a414e4a931",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Labels",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 34,
                "Value": {
                    "id": "de24ab65-3f02-4c75-a5c6-296030766dcc",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Manticore_Specials",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 35,
                "Value": {
                    "id": "2bb05439-522a-4737-88a1-dae80e819ff4",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Enemy_Bullets_3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 36,
                "Value": {
                    "id": "26ac470c-c726-4f0d-8da1-6c3ffcd92cde",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLord_WingsTail",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 37,
                "Value": {
                    "id": "4058436d-29b4-49cf-8c58-a308838e4205",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordF2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 38,
                "Value": {
                    "id": "b305854d-6e6b-4df9-96eb-22c15bec5f5d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordF3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 39,
                "Value": {
                    "id": "0b85787a-aa9c-41dd-ad0b-ad5970b51279",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordI2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 40,
                "Value": {
                    "id": "3459ff18-d87b-4291-b6ea-004c3bbac12c",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "DragonLordI3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 41,
                "Value": {
                    "id": "d76a9c0e-5753-4f33-9f9b-217b7c2818ec",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Beach2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 42,
                "Value": {
                    "id": "9b115306-6681-48e8-9a2f-d3dfbc65e355",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "GGolem2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 43,
                "Value": {
                    "id": "3ca67e5f-984d-4aa2-935d-26392120dffb",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "BGolem2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 44,
                "Value": {
                    "id": "7d1f8636-5ae2-4f15-923d-baff5dbd7ce5",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "OCrab2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 45,
                "Value": {
                    "id": "d4262991-7174-4a8b-b144-65c731978e2f",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "ONeon2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 46,
                "Value": {
                    "id": "a5ad1fa7-b555-4f8d-8933-f655accdc995",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Dragonkin2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 47,
                "Value": {
                    "id": "c5856391-be62-4e00-8aba-9b428d6bf4b5",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Wastipede2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 48,
                "Value": {
                    "id": "efab7009-66a7-4531-a113-168bf3962a89",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Twinpeaks",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 49,
                "Value": {
                    "id": "1da8a3af-d1ee-44ab-b941-a08e90118ffb",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Rose1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 50,
                "Value": {
                    "id": "0a526868-6af4-45e2-a4a6-87ef8d7ac490",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Rose2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 51,
                "Value": {
                    "id": "e1ac5e86-7393-4783-8e11-00b41a58e878",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Gardens1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 52,
                "Value": {
                    "id": "054a89d9-80ad-4886-a1e3-7f97d0252ed6",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Rose3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 53,
                "Value": {
                    "id": "52720b22-a01b-4461-b569-a35493d6b9e6",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Orchid1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 54,
                "Value": {
                    "id": "4d55ce7c-bb45-4edd-a00c-8664099c0f81",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Orchid2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 55,
                "Value": {
                    "id": "f745c1fd-e6cd-470a-8282-44ad5d21aa2e",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Orchid3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 56,
                "Value": {
                    "id": "5ac65111-f9ad-4619-bbc9-753428d7447f",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Aster1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 57,
                "Value": {
                    "id": "32739dd5-c200-439e-ae7f-7db229f8f285",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Aster2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 58,
                "Value": {
                    "id": "61d08de5-a2ab-47a6-bfb5-74669e683d37",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Aster3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 59,
                "Value": {
                    "id": "da70fd96-3da7-4e13-808e-b486984f7365",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Ixia1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 60,
                "Value": {
                    "id": "d217ab31-8c9b-482c-84ff-8962ab707c4f",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Ixia2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 61,
                "Value": {
                    "id": "8974820b-0d6d-4ac1-a676-d34a5ad8f90d",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Ixia3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 62,
                "Value": {
                    "id": "1c0229f2-09a3-4424-baa0-8a8a7abaadef",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Dahlia1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 63,
                "Value": {
                    "id": "3d38fb0d-4b49-4df9-989d-45485ca373e6",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Dahlia2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 64,
                "Value": {
                    "id": "52dc3187-666a-4cba-a215-ef8426ccf638",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Dahlia3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 65,
                "Value": {
                    "id": "e7a68537-55db-402b-bb61-fb52a07196e2",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Lotus1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 66,
                "Value": {
                    "id": "ddc75484-59ed-4683-8b21-8295f21effa0",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Lotus2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 67,
                "Value": {
                    "id": "8164f61f-d3bc-4f89-8301-c2c7be3ac1ec",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Alraune_Lotus3",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 68,
                "Value": {
                    "id": "e701f2e5-2718-4c88-b369-cf99c7c10158",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Gardens2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 69,
                "Value": {
                    "id": "02919c35-5efd-4903-82e6-f5a794e2a2a2",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "Reliks",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 70,
                "Value": {
                    "id": "911605b7-82c6-4e28-b6b2-fbb4bf2eabbd",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "ExpBCKGRNDS1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 71,
                "Value": {
                    "id": "3d6f77fe-98ca-4db4-b74e-ac76d0599da5",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "VenomLord_1",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            },
            {
                "Key": 72,
                "Value": {
                    "id": "de065f3e-39d7-480d-a4dd-b87781339c30",
                    "modelName": "GMTextureGroup",
                    "mvc": "1.0",
                    "groupName": "VenomLord_2",
                    "targets": 461609314234257646,
                    "autocrop": true,
                    "border": 2,
                    "groupParent": "00000000-0000-0000-0000-000000000000",
                    "mipsToGenerate": 0,
                    "scaled": true
                }
            }
        ],
        "Checksum": "ࢺ懜璙ᓆ锯翋꤯맫",
        "Deletions": [
            
        ],
        "Ordering": [
            
        ]
    }
}←1225f6b0-ac20-43bd-a82e-be73fa0b6f4f|{
    "targets": 461609314234257646
}←7b2c4976-1e09-44e5-8256-c527145e03bb|{
    "targets": 461609314234257646
}