hsp = 0;
vsp = 0;
prevVSP = 0;
grv = 1;
walksp = 9; //--> No decimals to prevent sprite shaking
direc = 0;

Manticore_hp = (global.RELIKS[48,2]) ? global.player_max_health/2 : global.player_max_health;
auto_move = false;
regen_stop = (global.HuntRoom == noone) ? true : false; //--> If true, stops regen on Alarm5

airDash_used = false;
lghtDash_used = false;
walljump_used = false;
wallcling_cooldown = false;
doublejump_used = false;
windjump_used = false;
ExpUpDash = false;
air_attack = false;
platfDrop = false;
stepsSndFX = snd_RunningStone;
stepsPlatSndFX = snd_RunningStone;

neareastMovPlat = self; //--> For moving Platforms
oldPlatX = x;
oldPlatY = y;

onPlatform = false;

equiped_sword = 1;
equipped_axe = 1;
equipped_tw = 1;
tw_limit = false;
TWammo = array_create(16,0); //--> TWammo[X,0] is for current ammo, TWammo[X,1] is for Max amount
TWammo[1,0] = -4;
TWammo[1,1] = -4;
TWammo[2,0] = (global.TWs[2,3]==0) ? 80 : ((global.TWs[2,3]==1) ? 120 : ((global.TWs[2,3]==2) ? 160 : 200));
TWammo[2,1] = TWammo[2,0];
TWammo[3,0] = (global.TWs[3,3]==0) ? 10 : ((global.TWs[3,3]==1) ? 15 : ((global.TWs[3,3]==2) ? 20 : 25));
TWammo[3,1] = TWammo[3,0];
TWammo[4,0] = (global.TWs[4,3]==0) ? 6 : ((global.TWs[4,3]==1) ? 8 : ((global.TWs[4,3]==2) ? 10 : 12));
TWammo[4,1] = TWammo[4,0];
TWammo[5,0] = (global.TWs[5,3]==0) ? 20 : ((global.TWs[5,3]==1) ? 25 : ((global.TWs[5,3]==2) ? 30 : 35));
TWammo[5,1] = TWammo[5,0];
TWammo[6,0] = (global.TWs[6,3]==0) ? 20 : ((global.TWs[6,3]==1) ? 30 : ((global.TWs[6,3]==2) ? 40 : 50));
TWammo[6,1] = TWammo[6,0];
TWammo[7,0] = -4;
TWammo[7,1] = -4;
TWammo[8,0] = (global.TWs[8,3]==0) ? 90 : ((global.TWs[8,3]==1) ? 120 : ((global.TWs[8,3]==2) ? 160 : 200));
TWammo[8,1] = TWammo[8,0];
TWammo[9,0] = (global.TWs[9,3]==0) ? 15 : ((global.TWs[9,3]==1) ? 20 : ((global.TWs[9,3]==2) ? 25 : 30));
TWammo[9,1] = TWammo[9,0];
TWammo[10,0] = -4;
TWammo[10,1] = -4;
TWammo[11,0] = -4;
TWammo[11,1] = -4;
TWammo[12,0] = -4;
TWammo[12,1] = -4;
TWammo[13,0] = -4;
TWammo[13,1] = -4;
TWammo[14,0] = (global.TWs[14,3]==0) ? 8 : ((global.TWs[14,3]==1) ? 10 : ((global.TWs[14,3]==2) ? 12 : 14));
TWammo[14,1] = TWammo[14,0];
TWammo[15,0] = -4;
TWammo[15,1] = -4;
TWammo[16,0] = (global.TWs[16,3]==0) ? 9 : ((global.TWs[16,3]==1) ? 12 : ((global.TWs[16,3]==2) ? 15 : 18));
TWammo[16,1] = TWammo[16,0];

Gir_Tab_hit_used = false;

atk_type = 0;
attack_number = 1;
aux_Axe_atk_num = 1;
aux_Swd_atk_num = 1;

beginX_Manticore = x;
beginY_Manticore = y;
Boundary_minY = 0
Boundary_maxY = 0;
Boundary_minX = 0
Boundary_maxX = 0;

special_Argenta = 0;
special_Dyrnwyn = 0;
special_GirTab = 0;
special_Frostbiter = (global.RELIKS[13,2]) ? 50 : 0;
special_Korunax = (global.RELIKS[7,2]) ? 100 : 0;
special_Mirage = (global.RELIKS[43,2]) ? 40 : 0;
special_Mjolnir = (global.RELIKS[43,2]) ? 40 : 0;;
special_Whisperer = (global.RELIKS[8,2]) ? 100 : ((global.RELIKS[43,2]) ? 40 : 0);
VRcounter = 0;
TWTank = array_create(4,0);
for (var i = 1; i <= 4; i++)
{
	TWTank[i,0] = 0;
	if (i == 1) TWTank[i,0] = (global.RELIKS[18,2]) ? 7000 : 0;
	TWTank[i,1] = (i == 1) ? 1 : 0;
}

lowHealth = false;
spCharge_spd = (global.RELIKS[23,2]) ? 45 : 60;
current_sp_sword = special_Argenta;
current_sp_axe = special_Korunax;
switch_var = false;
alarm[8] = 120;
alarm[9] = 120;

state = move_Manticore;