/// @description Ends Dashes

ExpUpDash = false;

image_index = 0;

if (state == dash_Manticore)
{
	airDash_used = true;
	state = move_Manticore;
	alarm[4] = (global.RELIKS[28,2]) ? 60 : 10; //--> Dash Cooldown
}

if (state == wallattack_Manticore)
{
	if (instance_exists(obj_Wallattack))
	{
		if (obj_Wallattack.WAtype < 5)
		{
			sprite_index = spr_Manticore_spinning;
			image_index = 0;
			state = move_Manticore;
			doublejump_used = false;
			airDash_used = false;
			windjump_used = false;
			lghtDash_used = false;
		}
		else
		{
			sprite_index = spr_Manticore_doublejmp;
			image_index = 2;
			state = doublejump_Manticore;
		}
		
		obj_Wallattack.endShckwv = true;
	}
	else
	{
		sprite_index = spr_Manticore_doublejmp;
		image_index = 2;
		state = doublejump_Manticore;
	}
	
}