/// @description Wallcling Timeup release

if state == wallcling_Manticore  
{
	if (image_xscale == -1) x += -20;
	if (image_xscale == 1) x += 20; //--> Prevents getting stuck on wall after cling
	vsp = 0;
	wallcling_cooldown = true;
	alarm[2] = 20;
	state = move_Manticore;
}

//state = move_Manticore;