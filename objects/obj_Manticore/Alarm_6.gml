/// @description Stun Break
// You can write your code in this editor

if (state = stun_Manticore)
{
	state = move_Manticore;
	if (hsp != 0)
	{
		var oFloor = place_meeting(x,y+1,obj_Wall);

		//--> onPlatform Variable check
		var oPlat = false;
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	    (place_meeting(x,y+1,obj_Platform)) oPlat = true;
		else oPlat = false;
		
		if (oPlat) || (oFloor)
		{
			image_index = 0;
			sprite_index = spr_Manticore_landing;
		}
	}
}