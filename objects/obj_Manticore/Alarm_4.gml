/// @description Dash cooldown

//airDash_used = true;

if (global.RELIKS[28,2])
{
	for (var i = 1; i <= 6; i++)
	{
		//--> Substitute for circlingObjFX
		var FFX = instance_create_layer(x,y,"FX_Layer",obj_circlemovFX);
		FFX.sprite_index = spr_wFeatherFX;
		FFX.alarm[0] = 20;
		FFX.angle = i * 60;
		FFX.inst2circle = self;
	}
}