/// @description Special Axe Charge
// You can write your code in this editor

var chargeAmount = 1;
var currentCharge = 1;
if (artist.burnFX) chargeAmount = -2;
if (global.HuntRoom == noone) || (global.RELIKS[43,2]) chargeAmount = 0;

switch(equipped_axe)
{
	case 1: currentCharge = special_Korunax;
			special_Korunax += chargeAmount;
			special_Korunax = clamp(special_Korunax,0,100);
			break;
	case 2: currentCharge = special_Mirage;
			special_Mirage += chargeAmount;
			special_Mirage = clamp(special_Mirage,0,100);
			break;
	case 3: currentCharge = special_Mjolnir;
			special_Mjolnir += chargeAmount;
			special_Mjolnir = clamp(special_Mjolnir,0,100);
			break;
	case 4: currentCharge = special_Whisperer;
			special_Whisperer += chargeAmount;
			special_Whisperer = clamp(special_Whisperer,0,100);
			break;
}

if ((equipped_axe == 1)&&((special_Korunax == 100)&&(currentCharge < 100))) ||
   ((equipped_axe == 2)&&((special_Mirage == 100)&&(currentCharge < 100))) ||
   ((equipped_axe == 3)&&((special_Mjolnir == 100)&&(currentCharge < 100))) ||
   ((equipped_axe == 4)&&((special_Whisperer == 100)&&(currentCharge < 100))) &&
   (!audio_is_playing(snd_SPBar50)) && (!audio_is_playing(snd_SPBarFull))
{
	audio_sound_pitch(snd_SPBarFull,0.9);
	audio_play_sound(snd_SPBarFull,10,0);
}

//--> Special Ready FX
if ((equipped_axe == 1)&&((special_Korunax >= 50)&&(currentCharge < 50))) ||
   ((equipped_axe == 2)&&((special_Mirage >= 50)&&(currentCharge < 50))) ||
   ((equipped_axe == 3)&&((special_Mjolnir >= 50)&&(currentCharge < 50))) ||
   ((equipped_axe == 4)&&((special_Whisperer >= 50)&&(currentCharge < 50))) &&
   (!audio_is_playing(snd_SPBar50)) && (!audio_is_playing(snd_SPBarFull))
{
		audio_sound_pitch(snd_SPBar50,0.9);
		audio_play_sound(snd_SPBar50,10,0);
}

alarm[8] = spCharge_spd;