/// @description Regen over time

if (Manticore_hp == global.player_max_health) || (regen_stop) alarm[5] = noone;
else
{
	image_alpha = 0.8;
	image_blend = c_red;

	with (instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_LO_fx))
	{
		image_speed = 1;
		sprite_index = spr_Life_Orb_exp;
		image_alpha = 0.4;
		var plyrmnger = global.currentManager;
		image_blend = (plyrmnger.ManticoreVenomed) ? c_aqua : c_white;
	}
	
	if (global.RELIKS[3,2])
	{
		var healPlus = irandom(9);
		if (healPlus <= 2)
		{
			Manticore_hp += 1;
			Manticore_hp = clamp(Manticore_hp,0,global.player_max_health);
		}
	}
}

artist.regenFX = 0;