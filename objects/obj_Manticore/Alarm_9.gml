/// @description Special Sword Charge
// You can write your code in this editor
var chargeAmount = 1;
var currentCharge = 1;
if (artist.burnFX) chargeAmount = -2;
if (global.HuntRoom == noone) || (global.RELIKS[43,2]) chargeAmount = 0;

switch(equiped_sword)
{
	case 1: currentCharge = special_Argenta;
			special_Argenta += chargeAmount;
			special_Argenta = clamp(special_Argenta,0,100);
			break;
	case 2: currentCharge = special_Dyrnwyn;
			special_Dyrnwyn += chargeAmount;
			special_Dyrnwyn = clamp(special_Dyrnwyn,0,100);
			break;
	case 3: currentCharge = special_GirTab;
			special_GirTab += chargeAmount;
			special_GirTab = clamp(special_GirTab,0,100);
			break;
	case 4: currentCharge = special_Frostbiter;
			special_Frostbiter += chargeAmount;
			special_Frostbiter = clamp(special_Frostbiter,0,100);
			break;
}

if ((equiped_sword == 1)&&((special_Argenta == 100)&&(currentCharge < 100))) ||
   ((equiped_sword == 2)&&((special_Dyrnwyn == 100)&&(currentCharge < 100))) ||
   ((equiped_sword == 3)&&((special_GirTab == 100)&&(currentCharge < 100))) ||
   ((equiped_sword == 4)&&((special_Frostbiter == 100)&&(currentCharge < 100)))
{
	audio_play_sound(snd_SPBarFull,10,0);
}

//--> Special Ready FX
if ((equiped_sword == 1)&&((special_Argenta >= 50)&&(currentCharge < 50))) ||
   ((equiped_sword == 2)&&((special_Dyrnwyn >= 50)&&(currentCharge < 50))) ||
   ((equiped_sword == 3)&&((special_GirTab >= 50)&&(currentCharge < 50))) ||
   ((equiped_sword == 4)&&((special_Frostbiter >= 50)&&(currentCharge < 50)))
{
		audio_play_sound(snd_SPBar50,10,0);
}

alarm[9] = spCharge_spd;