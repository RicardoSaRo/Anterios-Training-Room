/// @description Insert description here
// You can write your code in this editor

if (!createSwitch)
{
	image_alpha += 0.05;
	if (image_alpha >= 0.4)
	{
		createSwitch = true;
		if (ROmngr.alarm[2] > 0)&&(!ROmngr.ManticoreVenomed) ROmngr.alarm[2] = 0;
		if (ROmngr.alarm[3] > 0) ROmngr.alarm[3] = 0;
		if (ROmngr.alarm[4] > 0) ROmngr.alarm[4] = 0;
	}
}

if (!permanent)&&(!ROmngr.ManticoreVenomed)
{
	if (artist.max_Regen > 60) artist.max_Regen = 60;
	if (obj_Manticore.alarm[5] > 60) obj_Manticore.alarm[5] = 60;
}

if (destroySwitch)&&(!permanent)
{
	image_alpha -= 0.05;
	if (image_alpha <= 0) instance_destroy();
}

if (instance_exists(obj2fllw))
{
	x = obj2fllw.x;
	y = obj2fllw.y;
	image_angle += obj2fllw.image_xscale;
}

//--> PRIORITY CHECK!!!
if (place_meeting(x,y,obj_EnemyBullet))
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (nearBullet.priority != 10)
		if (nearBullet.priority <= rsPrio) nearBullet.sprite_index = nearBullet.destroy_sprite;
}