/// @description Insert description here
// You can write your code in this editor
var sdwPrl1 = (global.RELIKS[40,2]) ? c_purple : c_red;

if (permanent)
{
	obj_Manticore.Manticore_hp += 4;
	obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
	if (ROmngr.alarm[2] > 0) ROmngr.alarm[2] = 0;
	if (ROmngr.alarm[3] > 0) ROmngr.alarm[3] = 0;
	if (ROmngr.alarm[4] > 0) ROmngr.alarm[4] = 0;
	fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,256,4,12,20,1,sdwPrl1,1);
	audio_play_sound(snd_AntaresOrb,10,0);
}
else
{
	if (ROmngr.ManticoreVenomed)
	{
		obj_Manticore.Manticore_hp += 4;
		obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
	}
	destroySwitch = true;
	fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,256,4,12,20,1,sdwPrl1,1);
	audio_play_sound(snd_AntaresOrb,10,0);
	obj2fllw = self;
}