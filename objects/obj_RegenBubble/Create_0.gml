/// @description Insert description here
// You can write your code in this editor

permanent = (global.RELIKS[30,2]) ? true : false;

if (global.RELIKS[40,2]) sprite_index = spr_RegenVoidBubble;
else sprite_index = spr_RegenBubble;

obj2fllw = obj_Manticore;

ROmngr = global.currentManager;

if (!permanent)
{
	if (!ROmngr.ManticoreVenomed) if (artist.max_Regen > 60) artist.max_Regen = 60;
	var sdwPrl1 = (global.RELIKS[40,2]) ? c_purple : c_red;
	fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,256,4,12,20,1,sdwPrl1,1);
	audio_play_sound(snd_GirTabSP1,12,0);
	audio_play_sound(snd_AntaresOrb,10,0);
}

var sdwPrl2 = (global.RELIKS[40,2]) ? global.SWORDS[3,3] : 0;
rsPrio = (permanent) ? 1 + sdwPrl2 : 7 + sdwPrl2;

createSwitch = false;
destroySwitch = false;
image_alpha = 0;