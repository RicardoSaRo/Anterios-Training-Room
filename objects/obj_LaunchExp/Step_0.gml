/// @description Insert description here
// You can write your code in this editor
if (place_meeting(x,y,obj_BossEnemy)) && (!LEhit_used)
{
	dmg_calc(LEhit_used,sprite_index);
	LEhit_used = true;
}

if (place_meeting(x,y,obj_EnemyBullet))
{
	var bullet = instance_place(x,y,obj_EnemyBullet);
	if (bullet.sprite_index != bullet.destroy_sprite) && (bullet.priority != 10)
	{
		bullet.sprite_index = bullet.destroy_sprite;
		bullet.image_index = 0;
	}
}