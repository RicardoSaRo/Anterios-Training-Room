/// @description Insert description here
// You can write your code in this editor

alarm[0] = 50;
sprite_index = spr_chispa_fx;

switch(obj_Manticore.equiped_sword)
{
	case 1: coloroption1 = c_silver; break;
	case 2: coloroption1 = make_color_rgb(241,100,31); break;
	case 3: coloroption1 = c_red; break;
	case 4: coloroption1 = c_aqua; break;
}

switch(obj_Manticore.equipped_axe)
{
	case 1: coloroption2 = make_color_rgb(211,151,65); break;
	case 2: coloroption2 = c_fuchsia; break;
	case 3: coloroption2 = make_color_rgb(39,100,205); break;
	case 4: coloroption2 = c_lime; break;
}

x1 = -10;
x2 = 10;
y1 = 1;
y2 = 3;

angl = 0;

//image_blend = choose(coloroption1,coloroption2);