/// @description Insert description here
// You can write your code in this editor

if (sprite_index == spr_hmmng_flame)
{
	var hommes = (wmov) ? choose(1,2) : 1;
	chasing_target = instance_nearest(x,y,obj_BossEnemy);

	var closestTarget = instance_nearest(x,y,chasing_target);
	if (hommes == 1)
	{
		x = lerp(x,chasing_target.x,0.1)
		y = lerp(y,chasing_target.y,0.1)
	}
	else
	{
		x += irandom_range(-30,30);
		y += irandom_range(-30,30);
	}
	
	for (var i = 0; i < 5; i++)
	{
		var chispa = instance_create_layer(x,y,"FX_Layer",obj_chispa);
		with (chispa)
		{
			chispa_colour = c_red;
			x += random_range(-15,15);
			y += random_range(-15,15);
		}
	}
	
	image_angle = point_direction(x,y,chasing_target.x,chasing_target.y)
}

if (place_meeting(x,y,obj_BossEnemy))
{
	image_angle = 0;
	if (sprite_index == spr_hmmng_flame) y += 60;
	sprite_index = spr_Explosion_hmmgF;
}

if (sprite_index == spr_Explosion_hmmgF)
{
	if (place_meeting(x,y,obj_BossEnemy))
	{
		if (!hf_hit_used) audio_play_sound(snd_shortExplosion,16,0);
		dmg_calc(hf_hit_used,sprite_index);
		hf_hit_used = true;
	}
	if (image_index > 12) instance_destroy();
}

if (place_meeting(x,y,obj_EnemyBullet)) sprite_index = spr_Explosion_hmmgF;