/// @description Insert description here
// You can write your code in this editor

if (image_xscale = -1) Angulo = 180;
else Angulo = 0;

if (instance_exists(obj_AntaresOrb))
{
	image_xscale = abs(distance_to_object(obj_AntaresOrb));
	Angulo = point_direction(x,y,obj_AntaresOrb.x,obj_AntaresOrb.y);
}

image_index = irandom_range(0,2);

image_blend = c_white;

alarm[0] = 4;

/*if (!audio_is_playing(snd_EnergyShot))*/ audio_play_sound(snd_EnergyShot,5,0);
//if (!audio_is_playing(snd_lightning1)) audio_play_sound(snd_lightning1,5,0);
if (audio_is_playing(snd_lightning1)) audio_play_sound(snd_lightning2,5,0);

//--> Shockwave FX
	fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],8,48,2,15,-20,1,c_white,1);