/// @description Insert description here
// You can write your code in this editor
sprite_index = spr_GraniteShield_create;

alarm[0] = (global.AXES[1,3]*30) + 500 + (obj_Manticore.current_sp_axe * 4);
if (obj_Manticore.current_sp_axe == 100) alarm[0] += 200;

alarm_switch = false;

GS_acc_spd = 0;

audio_play_sound(snd_GraniteShieldCreate,20,0);