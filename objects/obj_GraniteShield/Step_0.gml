/// @description Insert description here
// You can write your code in this editor

	

if (sprite_index == spr_GraniteShield_create)
{
	obj_Manticore.special_Korunax -= 3;
	obj_Manticore.special_Korunax = clamp(obj_Manticore.special_Korunax,0,100);
	obj_Manticore.alarm[9] = 60;
	
	if (image_index > 13)
	{
		image_speed = 1;
		sprite_index = spr_GraniteShield;
		x += 100 * image_xscale;
		y -= 15;
	}
}

if (sprite_index == spr_GraniteShield)
{
	image_speed = 0;
	image_xscale = 1;
	
	//--> Shield sprite matches positioning
	if (x > obj_Manticore.x-30) && (x < obj_Manticore.x+30) image_index = 1;
	else
	{
		if (x > obj_Manticore.x+30) image_index = 0;
		if (x < obj_Manticore.x-30) image_index = 2;
	}
	
	//--> Shield Movement
	var ManticoreX = obj_Manticore.x + (65 * obj_Manticore.image_xscale);
	var ManticoreY = obj_Manticore.y;
	
	//--> Shield Speed to chatch up player
	var shieldSPD = GS_acc_spd;
	
	if (image_index == 2 and obj_Manticore.image_xscale == 1) or
	   (image_index == 0 and obj_Manticore.image_xscale == -1) or
	   (image_index == 1)
	{
		if (image_index == 1)
		{
			var max_acc = 0.35;
			GS_acc_spd += 0.005;
		}
		else
		{
			var max_acc = 0.15;
			GS_acc_spd += 0.0025;
		}		
		GS_acc_spd = clamp(GS_acc_spd,0,max_acc);
		shieldSPD = GS_acc_spd;
	}
	if (image_index == 2 and obj_Manticore.image_xscale == -1) or
	   (image_index == 0 and obj_Manticore.image_xscale == 1)
	{
	   shieldSPD = 0.35;
	   GS_acc_spd = 0;
	}		
	
	x = lerp(x,ManticoreX,shieldSPD);
	y = lerp(y,ManticoreY,0.25);
	
	//--> Invincibility when Manticore attacks
//	if (obj_Manticore.state == sword_atks_Manticore) or (obj_Manticore.state == axe_atks_Manticore) or
//	   (obj_Manticore.state == doublejump_Manticore) or (obj_Manticore.state == special_Manticore)

	if (instance_exists(obj_Manticore_hitbox)) 
	{
		if (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox1) or
		   (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox2) or
		   (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox3) or
		   (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox4) or
		   (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox5) or
		   (obj_Manticore_hitbox.sprite_index == spr_Korunax_hitbox6)
		{
			with (obj_Manticore)
			{
				if !(alarm[11] > 3)
				{
					alarm[11] = 3;
					image_blend = c_yellow;
				}
			}
		}
	}
	
	//-->> Dash Invincibility
	if (obj_Manticore.state == dash_Manticore) or (obj_Manticore.state == wallcling_Manticore)
	{
		with (obj_Manticore)
		{
			if (alarm[0] > 0)
			{
				alarm[11] = alarm[0];
				image_blend = c_yellow;
			}
			if (alarm[1] > 0)
			{
				alarm[11] = alarm[1];
				image_blend = c_yellow;
			}
		}
	}
	
	image_blend = obj_Manticore.image_blend;
//	image_alpha = obj_Manticore.image_alpha;
}

if (alarm[0] <= 1) && (sprite_index != spr_GraniteShield_destroy)
{
	image_speed = 1;
	sprite_index = spr_GraniteShield_destroy;
	image_index = 0;
	if (obj_Manticore.image_blend == c_yellow) obj_Manticore.image_blend = c_white;
}

if (sprite_index == spr_GraniteShield_destroy) && (image_index > 6) instance_destroy();

//--> EnemyBullets COLLISION
if (place_meeting(x,y,obj_EnemyBullet))
{
	var bpoof = instance_nearest(x,y,obj_EnemyBullet);
	
	if (bpoof.priority != 10) bpoof.sprite_index = bpoof.destroy_sprite;
}