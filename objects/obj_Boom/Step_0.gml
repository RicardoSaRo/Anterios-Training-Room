/// @description Insert description here
// You can write your code in this editor

if (place_meeting(x,y,obj_Manticore)) //--> ONE HIT KILL!!!! + FXs
{
	consecutive_dmg = -1;
	repeat(global.player_max_health - 1) 
	{
		consecutive_dmg += 1;
		var lifeorb = instance_find(obj_Life_Orb,consecutive_dmg);
		lifeorb.sprite_index = spr_Life_Orb_empty;
		lifeorb.dmg = consecutive_dmg+1;
	}
	obj_Manticore.Manticore_hp -= global.player_max_health;
	obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
}

if (image_index >= 14) instance_destroy();