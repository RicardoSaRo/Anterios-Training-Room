/// @description Insert description here
// You can write your code in this editor

if (obj_Manticore.alarm[0] > 60)
{
//--> Resets Special Bar
obj_Manticore.special_Frostbiter -= 3;
obj_Manticore.special_Frostbiter = clamp(obj_Manticore.special_Frostbiter,0,100);
}

image_speed = 1;

var ManticoreX = obj_Manticore.x + (5 * obj_Manticore.image_xscale);
var ManticoreY = obj_Manticore.y;

fstblwrk_increm += 0.01;
//fstblwrk_increm = clamp(fstblwrk_increm,0,1);

if (fstblwrk_catch == false)
{
	x = lerp(x,ManticoreX,fstblwrk_increm);
	y = lerp(y,ManticoreY,fstblwrk_increm);
	
	if (x == ManticoreX) && (y == ManticoreY) fstblwrk_catch = true;
}
else
{
	x = ManticoreX;
	y = ManticoreY;
}

var rndmChspX = x + random_range(-100,100);
var rndmChspY = y + random_range(-100,100);
var chispa = instance_create_layer(rndmChspX,rndmChspY,"FX_Layer",obj_chispa);
with (chispa) chispa_colour = c_silver;

if (alarm[0] <= 1) sprite_index = spr_FrostBulwark_breaks;

if (sprite_index == spr_FrostBulwark_create) && (image_index > 5) sprite_index = spr_FrostBulwark;

if (sprite_index == spr_FrostBulwark) if (!audio_is_playing(snd_FrostBulwark)) audio_play_sound(snd_FrostBulwark,10,0);

if (alarm[0] mod 3 == 0) dmg_calc(fstblwrk_hit_used,spr_FrostBulwark); //--> Bulwark Damage

if (sprite_index == spr_FrostBulwark_breaks) && (image_index > 5)
{
	audio_stop_sound(snd_FrostBulwark);
	if (!audio_is_playing(snd_FrostBulwark_destroy)) audio_play_sound(snd_FrostBulwark_destroy,10,0);
	instance_destroy();
}

if (place_meeting(x,y,obj_EnemyBullet))
{
	var bullet = instance_place(x,y,obj_EnemyBullet);
	if (bullet.sprite_index != bullet.destroy_sprite) && (bullet.priority != 10)
	{
		bullet.sprite_index = bullet.destroy_sprite;
		bullet.image_index = 0;
	}
}