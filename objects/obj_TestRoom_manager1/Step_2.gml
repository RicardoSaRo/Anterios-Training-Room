/// @description Insert description here
// You can write your code in this editor

obj_Manticore.x = round(obj_Manticore.x);
obj_Manticore.y = round(obj_Manticore.y);

if (obj_Manticore.y > room_height)
{
	obj_Manticore.y = ystart;
	obj_Manticore.x = xstart;
}

if (obj_Manticore.x < Boundary_minX)
{
	with (obj_Manticore)
	{
		var hspd = sign(hsp);
		if (hspd < 0) x -= hsp;
		if (place_meeting(x,y+1,obj_Wall)) sprite_index = spr_Manticore;
		hsp = 0;
		if (key_lshift) alarm[4] = 2;
	}
}

if (obj_Manticore.x > Boundary_maxX)
{
	with (obj_Manticore)
	{
		var hspd = sign(hsp);
		if (hspd > 0) x -= hsp;
		if (place_meeting(x,y+1,obj_Wall)) sprite_index = spr_Manticore;
		hsp = 0;
		if (key_lshift) alarm[4] = 2;
	}
}

//--> Poison Mechanic
if (ManticorePoisoned) obj_Manticore.alarm[5] += 1;

layer_x(BCKGRND,(global.view_x-250)/1.03);
layer_y(BCKGRND,(global.view_y-800)/1.03);

//--> Wither Flowers


//--> PARALLAX STUFF!!!

//with(obj_expBckgrndE7) x += (obj_Manticore.hsp / 35) * -1;

//with(obj_expBckgrndE8) x += (obj_Manticore.hsp / 35) * -1;

/*
layer_x(BackGrass,global.view_x/1.6);
layer_x(BTrees,global.view_x/1.6);
layer_x(FTrees,global.view_x/2);
layer_x(FTrees2,global.view_x/2.5);
layer_x(Vines1,global.view_x/-6);
layer_x(Vines2,global.view_x/-4);
//layer_y(Clouds,global.view_y/6);
layer_x(LeavesUP1,global.view_x/-8);
layer_x(LeavesUP2,global.view_x/-6);
layer_x(LeavesUP3,global.view_x/-7);
layer_x(ParaStump,1056+global.view_x/70);
//layer_x(Back_Trees,global.view_x/2);
//layer_x(Sky,global.view_x*0.7);
//layer_x(Clouds,global.view_x*0.7);
//layer_x(Palm1,(global.view_x-50)/-4);
//layer_x(Palm2,1800+global.view_x/-4);
//layer_x(Grass,global.view_x/-4);