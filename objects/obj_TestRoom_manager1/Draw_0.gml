/// @description Insert description here
// You can write your code in this editor
if (obj_Manticore.state == damage_Manticore)
{
	if (!audio_is_playing(snd_ManticoreDmg1)) && (!audio_is_playing(snd_ManticoreDmg2)) && (!audio_is_playing(snd_ManticoreDmg3))
		audio_play_sound(choose(snd_ManticoreDmg1,snd_ManticoreDmg2,snd_ManticoreDmg3),2,0);
	
	gpu_set_blendmode(bm_add);
	for(c = 0;c < 360;c += 360)
	{
		draw_sprite_ext(spr_dmgFX_Manticore,choose(1,2,3,3),obj_Manticore.x,obj_Manticore.y,2,2,random(360),c_white,1);
	}
	gpu_set_blendmode(bm_normal);
}

if (instance_exists(obj_Enemy_hitbox))
{
	with(obj_Enemy_hitbox)
	{
		if (place_meeting(x,y,obj_Manticore))
			draw_sprite_ext(spr_PoisonDMG_FX,choose(1,2,3,3),obj_Manticore.x,obj_Manticore.y,2,2,random(360),c_white,1);
	}
}

if (instance_exists(obj_EnemyBullet))
{
	with(obj_EnemyBullet)
	{
		if (place_meeting(x,y,obj_Manticore)) && (sprite_index == spr_Wstpd_Needle) && (obj_Manticore.state == damage_Manticore)
			draw_sprite_ext(spr_PoisonDMG_FX,choose(1,2,3,3),obj_Manticore.x,obj_Manticore.y,2,2,random(360),c_white,1);
	}
}
