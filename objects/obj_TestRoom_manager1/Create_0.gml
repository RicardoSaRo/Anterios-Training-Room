/// @description Insert description here

display_set_gui_size(1920,1080);

global.ExpRoom = room;
global.currentManager = self;
timerSecs = 0;
levelMusic = snd_silence;
global.player_max_health = 16;

//--> Creates 2 instances of Boss object
//Boss1 = instance_create_layer(608,512,"Enemy_Layer",obj_BossEnemy);
Boss = instance_create_layer(950,424,"Enemy_Layer",obj_BossEnemy);
obj_BossEnemy.enemy_name = "TestBot";
BossName = obj_BossEnemy.enemy_name;
obj_BossEnemy.sprite_index = spr_TrainingBot_ON;
obj_BossEnemy.defeated_spr = spr_TrainingBot_ON;
obj_BossEnemy.hit_points = 900000;
obj_BossEnemy.old_hit_points = 900000;
obj_BossEnemy.max_hit_points = 900000;
obj_BossEnemy.image_index = 0;
obj_BossEnemy.image_speed = 0;
obj_BossEnemy.bounce = 1;
obj_BossEnemy.AtkTurn = 1;
obj_BossEnemy.right_var = 1;
obj_BossEnemy.left_var = 1;

obj_BossEnemy.state = Tbot_Standing;
/*
//obj_BossEnemy.alarm[0] = 200;
obj_BossEnemy.hit_points = 22000;
obj_BossEnemy.old_hit_points = 22000;
obj_BossEnemy.max_hit_points = 22000;
obj_BossEnemy.minX = 576;//64
obj_BossEnemy.maxX = 4927;//2240
obj_BossEnemy.default_h = 960;//960
*/
//--> Manticore Boundary Limits
Boundary_minY = 158;//152
Boundary_maxY = 1119;//1064
Boundary_minX = 287;//445
Boundary_maxX = 1825;//2115
obj_Manticore.Boundary_minY = 158;//152
obj_Manticore.Boundary_maxY = 1119;//1064
obj_Manticore.Boundary_minX = 287;//445
obj_Manticore.Boundary_maxX = 1825;//2115

obj_Manticore.state = stageStart_Manticore;
obj_Manticore.alarm[2] = 100;


//--> For Poison Mechaninc
ManticorePoisoned = false;
ManticoreVenomed = false;
ManticoreBurn = false;
ManticoreFreeze = false;

//--> Trigger Vars
exitTrigger = false;
ownerTrigger = false;
cpuTrigger = false;

interact = false;

//--> Lights Variables


//Manticore = instance_create_layer(575,225,"Player_Layer",obj_Manticore);

//--> RANDOMIZE AGAIN!!!
randomize();


bckgrnd_color = 255;

finishStage = false;

BCKGRND = layer_get_id("Background");
Parallax1 = layer_get_id("Parallax1");
BCKPara1 = layer_background_get_id("Parallax1");

Black2Alfa = 1;
LoadingScreen = layer_get_id("Loading");
LoadingScreen_id = layer_background_get_id(LoadingScreen);
counter = 1;
alfaAmount = 0.1;

//GL_Stalac2 = layer_get_id("Stalatite2");
//GL_Light1 = layer_get_id("GLair_Light");
//GL_Light2 = layer_get_id("GLair_Light_1");
//GL_Light3 = layer_get_id("GLair_Light_1_1");

/*
//--> CLOUD SHADER
steps			= 4;
col_light		= [0.9, 0.5, 0.5];
//col_dark		= [0.1, 0.1, 0.2];

var tex			= sprite_get_texture(spr_Sunset_Clouds, 0);
texel_size		= [texture_get_texel_width(tex), texture_get_texel_height(tex)];

shader			= shd_cloud_fx;
u_col_light		= shader_get_uniform(shader, "col_light");
//u_col_dark		= shader_get_uniform(shader, "col_dark");
u_light_pos		= shader_get_uniform(shader, "light_pos");
u_texel_size	= shader_get_uniform(shader, "texel_size");
u_steps			= shader_get_uniform(shader, "steps");
*/

alarm[10] = 1;