/// @description Insert description here
// You can write your code in this editor
//show_debug_overlay(true);

var mngrKeyUp = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);

if (layer_background_get_alpha(LoadingScreen_id) > 0) && (!finishStage)
{
	var loadAlfa = layer_background_get_alpha(LoadingScreen_id);
	layer_background_alpha(LoadingScreen_id,loadAlfa - 0.1);
}

if (finishStage)
{
	if (layer_background_get_alpha(LoadingScreen_id) < 1) 
	{
		var loadAlfa = layer_background_get_alpha(LoadingScreen_id);
		layer_background_alpha(LoadingScreen_id,loadAlfa + 0.1);
	}
	else
	{
		room_goto(rm_title);
	}
}
else
{
	if (layer_background_get_alpha(LoadingScreen_id) > 0)
	{
		var loadAlfa = layer_background_get_alpha(LoadingScreen_id);
		layer_background_alpha(LoadingScreen_id,loadAlfa - 0.1);
	}
}

gamepad_set_axis_deadzone(global.controller_type,0.4);

quick_access();//--> Lets player change weapons for Manticore quickly

//--> Defeated Code
if (obj_Manticore.Manticore_hp <= 0) obj_Manticore.state = defeated_Manticore;
if (artist.fade2red >= 1) room_goto(rm_Defeat);


//--> CPU, DOOR and OWNER interactions
if (obj_Manticore.x > 366) && (obj_Manticore.x < 458)
{
	exitTrigger = false;
	with(obj_Manticore) other.exitTrigger = place_meeting(x,y,obj_Trigger_room);
	
	if (exitTrigger)
	{
		if (!instance_exists(obj_placeholder))
		{
			var leave = instance_create_layer(416,980,"Info_Layer",obj_placeholder);
			leave.sprite_index = spr_LeaveUP;
			leave.image_xscale = 2;
			leave.image_yscale = 2;
			leave.image_speed = 1;
		}
	
		if (mngrKeyUp)
		{
			finishStage = true;
		}
	}
}
else exitTrigger = false;

if (obj_Manticore.x > 653) && (obj_Manticore.x < 684)
{
	cpuTrigger = false;
	with (obj_Manticore) other.cpuTrigger = place_meeting(x,y,obj_Trigger_room);

	if (cpuTrigger) 
	{
		if (!instance_exists(obj_placeholder))
		{
			var read = instance_create_layer(672,400,"Info_Layer",obj_placeholder);
			read.sprite_index = spr_ReadUP;
			read.image_speed = 1;
			read.image_xscale = 2;
			read.image_yscale = 2;
		}
	}
}
else cpuTrigger = false;

if ((!cpuTrigger) && (!ownerTrigger) && (!exitTrigger)) if (instance_exists(obj_placeholder)) instance_destroy(obj_placeholder);

if (ManticorePoisoned)
{
	if (global.RELIKS[10,2])||(global.RELIKS[26,2])
	{
		artist.poisonFX = false;
		ManticorePoisoned = false;
		alarm[2] = 0;
	}
	else
	{
		if (alarm[2] mod 5 == 0)
		{
			var bbl = instance_create_layer(obj_Manticore.x,obj_Manticore.y-(random_range(0,30)),choose("Back_FX_Layer","FX_Layer"),obj_bubble);
			bbl.coloroption1 = (ManticoreVenomed) ? c_fuchsia : c_purple;
			bbl.coloroption2 = make_color_rgb(73,65,130);
			obj_Manticore.image_blend = (ManticoreVenomed) ? merge_color(c_purple,c_fuchsia,0.5) : c_purple;
		}
		if (alarm[2] mod 10 == 0) if (obj_Manticore.image_blend == c_purple) obj_Manticore.image_blend = c_white;
		if (global.RELIKS[13,2]) && (alarm[2] > 1) alarm[2] -= 1;
	}
}

if (ManticoreVenomed)
{
	var bbl = instance_create_layer(obj_Manticore.x,obj_Manticore.y-(random_range(0,30)),choose("Back_FX_Layer","FX_Layer"),obj_bubble);
	bbl.coloroption1 = c_fuchsia;
	bbl.coloroption2 = make_color_rgb(73,65,130);
	obj_Manticore.image_blend = merge_color(c_purple,c_fuchsia,0.5);
}

if (ManticoreFreeze)
{
	if (global.RELIKS[11,2])||(global.RELIKS[26,2])
	{
		artist.freezeFX = false;
		ManticoreFreeze = false;
		alarm[3] = 0;
	}
	else
	{
		if (alarm[3] mod 6 == 0)
		{
			var snowflake = instance_create_layer(obj_Manticore.x+(random_range(-30,30)),obj_Manticore.y-(random_range(0,30)),choose("Back_FX_Layer","FX_Layer"),obj_GrassLeaf);
			snowflake.sprite_index = spr_snowflake;
			snowflake.image_index = irandom(5);
			snowflake.image_blend = choose(c_white,c_aqua);
			obj_Manticore.image_blend = c_aqua;
		}
		if (global.RELIKS[13,2]) && (alarm[3] > 1) alarm[3] -= 1;
	}
}

if (ManticoreBurn)
{
	if (global.RELIKS[12,2])||(global.RELIKS[26,2])
	{
		artist.burnFX = false;
		ManticoreBurn = false;
		alarm[4] = 0;
	}
	else
	{
		if (alarm[4] mod 4 == 0)
		{
			var bbl = instance_create_layer(obj_Manticore.x,obj_Manticore.y-(random_range(0,30)),choose("Back_FX_Layer","FX_Layer"),obj_bubble);
			bbl.sprite_index = spr_burn_red;
			bbl.image_index = irandom(2);
			bbl.alarm[0] = 20;
			bbl.coloroption1 = c_red;
			bbl.coloroption2 = c_orange;
			obj_Manticore.image_blend = c_orange;
		}
		if (alarm[4] mod 10 == 0) if (obj_Manticore.image_blend == c_orange) obj_Manticore.image_blend = c_white;
		if (global.RELIKS[13,2]) && (alarm[4] > 1) alarm[4] -= 1;
	}
}

with (obj_Manticore)
{
	var dust = irandom(20);
	if (dust)
	{
		if place_meeting(x,y+1,obj_Wall)
		{
			if (sprite_index == spr_Manticore_run) or (sprite_index == spr_Manticore_bdash) or
			   (sprite_index == spr_Manticore_fdash) or (sprite_index == spr_Manticore_jump)
			{
				var dust = instance_create_layer(x+(random_range(-40,40)),bbox_bottom,"FX_Layer",obj_GrassLeaf);
				dust.alarm[0] = irandom_range(10,15);
				dust.raise = choose(3,4,5,6);
				dust.xmov = choose(1,1.5,2) * choose(-1,1);
				dust.sprite_index = spr_poof;
			}
		}
	}
}
