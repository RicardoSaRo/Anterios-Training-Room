/// @description Insert description here
// You can write your code in this editor
var wpnMenu_exists = instance_exists(obj_WeaponsMenu);
var mnMenu_exists = instance_exists(obj_MainMenu);
var videoMenu_exists = instance_exists(obj_VideoMenu);

if (wpnMenu_exists) || (mnMenu_exists) || (videoMenu_exists)
{
	if (wpnMenu_exists)
	{
		if (obj_WeaponsMenu.closing_menu) && (!obj_WeaponsMenu.openMain) menuBlkBkgnd = obj_WeaponsMenu.image_xscale/1.1;
		else menuBlkBkgnd = 1/1.1;
	}

	if (mnMenu_exists)
	{
		if (obj_MainMenu.closing_menu) && ((!obj_MainMenu.openWpns)&&(!obj_MainMenu.openOptions)) menuBlkBkgnd = obj_MainMenu.image_xscale;
		else menuBlkBkgnd = 1/1.1;
	}

	draw_set_alpha(menuBlkBkgnd);
	draw_rectangle_color(global.view_x,global.view_y,global.view_x+global.vsizeW,global.view_y+global.vsizeH,c_black,c_black,c_black,c_black,0);
	draw_set_alpha(1);
}
else menuBlkBkgnd = 0;