/// @description Insert description here
// You can write your code in this editor

if (alarm[0] > 3)  and (alarm[0] mod 10 == 0)
{
		var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
}

if (alarm[0] > 585)
{
	if (x < goalX) x += random_range(7,15);
	else x -= random_range(7,15);
	if (y < goalY) y += random_range(3,25);
	else y -= random_range(3,25);

}
else // Stop moving to target positions to move towards Manticore
{
	var closestTarget = instance_nearest(x,y,chasing_target);
	var choice = irandom(chase_speed);
	
	if (choice == 1)
	{
		if (closestTarget.x > x) x += 4 + (global.AXES[2,3]);
		else x -= 4 + (global.AXES[2,3]);
		if (closestTarget.y > y) y += 4 + (global.AXES[2,3]);
		else y -= 4 + (global.AXES[2,3]);
	}
	else
	{
		x += random_range(-4,4);
		y += random_range(-4,4);
	}
}

flame_colour = choose(c_lime,c_purple);
if (image_blend != c_purple) image_blend = choose(c_lime,c_green);

if (alarm[0] mod 6 == 0) dmg_calc(wowsp_hit_used,sprite_index);

if (alarm[0] <= 0)
{
	if image_alpha > 0 image_alpha -= 0.05;
	else instance_destroy();
}

//--> PRIORITY CHECK!!!
if (place_meeting(x,y,obj_EnemyBullet))
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (priority != 10)
		if (nearBullet.priority <= priority) nearBullet.sprite_index = nearBullet.destroy_sprite;
}