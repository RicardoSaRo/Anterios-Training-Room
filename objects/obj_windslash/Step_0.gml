/// @description Insert description here

if (alarm[0] mod 5 == 0) dmg_calc(ws_hit_used,sprite_index);

if (fades) image_alpha -= (source == "Whisperer") ? 0.2 : 0.4;

if (place_meeting(x,y,obj_Wall)) or (image_alpha <= 0) instance_destroy();

x += ws_movementX;

var rndmY = choose(ws_movementY1,ws_movementY2);

wss_onFloor = place_meeting(x,y+rndmY,obj_Wall);

if (!wss_onFloor) y += lerp(y,rndmY,1);
else
{
	y += rndmY;
}