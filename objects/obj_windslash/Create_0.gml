/// @description Insert description here
hsp = 12;

direc = obj_Manticore.image_xscale;

ws_hit_used = false;

fades = false;

source = "Whisperer";

image_xscale = direc;

if (source == "Whisperer")
{
	alarm[0] = 10 - (global.AXES[4,3]);
	alarm[1] = (global.AXES[4,3]*10) + 50;
}
else
{
	alarm[0] = 10 - (global.TWs[11,3]);
	alarm[1] = (global.TWs[11,3]*3);
}

ws_movementY1 = random_range(-5,5);
ws_movementY2 = ws_movementY1;
ws_movementX = hsp * direc;

image_angle = 0;