/// @description Insert description here

with (obj_Manticore)
{
	if (image_blend = c_red)
	{
		if (other.image_index mod 2 == 1) image_blend = c_white;
		else image_blend = c_red;
		other.x = x;
		other.y = y;
	}
}