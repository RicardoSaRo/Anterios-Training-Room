/// @description Insert description here
// You can write your code in this editor
obj_Manticore.special_Argenta -= 4;
obj_Manticore.special_Argenta = clamp(obj_Manticore.special_Argenta,0,100);
obj_Manticore.alarm[9] = 60;

if (place_meeting(x,y,obj_BossEnemy))
{
	var Special_var = instance_place(x,y,obj_BossEnemy);
	if (!Special_var.special_var)
	{
		dmg_calc(arg_wave_hit_used,spr_Argenta_wave2);
		Special_var.special_var = true;
	}
}


if (sprite_index == spr_Argenta_wave1) && (image_index < 2)
{
	audio_play_sound(snd_ArgentaWave2,15,0);
	sprite_index = spr_Argenta_wave2;
}
	
if (alarm[0] < 0) image_alpha -= 0.1;

if (image_alpha < 0) instance_destroy();

x += incremental;

incremental *= 2;

incremental = clamp(incremental,0,60*sign(incremental));

var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
afterimage.sprite_index = sprite_index;
afterimage.image_index = image_index;
afterimage.image_xscale = image_xscale;
afterimage.image_blend = c_gray;
afterimage.image_alpha = 0.6;

repeat(6)
{
	var chispa = instance_create_layer(x+(random_range(-25,25)),y+(random_range(-150,150)),"FX_Layer",obj_chispa);
	chispa.chispa_colour = c_gray;
	var rndmSpark = random_range(1,4);
	chispa.image_xscale = rndmSpark;
	chispa.image_yscale = rndmSpark;
}

//--> TW COLLISION
if (place_meeting(x,y,obj_EnemyBullet))
{
	var bpoof = instance_nearest(x,y,obj_EnemyBullet);
	
	if (bpoof.priority != 10) bpoof.sprite_index = bpoof.destroy_sprite;
}