/// @description Insert description here
// You can write your code in this editor

if (place_meeting(x,y,obj_Manticore))
{
	if (artist.poisonFX == false)
	{
		artist.poisonFX = true;
		if (instance_exists(obj_AlrauneRoom_manager))
		{
			with (obj_AlrauneRoom_manager)
			{
				ManticorePoisoned = true;
				switch(global.difficulty)
				{
					case "casual":	alarm[2] = 240; break;
					case "normal":	alarm[2] = 300; break;
					case "hard":	alarm[2] = 360; break;
					case "lunatic":	alarm[2] = 420; break;
				}
			}
		}
	}
}

if (place_meeting(x,y,obj_Wall)) || (place_meeting(x,y,obj_windslash)) || (place_meeting(x,y,obj_Tornado))
{
	sprite_index = spr_PoisonPowder_destroy;
	image_index = 0;
}

if (place_meeting(x,y,obj_Throwing_Wpn))
{
	var nrInst = instance_nearest(x,y,obj_Throwing_Wpn)
	if (nrInst.sprite_index == spr_Void_Ripple)
	{
		sprite_index = spr_PoisonPowder_destroy;
		image_index = 0;
	}
}

if (place_meeting(x,y,obj_TWSpecial))
{
	var nrInst = instance_nearest(x,y,obj_TWSpecial)
	if (nrInst.sprite_index == spr_miniRipple)
	{
		sprite_index = spr_PoisonPowder_destroy;
		image_index = 0;
	}
}

if (PPhsp > 0) PPhsp -= 0.5;
if (PPhsp < 0) PPhsp += 0.5;
if (PPvsp < 0) PPvsp += fallspd;
else PPvsp = 1;
x += PPhsp;
y += PPvsp;

if (sprite_index == spr_PoisonPowder_destroy) if (image_index >= 3.6) instance_destroy();