/// @description Insert description here
// You can write your code in this editor

if (!isFront)
{
	draw_self();

	gpu_set_blendmode(bm_add);
	for(c = 0;c < 360;c += 18)
	{
		draw_sprite_ext(spr_light_orb,image_index,x+lengthdir_x(3,c),y+lengthdir_y(3,c),lgt_orb_sizeX,lgt_orb_sizeY,image_angle,image_blend,lgt_orb_alfa-0.25)
	}
	gpu_set_blendmode(bm_normal);
}