/// @description Insert description here
// You can write your code in this editor
var old_ltob_alfa = lgt_orb_alfa;

if (!increm_stop) lgt_orb_alfa += 0.001;

if (alarm[0] <= 1) lgt_orb_alfa -= 0.001;

lgt_orb_alfa = clamp(lgt_orb_alfa,0,0.3);

lerp(lgt_orb_alfa,old_ltob_alfa,0.01); 

if (lgt_orb_alfa == 0) instance_destroy();

y = lerp(y,y-random_range(0.0001,2),maxspd); 