/// @description Insert description here
// You can write your code in this editor

x = lerp(x,obj_Manticore.x,trnd_spd);

var dmgFrec = 16 - (global.AXES[4,3]*2);

if (alarm[0] mod dmgFrec == 0)
{
	dmg_calc(trnd_hit_used,sprite_index);

	if (obj_Manticore.Manticore_hp != global.player_max_health) //--> Increase Regen
	{
		var Trndmngr = global.currentManager;
		if (place_meeting(x,y,obj_Manticore))&&(!Trndmngr.ManticoreVenomed)
		{
			obj_Manticore.alarm[5] -= 3;
			obj_Manticore.alarm[5] = clamp(obj_Manticore.alarm[5],0,artist.max_Regen);
			if (!audio_is_playing(snd_HealingWind)) audio_play_sound(snd_HealingWind,10,0);
			if (obj_Manticore.alarm[5] mod 2 == 0) //--> Heal FX
			{
				var healStar = instance_create_layer(random_range(x-100,x+100),random_range(y-200,y),"FX_Layer",obj_charge_chispa);
				healStar.sprite_index = spr_starShine;
				healStar.coloroption1 = c_lime;
				healStar.coloroption2 = c_green;
			}
		}
		else audio_stop_sound(snd_HealingWind);
	}
}

switch (sprite_index)
{
	case spr_TornadoINNER_back: case spr_TornadoINNER_front:
		image_speed = 0.8;
		if (alarm[0] > 1) image_alpha = 0.3;
		trnd_max_height = 1.7;
		if (!audio_is_playing(snd_TornadoIn)) audio_play_sound(snd_TornadoIn,10,0);
		break;
	case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front:
		image_speed = 1.3;
		if (!audio_is_playing(snd_TornadoOut)) audio_play_sound(snd_TornadoOut,10,0);
		break;
}

if (image_yscale < trnd_max_height)
{
	image_yscale += 0.01;

	//--> Resets Special Bar
	obj_Manticore.special_Whisperer -= 1;
	obj_Manticore.special_Whisperer = clamp(obj_Manticore.special_Whisperer,0,100);
	obj_Manticore.alarm[9] = 60;
}

if (alarm[0] < 1) image_alpha -= 0.01;

if (image_alpha < 0) instance_destroy();

if (place_meeting(x,y,obj_EnemyBullet))
{
	var bullet = instance_place(x,y,obj_EnemyBullet);
	if (bullet.sprite_index != bullet.destroy_sprite) && (bullet.priority != 10)
	{
		bullet.sprite_index = bullet.destroy_sprite;
		bullet.image_index = 0;
	}
}