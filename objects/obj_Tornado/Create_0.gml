/// @description Insert description here
// You can write your code in this editor

image_yscale = 0.01;

trnd_max_height = 0.7;
trnd_spd = 0.002 + (global.SWORDS[4,3] * 0.002);

alarm[0] = (global.AXES[4,3]*30) + 350 + (obj_Manticore.current_sp_axe * 4);
if (obj_Manticore.current_sp_axe == 100) alarm[0] += 300;

image_alpha = 0.7;

trnd_hit_used = false;