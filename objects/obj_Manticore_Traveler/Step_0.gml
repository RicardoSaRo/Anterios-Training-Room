/// @description Insert description here
// You can write your code in this editor

mov_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
mov_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);
interact = max(keyboard_check_pressed(vk_up),gamepad_button_check_pressed(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
accept = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
quit = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);

if (obj_City_manager.counter == 0)
{
	if (max(mov_left,mov_right))
	{
		if (mov_left)
		{
			sprite_index = spr_Manticore_Traveler_walk;
			x -= 4;
			image_speed = 1;
			image_xscale = -1;
		}
		else
		{
			sprite_index = spr_Manticore_Traveler_walk;
			x += 4;
			image_speed = 1;
			image_xscale = 1;
		}
	}
	else
	{
		sprite_index = spr_Manticore_Traveler;
		image_index = 0;
	}
	
	if (place_meeting(x,y,obj_Trigger_room))
	{
		switch(room)
		{
			case rm_City1:
				switch(x)
				{
					case 0:		global.cityActualRoom = rm_Map;
								global.cityPreviousRoom = rm_City1;
								obj_City_manager.Black2Alfa = false;
								break; //--> GOES TO MAP
					case 1500:	global.cityActualRoom = rm_City2;
								global.cityPreviousRoom = rm_City1;
								obj_City_manager.Black2Alfa = false;
								break;
				}
				if (x > 496) && (x < 528)
				{
					if (!instance_exists(obj_placeholder))
					{
						var enter = instance_create_layer(512,340,"Info_Layer",obj_placeholder);
						enter.sprite_index = spr_EnterUP;
					}
					
					if (interact)
					{
						global.cityActualRoom = rm_SnA_Shop;
						global.cityPreviousRoom = rm_City1;
						obj_City_manager.Black2Alfa = false;
					}
				}
				//--> Ampliar este else, incluir acá la tienda de TW... Al ultimo else destruir las flechas
				else if (instance_exists(obj_placeholder)) instance_destroy(obj_placeholder);
				break;
			
			case rm_City2:
				switch(x)
				{
					case 0:		global.cityActualRoom = rm_City1;
								global.cityPreviousRoom = rm_City2;
								obj_City_manager.Black2Alfa = false;
								break;
					case 1500:	global.cityActualRoom = rm_City3;
								global.cityPreviousRoom = rm_City2
								obj_City_manager.Black2Alfa = false;
								break;
				}
/*				if (x > 496) && (x < 528)
				{
					if (!instance_exists(obj_placeholder))
					{
						var enter = instance_create_layer(512,340,"Info_Layer",obj_placeholder);
						enter.sprite_index = spr_EnterUP;
					}
					
					if (interact)
					{
						global.cityActualRoom = rm_SnA_Shop;
						global.cityPreviousRoom = rm_City1;
						obj_City_manager.Black2Alfa = false;
					}
				}
				//--> Ampliar este else, incluir acá la tienda de TW... Al ultimo else destruir las flechas
				else (instance_exists(obj_placeholder)) instance_destroy(obj_placeholder);*/
				break;
			case rm_City3:
				switch(x)
				{
					case 0:		global.cityActualRoom = rm_City2;
								global.cityPreviousRoom = rm_City3;
								obj_City_manager.Black2Alfa = false;
								break;
					case 1500:	global.cityActualRoom = rm_Map;
								global.cityPreviousRoom = rm_City3;
								obj_City_manager.Black2Alfa = false;
								break; //--> GOES TO MAP
				}
/*				if (x > 496) && (x < 528)
				{
					if (!instance_exists(obj_placeholder))
					{
						var enter = instance_create_layer(512,340,"Info_Layer",obj_placeholder);
						enter.sprite_index = spr_EnterUP;
					}
					
					if (interact)
					{
						global.cityActualRoom = rm_SnA_Shop;
						global.cityPreviousRoom = rm_City1;
						obj_City_manager.Black2Alfa = false;
					}
				}
				//--> Ampliar este else, incluir acá la tienda de TW... Al ultimo else destruir las flechas
				else (instance_exists(obj_placeholder)) instance_destroy(obj_placeholder);*/
				break;
			case rm_SnA_Shop:
				if (x > 432) && (x < 464)
				{
					if (!instance_exists(obj_placeholder))
					{
						var enter = instance_create_layer(448,340,"Info_Layer",obj_placeholder);
						enter.sprite_index = spr_LeaveUP;
					}
					
					if (interact)
					{
						global.cityActualRoom = rm_City1;
						global.cityPreviousRoom = rm_SnA_Shop;
						obj_City_manager.Black2Alfa = false;
					}
				}
				else if (instance_exists(obj_placeholder)) instance_destroy(obj_placeholder);
				//--> CLAMP BOUNDARIES HERE!!!!!!!!!
				break;
		}
	}
	
}