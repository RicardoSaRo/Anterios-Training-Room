/// @description Insert description here
// You can write your code in this editor

image_xscale = WAowner.image_xscale;

x = WAowner.x;
y = WAowner.y;

switch(WAtype)
{
	case 1: WAdmg = 10 * ((global.SWORDS[1,3]/2) + 4); break;
	case 2: WAdmg = 10 * ((global.SWORDS[2,3]/2) + 2); break;
	case 3: WAdmg = (global.SWORDS[3,3]==0) ? 10 * choose(2,3) : ((global.SWORDS[3,3]==1) ? 10 * choose(2,3.5) : 10 * choose(3,3.5)); break;
	case 4: WAdmg = (global.SWORDS[4,3]==0) ? 30 : ((global.SWORDS[4,3]==1) ? 10 * choose(3,3.5) : ((global.SWORDS[4,3]==2) ? 10 * choose(3.5,4) : 40)); break;
	
	case 5: WAdmg = 10 * (global.AXES[1,3] + 12); break;
	case 6: WAdmg = 10 * (global.AXES[2,3] + 9); break;
	case 7: WAdmg = 10 * irandom_range(1,4); break;
	case 8: WAdmg = 10 * ((global.AXES[4,3]) + 7); break;
}

switch(WAtype) //-->> FX for different weapons
{
	case 2:	repeat(5)
			{
				var chispa = instance_create_layer(x,y,"Back_FX_Layer",obj_chispa);
				with(chispa)
				{
					x = other.x;
					y += random_range(-100,100);
				}
			}
			break;
	
	case 4:	var slowFX = instance_create_layer(x,y+random_range(-30,30),"FX_Layer",obj_GrassLeaf);
			slowFX.sprite_index = spr_snowflake;
			slowFX.xmov = irandom_range(-5,5);
			slowFX.rotatin = 1;
			slowFX.image_index = irandom(5);
			slowFX.image_alpha = choose(0.7,1);
			slowFX.alarm[0] = choose(30,40);
			slowFX.image_speed = 0;
			slowFX.image_angle = random(360);
			slowFX.image_xscale = random_range(0.3,0.7) * choose(-1,1);
			slowFX.image_yscale = random_range(0.3,0.7);
			break;
					
	case 7:	var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
			glare.sprite_index = choose(spr_spark,spr_starShine);
			var sz = choose(1,1.5);
			glare.image_xscale = sz;
			glare.image_yscale = sz;
			glare.image_blend = c_white;
			glare.Xmovement = random_range(0,3) * choose(1,-1);
			glare.Ymovement = random_range(0,3) * choose(1,-1);
			glare.bright = true;
			glare.fadeSpd = choose (0.01,0.02);
			break;
}

if (WAtype >= 5) || (global.RELIKS[35,2])
{
	//--> EnemyBullets COLLISION
	if (place_meeting(x,y,obj_EnemyBullet))
	{
		var bpoof = instance_nearest(x,y,obj_EnemyBullet);
	
		if (bpoof.priority != 10) bpoof.sprite_index = bpoof.destroy_sprite;
	}
}

if (WAowner.state != wallattack_Manticore) instance_destroy();