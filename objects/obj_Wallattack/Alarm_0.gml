/// @description Insert description here
// You can write your code in this editor

dmg_calc(WAhit_used,sprite_index);

switch(WAtype)
{
	case 1:	alarm[0] = 5; break;
	
	case 3:	alarm[0] = 4; break;
	
	case 4: alarm[0] = 4; 
			if (place_meeting(x,y,obj_BossEnemy))
			{
				var icy = instance_create_layer(x,y+random_range(-30,30),"FX_Layer",obj_rockice_FX);
				icy.sprite_index = spr_icys_FX;
			}
			break;
			
	case 2:	case 7: alarm[0] = 1; break;
	
	case 5:	alarm[0] = 7;
			if (place_meeting(x,y,obj_BossEnemy))
				instance_create_layer(x,y+random_range(-30,30),"FX_Layer",obj_rockice_FX);
			break;
	
	case 6: alarm[0] = 7;
			if (!place_meeting(x,y,obj_willowisp))
			{
				var willowisp3 = instance_create_layer(x,y+random_range(-30,30),"FX_Layer",obj_willowisp);
				willowisp3.image_xscale = image_xscale;
			}
			break;
	
	case 8:	alarm[0] = 7;
			var wndslsh = instance_create_layer(x,y,"FX_Layer",obj_windslash);
			wndslsh.image_xscale = owner.image_xscale * -1;
			break;
	
	default: alarm[0] = 5; break;
}