/// @description Insert description here

//--> Positioning
x = obj_Manticore.x;
y = obj_Manticore.y;

//--> Destroys hitbox instances in the indicated animation frames
switch (obj_Manticore.sprite_index)
{
	case spr_Argenta: case spr_ArgentaMix:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Argenta_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Argenta_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 16)
								instance_destroy();
						break;
								
	case spr_Argenta_DashStab:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_ArgentaSTR_hitbox)
								instance_destroy();
						break;
						
	case spr_AirArgenta: case spr_AirArgentaMix:
						if (obj_Manticore.image_index >= 1)
							if (sprite_index == spr_AirArgenta_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_AirArgenta_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_AirArgenta_hitbox3)
								instance_destroy();
						if (obj_Manticore.image_index >= 7)
							if (sprite_index == spr_AirArgenta_hitbox4)
								instance_destroy();
						break;
	case spr_Dyrnwyn: case spr_DyrnwynMix:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Dyrnwyn_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Dyrnwyn_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 16)
								instance_destroy();
						break;
						
	case spr_Dyrnwyn_DashStab:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_DyrnwynSTR_hitbox)
								instance_destroy();
						break;
						
	case spr_AirDyrnwyn: case spr_AirDyrnwynMix:
						if (obj_Manticore.image_index >= 1)
							if (sprite_index == spr_AirDyrnwyn_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_AirDyrnwyn_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_AirDyrnwyn_hitbox3)
								instance_destroy();
						if (obj_Manticore.image_index >= 7)
							if (sprite_index == spr_AirDyrnwyn_hitbox4)
								instance_destroy();
						break;
	case spr_GirTab: case spr_AirGirTab: case spr_GirTabUP: case spr_AirGirTabUP: case spr_GirTrident: 
	case spr_AirGirTrident: case spr_AirGirTridentUP: case spr_GirTabMix: case spr_AirGirTabMix:
	case spr_GirTridentMix: case spr_AirGirTridentMix: case spr_GirTridentUP:
						if (!soundSwitch)
						{
							if (!audio_is_playing(snd_Girtab1)) audio_play_sound(snd_Girtab1,14,0);
							else audio_play_sound(snd_Girtab2,14,0);
							soundSwitch = true;
						}
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_GirTab_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 7)
								instance_destroy();
						break;
	case spr_Frostbiter: case spr_AirFrostbiter: case spr_FrostbiterUP:
	case spr_AirFrostbiterUP: case spr_FrostbiterMix: case spr_AirFrostbiterMix:
						if (!soundSwitch)
						{
							if (!audio_is_playing(snd_Frostbiter1)) audio_play_sound(snd_Frostbiter1,14,0);
							else audio_play_sound(snd_Frostbiter2,14,0);
							if (!audio_is_playing(snd_freezeMain)) audio_play_sound(snd_freezeMain,14,0);
							soundSwitch = true;
						}
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_GirTab_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 7)
								instance_destroy();
						break;
	case spr_Korunax: case spr_AirKorunax:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Korunax_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 4)
							if (sprite_index == spr_Korunax_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_Korunax_hitbox3)
								instance_destroy();
						if (obj_Manticore.sprite_index == spr_AirKorunax) break;
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Korunax_hitbox4)
								instance_destroy();
						if (obj_Manticore.image_index >= 10)
							if (sprite_index == spr_Korunax_hitbox5)
								instance_destroy();
						if (obj_Manticore.image_index >= 11)
								instance_destroy();
						break;
	case spr_Mirage: case spr_AirMirage:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Mirage_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 4)
							if (sprite_index == spr_Mirage_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_Mirage_hitbox3)
								instance_destroy();
						if (obj_Manticore.sprite_index == spr_AirMirage) break;
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Mirage_hitbox4)
								instance_destroy();
						if (obj_Manticore.image_index >= 10)
							if (sprite_index == spr_Mirage_hitbox5)
								instance_destroy();
						if (obj_Manticore.image_index >= 11)
								instance_destroy();
						break;
	case spr_Mjolnir: case spr_AirMjolnir:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Mjolnir_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 4)
							if (sprite_index == spr_Mjolnir_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_Mjolnir_hitbox3)
								instance_destroy();
						if (obj_Manticore.sprite_index == spr_AirMjolnir) break;
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Mjolnir_hitbox4)
								instance_destroy();
						if (obj_Manticore.image_index >= 10)
							if (sprite_index == spr_Mjolnir_hitbox5)
								instance_destroy();
						if (obj_Manticore.image_index >= 11)
								instance_destroy();
						break;
	case spr_Whisperer: case spr_AirWhisperer:
						if (obj_Manticore.image_index >= 3)
							if (sprite_index == spr_Whisperer_hitbox1)
								instance_destroy();
						if (obj_Manticore.image_index >= 4)
							if (sprite_index == spr_Whisperer_hitbox2)
								instance_destroy();
						if (obj_Manticore.image_index >= 5)
							if (sprite_index == spr_Whisperer_hitbox3)
								instance_destroy();
						if (obj_Manticore.sprite_index == spr_AirWhisperer) break;
						if (obj_Manticore.image_index >= 9)
							if (sprite_index == spr_Whisperer_hitbox4)
								instance_destroy();
						if (obj_Manticore.image_index >= 10)
							if (sprite_index == spr_Whisperer_hitbox5)
								instance_destroy();
						if (obj_Manticore.image_index >= 11)
								instance_destroy();
						break;
	case spr_Special_GirTab: //-->> A bit different to prevent it triggers before intended
						if (obj_Manticore.image_index >= 37) || (obj_Manticore.image_index <= 32)
						{
							if (sprite_index == spr_GirTab_sp_hitbox)
							{
								instance_destroy();
								exit;
							}
						}
						break;
}

//--> Damage Enemy with the Hitbox
if (sprite_index == spr_GirTab_sp_hitbox)
{
	//-->> Restricts GirTab Special use to prevent it triggers before intended sometimes
	if (obj_Manticore.image_index > 32) dmg_calc(hit_used,obj_Manticore.sprite_index);
}
else dmg_calc(hit_used,obj_Manticore.sprite_index);

//--> Quits Multi-hits for these weapons
switch (sprite_index)
{
	case spr_Argenta_hitbox1: case spr_Argenta_hitbox2: case spr_Argenta_hitbox3: case spr_ArgentaSTR_hitbox:
	case spr_AirArgenta_hitbox1: case spr_AirArgenta_hitbox2: case spr_AirArgenta_hitbox3:
	case spr_AirArgenta_hitbox4: case spr_Korunax_hitbox1: case spr_Korunax_hitbox2:
	case spr_Korunax_hitbox3: case spr_Korunax_hitbox4: case spr_Korunax_hitbox5:
	case spr_Korunax_hitbox6: case spr_Mirage_hitbox1: case spr_Mirage_hitbox2:
	case spr_Mirage_hitbox3: case spr_Mirage_hitbox4: case spr_Mirage_hitbox5:
	case spr_Mirage_hitbox6: case spr_Whisperer_hitbox1: case spr_Whisperer_hitbox2:
	case spr_Whisperer_hitbox3: case spr_Whisperer_hitbox4: case spr_Whisperer_hitbox5:
	case spr_Whisperer_hitbox6:	case spr_GirTab_sp_hitbox: switch_hit = true; break;
}