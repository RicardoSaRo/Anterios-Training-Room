/// @description Insert description here
// You can write your code in this editor
image_alpha -= fadeSpd;

x += Xmovement;
y += Ymovement;

image_xscale += sizeMod;
image_yscale += abs(sizeMod);

if (image_alpha <= 0) instance_destroy();