/// @description Insert description here
// You can write your code in this editor
/*
if (!closing_menu)
{
	draw_sprite(spr_VideoOptions_lbl,0,x,y-160);
	
	var movPos = (moveOptions == 1);
	var chosenOption = (movPos) ? c_yellow : c_white;
	draw_sprite_ext(spr_WindowMode_lbl,0,x-50,y-70,1,1,0,chosenOption,1);
	var choosing = (movPos) ? selectionMain : selecWinMode;
	switch(choosing)
	{
		case 1: draw_sprite_ext(spr_Fullscreen_lbl,0,x+30,y-70,1,1,0,chosenOption,1); break;
		case 2: draw_sprite_ext(spr_WinBorder_lbl,0,x+30,y-70,1,1,0,chosenOption,1); break;
		case 3: draw_sprite_ext(spr_WinBorderless_lbl,0,x+30,y-70,1,1,0,chosenOption,1); break;
	}
	
	var movPos = (moveOptions == 2);
	var chosenOption = (movPos) ? c_yellow : c_white;
	draw_sprite_ext(spr_Vsync_lbl,0,x-50,y-10,1,1,0,chosenOption,1);
	var choosing = (movPos) ? selectionMain : selecVsync;
	switch(choosing)
	{
		case 1: draw_sprite_ext(spr_OFF_lbl,0,x+30,y-10,1,1,0,chosenOption,1); break;
		case 2: draw_sprite_ext(spr_ON_lbl,0,x+30,y-10,1,1,0,chosenOption,1); break;
	}
	
	var movPos = (moveOptions == 3);
	var chosenOption = (movPos) ? c_yellow : c_white;
	draw_sprite_ext(spr_AlphaBlend_lbl,0,x-50,y+50,1,1,0,chosenOption,1);
	var choosing = (movPos) ? selectionMain : selecAlfa;
	switch(choosing)
	{
		case 1: draw_sprite_ext(spr_High_lbl,0,x+30,y+50,1,1,0,chosenOption,1); break;
		case 2: draw_sprite_ext(spr_Medium_lbl,0,x+30,y+50,1,1,0,chosenOption,1); break;
		case 3: draw_sprite_ext(spr_Low_lbl,0,x+30,y+50,1,1,0,chosenOption,1); break;
	}
	
	var movPos = (moveOptions == 4);
	var chosenOption = (movPos) ? c_yellow : c_white;
	draw_sprite_ext(spr_Resolution_lbl,0,x-50,y+110,1,1,0,chosenOption,1);
	var choosing = (movPos) ? selectionMain : selecRes;
	switch(choosing)
	{
		case 1: draw_sprite_ext(spr_960x540_lbl,0,x+30,y+110,1,1,0,chosenOption,1); break;
		case 2: draw_sprite_ext(spr_1024x576_lbl,0,x+30,y+110,1,1,0,chosenOption,1); break;
		case 3: draw_sprite_ext(spr_1280x720_lbl,0,x+30,y+110,1,1,0,chosenOption,1); break;
		case 4: draw_sprite_ext(spr_1366x768_lbl,0,x+30,y+110,1,1,0,chosenOption,1); break;
		case 5: draw_sprite_ext(spr_1600x900_lbl,0,x+30,y+110,1,1,0,chosenOption,1); break;
	}
	
	var chosenOption = (moveOptions == 5)&&(selectionMain==1) ? c_yellow : c_white;
	draw_sprite_ext(spr_SaveChanges_lbl,0,x-50,y+190,1,1,0,chosenOption,1);
	
	var chosenOption = (moveOptions == 5)&&(selectionMain==2) ? c_yellow : c_white;
	draw_sprite_ext(spr_Back2Optns_lbl,0,x+30,y+190,1,1,0,chosenOption,1);
}