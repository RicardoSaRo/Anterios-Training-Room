/// @description Insert description here
// You can write your code in this editor

gamepad_set_axis_deadzone(global.controller_type,0.5);

if (closing_menu)
{
	image_xscale -= 0.2;
	image_yscale -= 0.2;
	
	if (image_xscale <= 0) instance_destroy();
}
else
{
	if (image_xscale < 1.2)
	{
		image_xscale += 0.2;
		image_yscale += 0.2;
	}
}

x = centerX;
y = centerY;

var haxis = gamepad_axis_value(global.controller_type,gp_axislh);
var vaxis = gamepad_axis_value(global.controller_type,gp_axislv);

if (gpAxisH) haxis = 0.1;
if (gpAxisV) vaxis = 0.1;

var menu_move_left = max(keyboard_check_pressed(vk_left),gamepad_button_check_pressed(global.controller_type,gp_padl),haxis*-1,0);
var menu_move_right = max(keyboard_check_pressed(vk_right),gamepad_button_check_pressed(global.controller_type,gp_padr),haxis,0);
var menu_move_down = max(keyboard_check_pressed(vk_down),gamepad_button_check_pressed(global.controller_type,gp_padd),vaxis,0);
var menu_move_up = max(keyboard_check_pressed(vk_up),gamepad_button_check_pressed(global.controller_type,gp_padu),vaxis*-1,0);
var menu_Esc = max(keyboard_check_pressed(vk_escape),gamepad_button_check_pressed(global.controller_type,gp_select),0);

//--> Moves around the menu using the script "wpn_menu_movement"
if (max(menu_move_down,menu_move_up,menu_move_left,menu_move_right,0))
{
	if (menu_move_up) moveOptions -= 1;
	if (menu_move_down) moveOptions += 1;
	if (menu_move_right) selectionMain += 1;
	if (menu_move_left) selectionMain -= 1;
	audio_play_sound(snd_MoveOption,7,0);
	moveOptions = clamp(moveOptions,1,5);
	switch(moveOptions)
	{
		case 1: selectionMain = clamp(selectionMain,1,3);
				if (menu_move_up) or (menu_move_down) selectionMain = selecWinMode;
				break;
				
		case 2: selectionMain = clamp(selectionMain,1,2);
				if (menu_move_up) or (menu_move_down) selectionMain = selecVsync;
				break;
				
		case 3: selectionMain = clamp(selectionMain,1,3);
				if (menu_move_up) or (menu_move_down) selectionMain = selecAlfa;
				break;
				
		case 4: selectionMain = clamp(selectionMain,1,5);
				if (menu_move_up) or (menu_move_down) selectionMain = selecRes;
				break;
				
		case 5: selectionMain = clamp(selectionMain,1,2); break;
	}
}

if (menu_Esc)
{
	closing_menu = true;
	audio_play_sound(snd_menuBack,7,0);
}

var menu_d = max(keyboard_check_pressed(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
if (closing_menu) or (image_xscale < 1.2) menu_d = false;

//--> Turns ON and OFF the Quick Access of every obtained weapons. Also checks that there is always
//--> at least one weapon with the Quick Access option available
if (menu_d)
{
	var newSelect = 0;
	var rndmXpos = 0;
	var rndmYpos = 0;
	
	switch(moveOptions)
	{
		//--> Window Mode
		case 1: newSelect = (selecWinMode != selectionMain);
				selecWinMode = selectionMain;
				rndmXpos = random_range(100,250);
				rndmYpos = -70;
				audio_play_sound(snd_WeaponEquip,7,0);
				break;
		//--> Vsync
		case 2: newSelect = (selecVsync != selectionMain);
				selecVsync = selectionMain;
				rndmXpos = random_range(50,100);
				rndmYpos = -10;
				audio_play_sound(snd_WeaponEquip,7,0);
				break;
		//--> Alpha Blending
		case 3: newSelect = (selecAlfa != selectionMain);
				selecAlfa = selectionMain;
				rndmXpos = random_range(50,100);
				rndmYpos = 50;
				audio_play_sound(snd_WeaponEquip,7,0);
		//--> Resolution	
		case 4: newSelect = (selecRes != selectionMain);
				selecRes = selectionMain;
				rndmXpos = random_range(100,200);
				rndmYpos = 110;
				audio_play_sound(snd_WeaponEquip,7,0);
				break;
		//--> Back to Options Menu		
		case 5: if (selectionMain == 1)
				{
					//Saves
					save_videoOptns();
					
					if (instance_number(obj_fadeAway)<10) newSelect = true;
					rndmXpos = random_range(-250,-150);
					rndmYpos = 190;
					audio_play_sound(snd_WeaponEquip,7,0);
				}
				else
				{
					closing_menu = true;//--> Back to Options
					audio_play_sound(snd_menuBack,7,0);
				}
				break;
	}
	
	if (newSelect) //--> Star FX when selecting new options
	{
		//Audio
		repeat(10)
		{
			var chooseFX = instance_create_layer(x+rndmXpos,y+rndmYpos,"Menus_Layer2",obj_fadeAway);
			chooseFX.sprite_index = spr_starShine;
			chooseFX.Xmovement = irandom_range(-7,7);
			chooseFX.fadeSpd = 0.03//random_range(0.01,0.08);
			chooseFX.image_blend = c_yellow;
			chooseFX.image_angle = random(360);
			var scale = irandom_range(2,4);
			chooseFX.image_xscale = scale;
			chooseFX.image_yscale = scale;
			chooseFX.visible = false;
		}
	}
}

if (gamepad_axis_value(global.controller_type,gp_axislh) > 0.5) gpAxisH = true;
else if (gamepad_axis_value(global.controller_type,gp_axislh) < -0.5) gpAxisH = true;
if (gamepad_axis_value(global.controller_type,gp_axislh) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislh) > -0.5) gpAxisH = false;

if (gamepad_axis_value(global.controller_type,gp_axislv) > 0.5) gpAxisV = true;
else if (gamepad_axis_value(global.controller_type,gp_axislv) < -0.5) gpAxisV = true;
if (gamepad_axis_value(global.controller_type,gp_axislv) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislv) > -0.5) gpAxisV = false;

//--> Closing Menu
if (gamepad_button_check_pressed(global.controller_type,gp_select)) if (image_xscale >= 1.2) closing_menu = true;