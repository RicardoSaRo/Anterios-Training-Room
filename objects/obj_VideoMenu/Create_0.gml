/// @description Insert description here
// You can write your code in this editor

closing_menu = false;
gpAxisH = false;
gpAxisV = false;

image_xscale = 0.2;
image_yscale = 0.2;

menu_view_w = display_get_width();
menu_view_h = display_get_height();
centerX = 1366/2;
centerY = 768/2;

counter = 5;
optionAvailable = false;

Back2Options = false;

var video_Optns_array = load_videoOptns();
WinMode		= video_Optns_array[1];
Vsync		= video_Optns_array[2];
AlfaBlend	= video_Optns_array[3];
ResW		= video_Optns_array[4];
ResH		= video_Optns_array[5];

moveOptions = 1;
selectionMain = WinMode;

selecWinMode = WinMode;
selecVsync = Vsync;
selecAlfa = AlfaBlend;
switch(ResH)
{
	case 540: selecRes = 1; break;
	case 576: selecRes = 2; break;
	case 720: selecRes = 3; break;
	case 768: selecRes = 4; break;
	case 900: selecRes = 5; break;
	default: selecRes = 1; break;
}