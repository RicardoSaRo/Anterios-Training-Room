/// @description Insert description here
// You can write your code in this editor

if (sprite_index == spr_Iceball_create) && (image_index > 8) sprite_index = spr_Iceball;

if (sprite_index == spr_Iceball)
{
	ib_vsp += ib_grv;
	
	if place_meeting(x,y+ib_vsp,obj_Wall)
	{
		var onepixel = sign(ib_vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		ib_vsp = 0;
		ib_hsp -= 0.5;
		ib_hsp = clamp(ib_hsp,0,15);
	}
	
	if place_meeting(x,y+ib_vsp,obj_Platform)
	{
		var onepixel = sign(ib_vsp);
		while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
		{
			ib_vsp = 0;
		ib_hsp -= 0.5;
		ib_hsp = clamp(ib_hsp,0,15);
		}
	}
	
	y += ib_vsp;
	
	if place_meeting(x+ib_hsp,y,obj_Wall)
	{
		var onepixel = sign(ib_hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		ib_direc *= -1;
	}
	
	x += ib_hsp * ib_direc;

	if (place_meeting(x,y,obj_Manticore_hitbox))
	{
		ib_vsp = -15;
		ib_hsp = 15;
		ib_direc = obj_Manticore_hitbox.image_xscale;
		ib_hitpoints -= 1;
		var icy = instance_create_layer(x,y,"FX_Layer",obj_rockice_FX);
		icy.sprite_index = spr_icys_FX;
	}
	
	if (ib_hsp != 0) && (alarm[0] <= 1) dmg_calc(ib_hit_used,sprite_index);
}

if (place_meeting(x,y,obj_EnemyBullet))
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (nearBullet.priority == 10) ib_hitpoints = 0;
	else
	{
		if (nearBullet.sprite_index != nearBullet.destroy_sprite) 
		{
			nearBullet.sprite_index = nearBullet.destroy_sprite;
			ib_hitpoints -= 5;
		}
	}
}

if (ib_hitpoints <= 0) or (obj_Manticore.equipped_tw != 13)	or
   (!instance_exists(owner)) sprite_index = spr_Iceball_destroy;

if (sprite_index == spr_Iceball_destroy)
{
	var sprW = sprite_width/2;
	var sprH = sprite_height/2;
	for (var i = 1; i < 4; i++)
	{
		var icy = instance_create_layer(x+random_range(-sprW,sprW),y+random_range(-sprH,sprH),"FX_Layer",obj_rockice_FX);
		icy.sprite_index = spr_icys_FX;
	}
	
	if (image_index > 6) instance_destroy();
}