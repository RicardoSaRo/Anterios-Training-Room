/// @description Insert description here
// You can write your code in this editor
owner = obj_Throwing_Wpn;

ib_grv = 1.5;
ib_direc = (instance_exists(obj_Throwing_Wpn)) ? owner.image_xscale : 1;

ib_hsp = 0;
ib_vsp = 0;

var upgradeHP = (global.TWs[13,3]==3) ? 20 : ((global.TWs[13,3]==2) ? 10 : 0);
var upgradeSize = (global.TWs[13,3]==3) ? 0.6 : ((global.TWs[13,3]==2) ? 0.3 : 0);
upgradeDmg = (global.TWs[13,3]==3) ? 5 : ((global.TWs[13,3]==2) ? 8 : 10);

ib_hitpoints = (global.RELIKS[9,2]) ? ((40 + upgradeHP) * 1.2) : 40 + upgradeHP;
image_xscale += upgradeSize;
image_yscale += upgradeSize;

ib_hit_used = false;

sprite_index = spr_Iceball_create;

alarm[0] = 10;