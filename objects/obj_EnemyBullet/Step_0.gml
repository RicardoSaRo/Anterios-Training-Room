/// @description Insert description here
// You can write your code in this editor
var defeated_spr = spr_poof;
playerCollision = place_meeting(x,y,obj_Manticore);

switch(obj_BossEnemy.sprite_index)
{
	case spr_GGolem_crumbles: case spr_BGolem_crumbles: case spr_OCrab_Defeated:
	case spr_Dragonkin_Defeated: case spr_Wastipede_Defeated:
	var defeated_spr = sprite_index; break;
}

//-->> Damages Player with collision and if alarm[11] (invincibility) if off
if (defeated_spr != obj_BossEnemy.sprite_index)
{
	if (playerCollision) and (obj_Manticore.alarm[11] <= 0)
	{
		//--> if Bullet isn't already destroyed or is a "stun" bullet, inflicts damage
		if (sprite_index != destroy_sprite) && (sprite_index != spr_WW_howlWave) &&
		   (sprite_index != spr_SpiritWolf_Howl) && (sprite_index != spr_SpiritWolf_Rush) &&
		   (NoDamage == false)
		{
			if (obj_Manticore.alarm[10] <= 0)
			{
				with (obj_Manticore)
				{
					var damage = (global.RELIKS[44,2]) ? choose(1,1,0) : 1;
					if (global.RELIKS[44,2])
					{
						switch(global.difficulty)//--> Invulnerability
						{
							case "casual":	alarm[11] = (global.RELIKS[33,2]) ? 200 : 100; break;
							case "normal":	alarm[11] = (global.RELIKS[33,2]) ? 160 : 80; break;
							case "hard":	alarm[11] = (global.RELIKS[33,2]) ? 120 : 60; break;
							case "lunatic": alarm[11] = (global.RELIKS[33,2]) ? 80 : 40; break;
						}
					
						if (damage == 0)
						{
							if (!audio_is_playing(snd_Parry)) audio_play_sound(snd_Parry,3,0);
							var parry = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_centerXYfollow);
							parry.sprite_index = spr_Parry;
							parry.image_blend = c_fuchsia;
							//parry.brightness = true;
							//parry.glow = true;
							parry.image_alpha = 0.5;
							parry.image_xscale = 2;
							parry.image_yscale = 2;
							parry.image_angle = random(360);
							parry.anim_end = 5;
						}
						else if (!audio_is_playing(snd_ManticoreDmg1)) && (!audio_is_playing(snd_ManticoreDmg2)) && (!audio_is_playing(snd_ManticoreDmg3))
							audio_play_sound(choose(snd_ManticoreDmg1,snd_ManticoreDmg2,snd_ManticoreDmg3),2,0);
					}
					else state = damage_Manticore;
					Manticore_hp -= damage;
					Manticore_hp = clamp(Manticore_hp, 0, global.player_max_health);

					alarm[10] = 10;
				}
			}
		}
	}
}

switch(room)
{
	case rm_Golem:		if (sprite_index != destroy_sprite) Golem_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Werewolf1:	if (sprite_index != destroy_sprite) Werewolf_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Crab:		if (sprite_index != destroy_sprite) Crab_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Dragonkin:	if (sprite_index != destroy_sprite) Dragonkin_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Wastipede:	if (sprite_index != destroy_sprite) Wastipede_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Meetonak:	if (sprite_index != destroy_sprite) Mee2Nak_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_DragonLord:	if (sprite_index != destroy_sprite) DragonLords_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Alraune:	if (sprite_index != destroy_sprite) Alraune_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_VenomLord:	if (sprite_index != destroy_sprite) VenomLord_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Phoenisk:	if (sprite_index != destroy_sprite) Phoenisk_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
}

//if (sprite_index == destroy_sprite) if (image_index >= 3) instance_destroy();