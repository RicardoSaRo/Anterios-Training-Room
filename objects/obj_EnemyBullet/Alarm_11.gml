/// @description Insert description here
// You can write your code in this editor
/// @description Insert description here
// You can write your code in this editor
/*var defeated_spr = spr_poof;

switch(obj_BossEnemy.sprite_index)
{
	case spr_GGolem_crumbles: case spr_BGolem_crumbles: case spr_OCrab_Defeated:
	case spr_Dragonkin_Defeated: case spr_Wastipede_Defeated:
	var defeated_spr = sprite_index; break;
}

//-->> Damages Player with collision and if alarm[11] (invincibility) if off
if (defeated_spr != obj_BossEnemy.sprite_index)
{
	if (place_meeting(x,y,obj_Manticore)) and (obj_Manticore.alarm[11] <= 0)
	{
		//--> if Bullet isn't already destroyed or is a "stun" bullet, inflicts damage
		if (sprite_index != destroy_sprite) && (sprite_index != spr_WW_howlWave) &&
		   (sprite_index != spr_SpiritWolf_Howl) && (sprite_index != spr_SpiritWolf_Rush) &&
		   (NoDamage == false)
		{
			if (obj_Manticore.alarm[10] <= 0)
			{
				with (obj_Manticore)
				{
					Manticore_hp -= 1;
					Manticore_hp = clamp(Manticore_hp, 0, global.player_max_health);
					state = damage_Manticore;
					alarm[10] = 10;
				}
			}
		}
	}
}

switch(room)
{
	case rm_Golem:		if (sprite_index != destroy_sprite) Golem_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Werewolf1:	if (sprite_index != destroy_sprite) Werewolf_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Crab:		if (sprite_index != destroy_sprite) Crab_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Dragonkin:	if (sprite_index != destroy_sprite) Dragonkin_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Wastipede:	if (sprite_index != destroy_sprite) Wastipede_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_Meetonak:	if (sprite_index != destroy_sprite) Mee2Nak_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
	case rm_DragonLord:	if (sprite_index != destroy_sprite) DragonLords_Bullets(sprite_index);
						else Destroy_Bullets(sprite_index);
						break;
}

//if (sprite_index == destroy_sprite) if (image_index >= 3) instance_destroy();

alarm[11] = 1;