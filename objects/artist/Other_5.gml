/// @description Variables at Room End
// Destroys surfaces for the Interpolation machine to prevent memory overflow, and initialize the Texel Filter and current manager check.

if (surface_exists(A_surf)) surface_free(A_surf);
	
if (surface_exists(B_surf)) surface_free(B_surf);

application_surface_draw_enable(true);

gpu_set_tex_filter(false);

global.currentManager = noone;