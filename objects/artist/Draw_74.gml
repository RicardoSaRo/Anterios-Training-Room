/// @description GUI Begin
//--> Top Screen Information
for (var tiled=0; tiled<=room_width; tiled+=48)
	draw_sprite_ext(spr_TopGUI,0,tiled,0,1,1,0,c_white,1);

draw_set_font(Antares18);
draw_set_colour(c_black);
draw_text(650, gui_y-744, "CURRENCY: $" + string(global.MONEY));
var huntORexp = "(ESC) Menu/Controls -- (ENTER) Weapons";
draw_text(1100, gui_y-744, string(huntORexp) + string(bossText));
draw_text(658, gui_y-744, "CURRENCY: $" + string(global.MONEY));
draw_text(1108, gui_y-744, "HUNTING: " + string(bossText));
draw_set_colour(merge_color(c_yellow,c_orange,0.2));
draw_text(654, gui_y-744, "CURRENCY: $" + string(global.MONEY));
draw_text(1104, gui_y-744, string(huntORexp) + string(bossText));
draw_set_colour(c_white);

if (!venomFX) var lerp_regen = lerp(abs(max_Regen-old_regen),abs(max_Regen-old_regen),0.2)/max_Regen;
else var lerp_regen = lerp(old_regen,old_regen,0.2)/max_Regen;

if (poisonFX)||(venomFX)
{
	var regColor = violet;
	var bblColor = c_purple;
}
else
{
	var regColor = c_red;
	var bblColor = c_white;
}

if (obj_Manticore.alarm[5] > 0)
{
	draw_sprite_ext(spr_RegenBar,count2twelve,94,120,1,lerp_regen,0,regColor,1);
	draw_sprite_ext(spr_RegenBbls,count2twelve,94,120,1,1,0,bblColor,1);
}

if (regenFX >= 0)
{
	var artmngr = global.currentManager;
	var regenFXclr = (artmngr.ManticoreVenomed) ? c_fuchsia : c_white;
	draw_sprite_ext(spr_RegenFX,regenFX,94,82,1,1,0,regenFXclr,1);
	regenFX += 0.2;
	if (regenFX > 8) regenFX = -1;
}

if (!venomFX)
{
	count2twelve += 0.1;
	if (count2twelve > 11) count2twelve = 0;
}
else
{
	count2twelve -= 0.1;
	if (count2twelve < 0) count2twelve = 11;
}

if (freezeFX) draw_sprite(spr_ScorpRing_Freeze,0,0,0);
else
{
	if (poisonFX)||(venomFX) draw_sprite(spr_ScorpRing_Poison,0,0,0);
	else draw_sprite(spr_ScorpRing_stage,0,0,0);
}

with(obj_Life_Orb) draw_sprite_ext(sprite_index,image_index,LO_xoffset,LO_yoffset,1,1,0,image_blend,1);

