/// @description Artist Draws all
// Artist draw most existing objects on the application surface to improve performance
// A state machine will be implemented to include only objects that could exist per Room to reduce code.

//-->DRAW EVENTS (NO BLENDING) Part1
#region //--> Obj_Shockwave_front
if (instance_exists(obj_Shockwave_back))
{
	with (obj_Shockwave_back)
	{
		shockwave_draw1();
	}
}
#endregion

gpu_set_blendmode(bm_add); //--> DRAWING WITH BLENDING
#region //--> Manticore Blend Mode

if (instance_exists(obj_Manticore))
{
	with (obj_Manticore)
	{
		switch (sprite_index)
		{
			case spr_Dyrnwyn:		
				for(var i = 0;i < 1;i ++)
				{
					draw_sprite_ext(spr_Dyrnwyn_flames,image_index,x,y,image_xscale,image_yscale,0,c_orange,1);
				}
				break;
			case spr_DyrnwynMix:		
				for(var i = 0;i < 1;i ++)
				{
					draw_sprite_ext(spr_DyrnwynMix_flames,image_index,x,y,image_xscale,image_yscale,0,c_orange,1);
				}
				break;
			case spr_AirDyrnwynMix:		
				for(var i = 0;i < 1;i ++)
				{
					draw_sprite_ext(spr_AirDyrnwynMix_flames,image_index,x,y,image_xscale,image_yscale,0,c_orange,1);
				}
				break;
			case spr_AirDyrnwyn:
				for(var i = 0;i < 1;i ++)
				{
					draw_sprite_ext(spr_AirDyrnwyn_flames,image_index,x,y,image_xscale,image_yscale,0,c_orange,1);
				}
				break;
			case spr_Dyrnwyn_sp_floor:
				for(var i = 0;i < 6;i ++)
				{
					draw_sprite_ext(spr_Dyrnwyn_sp_flame,image_index,x,y,image_xscale,image_yscale,0,c_orange,0.1);
				}
				break;
			case spr_Dyrnwyn_sp_air:
				for(var i = 0;i < 1;i ++)
				{
					draw_sprite_ext(spr_AirDyrnwyn_sp_flame,image_index,x,y,image_xscale,image_yscale,0,c_orange,1);
				}
				break;
			case spr_AirMjolnir:
				for(var i = 0;i < 3;i ++)
				{
					draw_sprite_ext(spr_AirMjolnir_lightning,image_index,x,y,image_xscale,image_yscale,0,c_white,0.2);
				}
				break;
			case spr_Mjolnir:
				for(var i = 0;i < 3;i ++)
				{
					draw_sprite_ext(spr_Mjolnir_lightning,image_index,x,y,image_xscale,image_yscale,0,c_white,0.2);
				}
				break;
			case spr_Mjolnir_sp:
				for(var i = 0;i < 5;i ++)
				{
					draw_sprite_ext(spr_Mjolnir_sp_lightning,image_index,x,y,image_xscale,image_yscale,0,c_white,0.5);
				}
				break;
			case spr_Manticore_spCharge:
				if (image_index > 6) && (image_index < 10)
				{
					switch(equiped_sword)
					{
						case 1: var c1 = c_silver; break;
						case 2: var c1 = make_color_rgb(241,100,31); break;
						case 3: var c1 = c_red; break;
						case 4: var c1 = c_aqua; break;
					}
					switch(equipped_axe)
					{
						case 1: var c2 = make_color_rgb(211,151,65); break;
						case 2: var c2 = c_fuchsia; break;
						case 3: var c2 = make_color_rgb(39,100,205); break;
						case 4: var c2 = c_lime; break;
					}
					for(var i = 0;i < 4;i ++)
					{
						draw_sprite_ext(spr_Manticore_spCharge,image_index,x,y,image_xscale,image_yscale,0,choose(c1,c2),0.2);
					}
				}
				break;
			case spr_Manticore_victory:
				if (image_index >= 13)&&(image_index <= 29)
				{
		//			gpu_set_blendmode(bm_add);
					var indx = image_index - 13;
		//			for(var i = 0;i < 1;i ++)
		//			{
						draw_sprite_ext(spr_Victory_lightning,indx,x,y,image_xscale,image_yscale,0,c_white,1);
		//			}
		//			gpu_set_blendmode(bm_normal);
				}
				break;
		}

		if (image_blend == c_yellow)
		{
			for(var i = 0;i < 3;i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,c_white,0.5);
		//		draw_sprite_ext(spr_GraniteShield,1,x,y,image_xscale,image_yscale,0,c_white,0.3);
			}
		}

		if (alarm[11] >= 1) && (image_alpha == 1)
		{
			for(c = 0;c < 360;c += 60)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),image_xscale,image_yscale,0,image_blend,image_alpha*0.35)
			}
		}
	}
}
#endregion

#region //--> Throwing Weapons Blend Mode
if (instance_exists(obj_Throwing_Wpn))
{
	with (obj_Throwing_Wpn)
	{
		if (sprite_index == spr_Void_Ripple)
		{
			for(c = 0;c < 360;c += 60)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),image_xscale,image_yscale,random(360),c_purple,image_alpha*0.25)
			}
	
			for(var i = 0; i < 10 - image_xscale; i++)
			{
				var rndm = random_range(-5,5);
				draw_sprite_ext(sprite_index,image_index,x+rndm,y+rndm,image_xscale,image_yscale,0,c_purple,0.5);
			}
		}

		if (sprite_index == spr_Elemental_Orb_OFF) && (alarm[0] mod 40 == 0)
		{
			for(var i = 0; i < 30; i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,image_blend,0.5);
			}
		}

		if (sprite_index == spr_Elemental_Orb_ON)
		{
			for(var i = 0; i < image_index*2; i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,image_blend,0.5);
			}

			for(c = 0;c < 360;c += 60)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),1.5,1.5,random(360),image_blend,image_alpha*0.25)
			}
		}

		if (sprite_index == spr_Explosion_bmb) or (sprite_index == spr_Explosion_RExp2)
		{
			for(c = 0;c < 360;c += 60)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),image_xscale,image_xscale,random(360),image_blend,image_alpha*0.25)
			}
	
			draw_self();
		}

		if (sprite_index == spr_Explosion_grnd) or (sprite_index == spr_Explosion_RExp1)
		{
			if (image_index > 6)
			{
				for(c = 0;c < 360;c += 120)
				{
				  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y-(sprite_height/3)+lengthdir_y(image_index,c),image_xscale,image_xscale,random(360),image_blend,image_alpha*0.25)
				}

				draw_self();
			}
		}

		if (image_blend == c_red)
		{
			for(c = 0;c < 360;c += 20)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),1.5,1.5,random(360),image_blend,image_alpha*0.25)
			}

			for(var i = 0; i < 2; i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,1);
			}
		}
	}
}
#endregion

#region //--> Spectral Bomb Blend Mode
if (instance_exists(obj_Spectral_Bomb))
{
	with (obj_Spectral_Bomb)
	{
		for(c = 0;c < 360;c += 180)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.1)
		}
		draw_self();
	}
}
#endregion
#region //--> Flarestar Blend Mode
if (instance_exists(obj_Flarestar))
{
	with (obj_Flarestar)
	{
		for(c = 0;c < 360;c += 60)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.1)
		}
		
		gpu_set_blendmode(bm_normal);
		
		draw_self();
		
		if (sprite_index == spr_Flarestar_create)
			draw_sprite_ext(spr_Flarestar_create_inner,image_index,x,y,1,1,0,c_white,1);
	
		if (sprite_index == spr_Flarestar)
			draw_sprite_ext(spr_Flarestar_inner,image_index,x,y,1,1,360-fs_img_angl,c_white,1);
	
		if (sprite_index == spr_Flarestar_exp)
		{
			draw_sprite_ext(spr_Flarestar_exp_inner,image_index,x,y,fs_increm,fs_increm,360-fs_img_angl,c_white,1);
		}
		
		gpu_set_blendmode(bm_add);
	}
}
#endregion
#region //--> Lightning Ball Blend Mode
if (instance_exists(obj_Lightningball))
{
	with (obj_Lightningball)
	{
		for(c = 0;c < 360;c += 180)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.25)
		}

		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,choose(c_navy,c_navy,c_navy,c_white),0.5);
		}
	}
}
#endregion
#region //--> Relampago Blend Mode
if (instance_exists(obj_relampago))
{
	with (obj_relampago)
	{
		for(var i = 0;i < 10;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,0.2);
		}
	}
}
#endregion
#region //--> V-Lightning Blend Mode
if (instance_exists(obj_V_lightning))
{
	with (obj_V_lightning)
	{
		for(var i = 0;i < 10;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,0.2);
		}
		gpu_set_blendmode(bm_normal);

		if (room == rm_Crab)
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,c_white,1);
		
		gpu_set_blendmode(bm_add);
	}
}
#endregion
#region //--> ThunderWave Blend Mode
if (instance_exists(obj_TWave))
{
	with (obj_TWave)
	{
		for(c = 0;c < 360;c += 20)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,c_aqua,image_alpha*0.25)
		}

		for(var i = 0;i < 2;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,c_white,0.5);
		}
	}
}
#endregion
#region //--> Homming Fireball Blend Mode
if (instance_exists(obj_Hmmng_Fireball))
{
	with (obj_Hmmng_Fireball)	
	{
		for(c = 0;c < 360;c += 90)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.25)
		}
	}
}
#endregion
#region //--> Iceball Blend Mode
if (instance_exists(obj_Iceball))
{
	with (obj_Iceball)
	{
		if (ib_hitpoints < 6) and (alarm[0] <= 1)
		{
			for(c = 0;c < 360;c += 90)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.25)
			}
		}
	}
}
#endregion
#region //--> Granite Shield Blend Mode
if (instance_exists(obj_GraniteShield))
{
	with (obj_GraniteShield)
	{
		if (image_blend == c_yellow)
		{
			for(var i = 0;i < 3;i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,c_white,0.5);
			}
		}
	}
}
#endregion
//--> FX Objects
#region //--> Scorpion FX Blend Mode
if (instance_exists(obj_FX_Scorpion))
{
	with (obj_FX_Scorpion)
	{
		for(var i = 0;i < 5;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,image_blend,image_alpha);
		}

		for(c = 0;c < 360;c += 90)
		{
		  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha)
		}
	}
}
#endregion
#region //--> Dash Afterimage Blend Mode
if (instance_exists(dash_afterimage))
{
	with (dash_afterimage)
	{
		draw_self();
		
		if (glowFX)
		{
			for(var i = 0;i < 5;i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}

			for(c = 0;c < 360;c += 90)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha)
			}
		}
	}
}
#endregion
#region //--> ShadowImage Blend Mode
if (instance_exists(obj_ShadowImage))
{
	with (obj_ShadowImage)
	{
		if (visible)
		{
			if (glowFX)
			{
				for(var i = 0;i < 5;i ++)
				{
					draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
				}

				for(c = 0;c < 360;c += 90)
				{
				  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha)
				}
			}
			
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,image_blend,1);
		}
	}
}
#endregion
#region //--> Obj_Chispa Blend Mode
if (instance_exists(obj_chispa))
{
	with (obj_chispa)
	{
		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,0,chispa_colour,1);
		}
	}
}
#endregion
#region //--> Obj_Charg_Chispa Blend Mode
if (instance_exists(obj_charge_chispa))
{
	with (obj_charge_chispa)
	{
		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,angl,image_blend,1);
		}
	}
}
#endregion
#region //--> Obj_Bubble Blend Mode
if (instance_exists(obj_bubble))
{
	with (obj_bubble)
	{
		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,angl,image_blend,1);
		}
	}
}
#endregion
#region //--> Obj_CenterXYFollow Blend Mode
if (instance_exists(obj_centerXYfollow))
{
	
}
#endregion

#region //--> Obj_RegenBubble Drawing
var regenbubble_exists = (instance_exists(obj_RegenBubble))
if (regenbubble_exists)
{
	with (obj_RegenBubble) draw_self();
}
#endregion
gpu_set_blendmode(bm_normal);

//-->DRAW EVENTS (NO BLENDING) Part2


#region //--> Obj_BossEnemy Drawing
var BossEnemy_exists = (instance_exists(obj_BossEnemy));
if (BossEnemy_exists)
{
	with (obj_BossEnemy)
	{
		var drawalready = (visible) ? false : true;

		if (!drawalready)
		{
			if (!other.RedShaderActive)
			{
				if (old_hit_points != hit_points) || (place_meeting(x,y,obj_Manticore_hitbox)) || (place_meeting(x,y,obj_Throwing_Wpn))
				{
					if (old_hit_points < hit_points)
					{
						var heal = choose(c_lime,merge_color(c_lime,c_green,0.5));
						gpu_set_fog(true,heal,0,0);
						draw_self();
						gpu_set_fog(false,heal,0,0);
					}
					else
					{
						var alf = choose(0,1);
						if (alf)
						{
							gpu_set_fog(true,dmgColor,0,0);
							draw_self();
							gpu_set_fog(false,dmgColor,0,0);
						}
						else draw_self();
						draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,dmgColor,0.5);
					}
				}
				else draw_self();
			}
			else
			{
				gpu_set_fog(true,c_white,0,0);
				draw_self();
				gpu_set_fog(false,c_white,0,0);
			}
			if ((enemy_name == "DragonLordF")||(enemy_name == "DragonLordI")) && (state == DragonLord_Defeated)
			{
				var SIZErndm = random_range(0.5,3);
				var explosion = (enemy_name == "DragonLordF") ? spr_Flarestar_exp : spr_FrostBulwark_breaks;
				var index = (enemy_name == "DragonLordF") ? choose(0,7) : choose(0,5);
				if (obj_DragonLordRoom_manager.Prodigy)
				{
					explosion = choose(spr_Flarestar_exp,spr_FrostBulwark_breaks);
					index = (explosion == spr_Flarestar_exp) ? choose(0,7) : choose(0,5);
				}
				draw_sprite_ext(explosion,index,x+random_range(-200,200),y+random_range(-200,200),SIZErndm,SIZErndm,0,c_white,1);
				
			}
		}
	}
}
#endregion
#region //--> Willowisp Blend Mode
if (instance_exists(obj_willowisp))
{
	with (obj_willowisp)
	{
		
		gpu_set_blendmode(bm_add);
		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(spr_willowisp,image_index,x,y,image_xscale,image_yscale,0,flame_colour,0.5);
			draw_self();
		}
		
		gpu_set_blendmode(bm_normal);
	}
}
#endregion
#region //--> Willowisp SP Blend Mode
if (instance_exists(obj_willowisp_sp))
{
	with (obj_willowisp_sp)
	{
		gpu_set_blendmode(bm_add);
		for(var i = 0;i < 1;i ++)
		{
			draw_sprite_ext(spr_willowisp,image_index,x,y,image_xscale,image_yscale,0,flame_colour,0.5);
			draw_self();
		}
		gpu_set_blendmode(bm_normal);
	}
}
#endregion


#region //--> Obj_Manticore Drawing
var Manticore_exists = (instance_exists(obj_Manticore))
if (Manticore_exists)
{
	with(obj_Manticore)
	{
		if (!other.RedShaderActive) draw_self();
		else
		{
			gpu_set_fog(true,c_white,0,0);
			draw_self();
			gpu_set_fog(false,c_white,0,0);
		}
		
		if (global.RELIKS[28,2])
		{
			switch(sprite_index)	
			{
				case spr_Manticore_run: case spr_Manticore_airthrow: case spr_Manticore_throw: case spr_Manticore_airthrow2:
				case spr_Manticore_throw_2: case spr_Manticore_TWSP1: case spr_Korunax_sp: case spr_Korunax_sp_air: case spr_Whisperer:
				case spr_Korunax: case spr_Mjolnir: case spr_Mirage: case spr_Argenta: case spr_Dyrnwyn:
					var Yrepos = 0;
					var Xrepos = 0;
					if (sprite_index = spr_Mjolnir)||(sprite_index = spr_Korunax)||(sprite_index = spr_Mirage)
					{
						Yrepos = (image_index < 7) ? 15 : 0;
						Xrepos = (image_index < 7) ? 0 : -15 * image_xscale;
					}
					draw_sprite_ext(spr_runFeatherCloak,image_index,x+Xrepos,y+Yrepos,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					break;
					
				case spr_Manticore_doublejmp: case spr_AirArgenta: case spr_AirDyrnwyn:
					draw_sprite_ext(spr_runFeatherCloak,image_index,x,y,image_xscale,image_yscale,(45*-image_xscale)*image_index,c_white,random_range(0.8,1));
					break;
					
				case spr_damage_Manticore:
					draw_sprite_ext(spr_dmgFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
				
				case spr_Manticore_wallcling:
					draw_sprite_ext(spr_wallFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					break;
					
				case spr_Manticore_jump:
					if (image_index == 0) draw_sprite_ext(spr_runFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					else draw_sprite_ext(spr_fallFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random(1));
					break;
				
				case spr_Manticore:
					draw_sprite_ext(spr_standFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					break;
					
				case spr_Manticore_spCharge: case spr_Manticore_TWSP2: case spr_Manticore_TWSP3: case spr_Argenta_Special_air:
				case spr_Argenta_Special_floor: case spr_Manticore_UpD_prep: case spr_Whisperer_sp: case spr_Mjolnir_sp:
				case spr_AirArgentaMix: case spr_ArgentaMix: case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_Manticore_CLSP:
				case spr_AirKorunax: case spr_AirMirage: case spr_AirWhisperer: case spr_AirMjolnir:
					var Yrepos = (sprite_index==spr_Manticore_spCharge)||(sprite_index==spr_Whisperer_sp)||(sprite_index==spr_Mjolnir_sp) ? -15 :
								 ((sprite_index==spr_AirKorunax)||(sprite_index==spr_AirWhisperer)||(sprite_index==spr_AirMjolnir)||(sprite_index==spr_AirMirage) ? 5 : 0);
					var Xrepos = (sprite_index==spr_Manticore_spCharge)||(sprite_index==spr_Whisperer_sp)||(sprite_index==spr_Mjolnir_sp) ? -15*image_xscale : 0;
					draw_sprite_ext(spr_fallFeatherCloak,image_index,x+Xrepos,y+Yrepos,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					break;
				
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_AirFrostbiterMix: case spr_AirFrostbiterUP: case spr_GirTab:
				case spr_AirGirTab: case spr_AirGirTabMix: case spr_AirGirTabUP: case spr_AirGirTrident: case spr_AirGirTridentMix:
				case spr_AirGirTridentUP: case spr_GirTabMix: case spr_GirTabUP: case spr_FrostbiterUP:
					var tilt = vsp * image_xscale * 2;
					draw_sprite_ext(spr_runFeatherCloak,image_index,x,y,image_xscale,image_yscale,tilt,c_white,random_range(0.8,1));
					break;
				
				case spr_CrouchDEF_Korunax: case spr_CrouchDEF_Mirage: case spr_CrouchDEF_Mjolnir: case spr_CrouchDEF_Whisperer:
					draw_sprite_ext(spr_crouchFeatherCloak,image_index,x,y,image_xscale,image_yscale,0,c_white,random_range(0.8,1));
					break;
			}
		}
	}
}
#endregion
#region //--> Petrification Drawing
var Petrification_exists = instance_exists(obj_petrification);
if (Petrification_exists)
{
	with(obj_petrification) draw_self();
}
#endregion
#region //--> Obj_WallAttack Drawing
var watk_exists = (instance_exists(obj_Wallattack))
if (watk_exists)
{
	with (obj_Wallattack) draw_self();
}
#endregion

#region //--> Obj_Fadeaway
if (instance_exists(obj_fadeAway))
{
	with (obj_fadeAway)
	{
		if (bright)
		{
			for(var i = 0;i < 1;i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
			}
		}
	}
}
#endregion
#region //--> Obj_CenterXYFollow Drawing
var CenterXY_exists = (instance_exists(obj_centerXYfollow))
if (CenterXY_exists)
{
	with (obj_centerXYfollow)
	{
		if (normalDraw) draw_self();
		gpu_set_blendmode(bm_add);
		if (glow)
		{
			for(c = 0;c < 360;c += g_amount)
			{
			  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.25)
			}
		}
		if (brightness)
		{
			for(var i = 0;i < b_amount;i ++)
			{
				draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,0.5);
			}
		}
		gpu_set_blendmode(bm_normal);
	}
}
#endregion

#region //--> Obj_EnemyBullet Drawing
var EnemyBullet_exists = (instance_exists(obj_EnemyBullet))
if (EnemyBullet_exists)
{
	with (obj_EnemyBullet)
	{
		//draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,image_angle,image_blend,image_alpha);
		draw_self();
	}
}
#endregion

#region //--> Obj_Speedflux Drawing
var speedflux_exists = (instance_exists(obj_Speedflux))
if (speedflux_exists)
{
	with (obj_Speedflux) draw_self();
}
#endregion
#region //--> Obj_StrAtk Drawing
var StrAtk_exists = (instance_exists(obj_StrAtk))
if (StrAtk_exists)
{
	with (obj_StrAtk)
	{
		if (sprite_index==spr_StrAtk_Mirage1)||(sprite_index==spr_StrAtk_Mirage2)||
		   (sprite_index==spr_StrAtk_Mjolnir1)||(sprite_index==spr_StrAtk_Mjolnir2)
		{
			if (start_draw) draw_self();
			gpu_set_blendmode(bm_add);
			for(var i = 0;i < 1;i ++)
			{
				if (start_draw)
				{
					draw_sprite_ext(sprite_index,image_index,x+(random_range(-10,10)),y+(random_range(-10,10)),image_xscale,image_yscale,0,choose(c_lime,c_purple),0.5);
					//draw_self();
				}
			}
			gpu_set_blendmode(bm_normal);
		}
		else if (start_draw) draw_self();
	}
}
#endregion
#region //--> Obj_TWSpecial
if (instance_exists(obj_TWSpecial))
{
	with (obj_TWSpecial)
	{
		if (visible)
		{
			if (sprite_index == spr_wavelightning) || (sprite_index == spr_flametongues)
			{
				if (sprite_index == spr_flametongues)
				{
					gpu_set_blendmode(bm_add);
					draw_self()
					gpu_set_blendmode(bm_normal);
				}
				else
				{
					gpu_set_blendmode(bm_add);
					for(c = 0;c < 360;c += 30)
					{
					  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y+lengthdir_y(image_index,c),image_xscale,image_yscale,image_angle,image_blend,image_alpha*0.25)
					}
			
					for(var i = 0; i < 6 - image_xscale; i++)
					{
						var rndm = random_range(-60,60);
						var rndm2 = random_range(-60,60);
						draw_sprite_ext(sprite_index,image_index,x+rndm,y+rndm2,image_xscale,image_yscale,0,c_blue,0.5);
					}
					gpu_set_blendmode(bm_normal);
				}
			}
			else
			{
				if (sprite_index == spr_TornadoINNER_front)	draw_sprite_ext(spr_TornadoINNER_back,image_index,x,y,image_xscale,image_yscale,0,c_white,image_alpha);
				if (sprite_index == spr_Explosion_grnd)
				{
					if (image_index > 6)
					{
						for(c = 0;c < 360;c += 120)
						  draw_sprite_ext(sprite_index,image_index,x+lengthdir_x(image_index,c),y-(sprite_height/3)+lengthdir_y(image_index,c),image_xscale,image_xscale,random(360),image_blend,image_alpha*0.25);
					}
				}
				draw_self();
			}
		}
	}
}
#endregion
#region //--> Throwing_Weapon Drawing
var TW_exists = (instance_exists(obj_Throwing_Wpn))
if (TW_exists)
{
	with(obj_Throwing_Wpn)
	{
		if (sprite_index != spr_Bomb1) && (sprite_index != spr_Bomb2) draw_self();

		if (sprite_index == spr_Bomb1) or (sprite_index == spr_Bomb2)
		{
			bounce_count += 2 * bomb_hsp * -direc;
			draw_sprite_ext(sprite_index,image_index,x,y,image_xscale,image_yscale,bounce_count,image_blend,image_alpha)
		}
	}
}
#endregion


#region//--> Obj_Show_Damage Drawing
var show_damage_exists = (instance_exists(obj_show_damage));
if (show_damage_exists)
{
	with (obj_show_damage)
	{
		if (weakness != noone)
		{
			draw_set_font(Antares);
			draw_set_halign(fa_center);
			draw_set_valign(fa_center);

			switch (weakness)
			{
				case 0:	draw_set_colour(c_black);
						draw_text_transformed(x,y, plus_or_minus + string(damage_display),1.2,1.2,1)
						draw_set_colour(font_color);
						draw_text_transformed(x+2,y-1, plus_or_minus + string(damage_display),1.2,1.2,1)
						break;
				case 1: draw_set_colour(c_yellow);
						draw_text_transformed(x,y, "-" + string(damage_display),1.5,1.5,1)
						draw_text_transformed_color(x+2,y-1, "-" + string(damage_display),1.5,1.5,1,c_white,c_red,c_red,c_orange,1);
						break;
				case -1: draw_text_transformed_color(x,y, "-" + string(damage_display),1,1,1,c_black,c_black,c_black,c_black,1);
						 draw_text_transformed_color(x+2,y-1, "-" + string(damage_display),1,1,1,c_white,c_aqua,c_aqua,c_blue,1);
						 break;
			}
	
			draw_set_color(c_white);
		}
		else //--> If immune to damage, show clank sprite instead of damage numbers
		{
			sprite_index = spr_clank;
			image_angle = random(360);
			var temp_scale = random_range(0.7,1.5);
			image_xscale = temp_scale;
			image_yscale = temp_scale;
			draw_self();
		}
	}
}
#endregion
#region //--> Obj_Shockwave_front
if (instance_exists(obj_Shockwave_front))
{
	with (obj_Shockwave_front)
	{
		shockwave_draw1();
	}
}
#endregion