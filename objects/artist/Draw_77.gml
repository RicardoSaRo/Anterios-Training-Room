/// @description Shader Effects
// This is "SHADER INTERPOLATION MACHINE" created by me to blend shaders on the application surface (inspired by the concept of serigraphy)
// instead of creating a new shader from the mix of two or more.

//-->SHADERS

if (RedShaderActive)||(EdgyShaderActive)||(BloomFilterActive)||(SepiaActive)||(InvertActive)||
   (VoidWorldActive)
{
	application_surface_draw_enable(false);
	var DispW = display_get_width();
	var DispH = display_get_height();
	
	if (!surface_exists(A_surf))
	{
		A_surf = surface_create(global.vsizeW,global.vsizeH);
		bloom_textureA = surface_get_texture(A_surf);
	}
	
	if (!surface_exists(B_surf))
	{
		B_surf = surface_create(global.vsizeW,global.vsizeH);
		bloom_textureB = surface_get_texture(B_surf);
	}
	
	var surf2draw = application_surface;
	
	if (RedShaderActive)
	{	
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_Red);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
		BloomFilterActive = false;
		BlurActive = false;
	}
	
	if (BloomFilterActive)
	{	
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_bloom_filter_luminance);
		shader_set_uniform_f(u_bloom_threshold,0.6);
		shader_set_uniform_f(u_bloom_range,0.2);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	if (BlurActive)
	{	
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_blur_2_pass_gauss_lerp);
		var blurInt = (VoidWorldActive) ? 0.3 : 0.6;
		shader_set_uniform_f(u_blur_steps,blurInt);
		shader_set_uniform_f(u_sigma,3.0);
		shader_set_uniform_f(u_blur_vector,1,0);
		shader_set_uniform_f(u_texel_size,texel_w,texel_h);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
		//--> Second pass
		shader_set_uniform_f(u_blur_vector,0,1);
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	if (SepiaActive)
	{	
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_sepia);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	if (EdgyShaderActive)
	{
		var edgy = shader_get_uniform(shd_edgedetect,"size");
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_edgedetect);
		var rndmFX = random_range(3,10);
		shader_set_uniform_f(edgy,room_width,room_height,rndmFX);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	if (VoidWorldActive)
	{
		var edgy = shader_get_uniform(shd_edgedetect,"size");
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_edgedetect);
		shader_set_uniform_f(edgy,room_width,room_height,3);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	if (InvertActive)
	{	
		if ((surf2draw == application_surface) || (surf2draw == B_surf)) surface_set_target(A_surf);
		else surface_set_target(B_surf);
		shader_set(shd_invert);
		draw_surface(surf2draw,0,0);
		surface_reset_target();
		surf2draw = ((surf2draw == application_surface) || (surf2draw == B_surf)) ? A_surf : B_surf;
	}
	
	shader_reset();
	if (!RedShaderActive) draw_surface_stretched(application_surface,0,0,DispW,DispH);
	if (BloomFilterActive)
	{
		gpu_set_blendmode(bm_add);
		draw_surface_stretched(surf2draw,0,0,DispW,DispH);
		gpu_set_blendmode(bm_normal);
	}
	else draw_surface_stretched(surf2draw,0,0,DispW,DispH);
	
	if (resultsSpr) //--> For Sepia Results Background
	{	
		if (global.results_bckgrnd == noone)
			global.results_bckgrnd = sprite_create_from_surface(surf2draw,0,0,DispW,DispH,true,true,DispW/2,DispH/2);
	}
}
else application_surface_draw_enable(true);
