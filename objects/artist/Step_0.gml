/// @description Artist Step
//--> Slow Room Speed if the player is damaged
if (obj_Manticore.state == damage_Manticore) && (room_speed == 60)
{
	room_speed = 30;
}

//--> Increase Room Speed by 1 if Room Speed is below 60.
if (room_speed < 60) room_speed += 1;

if (RedShaderActive) //--> For Gir-Tab Special Attack Effects
{
	artist.EdgyShaderActive = (artist.EdgyShaderActive) ? choose(true,true,false) : choose(false,false,true);
	
	if (room_speed >= 60)
	{
		RedShaderActive = false;
		EdgyShaderActive = false;
	}
	
	var rayo = instance_create_layer(obj_BossEnemy.x,obj_BossEnemy.y,choose("FX_Layer","Back_FX_Layer"),obj_centerXYfollow);
	rayo.sprite_index = spr_V_lightning;
	var rndmSIZE = irandom_range(2,5);
	rayo.image_angle = random(360);
	rayo.image_xscale = rndmSIZE;
	rayo.image_yscale = rndmSIZE;
	rayo.obj_2_follow = obj_BossEnemy;
	rayo.anim_end = 2;
	rayo.glow = true;
	rayo.brightness = true;
	var sndFX = choose(snd_lightning2,snd_lightning1);
	if (!audio_is_playing(snd_GirTabSP3)) audio_play_sound(snd_GirTabSP3,5,0);
	if (!artist.EdgyShaderActive)
	{
		audio_sound_pitch(sndFX,random_range(0.9,1.2));
		audio_play_sound(sndFX,20,0);
	}
}

if (obj_Manticore.sprite_index == spr_Special_GirTab) && (instance_exists(obj_Manticore_hitbox))
{
	with(obj_Manticore_hitbox)
	{
		if (place_meeting(x,y,obj_BossEnemy))
		{
			room_speed = 40;
			artist.RedShaderActive = true;
		}
	}
}