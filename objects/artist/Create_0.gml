/// @description Artist Create
// The artist is the object that Draws everything on the Application Surface (including Shaders).


frameskip = 0;
resultsSpr = false;

//--> Variables for normal display
cam_x = 1920;
cam_y = 1080;

//--> Variables for GUI display
gui_x = 1366;
gui_y = 768;

bossText = "";

//--> Names Boss or Explorations
if (global.HuntRoom != noone)
{
	switch(obj_BossEnemy.enemy_name)
	{
		case "Werewolf_Brown":	bossText = "TIMBER WEREWOLF"; break;
		case "Werewolf_Gray":	bossText = "SILVERBACK WEREWOLF"; break;
		case "Werewolf_Pink":	bossText = "PINKMANE WEREWOLF"; break;
		case "Werewolf_Foxy":	bossText = "SPIRITUAL WEREWOLF"; break;
		case "Werewolf_Gold":	bossText = "GOLDBACK WEREWOLF"; break;
		case "OCrab":			bossText = "KARKINOS"; break;
		case "GCrab":			bossText = "TAMBANOKANO"; break;
		case "BDragonkin":		bossText = "DRAGIANKIN"; break;
		case "EDragonkin":		bossText = "DRAGIANKIN ELITE"; break;
		case "Wastipede":		bossText = "WASTIPEDE"; break;
		case "Mantaspede": 		bossText = "MANTASPEDE"; break;
		case "Meetonak": 		bossText = "ME2NAK"; break;
		case "BGolem": 			bossText = "KORUNITE GIANT"; break;
		case "GGolem": 			bossText = "KORUNITE TITAN"; break;
		case "Rose": 			bossText = "ROSE ALRAUNE"; break;
		case "Aster": 			bossText = "ASTER ALRAUNE"; break;
		case "Dahlia": 			bossText = "DAHLIA ALRAUNE"; break;
		case "Ixia": 			bossText = "IXIA ALRAUNE"; break;
		case "Orchid": 			bossText = "ORCHID ALRAUNE"; break;
		case "Lotus": 			bossText = "LOTUS ALRAUNE"; break;
		case "DragonLordF":		bossText = "DRAGIAN FIRELORD"; break;
		case "DragonLordI":		bossText = "DRAGIAN ICELORD"; break;
		case "VenomLordB":		bossText = "DRAGIAN VENOMLORD"; break;
		case "VenomLordP":		bossText = "FROSTVENOMLORD"; break;
		case "PhoeniskR":		bossText = "DRAGIAN PHOENISK"; break;
		case "PhoeniskV":		bossText = "VIOLETFLAME PHOENISK"; break;
		case "3HeadHydra":		bossText = "BELICA HYDRA"; break;
	}
}


//--> Variables for Meetonak bosses
ArtBoss = obj_BossEnemy;
ArtBoss2 = obj_BossEnemy;

//--> Variables for Bars and tanks (GUI)
TWBar_Empty = ((global.RELIKS[41,2])||(global.RELIKS[42,2])) ? spr_TWPotion_Empty : spr_TWBar_Empty;
TWBar_Full = (global.RELIKS[42,2]) ? spr_TWPPotion_Full : ((global.RELIKS[41,2]) ? spr_TWPotion_Full : spr_TWBar_Full);
LockSpr = ((global.RELIKS[41,2])||(global.RELIKS[42,2])) ? spr_PotionLock : spr_Lock;
dmgTankMax = (global.RELIKS[25,2]) ? 7000 : 5000;

//--> For Regen animation
old_spbar1 = obj_Manticore.current_sp_sword;
old_spbar2 = obj_Manticore.current_sp_axe;
old_regen = obj_Manticore.alarm[5];
max_Regen = noone;
regenFX = -1;
count2twelve = 0; 

//--> Fades to red when player looses
fade2red = 0;

//--> Creates Life Orbs
for (var i = 0; i < global.player_max_health; i++)
{
	var temp_LO = instance_create_layer(0,0,"Life_n_Bars",obj_Life_Orb);
	temp_LO.LO_number = i+1;
	temp_LO.image_speed = 1;
	temp_LO.sprite_index = spr_Life_Orb;
}

if (global.RELIKS[50,2]) global.player_max_health = 2;
if (global.RELIKS[48,2]) global.player_max_health /= 2;

//if (global.RELIKS[29,2]) instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_Speedflux);
//if (global.RELIKS[30,2]) instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_RegenBubble);

//--> Assigns Enemy Bar border for each room

switch(room)
{
	case rm_testroom:	borderBar = spr_stonebar_border; borderX = 71; borderY = 670; break;
}

//--> For status ailments FX
poisonFX = false;
burnFX = false;
freezeFX = false;
venomFX = false;
violet = make_color_rgb(144,82,188);

//--> surfaces
A_surf = surface_create(global.vsizeW,global.vsizeH);
B_surf = surface_create(global.vsizeW,global.vsizeH);

//--> Uniforms and Texels for shaders
u_bloom_threshold = shader_get_uniform(shd_bloom_filter_luminance,"bloom_threshold");
u_bloom_range = shader_get_uniform(shd_bloom_filter_luminance,"bloom_range");
u_blur_steps			= shader_get_uniform(shd_blur_2_pass_gauss_lerp, "blur_steps");
u_sigma					= shader_get_uniform(shd_blur_2_pass_gauss_lerp, "sigma");
u_blur_vector			= shader_get_uniform(shd_blur_2_pass_gauss_lerp, "blur_vector");
u_texel_size			= shader_get_uniform(shd_blur_2_pass_gauss_lerp, "texel_size");
u_bloom_intensity		= shader_get_uniform(shd_bloom_blend, "bloom_intensity");
u_bloom_darken			= shader_get_uniform(shd_bloom_blend, "bloom_darken");
u_bloom_saturation		= shader_get_uniform(shd_bloom_blend, "bloom_saturation");
u_bloom_texture			= shader_get_sampler_index(shd_bloom_blend, "bloom_texture");
bloom_textureA = surface_get_texture(A_surf);
bloom_textureB = surface_get_texture(B_surf);

mirror_tex = shader_get_sampler_index(shd_mirror,"tex");
var tex	= sprite_get_texture(spr_Manticore, 0);
texel_w	= texture_get_texel_width(tex);
texel_h = texture_get_texel_height(tex);

//--> Shader Check Variables
BloomFilterActive = true;
BlurActive = true;
SepiaActive = false;
InvertActive = false;
RedShaderActive = false;
EdgyShaderActive = false;
VoidWorldActive = false;