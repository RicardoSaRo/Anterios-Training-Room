/// @description Frameskip
// Unused code for frameskip. Helps performance but its not decided if will be included in the final product as an option.
/*
frameskip = frameskip ? 0 : 1;
draw_enable_drawevent(frameskip);
