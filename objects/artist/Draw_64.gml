/// @description DRAW GUI
// Draws every info relevant for the player: Current life, Special Bars/Tanks, Equipped Weapons, Boss Life, etc.

current_sword = global.equippedSWORD;
current_axe = global.equippedAXE;
current_tw = global.equippedTW;

var artist_weapon = "";
var sword_icon = spr_icon_Argenta;
var axe_icon = spr_icon_Korunax;
var tw_icon = spr_icon_Chakram;
var sp1_colour = c_white;
var sp2_colour = c_white;
var sword_bar = obj_Manticore.special_Argenta;
var axe_bar = obj_Manticore.special_Korunax;
var ammo = obj_Manticore.TWammo[global.equippedTW,0];
var MAXammo = obj_Manticore.TWammo[global.equippedTW,1];

draw_set_font(Antares);
draw_set_halign(fa_center);
draw_set_valign(fa_center);

if (burnFX)
{
	draw_sprite(spr_icon_Burn,irandom(11),52,gui_y-622);
	draw_sprite(spr_icon_Burn,irandom(11),52,gui_y-570);
	draw_sprite(spr_icon_Burn,irandom(11),52,gui_y-518);
	draw_sprite(spr_specialbar_Burn,irandom(11),80,gui_y-640);
	draw_sprite(spr_specialbar_Burn,irandom(11),80,gui_y-590);
}

//--> Equipped Sword/Lance
switch (current_sword)
{
	case 1: artist_weapon = "ARGENTA";
			sword_bar = obj_Manticore.special_Argenta;
			sword_icon = spr_icon_Argenta;
			sp1_colour = c_silver;
			break;
	case 2: artist_weapon = "DYRNWYN";
			sword_bar = obj_Manticore.special_Dyrnwyn;
			sword_icon = spr_icon_Dyrnwyn;
			sp1_colour = make_color_rgb(241,100,31);
			break;
	case 3: artist_weapon = "GIR-TAB";
			sword_bar = obj_Manticore.special_GirTab;
			sword_icon = spr_icon_GirTab;
			sp1_colour = c_red;
			break;
	case 4: artist_weapon = "FROSTBITER";
			sword_bar = obj_Manticore.special_Frostbiter;
			sword_icon = spr_icon_Frostbiter;
			sp1_colour = c_aqua;
			break;
}

draw_set_halign(fa_left);
draw_set_valign(fa_left);
draw_sprite(sword_icon,0,52,gui_y-622);
draw_set_colour(c_black);
draw_text(80, gui_y-622, string(artist_weapon));
draw_text(360, gui_y-622, string(sword_bar)+"%");
if (sword_bar < 50) draw_set_colour(c_white);
if (sword_bar >= 50)&&(sword_bar <= 99) draw_set_color(c_yellow);
if (sword_bar == 100) draw_set_color(choose(c_yellow,c_red,c_orange,c_fuchsia,));
draw_text(82, gui_y-622, string(artist_weapon));
draw_text(362, gui_y-622, string(sword_bar)+"%");

//--> Equipped Axe/Mace
switch (current_axe)
{
	case 1: artist_weapon = "KORUNAX";
			axe_bar = obj_Manticore.special_Korunax;
			axe_icon = spr_icon_Korunax;
			sp2_colour = make_color_rgb(211,151,65);
			break;
	case 2: artist_weapon = "MIRAGE";
			axe_bar = obj_Manticore.special_Mirage;
			axe_icon = spr_icon_Mirage;
			sp2_colour = c_fuchsia;
			break;
	case 3: artist_weapon = "MJOLNIR";
			axe_bar = obj_Manticore.special_Mjolnir;
			axe_icon = spr_icon_Mjolnir;
			sp2_colour = make_color_rgb(39,100,205);
			break;
	case 4: artist_weapon = "WHISPERER";
			axe_bar = obj_Manticore.special_Whisperer;
			axe_icon = spr_icon_Whisperer;
			sp2_colour = c_lime;
			break;
}

draw_sprite(axe_icon,0,52,gui_y-570);
draw_set_colour(c_black);
draw_text(80, gui_y-570, string(artist_weapon));
draw_text(360, gui_y-570, string(axe_bar)+"%");
if (axe_bar < 50) draw_set_colour(c_white);
if (axe_bar >= 50)&&(axe_bar <= 99) draw_set_color(c_yellow);
if (axe_bar == 100) draw_set_color(choose(c_yellow,c_red,c_orange,c_fuchsia,));
draw_text(82, gui_y-570, string(artist_weapon));
draw_text(362, gui_y-570, string(axe_bar)+"%");

draw_set_colour(c_white);//--> Turns font color to normal

//--> Equipped Throwing Weapons
switch (current_tw)
{
	case 1: artist_weapon = "CHAKRAM";
			tw_icon = spr_icon_Chakram;
			break;
	case 2: artist_weapon = "KUNAI";
			tw_icon = spr_icon_Kunai;
			break;
	case 3: artist_weapon = "GRENADE";
			tw_icon = spr_icon_Granade;
			break;
	case 4: artist_weapon = "BOMB";
			tw_icon = spr_icon_Bomb;
			break;
	case 5: artist_weapon = "GUILLOTINE";
			tw_icon = spr_icon_Guillotine;
			break;
	case 6: artist_weapon = "SHURIKEN";
			tw_icon = spr_icon_Shuriken;
			break;
	case 7: artist_weapon = "TRIBLADER";
			tw_icon = spr_icon_Triblader;
			break;
	case 8: artist_weapon = "STARBLADES";
			tw_icon = spr_icon_Starblades;
			break;
	case 9: artist_weapon = "ASSEGAI";
			tw_icon = spr_icon_Assegai;
			break;
	case 10: artist_weapon = "THUNDER ORB";
		     tw_icon = spr_icon_T_Orb;
			 break;
	case 11: artist_weapon = "WIND ORB";
		     tw_icon = spr_icon_W_Orb;
			 break;
	case 12: artist_weapon = "FIRE ORB";
		     tw_icon = spr_icon_F_Orb;
			 break;
	case 13: artist_weapon = "ICE ORB";
		     tw_icon = spr_icon_I_Orb;
			 break;
	case 14: artist_weapon = "REMOTE EXPLOSIVE";
		     tw_icon = spr_icon_RemoteExp;
			 break;
	case 15: artist_weapon = "COIN_LAUNCHER";
		     tw_icon = spr_icon_CoinLaunch;
			 break;
	case 16: artist_weapon = "VOID RIPPLES";
		     tw_icon = spr_icon_VoidRipples;
			 break;
}

//--> Draws Tanks
if (instance_exists(obj_Manticore))
{
	//--> TW Tank1
	if (obj_Manticore.TWTank[1,0] < dmgTankMax) draw_sprite(TWBar_Empty,0,93,gui_y-532);
	else draw_sprite(TWBar_Full,irandom(3),93,gui_y-532);
	//--> TW Tank 2
	if (obj_Manticore.TWTank[2,0] < dmgTankMax) draw_sprite(TWBar_Empty,0,119,gui_y-532);
	else draw_sprite(TWBar_Full,irandom(3),119,gui_y-532);
	if (global.TWs[global.equippedTW,3] < 1) draw_sprite(LockSpr,0,119,gui_y-532);
	//--> TW Tank 3
	if (obj_Manticore.TWTank[3,0] < dmgTankMax) draw_sprite(TWBar_Empty,0,145,gui_y-532);
	else draw_sprite(TWBar_Full,irandom(3),145,gui_y-532);
	if (global.TWs[global.equippedTW,3] < 2) draw_sprite(LockSpr,0,145,gui_y-532);
	//--> TW Tank 4
	if (obj_Manticore.TWTank[4,0] < dmgTankMax) draw_sprite(TWBar_Empty,0,171,gui_y-532);
	else draw_sprite(TWBar_Full,irandom(3),171,gui_y-532);
	if (global.TWs[global.equippedTW,3] < 3) draw_sprite(LockSpr,0,171,gui_y-532);
}

draw_sprite(tw_icon,0,52,gui_y-518);
draw_set_colour(c_black);
draw_text(80, gui_y-525, string(artist_weapon));
var ammoColor = ((ammo == noone)||(ammo > (MAXammo/2))) ? c_white : ((ammo > (MAXammo/3)) ? c_yellow : c_red);
draw_sprite_ext(spr_Times,0,38,gui_y-480,1,1,0,ammoColor,1);
if (ammo == noone) draw_sprite(spr_Infinite,0,60,gui_y-482);
else draw_text(46,gui_y-492,string(ammo));

//--> For Remote Explosive Weapon
if (tw_icon == spr_icon_RemoteExp)
{
	if instance_exists(obj_Throwing_Wpn)
	{
		switch(obj_Throwing_Wpn.sprite_index)
		{
			case spr_RemoteExp_low:	draw_text(80, gui_y-505, "Low Damage"); break;
			case spr_RemoteExp_mid:	draw_text(80, gui_y-505, "Mid Damage"); break;
			case spr_RemoteExp_high: draw_text(80, gui_y-505, "High Damage"); break;
		}
	}
}
draw_set_colour(c_white);
draw_text(82, gui_y-525, string(artist_weapon));
draw_set_colour(ammoColor);
if (ammo != noone) draw_text(48,gui_y-492,string(ammo));

//--> For Remote Explosive Weapon (Shadow)
if (tw_icon == spr_icon_RemoteExp)
{
	if instance_exists(obj_Throwing_Wpn)
	{
		switch(obj_Throwing_Wpn.sprite_index)
		{
			case spr_RemoteExp_low:	draw_set_color(c_lime);
									draw_text(82, gui_y-505, "Low Damage"); break;
			case spr_RemoteExp_mid:	draw_set_color(c_yellow);
									draw_text(82, gui_y-505, "Mid Damage"); break;
			case spr_RemoteExp_high: draw_set_color(c_red);
									 draw_text(82, gui_y-505, "High Damage"); break;
		}
	}
}

//--> Centers Font again
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_set_colour(c_white);

//--> SPECIAL ATTACK BARS
draw_sprite(spr_specialbar_empty,0,80,gui_y-640);
var lerp_sp1 = lerp(old_spbar1/100,sword_bar/100,1);
if (sword_bar == 100)
	draw_sprite_ext(spr_specialbar,1,84,gui_y-631,lerp_sp1,1,0,choose(c_red,c_yellow,c_orange,c_fuchsia),1);
else
	draw_sprite_ext(spr_specialbar,1,84,gui_y-631,lerp_sp1,1,0,sp1_colour,1);

draw_sprite(spr_specialbar_empty,0,80,gui_y-590);
var lerp_sp2 = lerp(old_spbar2/100,axe_bar/100,1);
if (axe_bar == 100)
	draw_sprite_ext(spr_specialbar,1,84,gui_y-581,lerp_sp2,1,0,choose(c_red,c_yellow,c_orange,c_fuchsia),1);
else
	draw_sprite_ext(spr_specialbar,1,84,gui_y-581,lerp_sp2,1,0,sp2_colour,1);

if (global.HuntRoom != noone)
{
	//--> ENEMY HEALTH BAR
	draw_sprite(spr_stonebar_inner,1,gui_x-70,gui_y-670);
	//--> "lerp_hp" smooths hp reduction visually
	var lerp_hp = lerp(obj_BossEnemy.old_hit_points/obj_BossEnemy.max_hit_points,obj_BossEnemy.hit_points/obj_BossEnemy.max_hit_points,0.2);
	if (obj_BossEnemy.hit_points > 0) //--> shows damage reduction
		draw_sprite_ext(spr_stonebar_bar,1,gui_x-70,gui_y-288,1,lerp_hp,0,c_white,1);
	draw_sprite(borderBar,1,gui_x-borderX,gui_y-borderY);

	//--> If Second Boss exists, creates the Health bar
	if (ArtBoss != ArtBoss2)
	{
		draw_sprite(spr_stonebar_inner,1,gui_x-150,gui_y-670);
		//--> "lerp_hp" smooths hp reduction visually
		var lerp_hp2 = lerp(ArtBoss2.old_hit_points/ArtBoss2.max_hit_points,ArtBoss2.hit_points/ArtBoss2.max_hit_points,0.2);
		if (ArtBoss2.hit_points > 0) //--> shows damage reduction
			draw_sprite_ext(spr_stonebar_bar,1,gui_x-150,gui_y-288,1,lerp_hp2,0,c_olive,1);
		draw_sprite(borderBar,1,gui_x-(borderX+80),gui_y-borderY);
	}
}

//--> Defeated Fade2Red
if (obj_Manticore.state == defeated_Manticore)
{
	draw_rectangle_color(0,0,gui_x,gui_y,c_red,c_red,c_red,c_red,0);
	draw_set_alpha(fade2red);
	fade2red += 0.05;
	fade2red = clamp(fade2red,0,1);
	room_speed = 15;
}
else
{
	fade2red = 0;
	draw_set_alpha(1);
}