/// @description Insert description here
// You can write your code in this editor
sprite_index = spr_AntaresOrb_destroy;
if (global.cam_follow == self) global.cam_follow = obj_Manticore;

repeat(25)
{
	var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
	glare.sprite_index = spr_GlowingBall;
	glare.image_blend = merge_color(c_white,c_red,0.8);
	glare.Xmovement = random_range(1,3) * choose(1,-1);
	glare.Ymovement = random_range(1,3) * choose(1,-1);
	glare.bright = true;
	glare.fadeSpd = choose (0.01,0.02);
}