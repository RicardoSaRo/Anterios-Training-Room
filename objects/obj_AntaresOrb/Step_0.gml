/// @description Insert description here
// You can write your code in this editor

if (!orb_switchvar)
{
	repeat(25)
	{
		var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
		glare.sprite_index = spr_starShine;
		glare.image_xscale = 3;
		glare.image_yscale = 3;
		glare.image_blend = merge_color(c_white,c_red,0.8);
		glare.Xmovement = random_range(1,3) * choose(1,-1);
		glare.Ymovement = random_range(1,3) * choose(1,-1);
		glare.bright = true;
		glare.fadeSpd = choose (0.01,0.02);
	}
	orb_switchvar = true;
}

if (alarm[0] mod 60 == 0)
{
	var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
	with (FX)
	{
		sprite_index = spr_firering_FX;
		image_blend = c_red;
		obj_2_follow = other;
		anim_end = 3;
		brightness = true;
		glow = true;
		g_amount = 20;
		image_alpha = 0.3;
		image_xscale = other.image_xscale + 3;
		image_yscale = random_range(0.5,other.image_yscale + 3);
		image_angle = random(360);
	}
	
	bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,208,2,24,70,1,c_red,1);
	bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,208,2,24,70,1,c_red,1);
	
	audio_play_sound(snd_AntaresOrb,18,0);
}


if (image_alpha < 1) image_alpha += 0.2;
image_angle = random(360);

if (image_xscale < 1.5)
{
	image_xscale += 0.1;
	image_yscale += 0.1;
}

if (sprite_index == spr_AntaresOrb)
{
	for (var i = 0; i < 5; i++)
	{
		var chispa = instance_create_layer(x,y,"FX_Layer",obj_chispa);
		with (chispa)
		{
			chispa_colour = c_red;
			x += random_range(-20,20);
			y += random_range(-20,20);
		}
	}
}
if (instance_exists(obj_V_lightning))
{
	sprite_index = spr_AntaresOrb_destroy;
	
	var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
	glare.sprite_index = spr_GlowingBall;
	glare.image_blend = merge_color(c_white,c_red,0.5);
	glare.Xmovement = random_range(1,3) * choose(1,-1);
	glare.Ymovement = random_range(1,3) * choose(1,-1);
	glare.bright = true;
	glare.fadeSpd = choose (0.01,0.02);
	
}

if (alarm[0] <= 320) global.cam_follow = obj_Manticore;

if (sprite_index == spr_AntaresOrb_destroy)
{
	image_speed = 0.2;
	manager.finishStage = true;
	if (image_index >= 4)
	{
		if (global.cam_follow == self) global.cam_follow = obj_Manticore;
		instance_destroy();
	}
}
else y -= choose (0.2,0.5);