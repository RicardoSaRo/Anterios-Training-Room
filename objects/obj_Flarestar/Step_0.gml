/// @description Insert description here
// You can write your code in this editor

//--> Movement Capture to move Flarestar during channeling
if ((obj_Manticore.sprite_index == spr_Dyrnwyn_sp_floor)or
(obj_Manticore.sprite_index == spr_Dyrnwyn_sp_air))
{
	fs_key_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
	fs_key_down = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);
	fs_key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	fs_key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);	

	var starMov = (global.SWORDS[2,3]/2) + 6;

	if (fs_key_up) y -= starMov;
	if (fs_key_down) y += starMov;
	if (fs_key_left) x -= starMov;
	if (fs_key_right) x += starMov;
	
	//--> Restarts Special Bar
	obj_Manticore.special_Dyrnwyn -= 2;
	obj_Manticore.special_Dyrnwyn = clamp(obj_Manticore.special_Dyrnwyn,0,100);
	obj_Manticore.alarm[9] = 60;
}

var RedFlame = instance_create_layer(x+random_range(-150,150),y+random_range(-150,150),"Back_FX_Layer",obj_centerXYfollow);
RedFlame.sprite_index = choose(spr_RedFlames1,spr_RedFlames2,spr_RedFlames3,spr_RedFlames4,spr_RedFlames5,spr_RedFlames6);
RedFlame.anim_end = 3;
RedFlame.image_blend = c_yellow;
RedFlame.image_xscale = choose(-1,1);
RedFlame.image_alpha = random_range(0.2,0.6);
RedFlame.glow = choose(true,false);
RedFlame.brightness = choose(true,false);
var smoke = instance_create_layer(x+random_range(-150,150),y+random_range(-150,150),"Back_FX_Layer",obj_centerXYfollow);
smoke.sprite_index = spr_poof;
smoke.anim_end = 4;
smoke.image_blend = c_maroon;
smoke.image_xscale = 4;
smoke.image_yscale = 4;
smoke.image_alpha = random_range(0.2,0.6);
smoke.glow = choose(true,false);
smoke.brightness = choose(true,false);

image_speed = 1;
//--> CREATE ANIMATION
if (image_index > 7) && (sprite_index == spr_Flarestar_create)
	sprite_index = spr_Flarestar;

//--> NORMAL SPRITE ANIMATION
if (sprite_index == spr_Flarestar)
{
	sprite_index = spr_Flarestar
	fs_img_angl += 1;
	if (fs_img_angl > 360) fs_img_angl = 0;
	image_angle = fs_img_angl;
	
	if instance_exists(obj_BossEnemy)//--> Hommes Nearest Enemy
	{
		var closestTarget = instance_nearest(x,y,obj_BossEnemy);
		if (closestTarget.x > x) x += fs_chase_spd;
		else x -= fs_chase_spd;
		if (closestTarget.y > y) y += fs_chase_spd;
		else y -= fs_chase_spd;
	}
	
	//--> EXPLOSION ANIMATION AFTER ALARM
	if (alarm[0] <= 1)
	{
		audio_stop_sound(snd_FlareStar);
		audio_play_sound(snd_FlareStar_destroy,18,0);
		sprite_index = spr_Flarestar_exp;
		image_index = 0;
	}
}

 //--> DAMAGES ENEMY
dmg_calc(fs_hit_used,sprite_index);

//--> EXPLOSION FX
if (sprite_index == spr_Flarestar_exp)
{
	fs_hit_used = true;
	
	if (sign(image_xscale) < 0)
	{
		image_xscale = 1;
		image_yscale = 1;
	}
		
	fs_increm *= 0.1;
	image_xscale += fs_increm;
	image_yscale += fs_increm;
}

//--> DESTROYS AFTER EXPLOSION
if (sprite_index == spr_Flarestar_exp) && (image_index > 7) instance_destroy();