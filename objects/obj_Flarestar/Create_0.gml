/// @description Insert description here
// You can write your code in this editor
sprite_index = spr_Flarestar_create;

fs_chase_spd = 0.01;

fs_alarm_countdown = (global.SWORDS[2,3]*20)+(obj_Manticore.current_sp_sword * 6);

alarm[0] = fs_alarm_countdown;

fs_img_angl = 0;

fs_increm = obj_Manticore.image_index;

fs_hit_used = false;

fs_chase_spd = (global.SWORDS[2,3]/3) + 0.5;

if (obj_Manticore.current_sp_sword == 100)
{
	fs_alarm_countdown += 100;
	fs_chase_spd += 0.5;
}

audio_play_sound(snd_FlareStar,20,0);