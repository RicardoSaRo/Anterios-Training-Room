/// @description Insert description here
// You can write your code in this editor
//Fades image and destroy when invisible

image_speed = imagespeed;

if (!SIswitch_var)
{
	if (global.RELIKS[27,2]) choose(c_yellow,c_red,c_aqua,c_lime,c_fuchsia,c_teal,c_orange);
	
	if (sprite_index != spr_ShadowHitbox)
	{
		var SIHB = instance_create_layer(x,y,"Back_FX_Layer",obj_ShadowImage);
		SIHB.sprite_index = spr_ShadowHitbox;
		SIHB.image_xscale = image_xscale;
		SIHB.visible = false;
	}
	
	SIswitch_var = true;
}

if (sprite_index == spr_ShadowHitbox)
{
	if (place_meeting(x,y,obj_BossEnemy)) && (!SIhit_used)
	{
		dmg_calc(SIhit_used,sprite_index);
		SIhit_used = true;
	}

	if (place_meeting(x,y,obj_EnemyBullet)) && (!SIblockbullet)
	{
		var bullet = instance_place(x,y,obj_EnemyBullet);
		if (bullet.sprite_index != bullet.destroy_sprite) && (bullet.priority != 10)
		{
			bullet.sprite_index = bullet.destroy_sprite;
			bullet.image_index = 0;
		}
	
		SIblockbullet = true;
	}
}

if image_alpha > 0 image_alpha -= 0.02;
else instance_destroy();