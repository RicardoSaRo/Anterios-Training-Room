/// @description Insert description here
// You can write your code in this editor

image_xscale = 0.01;
image_yscale = 0.01;

max_size = 3 + (global.TWs[10,3] / 2);

tw_hit_used = false;

owner = obj_Throwing_Wpn;

image_angle = random(360);

alarm[0] = 10;

priority = 4 + global.TWs[10,3];

audio_play_sound(snd_ThunderWave,14,0);