/// @description Insert description here
// You can write your code in this editor
if instance_exists(owner)
{
	if (owner.sprite_index == spr_Elemental_Orb_ON)
	{
		x = owner.x;
		y = owner.y;
	}
}

image_xscale += 0.1;
image_yscale += 0.1;

if (image_xscale > max_size) image_alpha -= 0.1;

if (alarm[0] <= 1) dmg_calc(tw_hit_used,sprite_index);

//--> TW COLLISION
if (place_meeting(x,y,obj_EnemyBullet))
{
	var bpoof = instance_nearest(x,y,obj_EnemyBullet);
	
	if (bpoof.priority != 10) if (bpoof.priority <= priority) bpoof.sprite_index = bpoof.destroy_sprite;
}

if (image_alpha <= 0)
{
	audio_stop_sound(snd_ThunderWave);
	instance_destroy();
}

