/// @description Insert description here
// You can write your code in this editor
var TbotMenu_exists = instance_exists(obj_TBot_Menu);

if (TbotMenu_exists)
{
	if (obj_TBot_Menu.closing_menu) menuBlkBkgnd = obj_TBot_Menu.image_xscale/1.1;
	else menuBlkBkgnd = 1/1.1;
	
	draw_set_alpha(menuBlkBkgnd);
	draw_rectangle_color(global.view_x,global.view_y,global.view_x+global.vsizeW,global.view_y+global.vsizeH,c_black,c_black,c_black,c_black,0);
	draw_set_alpha(1);
}
else menuBlkBkgnd = 0;