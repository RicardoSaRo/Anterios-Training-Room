/// @description Insert description here
// You can write your code in this editor
var startBtn = gamepad_button_check_pressed(global.controller_type,gp_start);
var selectBtn = gamepad_button_check_pressed(global.controller_type,gp_start);

if (!instance_exists(obj_MainMenu)) && (!instance_exists(obj_WeaponsMenu))
{
	if (selectBtn) && (!startBtn)
	{
		instance_deactivate_all(self);
		instance_create_layer(global.view_x + (display_get_width()/2),global.view_y + (display_get_height()/2),"Menus_Layer",obj_MainMenu);
	}

	if (startBtn) && (!selectBtn)
	{
		instance_deactivate_all(self);
		instance_create_layer(global.view_x + (display_get_width()/2),global.view_y + (display_get_height()/2),"Menus_Layer",obj_WeaponsMenu);
	}
}

/*
if (gamepad_button_check_pressed(global.controller_type,gp_start))
{
	if (!instance_exists(obj_WeaponsMenu))
	{
		instance_deactivate_all(self);
		instance_create_layer(global.view_x + (display_get_width()/2),global.view_y + (display_get_height()/2),"Menus_Layer",obj_WeaponsMenu)
	}
}