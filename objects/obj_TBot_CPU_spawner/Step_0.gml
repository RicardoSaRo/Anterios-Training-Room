/// @description Insert description here
// You can write your code in this editor
var enterKey = keyboard_check_pressed(vk_enter);
var escKey = keyboard_check_pressed(vk_escape);
var upKey = keyboard_check_pressed(vk_up);

if (!instance_exists(obj_MainMenu)) && (!instance_exists(obj_WeaponsMenu)) && (!instance_exists(obj_TBot_Menu))
{
	if (TRmngr.cpuTrigger)
	{
		if (upKey) && (!escKey) && (!enterKey)
		{
			audio_pause_all();
			if (global.currentManager != noone) //--> Sound volumen change
			{
				var rm_mngr = global.currentManager;
				var msc_gain = audio_sound_get_gain(rm_mngr.levelMusic);
				audio_sound_gain(rm_mngr.levelMusic,msc_gain/2,0);
				audio_resume_sound(rm_mngr.levelMusic);
				audio_play_sound(snd_menuOpen,7,0);
			}
			bounce = obj_BossEnemy.bounce;
			AtkTurn = obj_BossEnemy.AtkTurn;
			right_var = obj_BossEnemy.right_var;
			left_var = obj_BossEnemy.left_var;
			instance_deactivate_all(self);
			instance_create_layer(global.view_x + (display_get_width()/2),global.view_y + (display_get_height()/2),"Menus_Layer",obj_TBot_Menu);
		}
	}
}