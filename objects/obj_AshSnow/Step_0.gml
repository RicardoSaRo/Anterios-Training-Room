/// @description Insert description here
// You can write your code in this editor
if (!beingAbsorbed)
{
	if (isAsh) y += random_range(-10,-1);
	else
	{
		y += snowYSpd;
		speed += 0.1;
		speed = clamp(speed,0,5);
	}
}
else
{
	if (instance_exists(absorb_obj)) direction = point_direction(x,y,absorb_obj.x,absorb_obj.y);
	else instance_destroy();
	speed = 10;
}

if ((x <= 0)or(x >= room_width)) or ((y <= 0)or(y >= room_height)) instance_destroy();