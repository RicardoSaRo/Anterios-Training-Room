/// @description Insert description here
// You can write your code in this editor
with (obj_BossEnemy) special_var = false;

if (sprite_index == spr_StrAtk_Korunax1) || (sprite_index == spr_StrAtk_Korunax2) || (sprite_index == spr_StrAtk_KorunaxFrostbiter) &&
   (active_dmg)
{
	//--> Creates Debris
	repeat(25)
	{
		var rocks = instance_create_layer(x+(150*image_xscale)+random_range(-64,64),y+random_range(-120,64),"FX_Layer",obj_rockice_FX);
		rocks.rockice_vsp = irandom_range(-5,-10);
		rocks.rockice_hsp = irandom_range(-10,10);
		if (sprite_index == spr_StrAtk_Korunax1) rocks.sprite_index = spr_rocks_FX;
		else
		{
			if (sprite_index == spr_StrAtk_Korunax2) rocks.sprite_index = spr_rocks_FX_G;
			else if (sprite_index == spr_StrAtk_KorunaxFrostbiter) rocks.sprite_index = spr_icys_FX;
		}
	}
	if (sprite_index == spr_StrAtk_KorunaxFrostbiter) audio_play_sound(snd_FrostBulwark_destroy,12,0);
}

if (sprite_index == spr_StrAtk_Mjolnir1) || (sprite_index == spr_StrAtk_Mjolnir2) && (active_dmg)
{
	repeat(50)
	{
		var chispa = instance_create_layer(x+(150*sign(image_xscale))+random_range(-150,150),y+random_range(-350,50),"FX_Layer",obj_chispa); //--> Spark FX
		with (chispa)
		{
			chispa_colour = (global.RELIKS[40,2]) ? choose(c_white,c_gray) : c_white;
			image_xscale = 1.5;
			image_yscale = 1.5;
			chispaX = random_range(-5,5);
			chispaY = random_range(-5,5);
		}
	}
}

if (sprite_index == spr_StrAtk_WhispererDyrnwyn)
{
	
}

if (sprite_index == spr_StrAtk_Whisperer1) || (sprite_index == spr_StrAtk_Whisperer2) || (sprite_index == spr_StrAtk_WhispererDyrnwyn)
{
	var WD = (sprite_index == spr_StrAtk_WhispererDyrnwyn) ? true : false;
	if (WD)
	{
		repeat(30)
		{
			var chispitas = instance_create_layer(x+(150*image_xscale)+random_range(-150,150),y+random_range(-350,50),"Back_FX_Layer",obj_chispa);
			var DSize = choose(1,2,3);
			chispitas.image_xscale = DSize;
			chispitas.image_yscale = DSize;
		}
	}
	
	for(var i=1; i<=3; i++)
	{
		var ww = instance_create_layer(x+(150*image_xscale),y-200,"Info_Layer",obj_TWSpecial);
		ww.sprite_index = spr_Whirlwind;
		ww.Consum = false;
		ww.image_blend = (WD) ? c_orange : ((global.RELIKS[40,2]) ? make_color_rgb(74,109,168) : c_white);
		ww.image_xscale = image_xscale;
		ww.angle = i * 120;
		ww.instnc = self;
		ww.alarm[0] = 40;
		ww.alarm[1] = 20;
		ww.alarm[2] = 3;
	}
}

global.camera_shake = false;