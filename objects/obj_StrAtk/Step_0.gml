/// @description Insert description here
// You can write your code in this editor

if (obj_Manticore.sprite_index != spr_AxeStrongAtk)
{
	if (image_index >= 2) active_dmg = true;
}

if ((global.RELIKS[7,2])&&((sprite_index==spr_StrAtk_Korunax1)||(sprite_index==spr_StrAtk_Korunax2)||(sprite_index==spr_StrAtk_KorunaxFrostbiter))) ||
   ((global.RELIKS[8,2])&&((sprite_index==spr_StrAtk_Whisperer1)||(sprite_index==spr_StrAtk_Whisperer2)||(sprite_index==spr_StrAtk_WhispererDyrnwyn))) ||
   ((global.RELIKS[14,2])&&((sprite_index==spr_StrAtk_Mjolnir1)||(sprite_index==spr_StrAtk_Mjolnir2)))||
   ((global.RELIKS[15,2])&&((sprite_index==spr_StrAtk_Mirage1)||(sprite_index==spr_StrAtk_Mirage2)))
{
	with (obj_Manticore)
	{
		if !(alarm[11] > 3)
		{
			alarm[11] = 3;
			image_blend = c_yellow;
		}
	}
}

if (active_dmg)
{
	if (sprite_index == spr_StrAtk_Whisperer1)||
	   (sprite_index == spr_StrAtk_Whisperer2)||(sprite_index == spr_StrAtk_WhispererDyrnwyn)
	{		
		if (global.RELIKS[16,2])
		{
			var wndjmpFX = instance_create_layer(x+(150*image_xscale),y-(random_range(50,200)),"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
			wndjmpFX.sprite_index = spr_windjmp_fx;
			wndjmpFX.image_blend = (sprite_index == spr_StrAtk_WhispererDyrnwyn) ? c_orange : c_white;
			wndjmpFX.yMov = -10;
		}
		if (!audio_is_playing(snd_WhispererStrong)) //--> Sound FX
		{
			audio_sound_pitch(snd_WhispererStrong,random_range(1,1.1))
			audio_play_sound(snd_WhispererStrong,12,0);
		}
	
		if (sprite_index == spr_StrAtk_WhispererDyrnwyn)
		{
			var chispitas = instance_create_layer(x+(150*image_xscale)+random_range(-150,150),y+random_range(-350,50),"Back_FX_Layer",obj_chispa);
			var DSize = choose(1,2,3);
			chispitas.image_xscale = DSize;
			chispitas.image_yscale = DSize;
			
			if (!audio_is_playing(snd_DyrnwynStrong)) //--> Sound FX
			{
				audio_sound_pitch(snd_DyrnwynStrong,random_range(1,1.1))
				audio_play_sound(snd_DyrnwynStrong,12,0);
			}
		}
	}
	
	if (sprite_index == spr_StrAtk_Korunax1) || (sprite_index == spr_StrAtk_Korunax2) || (sprite_index == spr_StrAtk_KorunaxFrostbiter)
	{
		if (place_meeting(x,y,obj_BossEnemy))
		{
			var Special_var = instance_place(x,y,obj_BossEnemy);
			if (!Special_var.special_var)
			{
				dmg_calc(StrAtk_hit_used,sprite_index);
				
				if (sprite_index == spr_StrAtk_KorunaxFrostbiter) //--> Slows if enemy is on Standby
				{
					if (!audio_is_playing(snd_freeze4)) //--> Sound FX
					{
						audio_play_sound(snd_freeze4,12,0);
					}
					
					var slowAmount = 90;
					var Slow = frostbiterSlow(Special_var,slowAmount);
				
					if (Slow)
					{
						var hit_y = clamp(y,bbox_top,bbox_bottom);
						var hit_x = clamp(x,bbox_left,bbox_right);
						repeat(6)
						{
							var slowFX = instance_create_layer(hit_x,hit_y,"FX_Layer",obj_GrassLeaf);
							slowFX.sprite_index = spr_snowflake;
							slowFX.xmov = irandom_range(-5,5);
							slowFX.rotatin = 1;
							slowFX.image_index = irandom(5);
							slowFX.image_alpha = choose(0.7,1);
							slowFX.alarm[0] = choose(30,40);
							slowFX.image_speed = 0;
							slowFX.image_angle = random(360);
							slowFX.image_xscale = random_range(1,2) * choose(-1,1);
							slowFX.image_yscale = random_range(1,2);
						}
					}
				}
			
				Special_var.special_var = true;
			}
		}
		
		if (!audio_is_playing(snd_KorunaxStrong)) //--> Sound FX
		{
			audio_sound_pitch(snd_KorunaxStrong,random_range(1,1.1))
			audio_play_sound(snd_KorunaxStrong,12,0);
		}
		
		var rocks = instance_create_layer(x+(100*image_xscale)+random_range(-64,64),y+random_range(-64,64),"FX_Layer",obj_rockice_FX);
		rocks.rockice_vsp = irandom_range(-5,-10);
		rocks.rockice_hsp = irandom_range(-10,10);
		if (sprite_index == spr_StrAtk_Korunax1) rocks.sprite_index = spr_rocks_FX;
		else
		{
			if (sprite_index == spr_StrAtk_Korunax2) rocks.sprite_index = spr_rocks_FX_G;
			else if (sprite_index == spr_StrAtk_KorunaxFrostbiter) rocks.sprite_index = spr_icys_FX;
		}
	}
	
	if (sprite_index == spr_StrAtk_Mirage1) || (sprite_index == spr_StrAtk_Mirage2)
	{
		image_blend = choose(c_white,c_green);
		
		if (place_meeting(x,y,obj_BossEnemy))
		{
			var Special_var = instance_place(x,y,obj_BossEnemy);
			if (!Special_var.special_var)
			{
				dmg_calc(StrAtk_hit_used,sprite_index);
				Special_var.special_var = true;
			}
		}
		
		if (!audio_is_playing(snd_MirageStrong)) //--> Sound FX
		{
			audio_sound_pitch(snd_MirageStrong,random_range(1,1.1))
			audio_play_sound(snd_MirageStrong,12,0);
		}
		if (!audio_is_playing(snd_DyrnwynStrong))
		{
			audio_sound_pitch(snd_DyrnwynStrong,random_range(1,1.1))
			audio_play_sound(snd_DyrnwynStrong,12,0);
		}
		
		var flamespawn = irandom(5 - global.AXES[2,3]);
		if (flamespawn == 1)
		{
			if (image_index > 4) && (image_index < 6)
			{
				var wow = instance_create_layer(x+(150*image_xscale)+random_range(-50,50),y+random_range(-350,50),"FX_Layer",obj_willowisp);
				if (global.RELIKS[40,2]) wow.image_blend = c_purple;
			}
		}
	}
		
	if (sprite_index == spr_StrAtk_Mjolnir1) || (sprite_index == spr_StrAtk_Mjolnir2)
	{
		var glare = instance_create_layer(x+(150*image_xscale)+random_range(-50,100),y+random_range(-250,-50),"FX_Layer",obj_fadeAway);
		glare.sprite_index = choose(spr_spark,spr_starShine);
		var sz = choose(0.5,1);
		glare.image_xscale = sz;
		glare.image_yscale = sz;
		glare.image_blend = (global.RELIKS[40,2]) ? c_black : c_white;
		glare.Xmovement = random_range(0,3) * choose(1,-1);
		glare.Ymovement = random_range(0,3) * choose(1,-1);
		glare.bright = true;
		glare.fadeSpd = 0.02;
		if (start_anim) image_xscale += 0.01 * sign(image_xscale);
		
		if (!audio_is_playing(snd_MjolnirStrong)) //--> Sound FX
		{
			audio_sound_pitch(snd_MjolnirStrong,random_range(1,1.1))
			audio_play_sound(snd_MjolnirStrong,12,0);
		}
	}
	
	global.camera_shake = true;
}

if (start_anim) image_speed = 1;

if (image_index > 2) && (image_index < 3)
{
	if (!audio_is_playing(snd_MaceStrong))
	{
		audio_sound_pitch(snd_MaceStrong,random_range(1,1.1));
		audio_play_sound(snd_MaceStrong,12,0);
	}
}