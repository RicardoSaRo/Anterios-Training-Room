/// @description Insert description here
if (image_index > 2) instance_destroy();

if ((hit_FX != spr_burn_red) or (hit_FX != spr_burn_green))
{
	y -= 5;
	image_alpha = random_range(0.8,1);
	image_xscale = choose(-1,1);
}

if (sprite_index == spr_spear_hit) image_yscale = choose(-1,1);

x += extraXmov;
y += extraYmov;

image_xscale -= shrinkX;
image_yscale -= shrinkY;

image_angle += extraAngl;