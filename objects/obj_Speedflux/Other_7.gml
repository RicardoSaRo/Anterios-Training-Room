/// @description Insert description here
// You can write your code in this editor

image_speed = choose(1,1.5,2);
image_angle = choose (0,45,90,135,180,225,275,315);
image_xscale = choose(-1,1);
image_yscale = choose(-1,1);
image_alpha = choose(0.5,0.5,0.6,0.6,0.7,0.8);