/// @description Insert description here
// You can write your code in this editor

if (obj_BossEnemy.enemy_name == "GolemB")or(obj_BossEnemy.enemy_name == "GolemG")
	if (obj_BossEnemy.image_index >= 3) instance_destroy();

if (place_meeting(x,y,obj_Manticore)) and (obj_Manticore.alarm[11] <= 0)
{
	if  (!EH_hit_used) //--> Hitboxes that can be parried
	{
		if (obj_Manticore.alarm[10] <= 0)
		{
			with (obj_Manticore)
			{
				Manticore_hp -= 1;
				Manticore_hp = clamp(Manticore_hp, 0, global.player_max_health);
				state = damage_Manticore;
				alarm[10] = 10;
			}
		}
	
		if (obj_BossEnemy.enemy_name == "Wastipede")
		{
			artist.poisonFX = true;
			if (instance_exists(obj_WastipedeRoom_manager))
			{
				with (obj_WastipedeRoom_manager)
				{
					ManticorePoisoned = true;
					switch(global.difficulty)
					{
						case "casual":	alarm[2] = 240; break;
						case "normal":	alarm[2] = 300; break;
						case "hard":	alarm[2] = 360; break;
						case "lunatic":	alarm[2] = 420; break;
					}
				}
			}
		}
	
		if (sprite_index == spr_PhoeniskTail_htbx)
		{
			if (global.BossVariant == 0) bulletCollidesPlayer(1,1,global.currentManager,0,1,0,0);
			else bulletCollidesPlayer(1,2,global.currentManager,1,1,0,0);
		
			if (obj_Manticore.state != stun_Manticore)
			{
				with (obj_Manticore)
				{
					//global.player_max_health = Manticore_hp;
					alarm[6] = 30;
					state = stun_Manticore;
					vsp = -15;
					hsp = 15 * sign(other.image_xscale);
					y--;
				}
			}
		
			EH_hit_used = true;
		}
	}
	
	//--> This Hitboxes CANT be parried
	if (sprite_index == spr_PhoeniskSwd1_htbx1) || (sprite_index == spr_PhoeniskSwd3_htbx)
	{
		if (global.BossVariant == 0) bulletCollidesPlayer(2,2,global.currentManager,0,1,0,0);
			else bulletCollidesPlayer(2,4,global.currentManager,1,1,0,0);
		
			if (obj_Manticore.state != stun_Manticore)
			{
				with (obj_Manticore)
				{
					//global.player_max_health = Manticore_hp;
					alarm[6] = 20;
					state = stun_Manticore;
					hsp = 15 * sign(other.image_xscale);
					y--;
				}
			}
	}
	
	if (sprite_index == spr_PhoeniskSwd1_htbx2)
	{
		if (global.BossVariant == 0) bulletCollidesPlayer(2,2,global.currentManager,0,1,0,0);
			else bulletCollidesPlayer(2,4,global.currentManager,1,1,0,0);
		
			if (obj_Manticore.state != stun_Manticore)
			{
				with (obj_Manticore)
				{
					//global.player_max_health = Manticore_hp;
					alarm[6] = 30;
					state = stun_Manticore;
					vsp = -15;
					hsp = 15 * sign(other.image_xscale);
					y--;
				}
			}
	}
	
	if (sprite_index == spr_PhoeniskSwd4)
	{
		if (global.BossVariant == 0) bulletCollidesPlayer(0,0,global.currentManager,0,1,0,0);
			else bulletCollidesPlayer(0,0,global.currentManager,1,1,0,0);
		
			if (obj_Manticore.state != stun_Manticore)
			{
				with (obj_Manticore)
				{
					//global.player_max_health = Manticore_hp;
					alarm[6] = 40;
					state = stun_Manticore;
					vsp = -20;
					hsp = 20 * sign(other.image_xscale);
					y--;
				}
			}
	}
}

	
