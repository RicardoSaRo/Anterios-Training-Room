/// @description Insert description here
// You can write your code in this editor
draw_self();

if (shieldAmount > 0)
{
	switch(obj_Manticore.equipped_axe)
	{
		case 1: shieldColor = c_yellow; break;
		case 2: shieldColor = c_fuchsia; break;
		case 3: shieldColor = c_aqua; break;
		case 4: shieldColor = c_lime; break;
	}
	
	var shieldColor2 = choose(shieldColor,c_white);
	
	switch(shieldAmount)
	{
		case 2: draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1,1,0,shieldColor2,1); break;
		case 3: draw_sprite_ext(spr_cDEF_Shield,0,x+16,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x-16,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x+16,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x-16,y-50,1,1,0,shieldColor2,1);break;
		case 4: draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1,1,0,shieldColor2,1); break;
		case 5: draw_sprite_ext(spr_cDEF_Shield,0,x,y-80,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-80,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1,1,0,shieldColor2,1); break;
		case 6: draw_sprite_ext(spr_cDEF_Shield,0,x+16,y-80,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x-16,y-80,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1.5,1.5,0,shieldColor,0.5);
				draw_sprite_ext(spr_cDEF_Shield,0,x+16,y-80,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x-16,y-80,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x+30,y-50,1,1,0,shieldColor2,1);
				draw_sprite_ext(spr_cDEF_Shield,0,x-30,y-50,1,1,0,shieldColor2,1); break;
	}
}

if (place_meeting(x,y,obj_EnemyBullet))
{
	repeat(6)
	{
		draw_sprite_ext(hitFX,anim_count,x+random_range(-40,40),y+random_range(-40,40),1,1,anglFX,c_white,1);
	}
}