/// @description Insert description here
// You can write your code in this editor

obj_Manticore.alarm[8] = freezeAxeSP;
obj_Manticore.alarm[9] = freezeSwrdSP;

if (shieldAmount <= 0)
{
	with (obj_Manticore)
	{
		Manticore_hp -= 1;
		Manticore_hp = clamp(Manticore_hp, 0, global.player_max_health);
		state = damage_Manticore;
		alarm[10] = 10;
	}
	
	instance_destroy();
}

switch(obj_Manticore.equipped_axe)
{
	case 1: hitFX = spr_rocks_FX;
			anim_count = 4;
			anglFX = random(360); break;
	case 2:	hitFX = spr_burn_green;
			anim_count = 2; 
			anglFX = 0; break;
	case 3: hitFX = spr_spark;
			anim_count = 2;
			anglFX = random(360); break;
	case 4: hitFX = spr_GrassLeaf;
			anim_count = 2;
			anglFX = random(360); break;
}

if (obj_Manticore.state != crouchDEF_Manticore) instance_destroy();

image_xscale = obj_Manticore.image_xscale;

if (place_meeting(x,y,obj_EnemyBullet))
{
	var bpoof = instance_nearest(x,y,obj_EnemyBullet);
	
	if (bpoof.priority != 10)
	{
		if (!bpoof.cDefClank)
		{
			shieldAmount -= 1;
			
			with(obj_Manticore)
			{
				switch(equipped_axe)
				{
					case 1:	special_Korunax -= other.SPdrain;
							special_Korunax = clamp(special_Korunax,0,100); break;
					case 2: special_Mirage -= other.SPdrain;
							special_Mirage = clamp(special_Mirage,0,100); break;
					case 3: special_Mjolnir -= other.SPdrain;
							special_Mjolnir = clamp(special_Mjolnir,0,100); break;
					case 4: special_Whisperer -= other.SPdrain;
							special_Whisperer = clamp(special_Whisperer,0,100); break;
				}
			}
			
			with (instance_create_layer(x+50,y,"FX_Layer",obj_show_damage))
			{
				switch(obj_Manticore.equipped_axe)
				{
					case 1: font_color = make_color_rgb(211,151,65); break;
					case 2: font_color = c_fuchsia; break;
					case 3: font_color = make_color_rgb(39,100,205); break;
					case 4: font_color = c_lime; break;
				}
				damage_display = other.SPdrain;
				plus_or_minus = "-";
				y_speed = 1;
			}
		}
		bpoof.sprite_index = bpoof.destroy_sprite;
		bpoof.cDefClank = true;
	}
}