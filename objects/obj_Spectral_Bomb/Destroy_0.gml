/// @description Insert description here
// You can write your code in this editor

x -= 15;
y -= 30;

audio_play_sound(snd_SpectralBomb2,20,0);

for(var i = 1; i <= 24; i++)
{
	var wow_sp = instance_create_layer(x,y,choose("Info_Layer","Back_FX_Layer"),obj_willowisp_sp)
	
	with (wow_sp)
	{
		startX = x;
		startY = y;
		chase_speed = round(600/other.mirage_bar_amount);
		image_xscale = 2;
		image_yscale = 2;
		if (global.RELIKS[40,2]) image_blend = c_purple;
	}
	
	switch(i)
	{
		case 1: wow_sp.goalX = x;
				wow_sp.goalY = y - 60;
				break;
		case 2: wow_sp.goalX = x + 30;
				wow_sp.goalY = y - 30;
				break;
		case 3: wow_sp.goalX = x + 60;
				wow_sp.goalY = y;
				break;
		case 4: wow_sp.goalX = x + 30;
				wow_sp.goalY = y + 30;
				break;
		case 5: wow_sp.goalX = x;
				wow_sp.goalY = y + 60;
				break;
		case 6: wow_sp.goalX = x - 30;
				wow_sp.goalY = y + 30;
				break;
		case 7: wow_sp.goalX = x - 60;
				wow_sp.goalY = y;
				break;
		case 8: wow_sp.goalX = x - 30;
				wow_sp.goalY = y - 30;
				break;
		case 9: wow_sp.goalX = x;
				wow_sp.goalY = y - 120;
				break;
		case 10: wow_sp.goalX = x + 70;
				 wow_sp.goalY = y - 70;
				 break;
		case 11: wow_sp.goalX = x + 120;
				 wow_sp.goalY = y;
				 break;
		case 12: wow_sp.goalX = x + 70;
				 wow_sp.goalY = y + 70;
				 break;
		case 13: wow_sp.goalX = x;
				 wow_sp.goalY = y + 120;
				 if (other.mirage_bar_amount > 60) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 14: wow_sp.goalX = x - 70;
				 wow_sp.goalY = y + 70;
				 if (other.mirage_bar_amount > 60) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 15: wow_sp.goalX = x - 120;
				 wow_sp.goalY = y;
				 if (other.mirage_bar_amount > 70) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 16: wow_sp.goalX = x - 70;
				 wow_sp.goalY = y - 70;
				 if (other.mirage_bar_amount > 70) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 17: wow_sp.goalX = x;
				 wow_sp.goalY = y - 150;
				 if (other.mirage_bar_amount > 80) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 18: wow_sp.goalX = x + 110;
				 wow_sp.goalY = y - 110;
				 if (other.mirage_bar_amount > 80) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 19: wow_sp.goalX = x + 150;
				 wow_sp.goalY = y;
				 if (other.mirage_bar_amount > 90) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 20: wow_sp.goalX = x + 110;
				 wow_sp.goalY = y + 110;
				 if (other.mirage_bar_amount > 90) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 21: wow_sp.goalX = x;
				 wow_sp.goalY = y + 150;
				 if (other.mirage_bar_amount == 100) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 22: wow_sp.goalX = x - 110;
				 wow_sp.goalY = y + 110;
				 if (other.mirage_bar_amount == 100) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 23: wow_sp.goalX = x - 150;
				 wow_sp.goalY = y;
				 if (other.mirage_bar_amount == 100) wow_sp.chasing_target = obj_BossEnemy;
				 break;
		case 24: wow_sp.goalX = x - 110;
				 wow_sp.goalY = y - 110;
				 if (other.mirage_bar_amount == 100) wow_sp.chasing_target = obj_BossEnemy;
				 break;
	}
}