/// @description Insert description here
// You can write your code in this editor

obj_Manticore.special_Mirage -= 4;
obj_Manticore.special_Mirage = clamp(obj_Manticore.special_Mirage,0,100);
obj_Manticore.alarm[9] = 60;

dmg_calc(wowBomb_hit_used,spr_wow_bomb);

if (maxheight mod 2 == 0) or (y <= maxheight)
{
	var afterimage = instance_create_layer(choose(x+9,x-9),choose(y+9,y-9),"Back_FX_Layer",dash_afterimage);
	afterimage.sprite_index = sprite_index;
	afterimage.image_index = image_index;
	afterimage.image_xscale = image_xscale;

	var Xflame = random_range(25,70);
	var Yflame = random_range(25,70);
	var xoff = x - 15;
	var yoff = y - 30;
	var FXflame = instance_create_layer(choose(xoff+Xflame,xoff-Xflame),choose(yoff+Yflame,yoff-Yflame),"FX_Layer",dash_afterimage);
	FXflame.sprite_index = spr_willowisp;
	FXflame.image_alpha = 0.5;
	FXflame.image_blend = choose(c_white,c_aqua);
	FXflame.image_index = choose(0,1,2,3,4,5);
}

var actualX = x;
x += random_range(-15,15);
y += random_range(-2,-1);

image_angle = irandom_range(0,360);

if (y <= maxheight)
{
	image_xscale -= 0.05;
	image_yscale -= 0.05;
	x = actualX;
	y = maxheight;
//	image_xscale = clamp(image_xscale,0,1);
//	image_yscale = clamp(image_yscale,0,1);
}

if (image_xscale <= 0) or (image_yscale <= 0) instance_destroy();