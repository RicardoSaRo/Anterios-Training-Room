/// @description Insert description here
// You can write your code in this editor

sprite_index = spr_wow_bomb;

wowBomb_hit_used = false;

maxheight = y - 60;

mirage_bar_amount = obj_Manticore.current_sp_axe;

audio_play_sound(snd_SpectralBomb1,20,0);

if (global.RELIKS[40,2]) image_blend = c_purple;
else image_blend = c_green;