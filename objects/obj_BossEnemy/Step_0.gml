/// @description Insert description here

/*switch(sprite_index)
{
	case spr_GGolem_crumbles: case spr_BGolem_crumbles: case spr_WWBrown_Defeat:
	case spr_WWGray_Defeat: case spr_WWPink_Defeat: case spr_WWFoxy_Defeat:
	case spr_WWGold_Defeat: case spr_OCrab_Defeated: case spr_Dragonkin_Defeated:
	case spr_Wastipede_Defeated: /*case spr_Rose_DefeatREAL: case spr_Rose_DefeatFAKE:
	case spr_Orchid_DefeatREAL: case spr_Orchid_DefeatFAKE: case spr_Aster_DefeatREAL:
	case spr_Aster_DefeatFAKE: case spr_Ixia_DefeatREAL: case spr_Ixia_DefeatFAKE:
	case spr_Dahlia_DefeatREAL: case spr_Dahlia_DefeatFAKE: case spr_Lotus_DefeatREAL:
	case spr_Lotus_DefeatFAKE:
		defeated_spr = sprite_index; break;
}*/

//-->> Damages Player with collision and if alarm[11] (invincibility) if off
//-->> Prevents damage to player if enemy have some specific names
if (defeated_spr != sprite_index) && (enemy_name != "DragonLordF") && (enemy_name != "DragonLordI") && 
   (enemy_name != "normalVine") && (enemy_name != "VenomLordB") && (enemy_name != "VenomLordP") && (enemy_name != "rainbowSphere")   
{
	if (place_meeting(x,y,obj_Manticore)) and (obj_Manticore.alarm[11] <= 0)
	{
		if (obj_Manticore.alarm[10] <= 0)
		{
			with (obj_Manticore)
			{
				var damage = (global.RELIKS[44,2]) ? choose(1,1,0) : 1;
				if (global.RELIKS[44,2])
				{
					switch(global.difficulty)//--> Invulnerability
					{
						case "casual":	alarm[11] = (global.RELIKS[33,2]) ? 200 : 100; break;
						case "normal":	alarm[11] = (global.RELIKS[33,2]) ? 160 : 80; break;
						case "hard":	alarm[11] = (global.RELIKS[33,2]) ? 120 : 60; break;
						case "lunatic": alarm[11] = (global.RELIKS[33,2]) ? 80 : 40; break;
					}
					
					if (damage == 0)
					{
						if (!audio_is_playing(snd_Parry)) audio_play_sound(snd_Parry,3,0);
						var parry = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_centerXYfollow);
						parry.sprite_index = spr_Parry;
						parry.image_blend = c_fuchsia;
						//parry.brightness = true;
						//parry.glow = true;
						parry.image_alpha = 0.5;
						parry.image_xscale = 2;
						parry.image_yscale = 2;
						parry.image_angle = random(360);
						parry.anim_end = 5;
					}
					else if (!audio_is_playing(snd_ManticoreDmg1)) && (!audio_is_playing(snd_ManticoreDmg2)) && (!audio_is_playing(snd_ManticoreDmg3))
						audio_play_sound(choose(snd_ManticoreDmg1,snd_ManticoreDmg2,snd_ManticoreDmg3),2,0);
				}
				else state = damage_Manticore;
				Manticore_hp -= damage;
				Manticore_hp = clamp(Manticore_hp, 0, global.player_max_health);

				alarm[10] = 10;
			}
		}
	}
}

if (alarm[0] <= 1)
{
	script_execute(state);
}

//--> Consecutive Damage Counters
//--> This checks if the Boss is being being attacked with only TW or Melee atks.
//--> Each thime the Boss is hit with one type of atk, the other counter is reset.
if place_meeting(x,y,obj_Throwing_Wpn)
{
	consecutive_dmg = 0;
	consec_TW_dmg += 1;
}
if place_meeting(x,y,obj_Manticore_hitbox)
{
	consecutive_dmg += 1;
	consec_TW_dmg = 0;
}