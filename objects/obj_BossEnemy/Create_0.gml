/// @description Insert description here
cam_x = surface_get_width(application_surface);
cam_y = surface_get_height(application_surface);

object_type = "enemy";
enemy_name = "Test_Enemy";
hit_points = 1000;
image_alpha = 1;
old_hit_points = hit_points;
max_hit_points = hit_points;
hsp = 0;
vsp = 0;
grv = 0.8;
walksp = 9; //--> No decimals to prevent sprite shaking
direc = 0;
state = InGame_Credits;
prevState = -1;
canSpecial = false;
defeated_spr = spr_poof;
grunt1 = snd_EffectiveDmg;
grunt2 = snd_EffectiveDmg;

default_h = y;
minX = 0;
maxX = 0;
minY = 0;
maxY = 0;

bounce = 0;
counter = 0;
counter2 = 0; //--> Exclusive for Special Alarm
consecutive_dmg = 0;
consec_TW_dmg = 0;
AtkTurn = 0;

randomX = 0;
randomY = 0;
switch_var = false;
switch_var2 = false;
switch_var3 = false;

HBox_var = false;
special_var = false;
specialTW_var = false;

interact_instance = obj_BossEnemy;
interact_instance2 = obj_BossEnemy;
right_var = false;
left_var = false;
dmgColor = c_white;

alarm[3] = 1;