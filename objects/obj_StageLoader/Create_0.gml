/// @description Insert description here
// You can write your code in this editor

draw_texture_flush(); //--> Flush all previous texture pages to load new ones.

gpu_set_alphatestenable(true); //--> alpha Test to improve performance
gpu_set_alphatestref(1);

tipsText = @"This is a small portion of the game Anterios, in development by RSR Studios.
			 The Training Room is for testing and player's training. Please have a good time
			 with Manticore and his arsenal! Any feedback please send it to:
			 anteriosgame@gmail.com.
			 
			 All rights reserved. RSR Studios (C) 2020 - 2022";
global.tips += 1;
if (global.tips > 2) global.tips -= 2;

global.currentManager = obj_TestRoom_manager1;

global.room2Load = rm_testroom;

spr2draw = spr_Manticore;

LoadAnim = 0; //--> Number to move the Loading image index in the Draw GUI

LoadNext = 0; //--> Variable used to load texture pages one by one on the main switch

AllLoaded = false; //--> True if everything needed for the room is loaded

keyWait = false;