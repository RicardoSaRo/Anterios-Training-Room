/// @description Insert description here
// You can write your code in this editor
//instance_deactivate_all(self); //--> Deactivate all instances except itself

var textures = undefined;

var anyKey = keyboard_check_pressed(vk_anykey);

switch(LoadNext) //--> Mandatory Texture Loads
{
	case 0:		textures = texturegroup_get_sprites( "Manticore");
				spr2draw = spr_Manticore;
				break;
	case 1:		textures = texturegroup_get_sprites("Manticore_Hitboxes");
				spr2draw = spr_Argenta_hitbox1;
				break;
	case 2:		textures = texturegroup_get_sprites("Throwing_Weapons"); break;
				spr2draw = spr_Chakram;
				break;
	case 3:		textures = texturegroup_get_sprites("GUI"); break;
				spr2draw = spr_ScorpRing_Freeze;
				break;
	case 4:		textures = texturegroup_get_sprites("FX"); break;
				spr2draw = spr_clank;
				break;
	case 5:		textures = texturegroup_get_sprites("Weapons_Menu");
				spr2draw = spr_Argenta_Avatar;
				break;
	case 6:		textures = texturegroup_get_sprites("Manticore_Specials");
				spr2draw = spr_Argenta_wave1;
				break;
	case 7:		textures = texturegroup_get_sprites("Default");
				spr2draw = spr_Manticore_Traveler;
				break;
}

switch(global.room2Load) //--> Room Specifics
{
		case rm_testroom:
		switch(LoadNext)
		{
			//--> Load Sound
			case 8:		if (!audio_group_is_loaded(audiogroup_default))	audio_group_load(audiogroup_default);
						break;
			case 9:	//--> Include a Switch for different characters
						if (!audio_group_is_loaded(Manticore_audio)) audio_group_load(Manticore_audio);
						break;
			//--All Loaded
			case 10:	AllLoaded = true; break;
			
		}
		break;
}

//--> Prefetch all Sprites in the Texture Page AND draws the first one
if (textures != undefined)
{
	for (var i = 0; i < array_length_1d(textures); ++i;)
	{
		sprite_prefetch(textures[i]);
	}
	draw_sprite(textures[0],0,4000,4000);
}

//--> If everything is loaded the instance is destroyed to proceed to the next room
if (!AllLoaded) LoadNext += 1;
else keyWait = true;

if (anyKey) && (AllLoaded) instance_destroy();