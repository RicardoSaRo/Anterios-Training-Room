/// @description Insert description here
// You can write your code in this editor

if (instance_exists(obj_2_follow))
{
	x = obj_2_follow.x;
	y = obj_2_follow.y;
}

if (!audio_is_playing(snd_Golem_rollingBlur))
{
	audio_sound_pitch(snd_Golem_rollingBlur,random_range(0.8,1.2));
	audio_play_sound(snd_Golem_rollingBlur,24,0);
}

if (image_index > anim_end) instance_destroy();