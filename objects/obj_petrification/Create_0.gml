/// @description Insert description here
// You can write your code in this editor
var chance = irandom(100);

sprite_index = (chance == 1) ? spr_petri3_Manticore : ((chance <= 50) ? spr_petri2_Manticore : spr_petri1_Manticore);

image_xscale = obj_Manticore.image_xscale;

obj_Manticore.hsp = 0;

switch(global.difficulty)
{
	case "casual":	petriHP = 10; break;
	case "normal":	petriHP = 15; break;
	case "hard":	petriHP = 20; break;
	case "lunatic": petriHP = 25; break;
	default:		petriHP = 15; break;
}