/// @description Insert description here
// You can write your code in this editor
audio_play_sound(snd_Golem_punch,6,0);

repeat(25)
{
	var rocks = instance_create_layer(x+random_range(-40,40),y+random_range(-50,50),"FX_Layer",obj_rockice_FX);
	rocks.rockice_vsp = irandom_range(-5,-10);
	rocks.rockice_hsp = irandom_range(-10,10);
	rocks.sprite_index = spr_rocks_FX_G;
}

var rainbowCircle2 = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
rainbowCircle2.sprite_index = spr_Smack;
rainbowCircle2.image_index = irandom(4);
rainbowCircle2.image_alpha = random_range(0.8,1);
rainbowCircle2.image_speed = 0;
rainbowCircle2.fadeSpd = random_range(0.03,0.05);
rainbowCircle2.Ymovement = 0;
rainbowCircle2.Xmovement = 0;
rainbowCircle2.sizeMod = random_range(0.01,0.05);
var circSiz = random_range(0.5,1.5);
rainbowCircle2.image_xscale = circSiz;
rainbowCircle2.image_yscale = circSiz;

obj_Manticore.state = move_Manticore;
obj_Manticore.alarm[6] = 10;