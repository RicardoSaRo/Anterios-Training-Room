/// @description Insert description here
// You can write your code in this editor

x = obj_Manticore.x;
y = obj_Manticore.y;

//--> Release inputs check
if (image_speed == 0)
{
	var kybrd = keyboard_check_pressed(vk_anykey);
	var controller = 0;
	
	for (j=32769;j<32789;j+=1)
	{
		if (gamepad_button_check_pressed(global.controller_type,j)) controller = j;
    }
	
	var petriRelease = max(kybrd,controller,0);
	
	if (petriRelease)
	{
		var rocks = instance_create_layer(x,y,"FX_Layer",obj_rockice_FX);
		rocks.rockice_vsp = irandom_range(-5,-10);
		rocks.rockice_hsp = irandom_range(-10,10);
		rocks.sprite_index = spr_rocks_FX_G;
		
		var rndmpos = irandom_range(-20,20);
		var rainbowCircle2 = instance_create_layer(x+(rndmpos*image_xscale),y+rndmpos,"FX_Layer",obj_fadeAway);
		rainbowCircle2.sprite_index = choose(spr_spear_hit,spr_hit_FX);
		rainbowCircle2.image_alpha = random_range(0.8,1);
		rainbowCircle2.fadeSpd = random_range(0.03,0.05);
		rainbowCircle2.Ymovement = 0;
		rainbowCircle2.Xmovement = 0;
		rainbowCircle2.sizeMod = random_range(0.01,0.05);
		rainbowCircle2.image_angle = random(360);
		var circSiz = random_range(0.5,1.5);
		rainbowCircle2.image_xscale = circSiz;
		rainbowCircle2.image_yscale = circSiz;
		
		petriHP--;
		
		x += choose(-2,-1,1,2);
		y += choose(-2,-1,1,2);
	}
}

image_xscale = obj_Manticore.image_xscale;

obj_Manticore.state = stun_Manticore;

if (petriHP <= 0) || (obj_Manticore.hsp != 0) instance_destroy();