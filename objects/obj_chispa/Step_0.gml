/// @description Insert description here
// You can write your code in this editor

switch (chispa_colour)
{
	case c_red: case c_yellow: case c_orange:
		if (sprite_index != spr_starShine) chispa_colour = choose(c_red,c_yellow,c_orange);
		y = y + 1;
		break;
	case c_white: case c_blue:
		chispa_colour = choose(c_white,c_blue);
		if (instance_exists(obj_Lightningball))
		{
			var rndm = irandom(1);
			if (rndm)
			{
				x += chispaX;
				y += chispaY;
			}
			else
			{
				x += random_range(-5,5);
				y += random_range(-5,5);
			}
		}
		break;
	case c_aqua: case c_silver:
		chispa_colour = choose(c_aqua,c_silver);
		break;
	case c_gray:
		chispa_colour = c_gray;
		alfa = random_range(0.2,1);
		y = y + 1;
		break;
}
		


