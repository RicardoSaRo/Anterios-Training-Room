/// @description Insert description here
obj_Manticore.tw_limit = false;

//if (sprite_index == spr_Void_Ripple) && (instance_number(obj_Throwing_Wpn) == 1)
//	obj_Manticore.void_ripples_charges -= 1;

if (sprite_index == spr_Void_Ripple)
{
	artist.VoidWorldActive = false;
	artist.BloomFilterActive = true;
	artist.BlurActive = true;
}

if (sprite_index == spr_Triblader)
{
	audio_stop_sound(snd_TribladerBack);
	audio_stop_sound(snd_TribladerGO);
}