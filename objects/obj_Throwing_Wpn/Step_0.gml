/// @description Insert description here

switch(sprite_index)
{
	case spr_Chakram:	if (place_meeting(x,y,obj_Wall))
						{
							bounce_count += 1;
							direc *= -1;
							audio_play_sound(snd_wallBounce,30,0);
						}
						var bmax = (global.TWs[1,3]<=1) ? 2 : 3;
						if (bounce_count >= bmax) or (obj_Manticore.equipped_tw != 1) sprite_index = spr_poof;
						tw_hsp = 12;
						x += tw_hsp * direc;
						dmg_calc(tw_hit_used,sprite_index);
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							if (direc != obj_Manticore_hitbox.image_xscale) //--> Sound FX
							{
								audio_sound_pitch(snd_Clank1,random_range(0.9,1.1))
								audio_play_sound(snd_Clank1,30,0);
							}
							direc = obj_Manticore_hitbox.image_xscale;
						}
						if (alarm[11] > 2)
						{
							var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_blend = c_white;
						}
						break;
	
	case spr_Kunai:		if (!TWswitch_var)
						{
							tw_vsp = (starblade_angle==-1) ? -24 : ((starblade_angle==1) ? 24 : -37);
							TWswitch_var = true;
						}
						if (place_meeting(x,y,obj_Wall)) or (obj_Manticore.equipped_tw != 2)
						{
							audio_play_sound(snd_wallBounce,30,0);
							sprite_index = spr_poof;
						}
						if (place_meeting(x,y,obj_BossEnemy)) or (place_meeting(x,y,obj_Enemy_hurtbox))
						{
							dmg_calc(tw_hit_used,sprite_index);
							tw_hit_used = true;
						}
						tw_hsp = 24;
						if (tw_hit_used = true)
						{
							tw_hsp = 6;
							x += tw_hsp * -direc;
							y += kunai_Ymovement * kunai_angle_direc;
							image_angle += 30 * kunai_angle_direc;
							image_alpha -= 0.1;
						}
						else
						{
							x += tw_hsp * direc;
							y += ((tw_vsp == 24)||(tw_vsp == -24)) ? tw_vsp : 0;
						}
						if (alarm[11] > 0)
						{
							var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_angle = image_angle;
							afterimage.image_blend = c_ltgray;
						}
						break;
						break;
	
	case spr_Shuriken:	if (place_meeting(x,y,obj_Wall)) or (obj_Manticore.equipped_tw != 6)
							sprite_index = spr_poof;
						tw_hsp = 18;
						x += tw_hsp * direc;
						if (instance_exists(obj_BossEnemy))&&(alarm[2] < 1)
						{
							if (y < obj_BossEnemy.y) var up_down = 1;
							else var up_down = -1;
							shuriken_curve += 1 * up_down;
							shuriken_curve = clamp(shuriken_curve,-20,20);
						}
						y += shuriken_curve;
						if (alarm[11]<=1) dmg_calc(tw_hit_used,sprite_index);
						if (alarm[11] > 0)
						{
							var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_blend = c_dkgray;
						}
						break;
						
	case spr_Triblader: if (obj_Manticore.equipped_tw != 7) sprite_index = spr_poof;
						if (!TWswitch_var)
						{
							triblade_max_Y = (starblade_angle==-1) ? y + ((global.TWs[7,3]*50) + 300) * -1 :
											 ((starblade_angle==1) ? y + ((global.TWs[7,3]*50) + 300) : y);
							TWswitch_var = true;
						}
						if (!guillotine_ready2fall)
						{
							x = lerp(x,triblade_max_range,0.10);
							if ((direc)&&(x < triblade_max_range-150)) or
							   ((!direc)&&(x > triblade_max_range+150)) alarm[2] = 40;
							y = lerp(y,triblade_max_Y,0.10);
							if (!audio_is_playing(snd_TribladerGO))
							{
								audio_sound_pitch(snd_TribladerGO,random_range(0.9,1));
								audio_play_sound(snd_TribladerGO,15,0);
							}
						}
						else
						{
							x = lerp(x,obj_Manticore.x,triblade_speed);
							y = lerp(y,obj_Manticore.y,triblade_speed);
							triblade_speed += 0.01;
							triblade_speed = clamp(triblade_speed,0,15);
							if (!audio_is_playing(snd_TribladerBack))
							{
								audio_sound_pitch(snd_TribladerBack,random_range(0.9,1.1));
								audio_play_sound(snd_TribladerBack,25,0);
							}
						}
						if ((alarm[2] < 1) && ((direc)&&(x < triblade_max_range-1))) or
						   ((alarm[2] < 1) && ((!direc)&&(x > triblade_max_range+1)))
						   
						{
							if (!guillotine_ready2fall)
							{
								triblade_returnX = obj_Manticore.x;
								guillotine_ready2fall = true;
							}
						}
						if (place_meeting(x,y,obj_Manticore)) instance_destroy();
						if (alarm[11]<=1) dmg_calc(tw_hit_used,sprite_index);
						if (alarm[11] > 0)
						{
							var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_blend = c_yellow;
						}
						break;
							
	case spr_Assegai:	tw_grv = 1.5;
						bomb_vsp += tw_grv;
						var enmyHit = place_meeting(x,y,obj_BossEnemy);
						var enmyHB_hit = place_meeting(x,y,obj_Enemy_hurtbox);
						var wallHit = place_meeting(x,y,obj_Wall);
						if (!tw_hit_used) if (!audio_is_playing(snd_Assegai)) audio_play_sound(snd_Assegai,15,0);
						if (enmyHit) or (wallHit) or (enmyHB_hit)
						{
							if (!tw_hit_used)
							{
								audio_stop_sound(snd_Assegai);
								if ((enmyHit)||(enmyHB_hit)) audio_play_sound(snd_Assegai_hit,17,0);
								else if (wallHit) audio_play_sound(snd_Clank2,17,0);
							}
							tw_hsp = 0;
							bomb_vsp += 1;
							image_angle += choose(1,2) * direc;
							image_alpha -= 0.03;
							dmg_calc(tw_hit_used,sprite_index);
							tw_hit_used = true;
						}
						else
						{
							x += tw_hsp * direc;
							y += bomb_vsp;
							image_angle = bomb_vsp * -direc;
							if (alarm[3] <=1) starblade_angle += 1;
							if (starblade_angle > 10)
								if (alarm[3] <=1) starblade_angle +=2;
							if (starblade_angle > 40)
								if (alarm[3] <=1) starblade_angle +=3;
						}						
						if (obj_Manticore.equipped_tw != 9) sprite_index = spr_poof;
						break;
						
	case spr_Coin_Launcher: case spr_BigCoin: case spr_BigTripleCoin:
						if (obj_Manticore.equipped_tw != 15) sprite_index = spr_poof;
						if (place_meeting(x,y,obj_BossEnemy)) or (place_meeting(x,y,obj_Wall))
						{
							dmg_calc(tw_hit_used,sprite_index);
							tw_hit_used = true;
							for (var i = 0; i<3; i++)
							{
								var coin = instance_create_layer(x,y,"FX_Layer",obj_rockice_FX)
								with(coin)
								{
									coin.sprite_index = spr_coin;
								}
							}
						}
						tw_hsp = 24;
						var shine = instance_create_layer(x,y,"FX_Layer",obj_chispa)
						with(shine)
						{
							sprite_index = spr_starShine;
							y += random_range(-10,10);
							image_xscale = 0.5;
							image_yscale = 0.5;
						}
						if (tw_hit_used == true) or (place_meeting(x,y,obj_Wall))
						{
							audio_play_sound(choose(snd_Coins1,snd_Coins2),11,0);
							instance_destroy();
						}
						else
						{
							x += tw_hsp * direc;
							y += starblade_angle;
						}
						break;
						
	case spr_Void_Ripple:
						if (!TWswitch_var)
						{
							bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],140,512,13,12,-25,1,c_white,1);
							fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],56,512,10,20,83,1,c_fuchsia,1);
							fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],56,512,10,20,83,1,c_fuchsia,1);
							audio_play_sound(snd_VoidRipples,20,0);
							TWswitch_var = true;
						}
						artist.VoidWorldActive = true;
						artist.BloomFilterActive = false;
						artist.BlurActive = false;
						var rndm = random_range(0.05,0.7);
						image_xscale += rndm;
						image_yscale += rndm;
						image_alpha = random_range(0.3,0.6);
						if (image_xscale > 15) or (image_yscale > 15) instance_destroy();
						break;
						
	case spr_Starblades:
						//--> Checks Collision on Floor
						if place_meeting(x,y+bomb_vsp,obj_Wall)
						{
							audio_play_sound(snd_wallBounce,30,0);
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
							if (starblade_angle == 0) starblade_angle = choose(-1,1);
							else
							{
								starblade_angle *= -1;
								bomb_vsp = irandom_range(1,10);
							}
							y += 10 * starblade_angle;
						}
						//--> Check Collision on Platforms
						if (y < instance_nearest(x,y,obj_Platform).bbox_top) &&
						   (place_meeting(x,y+bomb_vsp,obj_Platform))
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
							if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
							{
								if (starblade_angle == 0) starblade_angle = choose(-1,1);
								else
								{
									starblade_angle *= -1;
									bomb_vsp = irandom_range(1,10);
								}
								y += 10 * starblade_angle;
							}
						}
						else y += bomb_vsp * starblade_angle;
						if place_meeting(x+bomb_hsp,y,obj_Wall)
						{
							audio_play_sound(snd_wallBounce,30,0);
							var onepixel = sign(bomb_hsp);
							while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
							direc *= -1;
							x += 10 * direc;
							starblade_angle = choose(-1,1);
						}
						else x += bomb_hsp * direc;
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							starblade_angle = choose(-1,1);
							y += 10 * starblade_angle;
							direc = obj_Manticore_hitbox.image_xscale;
						}
						if (alarm[0] < 1) or (obj_Manticore.equipped_tw != 8) sprite_index = spr_poof;
						if (alarm[11]<=1) dmg_calc(tw_hit_used,sprite_index);
						if (alarm[11] > 0) && (!shuriken_curve)
						{
							var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_blend = c_dkgray;
						}
						break;
						
	case spr_Guillotine:
						if (!TWswitch_var)
						{
							guillotine_min_h += global.TWs[5,3] * 50;
							TWswitch_var = true;
						}
						if (!audio_is_playing(snd_Guillotine1))
						{
							audio_sound_pitch(snd_Guillotine1,random_range(0.6,1));
							audio_play_sound(snd_Guillotine1,20,0);
						}
						else
						{
							if (!audio_is_playing(snd_Guillotine2))
							{
								audio_sound_pitch(snd_Guillotine2,random_range(0.6,1));
								audio_play_sound(snd_Guillotine2,20,0);
							}
							else
							{
								audio_sound_pitch(snd_Guillotine3,random_range(0.6,1));
								if (!audio_is_playing(snd_Guillotine3)) audio_play_sound(snd_Guillotine3,20,0);
							}
						}
						if (place_meeting(x,y,obj_BossEnemy))
						{
							dmg_calc(tw_hit_used,sprite_index);
							tw_hit_used = true;
						}
						if (alarm[11] >= 2.4) tw_hit_used = false;
						if (!guillotine_ready2fall)
						{
							x += 5 * direc;
							y -= 10;
							image_angle -= 20 * direc;
							if (y <= guillotine_max_h) guillotine_ready2fall = true;
						}
						else
						{
							if (y < guillotine_max_h + 20)
							{
								y += 1
								x += 5 * direc;
//								image_angle -= 10 * direc;
								image_angle -= 20 * direc;
							}
							else
							{
								x += 5 * direc;
								y += 10;
//								image_angle -= 5 * direc;
								image_angle -= 20 * direc;
							}
						}
						if ((y > guillotine_min_h)&&(guillotine_ready2fall)) or
						   (obj_Manticore.equipped_tw != 5)
						{
							sprite_index = spr_poof;
							image_xscale = 4;
							image_yscale = 4;
						}
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							if (guillotine_ready2fall) //--> Sound FX
							{
								audio_sound_pitch(snd_Clank1,random_range(0.9,1.1))
								audio_play_sound(snd_Clank1,30,0);
							}
							guillotine_ready2fall = false;
							tw_hit_used = false;
							image_xscale = obj_Manticore_hitbox.image_xscale;
							direc = image_xscale;
							x += 5 * direc;
							y -= 10;
							image_angle -= 20 * direc;
						}
						if (alarm[11] <= 1)
						{
							var afterimage = instance_create_layer(x+random_range(-15,15),y+random_range(-15,15),"Back_FX_Layer",dash_afterimage);
							afterimage.sprite_index = sprite_index;
							afterimage.image_index = image_index;
							afterimage.image_xscale = image_xscale;
							afterimage.image_angle = image_angle;
							afterimage.image_blend = c_silver;
						}
						break;
						
	case spr_Granade:	tw_vsp = tw_vsp + tw_grv;
						if (place_meeting(x,y,obj_BossEnemy)) or (place_meeting(x,y-10,obj_Wall)) or
						   (place_meeting(x,y,obj_Enemy_hurtbox))
						{
							image_angle = 0;
							sprite_index = spr_Explosion_grnd;
							if (!audio_is_playing(snd_Grenade1)) audio_play_sound(snd_Grenade1,20,0);
							else
							{
								if (!audio_is_playing(snd_Grenade2)) audio_play_sound(snd_Grenade2,20,0);
								else
								{
									if (!audio_is_playing(snd_Grenade3)) audio_play_sound(snd_Grenade3,20,0);
								}
							}
						}
						if (alarm[0] mod explosive_timer == 0) && (sprite_index != spr_Explosion_grnd)
						{
							image_blend = c_red;
							explosive_timer /= 2;
						}
						else image_blend = c_white;
						if (sprite_index == spr_Granade)
						{
							image_speed = 1;
							if (!place_meeting(x+tw_hsp*direc,y,obj_Wall)) tw_hsp -= 0.2;
							tw_hsp = clamp(tw_hsp,0,37);
							image_angle += 24 * -direc;
							if (place_meeting(x+tw_hsp*direc,y,obj_Wall))&&
							(!place_meeting(x,y+1,obj_Wall)) direc *= -1;
							else x += tw_hsp * direc;
							if (place_meeting(x,y+1,obj_Wall) or
							(y < instance_nearest(x,y,obj_Platform).bbox_top) &&
							(place_meeting(x,y+1,obj_Platform)))
							{
								if (bounce_count < 3)
								{
									tw_hsp -= 7;
									if (bounce_count = 1) tw_vsp = -15;
									else tw_vsp = -20;
								}
								else
								{
									tw_vsp = 0;
									tw_hsp = 0;
								}
								y += tw_vsp;
								bounce_count += 1;
							}
							else
							{
								tw_vsp += 2;
								tw_vsp = clamp(tw_vsp,-37,37);
								y += tw_vsp;
							}
							if (tw_vsp > -1) tw_vsp = 0;
							if (tw_vsp == 0) && (tw_hsp == 0) image_angle -= 24 * -direc;
							else
							{
								var frostchispa = instance_create_layer(x,y,"FX_Layer",obj_chispa);
								with (frostchispa)
								{
									chispa_colour = c_red;
									x = other.x + random_range(-10,10);
									y = other.y + random_range(-10,10);
								}	
							}
						}
						if (alarm[0] <= 1)
						{
							image_angle = 0;
							sprite_index = spr_Explosion_grnd;
							if (!audio_is_playing(snd_Grenade1)) //--> GrenadeExplosion FX
							{
								audio_sound_pitch(snd_Grenade1,random_range(1,1.1))
								audio_play_sound(snd_Grenade1,20,0);
							}
							else
							{
								if (!audio_is_playing(snd_Grenade2))
								{
									audio_sound_pitch(snd_Grenade2,random_range(1,1.1))
									audio_play_sound(snd_Grenade2,20,0);
								}
								else
								{
									if (!audio_is_playing(snd_Grenade3))
									{
										audio_sound_pitch(snd_Grenade3,random_range(1,1.1))
										audio_play_sound(snd_Grenade3,20,0);
									}
								}
							}
						}
						if (obj_Manticore.equipped_tw != 3) sprite_index = spr_poof;
						break;
						
	case spr_Bomb1: case spr_Bomb2:
						tw_grv = 1.5;
						bomb_vsp += tw_grv;
						if place_meeting(x,y+bomb_vsp,obj_Wall)
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
							bomb_vsp = 0;
							bomb_hsp -= 0.5;
							bomb_hsp = clamp(bomb_hsp,0,15);
						}
						if place_meeting(x,y+bomb_vsp,obj_Platform)
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
							if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
							{
								bomb_vsp = 0;
								bomb_hsp -= 0.5;
								bomb_hsp = clamp(bomb_hsp,0,15);
							}
						}
						y += bomb_vsp;
						if place_meeting(x+bomb_hsp,y,obj_Wall)
						{
							var onepixel = sign(bomb_hsp);
							while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
							direc *= -1;

						}
						x += bomb_hsp * direc;
//						image_angle = bomb_vsp * -direc;
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							bomb_vsp = -15;
							bomb_hsp = 15;
							direc = obj_Manticore_hitbox.image_xscale;
						}
						if (alarm[1] < 1)
						{
							image_angle = 0;
							sprite_index = spr_Explosion_bmb;
						}
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							bomb_vsp = -15;
							bomb_hsp = 15;
							direc = obj_Manticore_hitbox.image_xscale;
						}
						if (bomb_vsp != 0) && (bomb_hsp != 0)
						{
							var frostchispa = instance_create_layer(x,y,"FX_Layer",obj_chispa);
							with (frostchispa)
							{
								chispa_colour = c_red;
								x = other.x + random_range(-10,10);
								y = other.y + random_range(-10,10);
							}	
						}
						if (alarm[1] > 100)
						{
							if (alarm[1] mod 50 == 0) image_blend = c_red;
							else image_blend = c_white;
						}
						else
						{
							if (alarm[1] mod 5 == 0) image_blend = c_red;
							else image_blend = c_white;
						}
						if (obj_Manticore.equipped_tw != 4) sprite_index = spr_poof;
						break;

	case spr_RemoteExp_OFF:
						bomb_vsp += tw_grv;
						bounce_count = 15;
						image_angle = 45 * direc;
						if (place_meeting(x,y+1,obj_Wall))
						{
							bomb_vsp = 0;
							bomb_hsp = 0;
							image_angle = 0;
						}
						if place_meeting(x,y+bomb_vsp,obj_Platform)
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
							if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
							{
								bomb_vsp = 0;
								bomb_hsp = 0;
								image_angle = 0;
							}
						}
						y += bomb_vsp;
						if (place_meeting(x+bomb_hsp,y,obj_Wall))
						{
							bomb_vsp = 0;
							bomb_hsp = 0;
							image_angle = 90;
						}
						x += bomb_hsp * direc;
						if (place_meeting(x,y,obj_BossEnemy)) && (alarm[2] >= 6)
						{
							bomb_vsp = 0;
							bomb_hsp = 0;
							x = obj_BossEnemy.x;
							y = obj_BossEnemy.y;
							image_angle = random(360);
							seeking = true;
						}
						if (bomb_hsp == 0) && (bomb_vsp == 0) sprite_index = spr_RemoteExp_low;
						if (obj_Manticore.equipped_tw != 14) sprite_index = spr_poof;
						break;
	case spr_RemoteExp_low:
						if (seeking)
						{
							x = obj_BossEnemy.x;
							y = obj_BossEnemy.y;
						}
						bounce_count += 0.2 + (global.TWs[14,3]/10); //--> Used to damage and increases over time
						var detonate = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
						if (detonate)
						{
							image_angle = 0;
							sprite_index = spr_Explosion_RExp1;
						}
						if (bounce_count > 75) sprite_index = spr_RemoteExp_mid;
						if (obj_Manticore.equipped_tw != 14) sprite_index = spr_poof;
						if (!audio_is_playing(snd_RemExplsv1))&&(!audio_is_playing(snd_RemExplsv2))&&
						   (!audio_is_playing(snd_RemExplsv3)) audio_play_sound(snd_RemExplsv1,13,0);
						break;
	case spr_RemoteExp_mid:
						if (seeking)
						{
							x = obj_BossEnemy.x;
							y = obj_BossEnemy.y;
						}
						bounce_count += 0.3 + (global.TWs[14,3]/10); //--> Used to damage and increases over time
						var detonate = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
						if (detonate)
						{
							image_angle = 0;
							sprite_index = spr_Explosion_RExp1;
						}
						if (bounce_count > 150) sprite_index = spr_RemoteExp_high;
						if (obj_Manticore.equipped_tw != 14) sprite_index = spr_poof;
						if (!audio_is_playing(snd_RemExplsv1))&&(!audio_is_playing(snd_RemExplsv2))&&
						   (!audio_is_playing(snd_RemExplsv3)) audio_play_sound(snd_RemExplsv2,13,0);
						break;
	case spr_RemoteExp_high:
						if (seeking)
						{
							x = obj_BossEnemy.x;
							y = obj_BossEnemy.y;
						}
						var detonate = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
						if (detonate)
						{
							image_angle = 0;
							sprite_index = spr_Explosion_RExp2;
						}
						if (obj_Manticore.equipped_tw != 14) sprite_index = spr_poof;
						if (!audio_is_playing(snd_RemExplsv1))&&(!audio_is_playing(snd_RemExplsv2))&&
						   (!audio_is_playing(snd_RemExplsv3)) audio_play_sound(snd_RemExplsv3,13,0);
						break;

	case spr_Elemental_Orb_OFF:
						tw_grv = 1.5;
						bomb_vsp += tw_grv;
						if (place_meeting(x,y+bomb_vsp,obj_Wall))
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Wall)) y += onepixel;
							bomb_vsp = 0;
							bomb_hsp -= 0.5;
							bomb_hsp = clamp(bomb_hsp,0,15);
						}
						if place_meeting(x,y+bomb_vsp,obj_Platform)
						{
							var onepixel = sign(bomb_vsp);
							while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
							if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
							{
								bomb_vsp = 0;
								bomb_hsp -= 0.5;
								bomb_hsp = clamp(bomb_hsp,0,15);
							}
						}
						y += bomb_vsp;
						if place_meeting(x+bomb_hsp,y,obj_Wall)
						{
							var onepixel = sign(bomb_hsp);
							while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
							direc *= -1;

						}
						x += bomb_hsp * direc;
//						image_angle = bomb_vsp * -direc;
						if (place_meeting(x,y,obj_Manticore_hitbox))
						{
							bomb_vsp = -15;
							bomb_hsp = 15;
							direc = obj_Manticore_hitbox.image_xscale;
						}
						switch(image_blend)
						{
							case c_white: if (obj_Manticore.equipped_tw != 10) sprite_index = spr_poof; break;
							case c_lime: if (obj_Manticore.equipped_tw != 11) sprite_index = spr_poof; break;
							case c_orange: if (obj_Manticore.equipped_tw != 12) sprite_index = spr_poof; break;
							case c_aqua: if (obj_Manticore.equipped_tw != 13) sprite_index = spr_poof; break;
						}
						if (alarm[0] mod 40 == 0)
						{
							with (instance_create_layer(x,y,"FX_Layer",obj_show_damage))
							{
								font_color = c_white;
								damage_display = other.orb_countdwn;
								plus_or_minus = "";
								y_speed = 1;
							}
							orb_countdwn -= 1;
						}
						if (alarm[0] <= 1)
						{
							sprite_index = spr_Elemental_Orb_ON;
							switch(obj_Manticore.equipped_tw)
							{
								case 10: alarm[1] = 350 + (global.TWs[10,3] * 50); break;
								case 11: alarm[1] = 175 + (global.TWs[11,3] * 25); break;
								case 12: alarm[1] = 350 + (global.TWs[12,3] * 50); break;
								case 13: alarm[1] = 40; break;
							}
							if (obj_Manticore.equipped_tw == 11) alarm[1] = (global.RELIKS[9,2]) ? 300 : 250;
							else
							{
								if (obj_Manticore.equipped_tw == 13) alarm[1] = 40;
								else alarm[1] = (global.RELIKS[9,2]) ? 600 : 500;
							}
						}
						break;
						
	case spr_Elemental_Orb_ON:
						priority = 9;
						if (image_blend == c_lime) y -= 2;
						else
						{
							if (image_blend == c_aqua)&&(bounce_count == 0) y -= 2;
							else y -= 0.5;						
						}
						switch(orb_type)
						{
							case "Thunder": if (!place_meeting(x,y,obj_relampago)) && (alarm[0] <= 0)
											{
												var TW = instance_create_layer(x,y,"Back_FX_Layer",obj_TWave);
												TW.owner = self;
												alarm[0] = 40;
												audio_play_sound(snd_EnergyShot,20,0);
											}
											
											if (y mod 25 < 1)
											{
												var hover = instance_create_layer(x,bbox_bottom+10,"Back_FX_Layer",obj_doublejmp_fx);
												with (hover)
												{
													image_blend = c_ltgray;
													image_speed = 2;
												}
											}
											if (alarm[1] <= 1) or (obj_Manticore.equipped_tw != 10)
											{
												sprite_index = spr_poof;
												image_xscale = 2;
												image_yscale = 2;
											}
											break;
							
							case "Wind":	if (y mod 25 < 1)
											{
												var hover = instance_create_layer(x,bbox_bottom+10,"Back_FX_Layer",obj_doublejmp_fx);
												with (hover)
												{
													image_blend = c_lime;
													image_speed = 2;
												}
											}
											image_blend = c_lime;
											if (alarm[2] < 1)
											{
												if (!place_meeting(x,y,obj_windslash))
												{
													var WS = instance_create_layer(x,y,"Back_FX_Layer",obj_windslash);
													with (WS)
													{
														ws_movementX = choose(random_range(-30,-15),random_range(15,30));
														ws_movementY1 = random_range(-30,-15);
														ws_movementY2 = random_range(15,30);
														source = "WindOrb";
														image_xscale = sign(ws_movementX);
														image_yscale = sign(choose(ws_movementY1,ws_movementY2));
													}
													if (!audio_is_playing(snd_EnergyShot)) audio_play_sound(snd_EnergyShot,20,0);
												}
											}
											if (alarm[1] <= 1) or (obj_Manticore.equipped_tw != 11)
											{
												sprite_index = spr_poof;
												image_speed = 0.5;
												image_xscale = 2;
												image_yscale = 2;
											}
											break;
							case "Fire":	if (y mod 25 < 1)
											{
												var hover = instance_create_layer(x,bbox_bottom+10,"Back_FX_Layer",obj_doublejmp_fx);
												with (hover)
												{
													image_blend = c_orange;
													image_speed = 2;
												}
											}
											if (!TWswitch_var)
											{
												//--> Starblade Angle var is used to increase radius with upgrades
												starblade_angle = (global.TWs[12,3]==3) ? 90 : ((global.TWs[12,3]==2) ? 45 : 0);
												TWswitch_var = true;
											}
											var radius = 400 + (global.TWs[12,3] * starblade_angle);
											var shotFrec = 50 - (global.TWs[12,3] * 5);
											if (distance_to_object(obj_BossEnemy)<radius) && (alarm[1] mod shotFrec == 0)
											{
												instance_create_layer(x,y-20,"Back_FX_Layer",obj_Hmmng_Fireball);
												var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
												with (FX)
												{
													sprite_index = spr_firering_FX;
													obj_2_follow = other;
													anim_end = 3;
													brightness = true;
													glow = true;
													g_amount = 20;
													image_xscale = 2;
													image_yscale = 2;
												}
												audio_play_sound(snd_EnergyShot,20,0);
											}
											if (alarm[1] <= 1) or (obj_Manticore.equipped_tw != 12)
											{
												sprite_index = spr_poof;
												image_xscale = 2;
												image_yscale = 2;
											}
											break;
							case "Ice":		if (alarm[1] <= 1)
											{
												if (!TWswitch_var)
												{
													bounce_count = instance_create_layer(x,y,"FX_Layer",obj_Iceball);
													bounce_count.owner = self;
													TWswitch_var = true;
													audio_play_sound(snd_EnergyShot,20,0);
												}
												if (instance_exists(bounce_count))
												{
													x = bounce_count.x;
													y = bounce_count.y;
													if (bounce_count.sprite_index == spr_Iceball_destroy)
													{
														sprite_index = spr_poof;
														image_xscale = 2;
														image_yscale = 2;
													}
												}
											}
											if (obj_Manticore.equipped_tw != 13)
											{
												sprite_index = spr_poof;
												image_xscale = 2;
												image_yscale = 2;
											}
											break;
						}
						break;
}

if (x > room_width) or (x < 0) sprite_index = spr_poof;

if (sprite_index == spr_poof) if (image_index > 4) instance_destroy();

if (sprite_index == spr_Explosion_bmb) or (sprite_index == spr_Explosion_RExp2)
{
	if(!audio_is_playing(snd_Bomb)) audio_play_sound(snd_Bomb,5,0);
	image_blend = c_white;
	dmg_calc(tw_hit_used,sprite_index);
	if (place_meeting(x,y,obj_BossEnemy)) tw_hit_used = true;
	if (image_index > 14) instance_destroy();
}

if (sprite_index == spr_Explosion_grnd) or (sprite_index == spr_Explosion_hmmgF) or
   (sprite_index == spr_Explosion_RExp1)
{
	dmg_calc(tw_hit_used,sprite_index);
	if (place_meeting(x,y,obj_BossEnemy)) tw_hit_used = true;
	if (image_index > 12) instance_destroy();
	if (!TWswitch_var)
	{
		if (sprite_index == spr_Explosion_RExp1)&&((bounce_count>75)&&(bounce_count<150))
		{
			audio_sound_pitch(snd_shortExplosion,random_range(0.9,1.1));
			audio_play_sound(snd_shortExplosion,19,0);
			if (!audio_is_playing(snd_DyrnwynSwing2)) audio_play_sound(snd_DyrnwynSwing2,19,0);
		}
		else
		{
			audio_sound_pitch(snd_smallExplosion,random_range(0.9,1.1));
			audio_play_sound(snd_smallExplosion,19,0);
			if (!audio_is_playing(snd_DyrnwynSwing2)) audio_play_sound(snd_DyrnwynSwing2,19,0);
		}
			
		TWswitch_var = true;
	}
}

if (image_alpha <= 0) instance_destroy();

//--> PRIORITY COLLISION
if (place_meeting(x,y,obj_EnemyBullet))
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (priority != 10)
	{
		if (priority == nearBullet.priority)
		{
			nearBullet.sprite_index = nearBullet.destroy_sprite;
			if (sprite_index == spr_Granade)
			{
				image_index = 0;
				sprite_index = spr_Explosion_grnd;
			}
			else if (sprite_index != spr_Explosion_grnd) sprite_index = spr_poof;
		}
		else
		{
			if (priority > nearBullet.priority) nearBullet.sprite_index = nearBullet.destroy_sprite;
			else
			{
				if (sprite_index == spr_Granade)
				{
					image_index = 0;
					sprite_index = spr_Explosion_grnd;
				}
				else sprite_index = spr_poof;
			}
		}
	}
	if (sprite_index == spr_Void_Ripple) && (nearBullet.priority != 10)
		nearBullet.sprite_index = nearBullet.destroy_sprite;
}