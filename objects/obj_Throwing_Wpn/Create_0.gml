/// @description Insert description here

direc = obj_Manticore.image_xscale;
TWswitch_var = 0;

bounce_count = 0;

tw_grv = 6;
tw_hsp = 37;
tw_vsp = -37;

tw_hit_used = false;

bomb_hsp = 15;
bomb_vsp = -15;
explosive_timer = 160;

alarm[0] = 170;
alarm[1] = 500;
alarm[2] = 8;
alarm[11] = 3;

kunai_Ymovement = random_range(-15,15);
kunai_angle_direc = choose (-1,1);

guillotine_max_h = y - 350;
guillotine_min_h = y + 250;
guillotine_ready2fall = false;

triblade_max_range = x + ((global.TWs[7,3]*50) + 300) * direc;
triblade_max_Y = y;
triblade_returnX = x;
triblade_speed = 0.01;

starblade_angle = 0;

orb_type = "Thunder";
orb_countdwn = 4;

seeking = false;
shuriken_curve = 0;

priority = 0;

wave = self;

if (obj_Manticore.TWammo[global.equippedTW,0]!=noone) && (obj_Manticore.TWammo[global.equippedTW,0]!=0)
	obj_Manticore.TWammo[global.equippedTW,0] -= 1;

switch(obj_Manticore.equipped_tw)
{
	case 1:		audio_sound_pitch(snd_Cut2,random_range(1,1.1));
				audio_play_sound(snd_Cut2,15,0);
				break;
				
	case 6:		audio_sound_pitch(snd_Shuriken,random_range(0.95,1));
				audio_play_sound(snd_Shuriken,15,0);
				break;
}