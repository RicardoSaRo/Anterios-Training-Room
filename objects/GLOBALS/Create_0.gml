/// @description All Global variables used in the project are generated and assigned defaul values.
randomize();

global.controller_type = 4;
global.musicVolume = 0.4;
global.tips = irandom(2);
global.results_bckgrnd = noone;

global.camera_shake = false;
global.cameraZoom = 1;

global.player_max_health = 16;
global.difficulty = "normal";
global.mountain_tier = 4;
global.seaside_tier = 3;
global.ruins_tier = 4;
global.forest_tier = 3;
global.necropolis_tier = 1;

global.cityActualRoom = rm_testroom;
global.cityPreviousRoom = rm_testroom;
global.room2Load = rm_testroom;

global.equippedSWORD = 1;
global.equippedAXE = 1;
global.equippedTW = 1;

global.MONEY = irandom_range(15000,25000); //--> Randomly selected for testing
global.SWORDS = array_create(4,0);
global.AXES = array_create(4,0);
global.TWs = array_create(16,0);
global.RELIKS = array_create(52,0);

global.HuntRoom = noone;
global.ExpRoom = noone;
global.currentManager = noone; //--> Points to current room manager
global.DragonLordRoom = 0; //--> 0 is for FireLord and 1 is for IceLord
global.Tries = 3;
global.BossVariant = false;
global.BossVariant_var = 0;

//-->> Globals for Results
enum results
{
	life = 0,
	time = 1,
	overkill = 2,
	enemy = 3
}
enum lifeOrbs
{
	perfect = 0,
	full = 1,
	empty = 2,
	broken = 3
}
enum ovrkll
{
	okdmg = 0,
	girtabfinish = 1,
	enemyMaxhp = 2
}
enum enemyInfo
{
	foename = 0,
	foevariant = 1
}
global.results = array_create(4,0);
global.results[results.life,lifeOrbs.perfect] = true;
global.results[results.life,lifeOrbs.full] = true;
global.results[results.life,lifeOrbs.empty] = 0;
global.results[results.life,lifeOrbs.broken] = 0;
global.results[results.time,0] = noone;
global.results[results.overkill,ovrkll.okdmg] = 0;
global.results[results.overkill,ovrkll.girtabfinish] = false;
global.results[results.overkill,ovrkll.enemyMaxhp] = 0;
global.results[results.enemy,enemyInfo.foename] = "";
global.results[results.enemy,enemyInfo.foevariant] = false;


//--> Creates two Arrays to control Swords and Axes
//--> First cell if the array is the name of the weapon to identify it when needed
//--> Second cell is to know if its already obtained
//--> Third cell is Quick Access On/Off
//--> Fourth cell is to know the number of weapon upgrades
for (var i = 1; i <= 4; i++)
{
	switch(i)
	{
		case 1: var swrd = "ARGENTA"; var axe = "KORUNAX";break;
		case 2: var swrd = "DYRNWYN"; var axe = "MIRAGE"; break;
		case 3: var swrd = "GIR-TAB"; var axe = "MJOLNIR"; break;
		case 4: var swrd = "FROSTBITER"; var axe = "WHISPERER"; break;
		default: var swrd = ""; var axe = ""; break;
	}
	if (i == 1)
	{
		global.SWORDS[i,0] = swrd;
		global.SWORDS[i,1] = 1;
		global.SWORDS[i,2] = true;
		global.SWORDS[i,3] = 0;
		
		global.AXES[i,0] = axe;
		global.AXES[i,1] = 1;
		global.AXES[i,2] = true;
		global.AXES[i,3] = 0;
	}
	else
	{
		if (i == 3)
		{
			global.SWORDS[i,0] = swrd;
			global.SWORDS[i,1] = 1;
			global.SWORDS[i,2] = true;
			global.SWORDS[i,3] = 0;
			
			global.AXES[i,0] = axe;
			global.AXES[i,1] = 1;//0;
			global.AXES[i,2] = true;//false;
			global.AXES[i,3] = 0;
		}
		else
		{
			global.SWORDS[i,0] = swrd;
			global.SWORDS[i,1] = 1;//0;
			global.SWORDS[i,2] = true;//false;
			global.SWORDS[i,3] = 0;
		
			global.AXES[i,0] = axe;
			global.AXES[i,1] = 1;//0;
			global.AXES[i,2] = true;//false;
			global.AXES[i,3] = 0;
		}
	}
}

//--> Creates two Arrays to control Swords and Axes
//--> First cell if the array is the name of the weapon to identify it when needed
//--> Second cell is to know if its already obtained
//--> Third is for Quick Access
//--> Fourth cell is to know the number of upgrades of the weapon
for (var j = 1; j <= 16; j++)
{
	switch(j)
	{
		case 1: var tw = "CHAKRAM"; break;
		case 2: var tw = "KUNAI"; break;
		case 3: var tw = "GRENADE"; break;
		case 4: var tw = "BOMB"; break;
		case 5: var tw = "GUILLOTINE"; break;
		case 6: var tw = "SHURIKEN"; break;
		case 7: var tw = "TRI-BLADER"; break;
		case 8: var tw = "STARBLADES"; break;
		case 9: var tw = "ASSEGAI"; break;
		case 10: var tw = "THUNDER ORB"; break;
		case 11: var tw = "WIND ORB"; break;
		case 12: var tw = "FIRE ORB"; break;
		case 13: var tw = "ICE ORB"; break;
		case 14: var tw = "REMOTE EXPLOSIVE"; break;
		case 15: var tw = "COIN LAUNCHER"; break;
		case 16: var tw = "VOID RIPPLES"; break;
		default: var tw = ""; break;
	}
	if (j == 1)
	{
		global.TWs[j,0] = tw;
		global.TWs[j,1] = 1;
		global.TWs[j,2] = true;
		global.TWs[j,3] = 0;
	}
	else
	{
		global.TWs[j,0] = tw;
		global.TWs[j,1] = 1;//0;
		global.TWs[j,2] = true;//false;
		global.TWs[j,3] = 0;
	}
}

//--> Creates an Arrays to control Reliks
//--> First cell if the array is the name of the Relik to identify it when needed
//--> Second cell is to know if its already obtained
//--> Third is to know if it is equipped
//--> Fourth cell is the stamina needed to equip

global.RELIKS[0,0] = "";
global.RELIKS[0,1] = 0;
global.RELIKS[0,2] = 0;
global.RELIKS[0,3] = 12;

for (var k = 1; k <= 52; k++)
{
	switch(k)
	{
		case 1: var rlk = "SCORPION RING"; var cost = 0; break;
		case 2: var rlk = "RELIQUARY"; var cost = 1; break;
		case 3: var rlk = "HEART CHARM"; var cost = 1; break;
		case 4: var rlk = "VAMPIRIC NECKLACE"; var cost = 2; break;
		case 5: var rlk = "ANCIENT LIFEDROP"; var cost = 2; break;
		case 6: var rlk = "COLLECTOR'S COIN"; var cost = 2; break;
		case 7: var rlk = "KORUNITE ARMGUARD"; var cost = 3; break;
		case 8: var rlk = "WIND ARMGUARD"; var cost = 3; break;
		case 9: var rlk = "ORB ENHACER"; var cost = 2; break;
		case 10: var rlk = "BATRACHITE"; var cost = 3; break;
		case 11: var rlk = "SALAMANDER EYE"; var cost = 3; break;
		case 12: var rlk = "MERMAID TEAR"; var cost = 3; break;
		case 13: var rlk = "CRYSTAL LOTUS"; var cost = 3; break;
		case 14: var rlk = "FULGURITE BELT"; var cost = 3; break;
		case 15: var rlk = "FIRELORD SPURS"; var cost = 3; break;
		case 16: var rlk = "GRYFFON ANKLET"; var cost = 2; break;
		case 17: var rlk = "HYDRA POUCH"; var cost = 3; break;
		case 18: var rlk = "ANCIENT CHARGER"; var cost = 3; break;
		case 19: var rlk = "ALCHEMIC STONE"; var cost = 2; break;
		case 20: var rlk = "ALL4ONE"; var cost = 2; break;
		case 21: var rlk = "FLAMERUBY ARMLET"; var cost = 3; break;
		case 22: var rlk = "SILVER RING"; var cost = 3; break;
		case 23: var rlk = "GOLD RING"; var cost = 4; break;
		case 24: var rlk = "NAVARATNA TALISMAN"; var cost = 4; break;
		case 25: var rlk = "DRAGIAN HONORS"; var cost = 4; break;
		case 26: var rlk = "CINTANAMI"; var cost = 5; break;
		case 27: var rlk = "RAINBOW PEARL"; var cost = 4; break;
		case 28: var rlk = "FEATHER CLOAK"; var cost = 4; break;
		case 29: var rlk = "SPEEDFLUX"; var cost = 4; break;
		case 30: var rlk = "SCORPION BROOCH"; var cost = 5; break;
		case 31: var rlk = "PHOENIX HEART"; var cost = 2; break;
		case 32: var rlk = "GOLDEN CLOVER"; var cost = 2; break;
		case 33: var rlk = "JINGLE ACORNBELL"; var cost = 4; break;
		case 34: var rlk = "TRITON'S TRUMPET"; var cost = 4; break;
		case 35: var rlk = "DREAMCATCHER"; var cost = 4; break;
		case 36: var rlk = "VOID COMPASS"; var cost = 4; break;
		case 37: var rlk = "DRAGIAN HORN"; var cost = 4; break;
		case 38: var rlk = "ABUNDANCE SIGIL"; var cost = 4; break;
		case 39: var rlk = "GNOME FIGURINE"; var cost = 2; break;
		case 40: var rlk = "SHADOW PEARL"; var cost = 3; break;
		case 41: var rlk = "DMG FLASK"; var cost = 2; break;
		case 42: var rlk = "HEALING POISON"; var cost = 1; break;
		case 43: var rlk = "ELEMENTAL SCORPION"; var cost = 2; break;
		case 44: var rlk = "VENOMLORD SCALEMAIL"; var cost = 5; break;
		case 45: var rlk = "GALLANT CREST"; var cost = 2; break;
		case 46: var rlk = "ROUGH CREST"; var cost = 2; break;
		case 47: var rlk = "SKILL CREST"; var cost = 2; break;
		case 48: var rlk = "SKULL OF AGILITY"; var cost = 5; break;
		case 49: var rlk = "SKULL OF DEXTERITY"; var cost = 5; break;
		case 50: var rlk = "SKULL OF POWER"; var cost = 5; break;
		case 51: var rlk = "VOID SIGIL"; var cost = 5; break;
		case 52: var rlk = "ANTARES' BLESSING"; var cost = 7; break;
		default: var rlk = ""; var cost = 0; break;
	}
	if (k == 1)
	{
		global.RELIKS[k,0] = rlk;
		global.RELIKS[k,1] = 1;
		global.RELIKS[k,2] = true;
		global.RELIKS[k,3] = cost;
	}
	else
	{
		global.RELIKS[k,0] = rlk;
		global.RELIKS[k,1] = 1;//0;
		global.RELIKS[k,2] = false;
		global.RELIKS[k,3] = cost;
	}
}

// Create vertex format
vertex_format_begin();
vertex_format_add_position();
vertex_format_add_texcoord();
vertex_format_add_colour();
global.wave_vf = vertex_format_end();