/// @description Insert description here
if (alarm[7] <= 5)
{
	if (alfa_bf > 0)
	{
		alfa_bf -= 0.01;
		var lay_id = layer_get_id("Black_Fade");
		var back_id = layer_background_get_id(lay_id);
		layer_background_alpha(back_id, alfa_bf);
	}
}

if (alarm[1] >= 1) //--> Light 1 effect
{
	if (ttl_light1) lgt1_alfa -= ttl_incremental; else lgt1_alfa += ttl_incremental;
	var lay_id = layer_get_id("Title_Light1");
	var back_id = layer_background_get_id(lay_id);
	layer_background_alpha(back_id, lgt1_alfa);
	lgt1_alfa = clamp(lgt1_alfa,0,1);
}

if (alarm[2] >= 1) //--> Light 2 effect
{
	if (ttl_light2) lgt2_alfa -= ttl_incremental; else lgt2_alfa += ttl_incremental;
	var lay_id = layer_get_id("Title_Light2");
	var back_id = layer_background_get_id(lay_id);
	layer_background_alpha(back_id, lgt2_alfa);
	lgt2_alfa = clamp(lgt2_alfa,0,1);
}

if (alarm[3] >= 1) //--> Light 3 effect
{
	if (ttl_light3) lgt3_alfa -= ttl_incremental; else lgt3_alfa += ttl_incremental;
	var lay_id = layer_get_id("Title_Light3");
	var back_id = layer_background_get_id(lay_id);
	layer_background_alpha(back_id, lgt3_alfa);
	lgt3_alfa = clamp(lgt3_alfa,0,1);
}

if (alarm[4] >= 1) //--> Light 4 effect
{
	if (ttl_light4) lgt4_alfa -= ttl_incremental; else lgt4_alfa += ttl_incremental;
	var lay_id = layer_get_id("Title_Light4");
	var back_id = layer_background_get_id(lay_id);
	layer_background_alpha(back_id, lgt4_alfa);
	lgt4_alfa = clamp(lgt4_alfa,0,1);
}

if (alarm[5] >= 1) //--> Light 5 effect
{
	if (ttl_light5) lgt5_alfa -= ttl_incremental; else lgt5_alfa += ttl_incremental;
	var lay_id = layer_get_id("Title_Light5");
	var back_id = layer_background_get_id(lay_id);
	layer_background_alpha(back_id, lgt5_alfa);
	lgt5_alfa = clamp(lgt5_alfa,0,1);
}