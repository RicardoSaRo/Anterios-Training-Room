/// @description Insert description here
// You can write your code in this editor
alarm[0] = 200;
alarm[1] = random_range(200,500);
alarm[2] = random_range(200,500);
alarm[3] = random_range(200,500);
alarm[4] = random_range(200,500);
alarm[5] = random_range(200,500);
alarm[6] = 200;
alarm[7] = 50;

start_key = 0;

cam_x = display_get_width();
cam_y = display_get_height();

alfa_bf = 1;
lgt1_alfa = choose(0,1); if (lgt1_alfa) ttl_light1 = true; else ttl_light1 = false;
lgt2_alfa = choose(0,1); if (lgt1_alfa) ttl_light2 = true; else ttl_light2 = false;
lgt3_alfa = choose(0,1); if (lgt1_alfa) ttl_light3 = true; else ttl_light3 = false;
lgt4_alfa = choose(0,1); if (lgt1_alfa) ttl_light4 = true; else ttl_light4 = false;
lgt5_alfa = choose(0,1); if (lgt1_alfa) ttl_light5 = true; else ttl_light5 = false;

ttl_incremental = undefined;
ttl_orb_spawn = undefined;

var title = instance_create_layer(room_width/2,(room_height/4.5),"Instances",obj_placeholder);
//var subtitle = instance_create_layer(room_width/2,(room_height/2.5),"Instances",obj_placeholder);

title_temp = title;
//subtitle_temp = subtitle;

with (title_temp)
{
	image_xscale *= 0.5;
	image_yscale *= 0.5;
	image_speed = 0;
	sprite_index = spr_title;
	image_alpha = 0;
}

version = "--TRAINING ROOM--"
/*
with (subtitle_temp)
{
	image_xscale *= 0.5;
	image_yscale *= 0.5;
	image_speed = 0.5;
	sprite_index = spr_subtitle;
	image_alpha = 0;
}*/

/*
//--> BLOOM SHADING STUFF
shader_bloom_lum		= shd_bloom_filter_luminance;
u_bloom_threshold		= shader_get_uniform(shader_bloom_lum, "bloom_threshold");
u_bloom_range			= shader_get_uniform(shader_bloom_lum, "bloom_range");

shader_blur				= shd_blur_2_pass_gauss_lerp;
u_blur_steps			= shader_get_uniform(shader_blur, "blur_steps");
u_sigma					= shader_get_uniform(shader_blur, "sigma");
u_blur_vector			= shader_get_uniform(shader_blur, "blur_vector");
u_texel_size			= shader_get_uniform(shader_blur, "texel_size");

shader_bloom_blend		= shd_bloom_blend;
u_bloom_intensity		= shader_get_uniform(shader_bloom_blend, "bloom_intensity");
u_bloom_darken			= shader_get_uniform(shader_bloom_blend, "bloom_darken");
u_bloom_saturation		= shader_get_uniform(shader_bloom_blend, "bloom_saturation");
u_bloom_texture			= shader_get_sampler_index(shader_bloom_blend, "bloom_texture");

bloom_texture			= -1;
srf_ping				= -1;
srf_pong				= -1;

gui_w					= display_get_gui_width();
gui_h					= display_get_gui_height();

app_w					= gui_w / 3;
app_h					= gui_h / 3;

texel_w					= 1 / app_w;
texel_h					= 1 / app_h;
*/

//--> "CLOUD SHADER"
steps			= 4;
col_light		= [0.9, 0.9, 0.9];
//col_dark		= [0.1, 0.1, 0.2];

var tex			= sprite_get_texture(spr_title_glow, 0);
texel_size		= [texture_get_texel_width(tex), texture_get_texel_height(tex)];

shader			= shd_cloud_fx;
u_col_light		= shader_get_uniform(shader, "col_light");
//u_col_dark		= shader_get_uniform(shader, "col_dark");
u_light_pos		= shader_get_uniform(shader, "light_pos");
u_texel_size	= shader_get_uniform(shader, "texel_size");
u_steps			= shader_get_uniform(shader, "steps");

shader2			= shd_cloud_fx;

//--> Inicialize Lights variables for Cloud shader control
old_strongest = 340;
lightshade = undefined;
strongest_light = 890;
lerping_light = undefined;

lights_pos = array_create(5,0);
lights_pos[1] = 340;
lights_pos[2] = 540;
lights_pos[3] = 670;
lights_pos[4] = 780;
lights_pos[5] = 890;
