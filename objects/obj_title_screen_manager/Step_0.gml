/// @description Insert description here
// You can write your code in this editor

gamepad_set_axis_deadzone(global.controller_type,0.4);
//start_key = max(keyboard_check(vk_enter),gamepad_button_check(global.controller_type,gp_start),0);

//if (start_key) room_goto_next();

ttl_incremental = random_range(0.001,0.01);

if (alfa_bf == 0.1) alarm[6] = 600;

if (alfa_bf <= 0.5)
{
	if (!audio_is_playing(msc_Under_the_Zenith)) audio_play_sound(msc_Under_the_Zenith,2,true);
}

if (alarm[6] <= 0)
{
	with (title_temp)
	{
		if (image_alpha < 1) image_alpha += 0.01;
		image_alpha = clamp(image_alpha,0,1);
	}
	
	if (title_temp.image_alpha == 1)
	{
		title_temp.image_speed = (title_temp.image_index <= 5) ? 1 : 0;
/*		with (subtitle_temp)
		{
			if (image_alpha < 1) image_alpha += 0.01;
			image_alpha = clamp(image_alpha,0,1);
		}*/
	}
}

ttl_orb_spawn = irandom_range(0,100);

if (ttl_orb_spawn mod 15 == 0)
{
	var rndmX = irandom_range(0,room_width);
	var rndmY = irandom_range(0,room_height);
	var light_orb = instance_create_layer(rndmX,rndmY,"Light_Orbs",obj_light_orb);
	with (light_orb)
	{
		image_alpha = 0.1;
		var rndm_scale = random_range(0.25, 1.2);
		lgt_orb_sizeX = rndm_scale;
		lgt_orb_sizeY = rndm_scale;
	}
}

//--> VARIABLES TO MOVE CLOUD SHADER SEEKING THE STRONGER LIGHT BY ITS ALPHA
lightshade = ttl_stronger_light();

switch (lightshade)
{
	case lgt1_alfa: strongest_light = 540; break;
	case lgt2_alfa: strongest_light = 780; break;
	case lgt3_alfa: strongest_light = 670; break;
	case lgt4_alfa: strongest_light = 340; break;
	case lgt5_alfa: strongest_light = 890; break;
}

old_strongest = lerp(old_strongest,strongest_light,0.1);