/// @description Insert description here
// You can write your code in this editor
//var light_pos_x		= mouse_x;
//var light_pos_y		= mouse_y;

if (alarm[7] <= 1)
{
	shader_set(shader);
	shader_set_uniform_f_array(u_texel_size, texel_size);
	shader_set_uniform_f_array(u_col_light, col_light);
	//shader_set_uniform_f_array(u_col_dark, col_dark);
	shader_set_uniform_f(u_light_pos, old_strongest, 0);
	shader_set_uniform_f(u_steps, lightshade * 5);
	draw_sprite(spr_title_art, 0, room_width/2, room_height/2);
	draw_sprite(spr_title_glow, 0, room_width/2, room_height/2);
	draw_sprite(spr_title_Chakram, 0, room_width/2, room_height/2);
	shader_set_uniform_f(u_steps, lightshade * 15);
	if (title_temp.image_alpha = 1)
		draw_sprite_ext(spr_title,title_temp.image_index,room_width/2,room_height/4.5,image_xscale*0.5,image_yscale*0.5,0,c_white,1);
//	if (subtitle_temp.image_alpha = 1)
//		draw_sprite_ext(spr_subtitle,0,room_width/2,room_height/2.5,image_xscale*0.5,image_yscale*0.5,0,c_white,1);
	shader_reset();
}

if (title_temp.image_alpha == 1)
{
	draw_set_font(Antares18);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_text(room_width/2, (room_height/2) - 50, string(version));
	draw_set_font(Antares);
	draw_text(room_width/2, (room_height/2) - 25, string("PRESS ANY KEY TO PROCEED"));
}
/*
if (subtitle_temp.image_alpha = 1)
{
	draw_set_font(Antares);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	draw_set_colour(c_black);
	draw_text(room_width/2, (room_height-55), string("PRESS ANY KEY TO PROCEED"));
	draw_set_colour(choose(c_aqua,c_teal));
	draw_text((room_width/2)+2, (room_height-55), string("PRESS ANY KEY TO PROCEED"));
	draw_set_colour(c_red);
	draw_text((room_width/2)+2, (room_height-30), "Test Prototype of Antares -Under the Zenith-");
	draw_text((room_width/2)+2, (room_height-15), "-IP property and created by Ricardo Saborio Roman-");
}
*/
if (instance_exists(obj_light_orb))
{
	with(obj_light_orb)
	{
		if (isFront)
		{
			draw_self();

			gpu_set_blendmode(bm_add);
			for(c = 0;c < 360;c += 18)
			{
				draw_sprite_ext(spr_light_orb,image_index,x+lengthdir_x(3,c),y+lengthdir_y(3,c),lgt_orb_sizeX,lgt_orb_sizeY,image_angle,image_blend,lgt_orb_alfa-0.25)
			}
			gpu_set_blendmode(bm_normal);
		}
	}
}