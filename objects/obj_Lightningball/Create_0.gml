/// @description Insert description here
// You can write your code in this editor

sprite_index = spr_Lightball_create;

lghtball_spd = global.AXES[3,3]/2;
relampRadius = (global.AXES[3,3]*50) + 200;
ballFrec = 11 - global.AXES[3,3];
relampFrec = 30 - (global.AXES[3,3]*2);

alarm[0] = obj_Manticore.current_sp_axe * 6;
if (obj_Manticore.current_sp_axe == 100)
{
	lghtball_spd += 1;
	alarm[0] += 150;
}

image_alpha = 0.5;

lghtbll_hit_used = false;