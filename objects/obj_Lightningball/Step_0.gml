/// @description Insert description here
// You can write your code in this editor

//--> Resets Special Bar
obj_Manticore.special_Mjolnir -= 4;
obj_Manticore.special_Mjolnir = clamp(obj_Manticore.special_Mjolnir,0,100);
obj_Manticore.alarm[9] = 60;

if (image_index > 12) && (sprite_index == spr_Lightball_create) //--> Transition to main animation
	sprite_index = spr_Lightball;
	
if instance_exists(obj_BossEnemy)//--> Hommes Nearest Enemy
{
	var closestTarget = instance_nearest(x,y,obj_BossEnemy);
	if (closestTarget.x > x) x += lghtball_spd;
	else x -= lghtball_spd;
	if (closestTarget.y > y) y += lghtball_spd/2;
	else y -= lghtball_spd/2;
}

if (alarm[0] mod ballFrec == 0)
	dmg_calc(lghtbll_hit_used, spr_Lightball); //--> Ball does damage

var chispa = instance_create_layer(x,y,"FX_Layer",obj_chispa); //--> Spark FX
with (chispa)
{
	chispa_colour = c_white;
	image_xscale = 1.5;
	image_yscale = 1.5;
	chispaX = random_range(-5,5);
	chispaY = random_range(-5,5);
}


if (alarm[0] mod relampFrec == 0) && (sprite_index == spr_Lightball) &&
   (distance_to_object(obj_BossEnemy)<relampRadius)
{
	if (!place_meeting(x,y,obj_relampago))
	{
		var relampago = instance_create_layer(x,y,"Back_FX_Layer",obj_relampago);
		relampago.image_angle = point_direction(x,y,obj_BossEnemy.x,obj_BossEnemy.y)
		relampago.sprite_index = (global.AXES[3,3]==1) ? spr_relampago2 : ((global.AXES[3,3]==2) ? spr_relampago3 : ((global.AXES[3,3]==3) ? spr_relampago4 : spr_relampago));
	}
}

if (alarm[0] <= 1)
{
	if (sign(image_xscale) < 0)
	{
		image_xscale = 1;
		image_yscale = 1;
	}
	
	image_alpha -= 0.05;
	image_xscale -= 0.05;
	image_yscale -= 0.05;
	
	audio_stop_sound(snd_LightningBall);
	if (!audio_is_playing(snd_LightningBall_destroy)) audio_play_sound(snd_LightningBall_destroy,20,0);
}
else if (!audio_is_playing(snd_LightningBall)) audio_play_sound(snd_LightningBall,25,0);

if (image_alpha <= 0) instance_destroy();