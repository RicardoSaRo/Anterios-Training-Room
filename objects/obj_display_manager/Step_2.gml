/// @description End Step camera positions
// Camera is positioned at the End Step to prevent stutter movement from some objects. This varies depending of the current room.

switch (room)
{
	case rm_title:
					var _round = global.view_w/surface_get_width(application_surface);
					camera_set_view_size(view_camera[0],round_n(room_width,_round),round_n(room_height,_round));
					camera_set_view_pos(view_camera[0],0,0);
					break;
	case rm_Loader:
					display_set_gui_size(1366,768);
					break;
	case rm_testroom:
					var _round = global.view_w/surface_get_width(application_surface);
					var _roundY = global.view_h/surface_get_height(application_surface);
					var curX = camera_get_view_x(view_camera[0]);
					var curY = camera_get_view_y(view_camera[0]);
					global.view_x_staticlerp = lerp(curX,round_n(global.view_x,_round),0.5);
					camera_set_view_size(view_camera[0],ideal_width*global.cameraZoom,ideal_height*global.cameraZoom);
					camera_set_view_pos(view_camera[0],lerp(curX,round_n((global.view_x),_round),0.1),lerp(curY,round_n((global.view_y),_roundY),0.5));
					break;
}

//--> For shake camera Effect
if (global.camera_shake == true)
{
	var rndmX = random_range(-global.cam_shake_amount,global.cam_shake_amount);
	var rndmY = random_range(-global.cam_shake_amount,global.cam_shake_amount);
	camera_set_view_pos(view_camera[0],round_n(global.view_x,_round)+rndmX,round_n(global.view_y,_round)+rndmY);
}

