/// @description Step Event
// You can write your code in this editor


//--> Zoom for testing
var plusKey = keyboard_check_pressed(vk_add);
var minusKey = keyboard_check_pressed(vk_subtract);

if (plusKey) && (!minusKey) global.cameraZoom += 0.01;
if (!plusKey) && (minusKey) global.cameraZoom -= 0.01;

//Camera Follows Player
if instance_exists(global.cam_follow)
{
	xTo = global.cam_follow.x;
	yTo = global.cam_follow.y;
	
	if (global.option_camera == 1)
	{	
		global.view_x = global.cam_follow.x - (global.vsizeW*global.cameraZoom)/2;
		global.view_y = global.cam_follow.y - (global.vsizeH*global.cameraZoom)/2;
	}

	if (global.option_camera == 2)
	{
		var diffX = xTo - global.view_x;
		var diffY = yTo - global.view_y;
		if (abs(diffX) < 1) global.view_x = xTo;
		else global.view_x = diffX/wide_view;//--> Wide View
		if (abs(diffY) < 1) global.view_y = xTo;
		else global.view_y = diffX/wide_view;//--> Wide View
	}
}
else //--> If there is no player to follow, sets the camera in the center of the display
{
		global.view_x += global.view_w/2;
		global.view_y += global.view_h/2;
}


//--> Prevents the view to go beyond the room limits
global.view_x=clamp(global.view_x,0,room_width-(global.vsizeW));
global.view_y=clamp(global.view_y,0,room_height-(global.vsizeH));
