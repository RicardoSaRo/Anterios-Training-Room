/// @description Display Properties
// obj_display_manager
// Is a persistent object that manages resolution and camera movement
// Currently, the display is not dynamic and set to 1920 x 1080 for testing purposes.


ini_Optns(); //--> Loads player preferences or if there are none, creates default options

var start_video_optns = array_create(5,0);
start_video_optns = load_videoOptns();
DWinMode = start_video_optns[1];
DVsync = start_video_optns[2];
DAlfaBlend = start_video_optns[3];
DResW = start_video_optns[4];
DResH = start_video_optns[5];

//--> Gets display dimensions
display_width=display_get_width();
display_height=display_get_height();

//--> Resolutions
if (DResW < display_width) DResW = display_width;
if (DResH < display_height) DResW = display_height;
ideal_width=1920;
ideal_height=1080;
zoom=1;

//--> Globals for camera usage during the game
global.cam_follow = obj_Manticore;
global.option_camera = 1;
global.cam_shake_amount = 20;

//--> ASpect Ratio Calculations
aspect_ratio = display_width/display_height;
ideal_width = round(ideal_height*aspect_ratio);
ideal_height = round(ideal_width / aspect_ratio);

//Perfect Pixel Scaling
if(display_width mod ideal_width != 0)
{
  var d = round(display_width/ideal_width);
  ideal_width=display_width/d;
}
if(display_height mod ideal_height != 0)
{
  var d = round(display_height/ideal_height);
  ideal_height=display_height/d;
}

//Check for odd numbers
if(ideal_width & 1)
  ideal_width++;
if(ideal_height & 1)
  ideal_height++;
    
window_set_size(ideal_width*zoom,ideal_height*zoom);

//--> Creates application surface
surface_resize(application_surface,1920,1080);
display_set_gui_size(1920,1080);

alarm[1] = 1;
  
alarm[0] = 2;

camera = camera_create();

global.view_x = 0;
global.view_y = 0;
global.view_w = ideal_width;
global.view_h = ideal_height;
global.vsizeW = ideal_width; 
global.vsizeH = ideal_height; 

global.camCenter = false;

room_goto_next();
