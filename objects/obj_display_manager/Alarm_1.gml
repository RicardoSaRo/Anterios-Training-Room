/// @description Inicialize Video Options
//--> Inicialize Video Options
switch(DWinMode)
{
	case 1:	window_set_fullscreen(true);
			break;
	
	case 2: if (window_set_fullscreen(true)) window_set_fullscreen(false);
			break;
	
	case 3: window_set_fullscreen(false);
			window_set_size(display_get_width(),display_get_height());
			break;
}

var aa = (display_aa < 2) ? 0 : ((display_aa < 4) ? 2 : ((display_aa < 8) ? 4 : 8));
switch(DVsync)
{
	case 1: display_reset(0,false); break;
	case 2: display_reset(0,true); break;
}

switch(DAlfaBlend)
{
	case 1: gpu_set_alphatestenable(true);
			gpu_set_alphatestref(1);
			break;
			
	case 2: gpu_set_alphatestenable(true);
			gpu_set_alphatestref(16);
			break;
			
	case 3:	gpu_set_alphatestenable(true);
			gpu_set_alphatestref(32);
			break;
}