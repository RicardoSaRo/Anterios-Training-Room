/// @description Insert description here
// You can write your code in this editor

view_camera[0]=camera;
view_enabled=true;
view_visible[0]=true;

view_w_half = camera_get_view_width(camera) * 0.5;
view_h_half = camera_get_view_height(camera) * 0.5;

xTo = xstart;
yTo = ystart;

global.cam_shake_amount = 20;
global.camera_shake = false;

//--> If Manticore exists at the Room Start, positions the view over him
//if (instance_exists(obj_Manticore))	camera_set_view_pos(camera,obj_Manticore.x,obj_Manticore.y);

switch(room)
{
	case rm_testroom:
		global.vsizeW = ideal_width;//1366;
		global.vsizeH = ideal_height;//768;
		surface_resize(application_surface,global.vsizeW*zoom,global.vsizeH*zoom);
		//--> ZOOM HERE AND IN ARTIST SURFS TO FIX SHADER EFFECTS ***********************************
		break;
}

alarm[0] = 1;
