/// @description Insert description here
// You can write your code in this editor

switch(room)
{
	case rm_testroom:	priority = 1; 
						if (place_meeting(x,y,obj_Manticore_hitbox)) && (obj_Manticore.atk_type == 2)
							sprite_index = destroy_sprite;
						break;
}

if (sprite_index == spr_whitepoof)
{
	image_xscale = 4;
	image_yscale = 4;
	if (image_index >= 4) instance_destroy();
}
else
{
	x += spblltSPD * image_xscale;
	
	if (place_meeting(x,y,obj_Manticore))
	{
		with (obj_Manticore)
		{
			if (sprite_index != spr_Parry)
			{
				alarm[6] = 30;
				alarm[11] = 0;
				state = stun_Manticore;
				hsp = 0;
				//if (room == rm_Exp_Meadows1) x += 60 * other.image_xscale;
			}
		}
		
		image_index = 0;
		sprite_index = spr_whitepoof;
	}
}

image_angle += 30 * image_xscale;

if (place_meeting(x,y,obj_GraniteShield)) || (place_meeting(x,y,obj_FrostBulwark)) || (place_meeting(x,y,obj_Wall)) sprite_index = spr_whitepoof;