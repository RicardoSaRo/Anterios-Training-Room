/// @description Insert description here
// You can write your code in this editor

//draw_set_alpha(obj_WeaponsMenu.image_xscale/1.1);

//draw_sprite(avatar2display_SWRD,0,265, menu_view_h-565);
//draw_sprite(avatar2display_AXE,0,631, menu_view_h-565);

var _round = global.view_w/surface_get_width(application_surface);
camera_set_view_pos(view_camera[0],round_n(global.view_x,_round),round_n(global.view_y,_round));


//draw_set_alpha(1);

draw_self();

if (!closing_menu)
{
	draw_set_font(Antares18);
	draw_set_colour(c_green);
	draw_text(centerX, centerY-180, "TRANING BOT");
	draw_text(centerX, centerY-156, "ver 3.0.1");
	
	var movPos = (moveOptions == 1);
	var chsnOptn = (movPos) ? c_lime : c_green;
	draw_text_color(centerX-80,centerY-96,"Robot Active:",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1);
	var choosing = (movPos) ? selectionMain : selecBotON;
	switch(choosing)
	{
		case 1: draw_text_color(centerX+160,centerY-96,"= OFF >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 2: draw_text_color(centerX+160,centerY-96,"< ON =",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
	}
	
	var movPos = (moveOptions == 2);
	var chsnOptn = (movPos) ? c_lime : c_green;
	draw_text_color(centerX-80,centerY-36,"Robot Attack Speed:",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1);
	var choosing = (movPos) ? selectionMain : selecAtkSpd;
	switch(choosing)
	{
		case 1: draw_text_color(centerX+160,centerY-36,"= OFF >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 2: draw_text_color(centerX+160,centerY-36,"< 1 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 3: draw_text_color(centerX+160,centerY-36,"< 2 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 4: draw_text_color(centerX+160,centerY-36,"< 3 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 5: draw_text_color(centerX+160,centerY-36,"< 4 =",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
	}
	
	var movPos = (moveOptions == 3);
	var chsnOptn = (movPos) ? c_lime : c_green;
	draw_text_color(centerX-80,centerY+36,"Robot Movement Speed:",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1);
	var choosing = (movPos) ? selectionMain : selecMovSpd;
	switch(choosing)
	{
		case 1: draw_text_color(centerX+160,centerY+36,"= OFF >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 2: draw_text_color(centerX+160,centerY+36,"< 1 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 3: draw_text_color(centerX+160,centerY+36,"< 2 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 4: draw_text_color(centerX+160,centerY+36,"< 3 >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 5: draw_text_color(centerX+160,centerY+36,"< 4 =",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
	}
	
	var movPos = (moveOptions == 4);
	var chsnOptn = (movPos) ? c_lime : c_green;
	draw_text_color(centerX-80,centerY+96,"Stun Touch:",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1);
	var choosing = (movPos) ? selectionMain : selecTouchStun;
	switch(choosing)
	{
		case 1: draw_text_color(centerX+160,centerY+96,"= OFF >",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
		case 2: draw_text_color(centerX+160,centerY+96,"< ON =",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1); break;
	}
	
	var movPos = (moveOptions == 5);
	var chsnOptn = (movPos) ? c_lime : c_green;
	draw_text_color(centerX,centerY+170,"EXIT",chsnOptn,chsnOptn,chsnOptn,chsnOptn,1);
}