/// @description Insert description here
// You can write your code in this editor

instance_activate_all();
obj_BossEnemy.bounce = obj_TBot_CPU_spawner.bounce;
obj_BossEnemy.AtkTurn = obj_TBot_CPU_spawner.AtkTurn;
obj_BossEnemy.right_var = obj_TBot_CPU_spawner.right_var;
obj_BossEnemy.left_var = obj_TBot_CPU_spawner.left_var;
	
//--> Normal Music Volumen and all sound reactivation
if (global.currentManager != noone)
{
	var rm_mngr = global.currentManager;
	var msc_gain = audio_sound_get_gain(rm_mngr.levelMusic);
	audio_sound_gain(rm_mngr.levelMusic,msc_gain*2,0);
}

audio_resume_all();