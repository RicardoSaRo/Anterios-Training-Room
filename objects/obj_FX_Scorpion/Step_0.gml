/// @description Insert description here
image_alpha -= 0.005;

if (image_xscale > 0) image_xscale += 0.01;
else image_xscale -= 0.01;

image_yscale += 0.01;

x = obj_Manticore.x;
y = obj_Manticore.y;

if (image_alpha = 0) instance_destroy();