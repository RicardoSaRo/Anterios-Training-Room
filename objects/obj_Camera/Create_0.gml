/// @description Insert description here
zoom = 1;

ideal_width = display_get_width()/zoom;
ideal_height = display_get_height()/zoom;

surface_resize(application_surface,ideal_width,ideal_height);
display_set_gui_size(ideal_width,ideal_height);

camera = camera_create();

camera_set_view_size(camera,ideal_width,ideal_height);
camera_set_view_pos(camera,x - (ideal_width/2), y - (ideal_height/2));

window_set_size(display_get_width(),display_get_height());
window_set_rectangle(0,0,display_get_width(),display_get_height());

room_goto_next();