/// @description Insert description here
// You can write your code in this editor

camera_set_view_size(camera,global.view_x,global.view_y);

x = clamp(x,0,room_width-ideal_width/2);
y = clamp(y,0,room_height-ideal_height/2);

//--> Update Camera View
camera_set_view_pos(camera,x-view_w_half,y-view_h_half);