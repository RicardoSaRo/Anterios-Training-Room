/// @description Insert description here
// You can write your code in this editor

if (alarm[0] > 3)  and (alarm[0] mod 10 == 0)
{
		var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
}

x += random_range(-4,4);
y += random_range(-4,4);

flame_colour = (global.RELIKS[40,2]) ? c_purple : choose(c_lime,c_purple);
if (image_blend != c_purple) image_blend = choose(c_lime,c_green);

if (alarm[0] mod 6 == 0) dmg_calc(wow_hit_used,sprite_index);

if (alarm[0] <= 0)
{
	if image_alpha > 0 image_alpha -= 0.05;
	else instance_destroy();
}

//--> PRIORITY CHECK!!!
if (place_meeting(x,y,obj_EnemyBullet))
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (priority != 10)
		if (nearBullet.priority <= priority) nearBullet.sprite_index = nearBullet.destroy_sprite;
}