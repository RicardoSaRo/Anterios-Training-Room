/// @description Insert description here
// You can write your code in this editor

//--> Assigns sprites to menu options with global variables storing weapons info

if (obj_WeaponsMenu.closing_menu)
{
	image_alpha -= 0.2;
	if (image_alpha <= 0) instance_destroy();
}
else
{
	if (obj_WeaponsMenu.image_xscale >= 1) && (image_alpha < 1)
	{
		image_alpha += 0.2;
	}
}

switch(sword)
{
	case 1: wpnname = "ARGENTA";
			if (global.SWORDS[sword,1] == 1)
			{
				if (global.equippedSWORD == sword) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.SWORDS[sword,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 2: wpnname = "DYRNWYN";
			if (global.SWORDS[sword,1] == 1)
			{
				if (global.equippedSWORD == sword) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.SWORDS[sword,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 3: wpnname = "GIR-TAB";
			if (global.SWORDS[sword,1] == 1)
			{
				if (global.equippedSWORD == sword) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.SWORDS[sword,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 4: wpnname = "FROSTBITER";
			if (global.SWORDS[sword,1] == 1)
			{
				if (global.equippedSWORD == sword) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.SWORDS[sword,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
}

switch(axe)
{
	case 1: wpnname = "KORUNAX";
			if (global.AXES[axe,1] == 1)
			{
				if (global.equippedAXE == axe) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.AXES[axe,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 2: wpnname = "MIRAGE";
			if (global.AXES[axe,1] == 1)
			{
				if (global.equippedAXE == axe) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.AXES[axe,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 3: wpnname = "MJOLNIR";
			if (global.AXES[axe,1] == 1)
			{
				if (global.equippedAXE == axe) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.AXES[axe,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 4: wpnname = "WHISPERER";
			if (global.AXES[axe,1] == 1)
			{
				if (global.equippedAXE == axe) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.AXES[axe,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
}

switch(tw)
{
	case 1: wpnname = "CHAKRAM";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 2: wpnname = "KUNAI";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 3: wpnname = "GRANADE";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 4: wpnname = "BOMB";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 5: wpnname = "GUILLOTINE";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 6: wpnname = "SHURIKEN";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 7: wpnname = "TRI-BLADER";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 8: wpnname = "STARBLADES";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 9: wpnname = "ASSEGAI";
			if (global.TWs[tw,1] == 1)
			{
				if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
				else sprite_index = spr_Weapon_option;
			}
			else sprite_index = spr_Weapon_empty;
			if (global.TWs[tw,2]) QuickAccess = true;
			else QuickAccess = false;
			break;
	case 10: wpnname = "THUNDER ORB";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 11: wpnname = "WIND ORB";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 12: wpnname = "FIRE ORB";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 13: wpnname = "ICE ORB";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 14: wpnname = "REMOTE EXPLOSIVE";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 15: wpnname = "COIN LAUNCHER";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
	case 16: wpnname = "VOID RIPPLES";
			 if (global.TWs[tw,1] == 1)
			 {
			 	if (global.equippedTW == tw) sprite_index = spr_Weapon_equipped;
		 		else sprite_index = spr_Weapon_option;
			 }
			 else sprite_index = spr_Weapon_empty;
			 if (global.TWs[tw,2]) QuickAccess = true;
			 else QuickAccess = false;
			 break;
}

//--> Used to write name if weapon is already obtained
if (sprite_index == spr_Weapon_empty) WpnObtained = false;
else WpnObtained = true;

//--> This checks if the redguide is in swords,axes and tws and compares if the obj is one of them
if (sword > 0) && (obj_WeaponsMenu.moveOptions[1,1] == "SWORD") wpntype = true;
else
{
	if (axe > 0) && (obj_WeaponsMenu.moveOptions[1,1] == "AXE") wpntype = true;
	else
	{
		if (tw > 0) && (obj_WeaponsMenu.moveOptions[1,1] == "TW") wpntype = true;
		else wpntype = false;
	}
}

//--> checks if the movement in the menu is the same as the obj position, that means redguide is on it
if (wpntype)
{
	var wpnnumber = max(sword,axe,tw,0);
	if (wpnnumber == obj_WeaponsMenu.moveOptions[1,2]) guideisHere = true;
	else guideisHere = false;
}
else guideisHere = false;


//--> For Blinking effect of the redguide
if (guideisHere)
{
	counter += 1;
	if (counter mod 4 == 0) guidecolor = c_gray;
	if (counter mod 8 == 0) guidecolor = c_white;
	if (counter > 1000) counter = 0;
}

//--> Border Shine Lvl3 Upgrade
if (wpnLevel >= 0)
borderShine += 0.1;
if (borderShine >= 6) borderShine = 0;