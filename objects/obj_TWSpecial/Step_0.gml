/// @description Insert description here
// You can write your code in this editor

sptw_key_a = max(keyboard_check_pressed(ord("A")),gamepad_button_check_pressed(global.controller_type,gp_shoulderrb),0);
sptw_keyhold_a = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);

if (Consum)
{
	ammo_consumption();
	Consum = false;
}

if (sprite_index == spr_ChakramSP) || (sprite_index == spr_Whirlwind)//--> MEGA CHAKRAM
{
	if (!audio_is_playing(snd_MegaChakram)) audio_play_sound(snd_MegaChakram,20,0);
	angle += 15;
	orbit += (sprite_index == spr_ChakramSP) ? 10 : 5;
	if (!instance_exists(instnc)) instnc = self;
	x = instnc.x + lengthdir_x(orbit,angle);
	y = instnc.y + lengthdir_y(orbit,angle);
	
	if (alarm[1] > 0) orbit = clamp(orbit,0,200);
	else image_alpha -= 0.02;
	
	var chispa = instance_create_layer(x+(random_range(-15,15)),y+(random_range(-15,15)),"FX_Layer",obj_chispa);
		chispa.chispa_colour = (sprite_index == spr_ChakramSP) ? c_gray : choose(c_lime,merge_color(c_lime,c_yellow,0.5));
		if (image_blend = make_color_rgb(74,109,168)) chispa.chispa_colour = make_color_rgb(74,109,168);
		var rndmSpark = random_range(1,4);
		chispa.image_xscale = rndmSpark;
		chispa.image_yscale = rndmSpark;
	
	if (image_alpha <= 0) instance_destroy();
}

if (sprite_index == spr_KunaiSP) //--> KUNAI RAIN
{	
	if (image_alpha < 1)
	{
		image_alpha += 0.1;
		if (!angle)//--> angle var is used to show smokeFX one per kunai
		{
			repeat(10)
			{
				var balls = instance_create_layer(x+random_range(-15,15),y+random_range(-15,15),"Info_Layer",obj_fadeAway)
				balls.sprite_index = spr_GlowingBall;
				balls.image_blend = (global.TWs[2,3]==0) ? c_white : ((global.TWs[2,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
									((global.TWs[2,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
				balls.Ymovement = choose(-1,1);
				balls.Xmovement = choose(-1,1);
				balls.fadeSpd = 0.03;
			}
			audio_sound_pitch(snd_KunaiSP,random_range(0.9,1.1));
			audio_play_sound(snd_KunaiSP,25,0);
		}
		angle = true;
	}
	else
	{
		x += 15 * image_xscale;
		y += 15;
		
		if (place_meeting(x,y,obj_BossEnemy))
		{
			if (sprite_index == spr_KunaiSP)
			{
				dmg_calc(sptw_hit_used,sprite_index);
				sprite_index = spr_KunaiSPdestroy;
				amount = choose(1,-1);
				image_index = 0;
			}
		}
	}
	
	if (image_alpha >= 1) && (!orbit) //--> Orbit is used to prevent mass kunai spawn
	{
		var dist = (global.TWs[2,3]==0) ? 150 : ((global.TWs[2,3]==1) ? 125 : ((global.TWs[2,3]==2) ? 100 : 75));
		var Kunai = instance_create_layer(x+(dist*image_xscale),y-(200*switch_var),"Info_Layer",obj_TWSpecial);
		Kunai.sprite_index = spr_KunaiSP;
		Kunai.image_xscale = image_xscale;
		Kunai.image_alpha = 0.1;
		Kunai.switch_var = -switch_var;
		
		orbit = true;
	}
	
	var chispa = instance_create_layer(x+(random_range(-5,5)),y+(random_range(-5,5)),"FX_Layer",obj_chispa);
	chispa.chispa_colour = c_gray;
	var rndmSpark = random_range(1,4);
	chispa.image_xscale = rndmSpark;
	chispa.image_yscale = rndmSpark;
		
	if (y < 0) || (x < 0) || (x > room_width) instance_destroy();
}
if (sprite_index == spr_KunaiSPdestroy) //--> Destroy Kunais
{
	y -= 1;
	x += amount;
	image_angle += amount * 10;
	
	if (image_index >= 3.6) instance_destroy();
}

if (sprite_index == spr_Explosion_grnd)
{
	if (image_index < 1)
	{
		if (!audio_is_playing(snd_Grenade1))
		{
			audio_sound_pitch(snd_Grenade1,random_range(1.5,1.8))
			audio_play_sound(snd_Grenade1,20,0);
		}
		else
		{
			if (!audio_is_playing(snd_Grenade2))
			{
				audio_sound_pitch(snd_Grenade2,random_range(1.5,1.8))
				audio_play_sound(snd_Grenade2,20,0);
			}
			else
			{
				if (!audio_is_playing(snd_Grenade3))
				{
					audio_sound_pitch(snd_Grenade3,random_range(1,1.1))
					audio_play_sound(snd_Grenade3,20,0);
				}
			}
		}
		
		global.camera_shake = true;
	}
	
	if (image_index > 8) global.camera_shake = false;
	
	if (image_index >= 10) && (!switch_var) && (amount > 0)
	{
		var GCX = obj_Manticore.x + random_range(-600,600);
		var GCY = obj_Manticore.y + random_range(-250,250);
		if (GCX < obj_Manticore.Boundary_minX) GCX = (obj_Manticore.Boundary_minX + random_range(0,250));
		if (GCX > obj_Manticore.Boundary_maxX) GCX = (obj_Manticore.Boundary_maxX - random_range(0,250));
		if (GCY < obj_Manticore.Boundary_minY) GCY = (obj_Manticore.Boundary_minY + random_range(0,150));
		if (GCY > obj_Manticore.Boundary_maxY) GCY = (obj_Manticore.Boundary_maxY - random_range(0,150));
		var GCluster = instance_create_layer(GCX,GCY,"Info_Layer",obj_TWSpecial);
		GCluster.sprite_index = spr_Explosion_grnd;
		GCluster.image_xscale = 1.5;
		GCluster.image_yscale = 1.5;
		GCluster.alarm[0] = 10 - global.TWs[3,3];
		GCluster.amount = amount - 1;
		
		switch_var = true;
	}
	
	if (image_index >= 12) instance_destroy();
}

if (sprite_index == spr_BombSP) //--> MEGA BLAST
{
	image_xscale += 0.2;
	image_xscale = clamp(image_xscale,0,3.5+(global.TWs[4,3]*1.5));
	image_yscale += 0.2;
	image_yscale = clamp(image_yscale,0,3.5+(global.TWs[4,3]*1.5));
	image_angle = irandom(360);
	
	if (!audio_is_playing(snd_MegaBomb)) audio_play_sound(snd_MegaBomb,5,0);
	global.camera_shake = true;
	
	if (instance_exists(obj_fadeAway))
	{
		with(obj_fadeAway)
		{
			if (sprite_index == spr_ExpSlash)
			{
				image_xscale = other.image_xscale * (sign(image_xscale)*2);
				image_yscale = other.image_yscale * 1.5;
			}
		}
	}
	
	if (place_meeting(x,y,obj_BossEnemy))
	{
		var int_instance = instance_place(x,y,obj_BossEnemy);
		if (!int_instance.specialTW_var)
		{
			dmg_calc(sptw_hit_used,sprite_index);
			int_instance.specialTW_var = true;
		}
	}
	
	if (image_index >= 12.6) instance_destroy();
}

if (sprite_index == spr_GuillotineSP) //--> MEGA GUILLOTINE
{
	if (!switch_var)
	{
		min_h += global.TWs[5,3] * 50;
		switch_var = true;
	}
	if (place_meeting(x,y,obj_BossEnemy))
	{
		dmg_calc(sptw_hit_used,sprite_index);
		sptw_hit_used = true;
	}
	if (alarm[0] >= 2.4) sptw_hit_used = false;
	if (!amount)
	{
		x += hsp * image_xscale;
		y -= 15;
		image_angle -= 20 * image_xscale;
		if (y <= max_h) amount = true;
	}
	else
	{
		if (y < max_h + 20)
		{
			y += vsp;
			x += hsp * image_xscale;
			image_angle -= 20 * image_xscale;
		}
		else
		{
			x += hsp * image_xscale;
			y += 15;
			image_angle -= 30 * image_xscale;
		}
	}
	if ((y > min_h)&&(amount)) instance_destroy();
	//--> SoundFX
	if (!audio_is_playing(snd_Guillotine1))
	{
		audio_sound_pitch(snd_Guillotine1,random_range(0.7,1.3));
		if (!audio_is_playing(snd_Guillotine1)) soundGain_by_distance(obj_Manticore,100,0.8,snd_Guillotine1,20,0);
	}
	else
	{
		if (!audio_is_playing(snd_Guillotine2))
		{
			audio_sound_pitch(snd_Guillotine2,random_range(0.7,1.3));
			if (!audio_is_playing(snd_Guillotine2)) soundGain_by_distance(obj_Manticore,100,0.8,snd_Guillotine2,20,0);
		}
		else
		{
			audio_sound_pitch(snd_Guillotine3,random_range(0.7,1.3));
			if (!audio_is_playing(snd_Guillotine3)) soundGain_by_distance(obj_Manticore,100,0.8,snd_Guillotine3,20,0);
		}
	}
}

if (sprite_index == spr_ShurikenSP) //--> MEGA SHURIKENS
{
	speed = amount * image_xscale;
	image_angle -= image_xscale * 40;
	
	if (switch_var)	vsp += grv;
	else vsp -= grv;
	
	if (vsp == 23) switch_var = false;
	if (vsp == -23) switch_var = true;
	
	y += vsp;
	
	if (x > room_width) or (x < 0) instance_destroy();
	
	if (!audio_is_playing(snd_MegaShuriken))
	{
		audio_sound_pitch(snd_MegaShuriken,random_range(1,1.2));
		soundGain_by_distance(obj_Manticore,200,0.8,snd_MegaShuriken,20,0);
	}
}

if (place_meeting(x,y,obj_EnemyBullet)) //--> Priority vs Enemy Bullets
{
	var nearBullet = instance_nearest(x,y,obj_EnemyBullet);
	
	if (nearBullet.priority != 10) nearBullet.sprite_index = nearBullet.destroy_sprite;
}

if (sprite_index == spr_TribladerSP) //--> TRIBLADER SHIELD
{
	if (sptw_keyhold_a) //--> SoundFX
	{
		audio_stop_sound(snd_TribladerShieldSLOW);
		if (!audio_is_playing(snd_TribladerShieldFAST)) audio_play_sound(snd_TribladerShieldFAST,17,0);
	}
	else
	{
		audio_stop_sound(snd_TribladerShieldFAST);
		if (!audio_is_playing(snd_TribladerShieldSLOW)) audio_play_sound(snd_TribladerShieldSLOW,17,0);
	}
	
	angle += (switch_var) ? -5 : ((sptw_keyhold_a) ? 15 : 5);
	if (orbit < 120) orbit += 5;
	else 
	{
		orbit += (switch_var) ? -5 : ((sptw_keyhold_a) ? 7 : -5);
		orbit = clamp(orbit,120,300);
	}
	x = obj_Manticore.x + lengthdir_x(orbit,angle)
	y = obj_Manticore.y + lengthdir_y(orbit,angle)
	image_angle += 40 * image_xscale;
	
	if (switch_var)	if (place_meeting(x,y,obj_Manticore)) instance_destroy();
	
	if (alarm[1] <= 0) instance_destroy();
}

if (sprite_index == spr_StarbladesSP) //--> STARBLADES MAYHEM
{
	angle += 2;
	orbit += 10;
	x = obj_Manticore.x + lengthdir_x(orbit,angle)
	y = obj_Manticore.y + lengthdir_y(orbit,angle)
	image_angle += angle;
	
	if (!audio_is_playing(snd_StarbladesSP)) audio_play_sound(snd_StarbladesSP,25,0);
	
	if (alarm[1] > 0) orbit = clamp(orbit,0,100);
	else image_alpha -= 0.02;
	
	var chispa = instance_create_layer(x+(random_range(-15,15)),y+(random_range(-15,15)),"FX_Layer",obj_chispa);
	chispa.chispa_colour = (global.TWs[8,3]==0) ? c_white : ((global.TWs[8,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
						   ((global.TWs[8,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
	var rndmSpark = random_range(1,4);
	chispa.image_xscale = rndmSpark;
	chispa.image_yscale = rndmSpark;
	
	if (image_alpha <= 0) instance_destroy();
}

if (sprite_index == spr_AssegaiSP)	
{
	grv = 1.5;
	vsp += grv;
	
	if (!switch_var)
	{
		repeat(10)
		{
			var balls = instance_create_layer(x+random_range(-15,15),y+random_range(-15,15),"Info_Layer",obj_fadeAway)
			balls.sprite_index = spr_GlowingBall;
			balls.Ymovement = choose(-1,1);
			balls.Xmovement = choose(-1,1);
			balls.image_blend = (global.TWs[9,3]==0) ? c_white : ((global.TWs[9,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
								((global.TWs[9,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
			balls.fadeSpd = 0.03;
		}
		
		switch_var = true;
	}
	
	var enmyHit = place_meeting(x,y,obj_BossEnemy);
	var enmyHB_hit = place_meeting(x,y,obj_Enemy_hurtbox);
	var wallHit = place_meeting(x,y,obj_Wall);
	if (enmyHit) or (enmyHB_hit) or (wallHit)
	{
		if (!sptw_hit_used)
		{
			audio_stop_sound(snd_Assegai);
			if ((enmyHit)||(enmyHB_hit)) if (!audio_is_playing(snd_Assegai_hit)) audio_play_sound(snd_Assegai_hit,17,0);
			else if (wallHit) audio_play_sound(snd_Clank2,17,0);
		}
		hsp = 0;
		vsp += 1;
		image_angle += choose(1,2) * image_xscale;
		image_alpha -= 0.03;
		dmg_calc(sptw_hit_used,sprite_index);
		sptw_hit_used = true;
	}
	else
	{
		x += hsp * image_xscale;
		y += vsp;
		if (image_alpha < 1) image_alpha += 0.2;
		image_angle = vsp * -image_xscale;
		if (alarm[0] <=1) angle += 1;
		if (angle > 10)
			if (alarm[0] <=1) angle +=2;
		if (angle > 40)
			if (alarm[0] <=1) angle +=3;
	}
	
	if (alarm[1] mod 3 == 0)
	{
		if (sprite_index == spr_AssegaiSP)
		{
			var ShotFX2 = instance_create_layer(x,y,"FX_Layer",obj_centerXYfollow);
			ShotFX2.sprite_index = spr_DL_ShotFX;
			ShotFX2.image_xscale = 0.5 * image_xscale;
			ShotFX2.image_yscale = 0.5;
			ShotFX2.image_blend = (global.TWs[9,3]==0) ? c_white : ((global.TWs[9,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
								   ((global.TWs[9,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
			ShotFX2.brightness = choose(false,true);
			ShotFX2.glow = choose(false,true);
			ShotFX2.image_speed = random_range(1,1.5);
			ShotFX2.anim_end = 5;
			ShotFX2.image_alpha = random_range(0.2,0.5);
		}
	}
	
	if (image_alpha <= 0) || (alarm[1] <= 0) instance_destroy();
}

if (sprite_index == spr_1OrbSP) || (sprite_index == spr_2OrbsSP)
{
	if (image_index >= 8.6)
	{
		if (global.TWs[equipped_tw,3]==3)
		{
			image_xscale *= 1.5;
			image_yscale *= 1.5;
			switch(orbTyp)
			{
				case "T": sprite_index = spr_ThunderFamiliar_Create; break;
				case "W": sprite_index = spr_WindFamiliar_Create; image_blend = c_white; break;
				case "F": sprite_index = spr_FireFamiliar_Create; image_blend = c_white; break;
				case "I": sprite_index = spr_IceFamiliar_Create; image_blend = c_white; break;
			}
		}
		else sprite_index = spr_Elemental_Orb_ON;
		image_index = 0;
		alarm[0] = 40;
		var relik = (global.RELIKS[9,2]) ? 1.2 : 1;
		alarm[1] = (400 + (global.TWs[equipped_tw,3] * 200)) * relik;
	}
}

if (sprite_index == spr_Elemental_Orb_ON)
{
	switch(orbLvl)
	{
		case 0:	switch(orbTyp)
				{
					case "T":	if (alarm[0] <= 0)
								{
									var TW = instance_create_layer(x,y,"Back_FX_Layer",obj_TWave);
									TW.owner = self;
									TW.priority = 9;
									alarm[0] = 40;
								}
								break;
								
					case "W":	if (alarm[0] < 1)
								{
									if (!place_meeting(x,y,obj_windslash))
									{
										var WS = instance_create_layer(x,y,"Back_FX_Layer",obj_windslash);
										with (WS)
										{
											ws_movementX = choose(random_range(-30,-15),random_range(15,30));
											ws_movementY1 = random_range(-30,-15);
											ws_movementY2 = random_range(15,30);
											source = "WindOrb";
											image_xscale = sign(ws_movementX);
											image_yscale = sign(choose(ws_movementY1,ws_movementY2));
										}
									}
									alarm[0] = 3;
								}
								break;
								
					case "F":	if (distance_to_object(obj_BossEnemy)<450) && (alarm[1] mod 40 == 0)
								{
									instance_create_layer(x,y-20,"Back_FX_Layer",obj_Hmmng_Fireball);
									var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
									with (FX)
									{
										sprite_index = spr_firering_FX;
										obj_2_follow = other;
										anim_end = 3;
										brightness = true;
										glow = true;
										g_amount = 20;
										image_xscale = 2;
										image_yscale = 2;
									}
								}
								break;
								
					case "I":	if (alarm[0] <= 0)
								{
									alarm[0] = 70;
									for(var i = 1; i <= 6; i++)
									{
										var ispkball = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
										ispkball.sprite_index = spr_IceSpikeball;
										ispkball.Consum = false;
										ispkball.image_xscale = 0.5;
										ispkball.image_yscale = 0.5;
										ispkball.alarm[1] = 120;
										ispkball.vsp = -27;
										ispkball.hsp = -10 + (i * 3);
									}
									
									var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
									with (FX)
									{
										sprite_index = spr_whitering_FX;
										obj_2_follow = other;
										anim_end = 3;
										brightness = true;
										glow = true;
										g_amount = 20;
										image_xscale = 2;
										image_yscale = 2;
									}
								}
								break;
				}
				x = obj_Manticore.x;
				y = obj_Manticore.y - 100;
				if (alarm[1] <= 0) instance_destroy();
				break;
				
		case 1:	case 2:
				var ManticoreX = obj_Manticore.x + (45 * obj_Manticore.image_xscale);
				var ManticoreY = obj_Manticore.y - (orbLvl * 100);
	
				//--> Speed to chatch up player
				var shieldSPD = hsp;
				var max_acc = 0.15;
				hsp += 0.0025;
				hsp = clamp(hsp,0,max_acc);
				shieldSPD = hsp;
				
				x = lerp(x,ManticoreX,shieldSPD);
				y = lerp(y,ManticoreY,0.25);
				
				switch(orbTyp)
				{
					case "T":	var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
								glare.sprite_index = choose(spr_spark,spr_starShine);
								var sz = choose(1,1.5);
								glare.image_xscale = sz;
								glare.image_yscale = sz;
								glare.image_blend = c_white;
								glare.Xmovement = random_range(0,3) * choose(1,-1) * orbLvl;
								glare.Ymovement = random_range(0,3) * choose(1,-1) * orbLvl;
								glare.bright = true;
								glare.fadeSpd = choose (0.01,0.02);
								
								if (alarm[0] <= 0)
								{
									var lvl2 = (orbLvl <= 1) ? 0 : 1;
									amount = choose(0,lvl2,1);
									if (!amount)
									{
										var TW = instance_create_layer(x,y,"Back_FX_Layer",obj_TWave);
										TW.owner = self;
										TW.priority = 9;
										TW.priority = 9;
										TW.max_size += amount;
										alarm[0] = 40;
									}
									else
									{
										var nearBoss = instance_nearest(x,y,obj_BossEnemy)
										if (distance_to_object(nearBoss)<200)
										{
											repeat(2)
											{
												var relampago = instance_create_layer(x,y,"Back_FX_Layer",obj_relampago);
												relampago.image_angle = point_direction(x,y,nearBoss.x,nearBoss.y)
											}
										}
									}
									alarm[0] = 40;
								}
								break;
								
					case "W":	var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
								FX.sprite_index = spr_DL_ShotFX;
								FX.obj_2_follow = self;
								FX.anim_end = 5;
								//FX.brightness = choose(false,true);
//								FX.glow = choose(false,true);
//								FX.g_amount = 2;
								FX.image_alpha = random_range(0.3,0.6);
								FX.image_angle = random(360);
								FX.image_blend = choose(c_white,c_lime,merge_color(c_lime,c_yellow,0.8));
								FX.image_xscale = random(0.7) * orbLvl;
								FX.image_yscale = random(0.7) * orbLvl;
								
								if (alarm[0] <= 0)
								{
									var lvl2 = (orbLvl <= 1) ? 0 : 1;
									amount = choose(0,lvl2,1);
									if (amount)
									{
										for(var i=1; i<=20; i++)
										{
											var WS = instance_create_layer(x,y,"Back_FX_Layer",obj_windslash);
											WS.ws_movementX = (i<=10) ? random_range(-30,-15) : random_range(15,30);
											WS.ws_movementY1 = random_range(-30,-15);
											WS.ws_movementY2 = random_range(15,30);
											WS.source = "WindOrb";
											WS.image_xscale = sign(WS.ws_movementX);
											WS.image_yscale = sign(choose(WS.ws_movementY1,WS.ws_movementY2));
										}
									}
									else
									{
										for(var i=1; i<=3; i++)
										{
											var ww = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
											ww.sprite_index = spr_Whirlwind;
											ww.Consum = false;
											ww.image_xscale = image_xscale;
											ww.angle = i * 120;
											ww.instnc = self;
											ww.alarm[0] = 3;
											ww.alarm[1] = 20;
											ww.alarm[2] = 3;
										}
									}
									alarm[0] = 60;
								}
								break;
								
					case "F":	var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
								glare.sprite_index = choose(spr_burn_red,spr_Flarestar_inner);
								var sz = random_range(1,1.5) * orbLvl;
								if (sprite_index==spr_Flarestar_inner) glare.image_angle = random(360);
								glare.image_xscale = sz;
								glare.image_yscale = sz;
								glare.image_alpha = random_range(0.4,0.9);
								glare.image_blend = c_yellow;
								glare.Xmovement = random_range(0,3) * choose(1,-1) * orbLvl;
								glare.Ymovement = random_range(0,3) * choose(1,-1) * orbLvl;
								glare.bright = choose(true,false);
								glare.fadeSpd = choose (0.02,0.03);
								
								if (alarm[0] <= 0)
								{
									var lvl2 = (orbLvl <= 1) ? 0 : 1;
									amount = choose(0,0,lvl2,1);
									if (amount)
									{
										for(var i=1; i<=(4+lvl2); i++)
										{
											var Fireball = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
											Fireball.sprite_index = spr_Fireball;
											Fireball.Consum = false;
											Fireball.hsp = random_range(-10,-20);
											Fireball.hsp *= (i<=2+lvl2) ? image_xscale : -image_xscale;
											Fireball.vsp = random_range(-10,-50);
											Fireball.image_alpha = 0.7;
											Fireball.speed = 10;
											Fireball.speed *= (i<=5) ? image_xscale : -image_xscale;
										}
									}
									else
									{
										if (distance_to_object(obj_BossEnemy)<450) && (alarm[1] mod 40 == 0)
										{
											var HFBs = 1 + lvl2;
											repeat(HFBs)
											{
												var HFB = instance_create_layer(x,y-20,"Back_FX_Layer",obj_Hmmng_Fireball);
												HFB.wmov = true;
											}
											var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
											with (FX)
											{
												sprite_index = spr_firering_FX;
												obj_2_follow = other;
												anim_end = 3;
												brightness = true;
												glow = true;
												g_amount = 20;
												image_xscale = 2;
												image_yscale = 2;
											}
										}
									}
									alarm[0] = 60 + (20 * lvl2);
								}
								break;
								
					case "I":	var flakes = choose(0,1);
								if (flakes)
								{
									var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
									glare.sprite_index = choose(spr_snowflake);
									glare.image_index = irandom(5);
									glare.image_angle = random(360);
									glare.image_xscale = random(orbLvl) + 0.5;
									glare.image_yscale = random(orbLvl) + 0.5;
									glare.image_alpha = random_range(0.9,1);
									glare.image_blend = choose(c_white,c_aqua);
									glare.Xmovement = random_range(0,3) * choose(1,-1) * orbLvl;
									glare.Ymovement = random_range(0,3) * choose(1,-1) * orbLvl;
									glare.bright = choose(true,false);
									glare.fadeSpd = choose (0.01,0.02);
								}
								
								if (alarm[0] <= 0)
								{
									alarm[0] = 70;
									var lvl2 = (orbLvl <= 1) ? 0 : 1;
									amount = choose(0,lvl2,1);
									if (!amount)
									{
										for(var i = 1; i <= 6; i++)
										{
											var ispkball = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
											ispkball.sprite_index = spr_IceSpikeball;
											ispkball.Consum = false;
											ispkball.image_xscale = 0.5;
											ispkball.image_yscale = 0.5;
											ispkball.alarm[1] = 120;
											ispkball.vsp = -27;
											ispkball.hsp = -10 + (i * 3);
										}
									}
									else
									{
										if (instance_exists(obj_Iceball)) with(obj_Iceball) ib_hitpoints = 0;
										for(var i = 1; i <= 2; i++)
										{
											var iceball = instance_create_layer(x,y,"Info_Layer",obj_Iceball)
											iceball.sprite_index = spr_Iceball;
											iceball.ib_hsp = (i == 1) ? 30 : -30;
											iceball.ib_vsp = 25;
											iceball.ib_hitpoints = 100;
											iceball.owner = self;
										}
									}
									
									var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
									with (FX)
									{
										sprite_index = spr_whitering_FX;
										obj_2_follow = other;
										anim_end = 3;
										brightness = true;
										glow = true;
										g_amount = 20;
										image_xscale = 2;
										image_yscale = 2;
									}
								}
								
				}
				if (alarm[1] <= 0) instance_destroy();
				break;
	}
}

if (sprite_index == spr_ThunderFamiliar_Standing1) || (sprite_index == spr_ThunderFamiliar_Standing2) ||
   (sprite_index == spr_ThunderFamiliar_Attack) || (sprite_index == spr_ThunderFamiliar_Create) ||
   (sprite_index == spr_ThunderFamiliar_Crumbles)
{
	Raiju();
}

if (sprite_index == spr_FireFamiliar_Standing) || (sprite_index == spr_FireFamiliar_Onfire) ||
   (sprite_index == spr_FireFamiliar_Attack) || (sprite_index == spr_FireFamiliar_Create) ||
   (sprite_index == spr_FireFamiliar_Crumbles)
{
	Pyrneth();
}

if (sprite_index == spr_WindFamiliar_Standing) || (sprite_index == spr_WindFamiliar_Attack) ||
   (sprite_index == spr_WindFamiliar_Create) || (sprite_index == spr_WindFamiliar_Crumbles)
{
	Zefirius();
}

if (sprite_index == spr_IceFamiliar_Standing) || (sprite_index == spr_IceFamiliar_Attack) ||
   (sprite_index == spr_IceFamiliar_Create) || (sprite_index == spr_IceFamiliar_Crumbles)
{
	Icelord();
}

if (sprite_index == spr_TornadoINNER_front) || (sprite_index == spr_FrostBulwark)
{
	if (!switch_var) image_alpha += 0.02;
	else image_alpha -= 0.02;
	
	if (sprite_index == spr_FrostBulwark)
	{
		amount += image_xscale;
		amount = clamp(amount,-40,40);
		image_angle += amount;
	}
	else
	{
		y -= 3;
		x += random_range(-20,20);
	}
	
	image_xscale += 0.02;
	image_yscale -= 0.02;
	
	if (image_alpha >= 1) switch_var = true;
	
	if (switch_var) && (image_alpha <= 0) instance_destroy();
}

if (sprite_index == spr_flametongues)
{
	amount += image_xscale;
	amount = clamp(amount,-40,40);
	image_angle += amount;
	if (amount >= 40)
	{
		hsp += 0.1;
		hsp = clamp(hsp,-7,7);
		x += hsp * image_xscale;
	}
	
	if (alarm[1] <= 0) instance_destroy();
}

if (sprite_index == spr_wavelightning)
{
	x += random_range(7,10) * image_xscale;
	y += irandom_range(-10,10);
	if (alarm[1] <= 0) instance_destroy();
}

if (sprite_index == spr_Fireball)
{
	vsp += grv;
	image_speed = 0.8;
	if (image_index >= 5) image_index = 5;
	if (place_meeting(x,y,obj_Wall)) or	(place_meeting(x,y,obj_BossEnemy)) ||
	   ((x < obj_BossEnemy.minX)or(x > obj_BossEnemy.maxX))
	{
		var xplsn = instance_create_layer(x,y,"Back_FX_Layer",obj_Hmmng_Fireball);
		xplsn.sprite_index = spr_Explosion_hmmgF;
		instance_destroy();
	}
	else
	{
		x += hsp;
		y += vsp;
		image_angle += hsp;
	}						
	repeat(2)
	{
		var spark = instance_create_layer(x+random_range(-30,30),y+random_range(-30,30),"FX_Layer",obj_charge_chispa)
		spark.coloroption1 = c_orange;
		spark.coloroption2 = c_red;
	}
}

if (sprite_index == spr_IceSpikeball)
{
	vsp += grv;
	image_speed = 0.8;
	if (image_index >= 5) image_index = 5;
	if (place_meeting(x,y,obj_Wall)) or (place_meeting(x,y,obj_BossEnemy)) or (alarm[1] <= 0)
	{
		dmg_calc(sptw_hit_used,sprite_index);
		repeat(3)
		{
			var sprW = sprite_width/2;
			var sprH = sprite_height/2;
			var icy = instance_create_layer(x+random_range(-sprW,sprW),y+random_range(-sprH,sprH),"FX_Layer",obj_rockice_FX);
			icy.sprite_index = spr_icys_FX;
		}
		instance_destroy();
	}
	else
	{
		x += hsp;
		y += vsp;
		image_angle += hsp/3;
		
		switch_var = (switch_var) ? 0 : 1;
		if (switch_var)
		{
			var icefireFX = instance_create_layer(x+random_range(-160,160),y+random_range(-160,160),"Back_FX_Layer",obj_GrassLeaf);
			icefireFX.sprite_index = spr_snowflake;
			icefireFX.image_index = irandom(5);
			icefireFX.image_blend = choose(c_aqua,c_white);
			if (!place_meeting(x,y,dash_afterimage))
			{
				var afterimage = instance_create_layer(x+random_range(-10,10),y+random_range(-10,10),"Back_FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_index = image_index;
				afterimage.image_blend = choose(c_aqua,c_white);
				afterimage.image_angle = image_angle;
			}
		}
	}						
}

if (sprite_index == spr_NeonLaser)
{
	image_angle += (image_xscale) ? 0.8 : -0.8;
	image_angle = (image_xscale) ? clamp(image_angle,225,315) : clamp(image_angle,45,135);
	if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
	if (image_index >= 8.6) instance_destroy();
	x = instnc.x + 30 * image_xscale;
	y = instnc.y-160;
}

if (sprite_index == spr_IceImplosion)
{
	if (image_index >= 3) && (!switch_var)
	{
		if ((x >= 0)&&(x <= room_width)) && ((y >= 0)&&(y <= room_height))
		{
			var xscl = (image_xscale == 0.5) ? 1 : -1;
			var movX = x + (random_range(200,250)*xscl);
			var bossY = obj_BossEnemy.y;
			if (bossY >= y) bossY += random_range(-100,50);
			else bossY -= random_range(50,100);
			
			if (amount > 0)
			{
				var newCluster = instance_create_layer(movX,bossY,"FX_Layer",obj_TWSpecial);
				newCluster.sprite_index = spr_IceImplosion;
				newCluster.Consum = false;
				newCluster.image_xscale = 0.5 * xscl;
				newCluster.image_yscale = 0.5;
				newCluster.image_angle = random(360);
				newCluster.amount = amount - 1;
			}
		}
		switch_var = true;
	}
	
	if (image_index >= 8)
	{
		sprite_index = spr_BigAquaExplosion;
		image_index = 0;
		alarm[0] = 3;
	}
}

if (sprite_index == spr_BigAquaExplosion)
{
	
	var icefireFX = instance_create_layer(x+random_range(-160,160),y+random_range(-160,160),"FX_Layer",obj_GrassLeaf);
	icefireFX.sprite_index = spr_snowflake;
	icefireFX.image_index = irandom(5);
	icefireFX.image_blend = choose(c_aqua,c_white);
	var afterimage = instance_create_layer(x+random_range(-10,10),y+random_range(-10,10),"Back_FX_Layer",dash_afterimage);
	afterimage.sprite_index = sprite_index;
	afterimage.image_index = image_index;
	afterimage.image_blend = choose(c_aqua,c_white);
	afterimage.image_angle = image_angle;
	afterimage.image_alpha = random_range(0.2,0.6);
		
	if (image_index >= 14) instance_destroy();
}

if (sprite_index == spr_RemoteExpSP)
{
	if (place_meeting(x,y,obj_Manticore_hitbox))
	{
		image_xscale = obj_Manticore_hitbox.image_xscale;
		hsp = 20 * image_xscale;
	}
	
	if (place_meeting(x,y,obj_Wall)) hsp = 0;
	else image_angle += image_xscale;
	
	if (hsp != 0) hsp += (sign(hsp)) ? -1 : 1;
	x += hsp;
	
	if (sptw_key_a)
	{
		image_index = 0;
		sprite_index = spr_BombSP;
		
		var slash1 = instance_create_layer(x,y-50,"Info_Layer",obj_fadeAway);
		slash1.sprite_index = spr_ExpSlash;
		slash1.fadeSpd = 0.05;
		var slash2 = instance_create_layer(x,y-50,"Info_Layer",obj_fadeAway);
		slash2.sprite_index = spr_ExpSlash;
		slash2.image_xscale = -1;
		slash2.fadeSpd = 0.05;
	}
	
	if (!audio_is_playing(snd_RemExplsv1))&&(!audio_is_playing(snd_RemExplsv2))&&
	   (!audio_is_playing(snd_RemExplsv3)) audio_play_sound(snd_RemExplsv3,13,0);
}

if (sprite_index == spr_CoinLauncherSP)
{
	if (!audio_is_playing(snd_Jackpot)) audio_play_sound(snd_Jackpot,21,0);
	if (obj_Manticore.sprite_index != spr_Manticore_CLSP) instance_destroy();
}

if (sprite_index == spr_VoidRippleSP)
{
	if (!switch_var)
	{
		bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],140,512,3,12,-25,1,c_white,1);
		var inviTime = (ammoConsum * 60) / 2;
		alarm[0] = inviTime;
		obj_Manticore.alarm[11] = inviTime;
			
		switch_var = true;
	}
	
	x = obj_Manticore.x;
	y = obj_Manticore.y;
	
	image_angle += random(10);
	var sizIncresX = random_range(1,3);
	var sizIncresY = random_range(1,3);
	image_xscale = sizIncresX;
	image_yscale = sizIncresY;
	image_alpha -= 0.02;
	if (image_alpha <= 0)
	{
		image_alpha = 1;
		image_xscale = 0.2;
		image_yscale = 0.2;
	}
	
	if (alarm[0] mod 15 == 0)
	{
		var miniRipples = instance_create_layer(x+(random_range(-150,150)),y+(random_range(-150,150)),"FX_Layer",obj_TWSpecial)
		miniRipples.sprite_index = spr_miniRipple;
		miniRipples.image_xscale = 0.2;
		miniRipples.image_yscale = 0.2;
	}
}

if (sprite_index == spr_miniRipple)
{
	if (!switch_var)
	{
		if (instance_number(obj_Shockwave_front)<6)
		{
			fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],56,96,8,20,53,1,c_fuchsia,1);
			fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],56,96,8,20,53,1,c_fuchsia,1);
		}
		amount = random_range(0.05,0.15);
		angle = random_range(-10,10);
		visible = false;
		audio_sound_pitch(snd_miniRipples,random_range(0.8,1.2));
		audio_play_sound(snd_miniRipples,30,0);
		
		switch_var = true;
	}
	
	image_alpha -= 0.02;
	image_xscale += amount;
	image_yscale += amount;
	image_angle += angle;
	
	if (image_alpha <= 0) instance_destroy();
}