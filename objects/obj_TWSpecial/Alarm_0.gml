/// @description Insert description here
// You can write your code in this editor

switch(sprite_index)
{
	case spr_ChakramSP: case spr_BigAquaExplosion:
		alarm[0] = 3;
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
	case spr_Explosion_grnd:
		alarm[0] = 15 - global.TWs[3,3];
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
	
	case spr_ShurikenSP:
		alarm[0] = 7 - ammoConsum;
		var afterimage = instance_create_layer(x+random_range(-15,15),y+random_range(-15,15),"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
		afterimage.image_angle = image_angle;
		afterimage.image_blend = (global.TWs[6,3]==0) ? c_white : ((global.TWs[6,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
								 ((global.TWs[6,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
	
	case spr_GuillotineSP:
		alarm[0] = 5 - ammoConsum;
		var afterimage = instance_create_layer(x+random_range(-15,15),y+random_range(-15,15),"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
		afterimage.image_yscale = image_yscale;
		afterimage.image_angle = image_angle;
		afterimage.image_blend = (global.TWs[5,3]==0) ? c_white : ((global.TWs[5,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
								 ((global.TWs[5,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
	case spr_TribladerSP:
		alarm[0] = 3;
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
		afterimage.image_yscale = image_yscale;
		afterimage.image_blend = (global.TWs[7,3]==0) ? c_yellow : ((global.TWs[7,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
								 ((global.TWs[7,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
		break;
	
	case spr_StarbladesSP: case spr_wavelightning:
		alarm[0] = (sprite_index == spr_StarbladesSP) ? 6 - global.TWs[8,3] : 3 - global.RELIKS[14,2];
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
	case spr_flametongues:
		alarm[0] = 5;
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_angle = image_angle;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
		afterimage.image_yscale = image_yscale;
		afterimage.glowFX = true;
		afterimage.image_blend = c_orange;
		break;
		
	case spr_TornadoINNER_front:
		alarm[0] = 6 - global.AXES[4,3];
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
	case spr_FrostBulwark:
		alarm[0] = 6 - global.SWORDS[4,3];
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
//	case spr_AssegaiSP: alarm[0] = 3; break;
	case spr_CoinLauncherSP:
		var coin = instance_create_layer(x+(random_range(5,15)*image_xscale),y+random_range(-5,-15),"FX_Layer",obj_rockice_FX)
		coin.sprite_index = spr_coin;
		coin.rockice_vsp = irandom_range(-15,-2);
		coin.rockice_hsp = irandom_range(2,15) * image_xscale;
		alarm[0] = 2;
		break;
	
	case spr_VoidRippleSP:
		artist.VoidWorldActive = false;
		artist.BloomFilterActive = true;
		artist.BlurActive = true;
		instance_destroy();
		break;
		
}

