/// @description Insert description here
// You can write your code in this editor
if (sprite_index == spr_BombSP)
{
	if (obj_Manticore.state == specialTW_Manticore) obj_Manticore.state = move_Manticore;
	
	with (obj_BossEnemy)
	{
		specialTW_var = false;
	}
}

if (sprite_index == spr_Explosion_grnd)||(sprite_index == spr_BombSP)
	if (global.camera_shake == true) global.camera_shake = false;
	
if (sprite_index == spr_GuillotineSP)
{
	audio_sound_gain(snd_Guillotine1,0.5,0);
	audio_sound_gain(snd_Guillotine2,0.5,0);
	audio_sound_gain(snd_Guillotine3,0.5,0);
}

if (sprite_index == spr_TribladerSP)
{
	audio_stop_sound(snd_TribladerShieldSLOW);
	audio_stop_sound(snd_TribladerShieldFAST);
}

if (sprite_index == spr_StarbladesSP) audio_stop_sound(snd_StarbladesSP);

if (sprite_index == spr_CoinLauncherSP) audio_stop_sound(snd_Jackpot);