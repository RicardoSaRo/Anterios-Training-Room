/// @description Insert description here
// You can write your code in this editor

switch(sprite_index)
{
	case spr_StarbladesSP:
		audio_play_sound(snd_Starblades,15,0);
		for (var i=1; i <=6; i++)
		{
			var Throwing_Wpn = instance_create_layer(x,y,"Player_Layer",obj_Throwing_Wpn);
			Throwing_Wpn.sprite_index = spr_Starblades;
			Throwing_Wpn.image_xscale = (i<=3) ? -1 : 1;
			Throwing_Wpn.priority = (global.TWs[8,3]==0) ? 1 : global.TWs[8,3];
			Throwing_Wpn.starblade_angle = ((i==1)||(i==4)) ? 1 : (((i==2)||(i==5)) ? 0 : -1);
			Throwing_Wpn.shuriken_curve = true;
			Throwing_Wpn.bomb_vsp = 10;
			Throwing_Wpn.bomb_hsp = 10;
			Throwing_Wpn.direc = (i<=3) ? -1 : 1;
			Throwing_Wpn.image_blend = (global.TWs[8,3]==0) ? c_white : ((global.TWs[8,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
									   ((global.TWs[8,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
			if (place_meeting(x,y,obj_Wall)) Throwing_Wpn.sprite_index = spr_poof;
		}
		alarm[2] = 60;
		break;
	
	case spr_Whirlwind:
		alarm[2] = 3;
		if (place_meeting(x,y,obj_BossEnemy)) dmg_calc(sptw_hit_used,sprite_index);
		break;
		
	case spr_CoinLauncherSP:
		var coins = instance_create_layer(x,y,"Player_Layer",obj_Throwing_Wpn);
		coins.sprite_index = choose(spr_Coin_Launcher,spr_Coin_Launcher,spr_BigCoin,spr_BigTripleCoin);
		coins.starblade_angle = choose(0,0,irandom_range(-3,-1),irandom_range(1,3));
		coins.image_xscale = image_xscale;
		coins.priority = 9;
		global.MONEY -= (global.TWs[15,3]==3) ? 500 : ((global.TWs[15,3]==2) ? 400 : ((global.TWs[15,3]==1) ? 300 : 200));
		alarm[2] = 5;
		break;
}