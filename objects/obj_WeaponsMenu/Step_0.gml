/// @description Insert description here
// You can write your code in this editor

gamepad_set_axis_deadzone(global.controller_type,0.5);

if (closing_menu)
{
	image_xscale -= 0.1;
	image_yscale -= 0.1;
	
	if (!openMain) if (!audio_is_playing(snd_menuClose)) audio_play_sound(snd_menuClose,7,0);
	
	if (image_xscale <= 0) instance_destroy();
}
else
{
	if (image_xscale < 1)
	{
		image_xscale += 0.1;
		image_yscale += 0.1;
	}
}

x = centerX;
y = centerY;

if (instance_exists(avatar2display_SWRD))
{
	switch(global.equippedSWORD)
	{
		case 1: avatar2display_SWRD.sprite_index = spr_Argenta_Avatar; avatar2display_SWRD.wpnLevel = global.SWORDS[1,3]; break;
		case 2: avatar2display_SWRD.sprite_index = spr_Dyrnwyn_Avatar; avatar2display_SWRD.wpnLevel = global.SWORDS[2,3];break;
		case 3: avatar2display_SWRD.sprite_index = (global.SWORDS[3,3]==3) ? spr_GirTrident_Avatar : spr_GirTab_Avatar;
				avatar2display_SWRD.wpnLevel = global.SWORDS[3,3]; break;
		case 4: avatar2display_SWRD.sprite_index = spr_Frostbiter_Avatar; avatar2display_SWRD.wpnLevel = global.SWORDS[4,3]; break;
		default: avatar2display_TW.sprite_index = spr_Unknown_Avatar; break;
	}
}

if (instance_exists(avatar2display_AXE))
{
	switch(global.equippedAXE)
	{
		case 1: avatar2display_AXE.sprite_index = spr_Korunax_Avatar; avatar2display_AXE.wpnLevel = global.AXES[1,3]; break;
		case 2: avatar2display_AXE.sprite_index = spr_Mirage_Avatar; avatar2display_AXE.wpnLevel = global.AXES[2,3]; break;
		case 3: avatar2display_AXE.sprite_index = spr_Mjolnir_Avatar; avatar2display_AXE.wpnLevel = global.AXES[3,3]; break;
		case 4: avatar2display_AXE.sprite_index = spr_Whisperer_Avatar; avatar2display_AXE.wpnLevel = global.AXES[4,3]; break;
		default: avatar2display_TW.sprite_index = spr_Unknown_Avatar; break;
	}
}

if (instance_exists(avatar2display_TW))
{
	switch(global.equippedTW)
	{
		case 1: avatar2display_TW.sprite_index = spr_Chakram_Avatar; avatar2display_TW.wpnLevel = global.TWs[1,3]; break;
		case 2: avatar2display_TW.sprite_index = spr_Kunai_Avatar; avatar2display_TW.wpnLevel = global.TWs[2,3]; break;
		case 3: avatar2display_TW.sprite_index = spr_Granade_Avatar; avatar2display_TW.wpnLevel = global.TWs[3,3]; break;
		case 4: avatar2display_TW.sprite_index = spr_Bomb_Avatar; avatar2display_TW.wpnLevel = global.TWs[4,3]; break;
		case 5: avatar2display_TW.sprite_index = spr_Guillotine_Avatar; avatar2display_TW.wpnLevel = global.TWs[5,3]; break;
		case 6: avatar2display_TW.sprite_index = spr_Shuriken_Avatar; avatar2display_TW.wpnLevel = global.TWs[6,3]; break;
		case 7: avatar2display_TW.sprite_index = spr_Triblader_Avatar; avatar2display_TW.wpnLevel = global.TWs[7,3]; break;
		case 8: avatar2display_TW.sprite_index = spr_Starblades_Avatar; avatar2display_TW.wpnLevel = global.TWs[8,3]; break;
		case 9: avatar2display_TW.sprite_index = spr_Assegai_Avatar; avatar2display_TW.wpnLevel = global.TWs[9,3]; break;
		case 10: avatar2display_TW.sprite_index = spr_ThunderOrb_Avatar; avatar2display_TW.wpnLevel = global.TWs[10,3]; break;
		case 11: avatar2display_TW.sprite_index = spr_WindOrb_Avatar; avatar2display_TW.wpnLevel = global.TWs[11,3]; break;
		case 12: avatar2display_TW.sprite_index = spr_FireOrb_Avatar; avatar2display_TW.wpnLevel = global.TWs[12,3]; break;
		case 13: avatar2display_TW.sprite_index = spr_IceOrb_Avatar; avatar2display_TW.wpnLevel = global.TWs[13,3]; break;
		case 14: avatar2display_TW.sprite_index = spr_RemoteExp_Avatar; avatar2display_TW.wpnLevel = global.TWs[14,3]; break;
		case 15: avatar2display_TW.sprite_index = spr_CoinLauncher_Avatar; avatar2display_TW.wpnLevel = global.TWs[15,3]; break;
		case 16: avatar2display_TW.sprite_index = spr_VoidRipples_Avatar; avatar2display_TW.wpnLevel = global.TWs[16,3]; break;
		default: avatar2display_TW.sprite_index = spr_Unknown_Avatar; break;
	}
}

var haxis = gamepad_axis_value(global.controller_type,gp_axislh);
var vaxis = gamepad_axis_value(global.controller_type,gp_axislv);

if (gpAxisH) haxis = 0.1;
if (gpAxisV) vaxis = 0.1;

var menu_move_left = max(keyboard_check_pressed(vk_left),gamepad_button_check_pressed(global.controller_type,gp_padl),haxis*-1,0);
var menu_move_right = max(keyboard_check_pressed(vk_right),gamepad_button_check_pressed(global.controller_type,gp_padr),haxis,0);
var menu_move_down = max(keyboard_check_pressed(vk_down),gamepad_button_check_pressed(global.controller_type,gp_padd),vaxis,0);
var menu_move_up = max(keyboard_check_pressed(vk_up),gamepad_button_check_pressed(global.controller_type,gp_padu),vaxis*-1,0);

//--> Moves around the menu using the script "wpn_menu_movement"
if (max(menu_move_down,menu_move_left,menu_move_right,menu_move_up,0))
{
	if (menu_move_up) moveOptions = wpn_menu_movement(moveOptions[1,1],moveOptions[1,2],"up");
	if (menu_move_down) moveOptions = wpn_menu_movement(moveOptions[1,1],moveOptions[1,2],"down");
	if (menu_move_left) moveOptions = wpn_menu_movement(moveOptions[1,1],moveOptions[1,2],"left");
	if (menu_move_right) moveOptions = wpn_menu_movement(moveOptions[1,1],moveOptions[1,2],"right");
	audio_play_sound(snd_MoveOption,5,0); //--> Menu Movement sound
}

var menu_s = max(keyboard_check_pressed(ord("S")),gamepad_button_check_pressed(global.controller_type,gp_face3),0);
var menu_d = max(keyboard_check_pressed(ord("D")),gamepad_button_check(global.controller_type,gp_face2),0);
var menu_plus = keyboard_check_pressed(vk_add);//---->>> EASY UPGRADES FOR TESTING

//--> Equips the weapon in the menu after pressing "D"
if (menu_d)
{
	switch(moveOptions[1,1])
	{
		case "SWORD": global.equippedSWORD = moveOptions[1,2]; break;
		case "AXE": global.equippedAXE = moveOptions[1,2]; break;
		case "TW": global.equippedTW = moveOptions[1,2]; break;
	}
	audio_play_sound(snd_WeaponEquip,5,0); //--> Weapon Equip FX
}

if (menu_plus) ///////------------->>>>>>> FOR EASY TESTING TESTING --- DELETE LATER
{
	switch(moveOptions[1,1])
	{
		case "SWORD":	if (global.SWORDS[moveOptions[1,2],3]==3) global.SWORDS[moveOptions[1,2],3] = 0;
						else global.SWORDS[moveOptions[1,2],3] += 1;
						break;
		case "AXE":		if (global.AXES[moveOptions[1,2],3]==3) global.AXES[moveOptions[1,2],3] = 0;
						else global.AXES[moveOptions[1,2],3] += 1;
						break;
		case "TW":		if (global.TWs[moveOptions[1,2],3]==3) global.TWs[moveOptions[1,2],3] = 0;
						else global.TWs[moveOptions[1,2],3] += 1;
						break;
	}
}


//--> Turns ON and OFF the Quick Access of every obtained weapons. Also checks that there is always
//--> at least one weapon with the Quick Access option available
if (menu_s)
{
	switch(moveOptions[1,1])
	{
		case "SWORD":	var SWRDmore_than_one = 0;
						var QAswrdCounter = 1;
						repeat(4)
						{
							if (global.SWORDS[QAswrdCounter,2]) SWRDmore_than_one += 1;
							QAswrdCounter += 1;
						}
						if (global.SWORDS[moveOptions[1,2],2])
						{
							if (SWRDmore_than_one > 1) global.SWORDS[moveOptions[1,2],2] = false;
							audio_play_sound(snd_QAUnequip,5,0); //--> FX
						}
						else
						{
							global.SWORDS[moveOptions[1,2],2] = true;
							audio_play_sound(snd_QuickAccess,5,0); //--> FX
						}
						break;
		
		case "AXE":		var AXEmore_than_one = 0;
						var QAaxeCounter = 1;
						repeat(4)
						{
							if (global.AXES[QAaxeCounter,2]) AXEmore_than_one += 1;
							QAaxeCounter += 1;
						}
						if (global.AXES[moveOptions[1,2],2])
						{
							if (AXEmore_than_one > 1) global.AXES[moveOptions[1,2],2] = false;
							audio_sound_pitch(snd_QAUnequip,0.9);//--> FX
							audio_play_sound(snd_QAUnequip,5,0);
						}
						else
						{
							global.AXES[moveOptions[1,2],2] = true;
							audio_sound_pitch(snd_QuickAccess,0.9);//--> FX
							audio_play_sound(snd_QuickAccess,5,0);
						}
						break;
		
		case "TW":		var TWmore_than_one = 0;
						var QAtwCounter = 1;
						repeat(16)
						{
							if (global.TWs[QAtwCounter,2]) TWmore_than_one += 1;
							QAtwCounter += 1;
						}
						if (global.TWs[moveOptions[1,2],2])
						{
							if (TWmore_than_one > 1) global.TWs[moveOptions[1,2],2] = false;
							audio_sound_pitch(snd_QAUnequip,0.8);//--> FX
							audio_play_sound(snd_QAUnequip,5,0);
						}
						else
						{
							global.TWs[moveOptions[1,2],2] = true;
							audio_sound_pitch(snd_QuickAccess,0.8);//--> FX
							audio_play_sound(snd_QuickAccess,5,0); 
						}
						break;
	}
}

if (gamepad_axis_value(global.controller_type,gp_axislh) > 0.5) gpAxisH = true;
else if (gamepad_axis_value(global.controller_type,gp_axislh) < -0.5) gpAxisH = true;
if (gamepad_axis_value(global.controller_type,gp_axislh) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislh) > -0.5) gpAxisH = false;

if (gamepad_axis_value(global.controller_type,gp_axislv) > 0.5) gpAxisV = true;
else if (gamepad_axis_value(global.controller_type,gp_axislv) < -0.5) gpAxisV = true;
if (gamepad_axis_value(global.controller_type,gp_axislv) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislv) > -0.5) gpAxisV = false;

//--> Closing Menu
if (gamepad_button_check_pressed(global.controller_type,gp_start)) if (image_xscale >= 1) closing_menu = true;
var goin2MM = max(gamepad_button_check_pressed(global.controller_type,gp_select),keyboard_check_pressed(vk_escape),0);
if (goin2MM)
{
	if (image_xscale >= 1) closing_menu = true;
	if (!openMain) audio_play_sound(snd_menuBack,7,0);
	openMain = true;
}