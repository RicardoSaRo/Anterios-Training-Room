/// @description Insert description here
// You can write your code in this editor

if (!openMain)
{
	instance_activate_all();
	
	//--> Normal Music Volumen and all sound reactivation
	if (global.currentManager != noone)
	{
		var rm_mngr = global.currentManager;
		var msc_gain = audio_sound_get_gain(rm_mngr.levelMusic);
		audio_sound_gain(rm_mngr.levelMusic,msc_gain*2,0);
	}
	
	audio_resume_all();
	display_set_gui_size(1920,1080);
}
else instance_create_layer(global.view_x + (display_get_width()/2),global.view_y + (display_get_height()/2),"Menus_Layer",obj_MainMenu);

if (room == rm_Map) if (instance_exists(obj_Map_manager)) layer_background_alpha(obj_Map_manager.Backgrnd_id,1);