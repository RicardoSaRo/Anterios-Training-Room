/// @description Insert description here
// You can write your code in this editor

closing_menu = false;
gpAxisH = false;
gpAxisV = false;

image_xscale = 0.1;
image_yscale = 0.1;

menu_view_w = global.vsizeW;
menu_view_h = global.vsizeH;
centerX = 1366/2;
centerY = 768/2;

avatar2display_SWRD = instance_create_layer(centerX,centerY,"Menus_Layer2",obj_WpnMenuElement);
avatar2display_SWRD.image_index = spr_Argenta_Avatar;
avatar2display_SWRD.x = centerX - 410;
avatar2display_SWRD.y = centerY - 180;
avatar2display_SWRD.swordAvatar = true;

avatar2display_AXE = instance_create_layer(x,y,"Menus_Layer2",obj_WpnMenuElement);
avatar2display_AXE.sprite_index = spr_Korunax_Avatar;
avatar2display_AXE.x = centerX + 410;
avatar2display_AXE.y = centerY - 180;
avatar2display_AXE.axeAvatar = true;

avatar2display_TW = instance_create_layer(x,y,"Menus_Layer2",obj_WpnMenuElement);
avatar2display_TW.sprite_index = spr_Chakram_Avatar;
avatar2display_TW.x = centerX - 420;
avatar2display_TW.y = centerY + 140;
avatar2display_TW.twAvatar = true;

swrdOptions = array_create(4,0);
axeOptions = array_create(4,0);
twOptions = array_create(16,0);
moveOptions = array_create(1,0);

var Yoffset = 20;
for (var i = 1; i <= 4; i++)
{
	var swrd = instance_create_layer(x,y,"Menus_Layer2",obj_WpnMenuElement);
	swrdOptions[i] = swrd;
	swrd.sword = i;
	swrd.x = centerX - 170;
	swrd.y = centerY - 240 + Yoffset;
	
	var axe = instance_create_layer(x,y,"Menus_Layer2",obj_WpnMenuElement);
	axeOptions[i] = axe;
	axe.axe = i;
	axe.x = centerX + 170;
	axe.y = centerY - 240 + Yoffset;
	
	Yoffset += 40;
}

Yoffset = 0;
var Xoffset = 0;
for (var j = 1; j <= 16; j++)
{
	var tw = instance_create_layer(x,y,"Menus_Layer2",obj_WpnMenuElement);
	twOptions[j] = tw;
	tw.tw = j;
	tw.x = centerX - 170 + Xoffset;
	tw.y = centerY + 80 + Yoffset;
	
	if (j == 5) 
	{
		Yoffset = 0;
		Xoffset += 280;
	}
	else
	{
		if (j == 10)
		{
			Yoffset = -22;
			Xoffset += 280;
		}
		else Yoffset += 40;
	}
}

moveOptions[1,1] = "SWORD";
moveOptions[1,2] = 1;

counter = 11;
optionAvailable = false;
openMain = false;