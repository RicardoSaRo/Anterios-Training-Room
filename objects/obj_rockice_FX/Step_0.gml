/// @description Insert description here
// You can write your code in this editor

if (sprite_index == spr_icys_FX)
{
	if (instance_number(obj_chispa) <= 50)
	{
		var frostchispa = instance_create_layer(x,y,"FX_Layer",obj_chispa);
		with (frostchispa)
		{
			chispa_colour = c_aqua;
			x += random_range(-15,15);
			y += random_range(-15,15);
		}
	}
}

if (sprite_index == spr_coin)
{
	var shine = instance_create_layer(x,y,"FX_Layer",obj_chispa);
	with (shine)
	{
		sprite_index = spr_starShine;
		image_angle = random(360);
		x += random_range(-15,15);
		y += random_range(-15,15);
		image_xscale = 0.5;
		image_yscale = 0.5;
	}
}

rockice_vsp = rockice_vsp + rockice_grv;

image_angle += rockice_hsp * rockice_direc;

if (y <= rockice_max_height) height_reached = true;

if (y > rockice_max_height)&&(!height_reached) y = lerp(y,rockice_max_height,0.8);
else y += rockice_vsp;

if (height_reached) rockice_hsp *= 0.95;

x += rockice_hsp * rockice_direc;

//--> Destroys rock if they touch walls or falls off stage
if (place_meeting(x,y,obj_Wall)) or (y > room_height) instance_destroy();