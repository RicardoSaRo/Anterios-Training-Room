/// @description Insert description here
// You can write your code in this editor

if (obj_Manticore.sprite_index == spr_Korunax) or (obj_Manticore.sprite_index == spr_AirKorunax)
	sprite_index = spr_rocks_FX;

if (obj_Manticore.sprite_index == spr_Frostbiter) or (obj_Manticore.sprite_index == spr_AirFrostbiter)
	sprite_index = spr_icys_FX;
	
image_index = irandom_range(0,4);

image_angle = random_range(70,110);

image_speed = random_range(0.8,1);

rockice_max_height = y - irandom_range(25,50);

rockice_hsp = irandom_range(3,6);

rockice_vsp = irandom_range(3,6);

rockice_grv = 0.2;

rockice_direc = choose(1,-1);

height_reached = false;