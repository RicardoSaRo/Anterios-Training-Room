/// @description Insert description here

var cgvx = camera_get_view_x(view_camera[0]);
var cgvy = camera_get_view_y(view_camera[0]);

LO_xoffset1 = 140 + (40 * (LO_number - 8));
LO_xoffset2 = 140 + (40 * LO_number);



//--> Life Orbs positions and follows camera + offsets for Draw event
if (LO_number >= 9)
{
	x = cgvx + LO_xoffset1;
	LO_xoffset = LO_xoffset1;
}
else
{
	x = cgvx + LO_xoffset2;
	LO_xoffset = LO_xoffset2;
}

if (LO_number >= 9)
{
	y = cgvy + 100;
	LO_yoffset = 100;
}
else
{
	y = cgvy + 55;
	LO_yoffset = 55;
}

//--> DAMAGE EFFECT
if (obj_Manticore.alarm[10] == 9) && (self.sprite_index == spr_Life_Orb_empty)
&& (LO_number == obj_Manticore.Manticore_hp + dmg)//--> TO DO: En lugar de +1, usar el daño del jefe en un loop FOR para hacer un efecto mas smooth de daño
{
	if (!obj_Manticore.lowHealth) //--> Sound Alert at Low Health
	{
		if (obj_Manticore.Manticore_hp <= 4)&&(obj_Manticore.Manticore_hp > 0)
		{
			if (!audio_is_playing(snd_LowHealth)) audio_play_sound(snd_LowHealth,5,0);
			obj_Manticore.lowHealth = true;
		}
	}
	
	with (instance_create_layer(other.x,other.y,"Life_n_Bars",obj_LO_fx))
	{
		image_speed = 1;
		sprite_index = spr_Life_Orb_exp;
		LOx = other.LO_xoffset;
		LOy = other.LO_yoffset;
	}
}

if (obj_Manticore.alarm[5] <= 0) && (!obj_Manticore.regen_stop)
{
	if (!LOmngr.ManticoreVenomed)
	{
		if (sprite_index == spr_Life_Orb_empty) and
		(LO_number == obj_Manticore.Manticore_hp + 1)
		{
			sprite_index = spr_Life_Orb_reg;
			var ancDrp = (global.RELIKS[5,2]) ? 30 : 0;
			var brooch = (global.RELIKS[30,2]) ? 20 : 0;
			obj_Manticore.alarm[5] = (300 - ancDrp) - brooch; //--> Fast Regen if not getting hit
			artist.max_Regen = (300 - ancDrp) - brooch;
		}
	}
	else
	{
		if ((sprite_index == spr_Life_Orb)||(sprite_index == spr_Life_Orb_vnmd)) &&
		(LO_number == obj_Manticore.Manticore_hp)
		{
			sprite_index = spr_Life_Orb_empty;
			var ancDrp = (global.RELIKS[5,2]) ? 50 : 0;
			var brooch = (global.RELIKS[30,2]) ? 50 : 0;
			obj_Manticore.alarm[5] = 400 + ancDrp + brooch;
			artist.max_Regen = 400 + ancDrp + brooch;
			
			with (instance_create_layer(other.x,other.y,"Life_n_Bars",obj_LO_fx))
			{
				image_speed = 1;
				sprite_index = spr_Life_Orb_exp;
				image_blend = c_aqua;
				LOx = other.LO_xoffset;
				LOy = other.LO_yoffset;
			}
			
			obj_Manticore.Manticore_hp -= 1; //-->> Dragian Venom Damage
			obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp, 0, global.player_max_health);
		}
	}
}

if (sprite_index == spr_Life_Orb_reg)
{
	if (image_index > 6)
	{
		obj_Manticore.Manticore_hp += 1;
		sprite_index = (LOmngr.ManticoreVenomed) ? spr_Life_Orb_vnmd : spr_Life_Orb;
		image_index = obj_Life_Orb.image_index;
		if (sprite_index == spr_Life_Orb) //--> Sound FX
		{
			if (!audio_is_playing(snd_HealLO)) 
			{
				audio_sound_pitch(snd_HealLO,1);
				audio_play_sound(snd_HealLO,5,0);
			}
		}
		else
		{
			if (!audio_is_playing(snd_VenomDmg))
			{
				audio_sound_pitch(snd_VenomDmg,1);
				audio_play_sound(snd_VenomDmg,5,0);
			}
		}
		if (obj_Manticore.Manticore_hp >= 5) obj_Manticore.lowHealth = false; //--> Resets var for Low health alert
	}
}
else
{
	if (LO_number > global.player_max_health)
	{
		if (sprite_index != spr_Life_Orb_broken)
		{
			image_index = 0;
			sprite_index = spr_Life_Orb_broken;
		}
		else if (image_index >= 7.2) image_speed = 0;
	}
	else
	{
		if (obj_Manticore.Manticore_hp < LO_number)	sprite_index = (LOmngr.ManticoreVenomed) ? spr_Life_Orb_vnmdempty : spr_Life_Orb_empty;

		if (obj_Manticore.Manticore_hp >= LO_number)
		{
			image_speed = 1;
			sprite_index = (LOmngr.ManticoreVenomed) ? spr_Life_Orb_vnmd : spr_Life_Orb;
		}
	}
}

dmg = 1; //--> Prevents continuosly getting 1-hit killed