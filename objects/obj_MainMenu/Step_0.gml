/// @description Insert description here
// You can write your code in this editor

gamepad_set_axis_deadzone(global.controller_type,0.5);

if (closing_menu)
{
	image_xscale -= 0.1;
	image_yscale -= 0.1;
	
	if (image_xscale <= 0) instance_destroy();
}
else
{
	if (image_xscale < 1)
	{
		image_xscale += 0.1;
		image_yscale += 0.1;
	}
}

x = centerX;
y = centerY;

var haxis = gamepad_axis_value(global.controller_type,gp_axislh);
var vaxis = gamepad_axis_value(global.controller_type,gp_axislv);

if (gpAxisH) haxis = 0.1;
if (gpAxisV) vaxis = 0.1;

var menu_move_left = max(keyboard_check_pressed(vk_left),gamepad_button_check_pressed(global.controller_type,gp_padl),haxis*-1,0);
var menu_move_right = max(keyboard_check_pressed(vk_right),gamepad_button_check_pressed(global.controller_type,gp_padr),haxis,0);
var menu_move_down = max(keyboard_check_pressed(vk_down),gamepad_button_check_pressed(global.controller_type,gp_padd),vaxis,0);
var menu_move_up = max(keyboard_check_pressed(vk_up),gamepad_button_check_pressed(global.controller_type,gp_padu),vaxis*-1,0);
var menu_Esc = max(keyboard_check_pressed(vk_escape),gamepad_button_check_pressed(global.controller_type,gp_select),0);

//--> Moves around the menu using the script "wpn_menu_movement"
if (max(menu_move_down,menu_move_left,menu_move_right,menu_move_up,0))
{
	if (menu_move_up) moveOptions -= 1;
	if (menu_move_down) moveOptions += 1;
	moveOptions = clamp(moveOptions,4,6);
	audio_play_sound(snd_MoveOption,7,0);
}

if (menu_Esc) closing_menu = true;
var menu_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
if (closing_menu)
{
	if (!openWpns) && (!audio_is_playing(snd_menuClose)) audio_play_sound(snd_menuClose,7,0);
	menu_d = false;
}

//--> Turns ON and OFF the Quick Access of every obtained weapons. Also checks that there is always
//--> at least one weapon with the Quick Access option available
if (menu_d)
{
	switch(moveOptions)
	{
		case 4: //--> Back to Game
				if (!closing_menu) && (!audio_is_playing(snd_menuClose)) audio_play_sound(snd_menuClose,7,0);
				closing_menu = true;
				break;
		
		case 5: //--> Weapons
				if (!closing_menu) audio_play_sound(snd_menuOpen,7,0);
				closing_menu = true;
				openWpns = true;
				break;
				
		case 6: //--> Exit Game
				game_end();
				break;
	}
}

if (gamepad_axis_value(global.controller_type,gp_axislh) > 0.5) gpAxisH = true;
else if (gamepad_axis_value(global.controller_type,gp_axislh) < -0.5) gpAxisH = true;
if (gamepad_axis_value(global.controller_type,gp_axislh) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislh) > -0.5) gpAxisH = false;

if (gamepad_axis_value(global.controller_type,gp_axislv) > 0.5) gpAxisV = true;
else if (gamepad_axis_value(global.controller_type,gp_axislv) < -0.5) gpAxisV = true;
if (gamepad_axis_value(global.controller_type,gp_axislv) < 0.5) && (gamepad_axis_value(global.controller_type,gp_axislv) > -0.5) gpAxisV = false;

//--> Closing Menu
if (gamepad_button_check_pressed(global.controller_type,gp_start)) if (image_xscale >= 1) closing_menu = true;