/// @description Insert description here
// You can write your code in this editor

//draw_set_alpha(obj_WeaponsMenu.image_xscale/1.1);

//draw_sprite(avatar2display_SWRD,0,265, menu_view_h-565);
//draw_sprite(avatar2display_AXE,0,631, menu_view_h-565);

var _round = global.view_w/surface_get_width(application_surface);
camera_set_view_pos(view_camera[0],round_n(global.view_x,_round),round_n(global.view_y,_round));


//draw_set_alpha(1);

draw_self();

draw_set_font(Antares);
draw_set_halign(fa_center);
draw_set_valign(fa_center);
draw_text(centerX,centerY-195,	"--CONTROLS--");
draw_text(centerX,centerY-160,	"(Sword Attack)= A       (Axe Attack)= S       (Throwing Weapon)= D");
draw_text(centerX,centerY-130,	"(Jump/Double Jump)= SpaceBar          (MOVE)= Left/Right arrows");
draw_text(centerX,centerY-100,	"(DASH)= Shift + Left/Right/Up/Down         (Crouch)= Down arrow");
draw_text(centerX,centerY-70,	"(Change Sword)= W        (Change Axe)=E        (Change TW)= NumPad0");
draw_text(centerX,centerY-40,	"(Charge Special)= Q        (Launch Special)= Q + A or S or D + 50% bar");
draw_text(centerX,centerY-10,	"(Drop Platform)= Crouch + Jump       (Grabs wall)= Left/Right arrow against wall");
draw_text(centerX,centerY+20,	"(Wall Dash Attack)= Grab Wall + A or B or D       (LevelUp Weapons)= + on menu");

if (!closing_menu)
{
	//draw_sprite(spr_MainMenu_lbl,0,centerX,centerY-160);
	
	/*var chosenOption = (moveOptions == 1) ? c_yellow : c_white;
	draw_sprite_ext(spr_BackToGame_lbl,0,centerX,centerY-85,1,1,0,chosenOption,1);
	
	var chosenOption = (moveOptions == 2) ? c_yellow : c_white;
	draw_sprite_ext(spr_Weapons_lbl,0,centerX,centerY-30,1,1,0,chosenOption,1);
	
	var chosenOption = (moveOptions == 3) ? c_yellow : c_white;
	draw_sprite_ext(spr_Relics_lbl,0,centerX,centerY+25,1,1,0,chosenOption,1);*/
	
	var chosenOption = (moveOptions == 4) ? c_yellow : c_white;
	draw_sprite_ext(spr_BackToGame_lbl,0,centerX,centerY+80,1,1,0,chosenOption,1);
	
	var chosenOption = (moveOptions == 5) ? c_yellow : c_white;
	draw_sprite_ext(spr_Weapons_lbl,0,centerX,centerY+135,1,1,0,chosenOption,1);
	
	var chosenOption = (moveOptions == 6) ? c_yellow : c_white;
	draw_sprite_ext(spr_ExitGame_lbl,0,centerX,centerY+190,1,1,0,chosenOption,1);
}