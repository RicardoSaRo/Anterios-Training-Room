function RelikTextboxTxtDisplay() {
	//RelikTextboxTxtDisplay: To display Reliks Info
	//if (image_alpha < 1) exit;

	var RlkMenuAvtr = obj_ReliksMenu.RelikAvatar;
	draw_set_font(Antares18);
	draw_set_halign(fa_center);
	draw_set_valign(fa_center);
	switch(RlkMenuAvtr.sprite_index)
	{
		case spr_RelikEmpty: break;
		case spr_ScorpionRing:
			draw_text(x,y - 78,"SCORPION RING");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Gives the bearer the ability to dash.");
			draw_text(x,y - 20,"Also, when fighting an enemy many times");
			draw_text(x,y,     "stronger, this ring helps regenerate");
			draw_text(x,y + 20,"health and slowly fill or charge primary");
			draw_text(x,y + 40,"weapons' Special Attacks.");
			draw_text(x,y + 75,"Equip cost: 0 Stamina.");
			draw_text(x,y + 95,"This Relik cannot be unequipped.");
			break;
		case spr_Reliquary:
			draw_text(x,y - 75,"RELIQUARY");
			draw_set_font(Antares);
			draw_text(x,y - 20,"Well crafted but very worn antique.");
			draw_text(x,y + 10,"Apparently does nothing.");
			draw_text(x,y + 80,"Equip cost: 1 Stamina.");
			break;
		case spr_HeartCharm:
			draw_text(x,y - 75,"HEART CHARM");
			draw_set_font(Antares);
			draw_text(x,y - 30,"Common but handy charm for adventurers.");
			draw_text(x,y + 10,"30% Chance to restore 2 Life Orbs");
			draw_text(x,y + 30,"per successful regeneration.");
			draw_text(x,y + 50,"Gives an extra try.");
			draw_text(x,y + 80,"Equip cost: 1 Stamina.");
			break;
		case spr_VampiricNecklace:
			draw_text(x,y - 75,"VAMPIRIC NECKLACE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Relik with the power to drain life.");
			draw_text(x,y,     "Increases your regen for every");
			draw_text(x,y + 20,"successful hit of your");
			draw_text(x,y + 40,"primary weapons.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_AncientLifeDrop:
			draw_text(x,y - 75,"ANCIENT LIFEDROP");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Unknown drop with restorative power");
			draw_text(x,y - 20,"preserved inside a crystal.");
			draw_text(x,y,     "Emanates an eerie aura capable to heal.");
			draw_text(x,y + 40,"Increses your regeneration speed.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_CollectorsCoin:
			draw_text(x,y - 75,"COLLECTOR'S COIN");
			draw_set_font(Antares);
			draw_text(x,y - 40,"An unique coin from a collection.");
			draw_text(x,y - 20,"Lucky charm for those who wants");
			draw_text(x,y,     "their valuables increased.");
			draw_text(x,y + 30,"Increases the rate to gain one");
			draw_text(x,y + 50,"extra item for each item drop.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_KoruniteArmguard: 
			draw_text(x,y - 75,"KORUNITE ARMGUARD");
			draw_set_font(Antares);
			draw_text(x,y - 30,"A protective gear made mostly of");
			draw_text(x,y - 10,"korunite capable to store power.");
			draw_text(x,y + 30,"Korunax Special starts at 100%.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_WindArmguard:
			draw_text(x,y - 75,"WIND ARMGUARD");
			draw_set_font(Antares);
			draw_text(x,y - 30,"A protective gear imbued with");
			draw_text(x,y - 10,"materials capable to store winds.");
			draw_text(x,y + 30,"Whisperer Special starts at 100%.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_OrbEnhacer:
			draw_text(x,y - 75,"ORB ENHACER");
			draw_set_font(Antares);
			draw_text(x,y - 40,"An ancient device created to power-up");
			draw_text(x,y - 20,"Orb weapons. Gives them more energy to");
			draw_text(x,y,     "last a bit longer.");
			draw_text(x,y + 40,"Orb Weapons and Holofamiliars duration +20%.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_Batrachite:
			draw_text(x,y - 75,"BATRACHITE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Incredible powerful and rare mineral");
			draw_text(x,y - 20,"capable of neutralizing almost every");
			draw_text(x,y,     "kind of poison.");
			draw_text(x,y + 40,"Gives immunity to Poison status.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_SalamanderEye:
			draw_text(x,y - 75,"SALAMANDER EYE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Instantly heats the body of the holder");
			draw_text(x,y - 20,"and keeps it warm. The fire of this eye");
			draw_text(x,y,     "shows hidden things.");
			draw_text(x,y + 40,"Gives immunity to Frozen status.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_MermaidTear:
			draw_text(x,y - 75,"MERMAID TEAR");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A melancholic jewel of a mermaid");
			draw_text(x,y - 20,"clinging to her sadness. Instantly");
			draw_text(x,y,     "heals your skin of any burn.");
			draw_text(x,y + 40,"Gives immunity to Burn status.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_CrystalLotus:
			draw_text(x,y - 75,"CRYSTAL LOTUS");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Flawlessly crafted crystal.");
			draw_text(x,y - 20,"Ease stress and helps to cure");
			draw_text(x,y,     "quicker any ailment the holder has.");
			draw_text(x,y + 40,"Halves the duration of all status ailments.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_FulguriteBelt:
			draw_text(x,y - 75,"FULGURITE BELT");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Made using minerals that fused");
			draw_text(x,y - 20,"together with the power of lightning strikes.");
			draw_text(x,y,     "Absorbs stored lightning to force dashes.");
			draw_text(x,y + 40,"Drains 10% of Mjolnir's Special Bar to");
			draw_text(x,y + 60,"dash a second time on air.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_FirelordSpurs:
			draw_text(x,y - 75,"FIRELORD SPURS");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Launches you upwards with explosions.");
			draw_text(x,y,     "Drains 10% of Mirage's Special to Vertical");
			draw_text(x,y + 20,"Dash. Gives invulnerability to most damage");
			draw_text(x,y + 40,"while dashing and dash duration is doubled.");
			draw_text(x,y + 60,"Can cling on walls indefinitely.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_GryffonAnklet:
			draw_text(x,y - 75,"GRYFFON ANKLET");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Powered-up with Gryffon's DNA.");
			draw_text(x,y - 20,"Absorbs stored wind to jump on mid air.");
			draw_text(x,y + 20,"Drains 10% of Whisperer's Special bar to");
			draw_text(x,y + 40,"perform a windjump after your double jump.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_HydraPouch:
			draw_text(x,y - 75,"HYDRA POUCH");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Pouch made with sea creatures' DNA.");
			draw_text(x,y - 20,"Increases resistance to carry more weight.");
			draw_text(x,y,     "Plus you can store more things inside it!.");
			draw_text(x,y + 40,"Max Ammo increased by 5 on all weapons.");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_AncientCharger:
			draw_text(x,y - 75,"ANCIENT CHARGER");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Artifact created using ancient tech");
			draw_text(x,y - 20,"parts. Can be attached to a DMG Tank");
			draw_text(x,y,     "for a while to charge it full.");
			draw_text(x,y + 40,"Starts with the first DMG Tank full");
			draw_text(x,y + 80,"Equip cost: 3 Stamina.");
			break;
		case spr_AlchemicStone:
			draw_text(x,y - 75,"ALCHEMIC STONE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A mixture of rare minerals with ancient");
			draw_text(x,y - 20,"technology. Converts all damage dealt");
			draw_text(x,y,     "with primary weapons into gold.");
			draw_text(x,y + 40,"Primary weapon damage increases gold amount.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_All4One:
			draw_text(x,y - 75,"ALL4ONE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Only a few of this were made long");
			draw_text(x,y - 20,"time ago. Near the chip in the back can");
			draw_text(x,y,     "be read: 'The power of all given to one'.");
			draw_text(x,y + 40,"Increase all damage by 1 per Relik equipped.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_FlamerubyArmlet:
			draw_text(x,y - 75,"Flameruby Armlet");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Made with three rare rubies.");
			draw_text(x,y - 20,"It has the power to drain all stored fire");
			draw_text(x,y,     "to unleash a powerful flame slash.");
			draw_text(x,y + 40,"Everytime you swing Dyrnwyn, depletes all");
			draw_text(x,y + 60,"Special bar to damage with the consumed amount.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_SilverRing: 
			draw_text(x,y - 75,"SILVER RING");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Reacts with other silver objects");
			draw_text(x,y - 20,"nearby and drains all it's stored power");
			draw_text(x,y,     "to unleash powerful attacks.");
			draw_text(x,y + 40,"Everytime you swing Argenta, depletes all");
			draw_text(x,y + 60,"Special bar to damage with the consumed amount.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_GoldRing:
			draw_text(x,y - 75,"GOLD RING");
			draw_set_font(Antares);
			draw_text(x,y - 30,"Made of 24K gold and Relik tech inside.");
			draw_text(x,y - 10,"Reacts with other gold objects enhacing");
			draw_text(x,y + 10,"some of their abilities.");
			draw_text(x,y + 50,"Primary weapons' Special bar fills 25% faster.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_NavaratnaTalisman:
			draw_text(x,y - 75,"NAVARATNA TALISMAN");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Beautiful and colorful Relik, crafted");
			draw_text(x,y - 20,"with many rare jewels.");
			draw_text(x,y + 20,"When charging Specials, charge all weapons");
			draw_text(x,y + 40,"instead of only the equipped ones. Charge");
			draw_text(x,y + 60,"Specials speeds up your regeneration too.");
			draw_text(x,y + 100,"Equip cost: 4 Stamina.");
			break;
		case spr_DragianHonors:
			draw_text(x,y - 75,"DRAGIAN HONORS");
			draw_set_font(Antares);
			draw_text(x,y - 30,"Given to those recognized by the");
			draw_text(x,y - 10,"dragians as the fiercest of warriors.");
			draw_text(x,y + 30,"Damage from Special Attacks charges");
			draw_text(x,y + 50,"DMG Tanks. But the tanks are harder to fill.");
			draw_text(x,y + 80,"Equip cost: 4 Stamina.");
			break;
		case spr_Cintanami:
			draw_text(x,y - 75,"CINTANAMI");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Super rare and valuable rocks.");
			draw_text(x,y - 20,"Exposed to energy from the planet's");
			draw_text(x,y,     "core for centuries. Keeps healthy");
			draw_text(x,y + 20,"anyone holding it.");
			draw_text(x,y + 60,"Immunity to all status ailments.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_RainbowPearl:
			draw_text(x,y - 75,"RAINBOW PEARL");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A very sought Relik around Nuterra.");
			draw_text(x,y - 20,"Gives the bearer's dash more speed.");
			draw_text(x,y + 20,"increasing the dash range too.");
			draw_text(x,y + 40,"Dash speed increases by 66%.");
			draw_text(x,y + 80,"Equip cost: 4 Stamina.");
			break;
		case spr_FeatherCloak:
			draw_text(x,y - 75,"FEATHER CLOAK");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Powerful Relik and difficult to craft.");
			draw_text(x,y - 20,"Protects the bearer when moving swiftly.");
			draw_text(x,y + 20,"Invulnerability to most damage while");
			draw_text(x,y + 40,"dashing. Dash duration increases by 33%.");
			draw_text(x,y + 60,"Dash cooldown increases to 1 second.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_Speedflux:
			draw_text(x,y - 75,"SPEEDFLUX");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A device created imitating ancient.");
			draw_text(x,y - 20,"technology. Anything to a Speedflux");
			draw_text(x,y,     "becomes more fragile but faster.");
			draw_text(x,y + 40,"Movement speed increased by 66%.");
			draw_text(x,y + 60,"Most damage received increases by 1.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_ScorpionBrooch:
			draw_text(x,y - 75,"SCORPION BROOCH");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A brooch of a caduceus with a");
			draw_text(x,y - 20,"two-tailed scorpion. It's said to be made");
			draw_text(x,y,     "using a tiny part from the Metal Beast.");
			draw_text(x,y + 40,"Permanent Scorpion Field with reduced");
			draw_text(x,y + 60,"healing speed and protection.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_PhoenixHeart:
			draw_text(x,y - 75,"PHOENIX HEART");
			draw_set_font(Antares);
			draw_text(x,y - 50,"Contains the regeneration and");
			draw_text(x,y - 30,"rebirth force of a phoenix.");
			draw_text(x,y + 10,"Per successful regeneration you have 30%");
			draw_text(x,y + 30,"Chance to restore 2 Life Orbs, 2 ammo of");
			draw_text(x,y + 50,"your equipped weapon and 1 broken Life Orb.");
			draw_text(x,y + 70,"Infinite tries in hunts and explorations.");
			draw_text(x,y + 100,"Equip cost: 2 Stamina.");
			break;
		case spr_GoldenClover:
			draw_text(x,y - 75,"GOLDEN CLOVER");
			draw_set_font(Antares);
			draw_text(x,y - 30,"Brings good luck to its owner.");
			draw_text(x,y - 10,"Hunters and explorers loves to have");
			draw_text(x,y + 10,"it equipped to improve their findings.");
			draw_text(x,y + 50,"Increased rate to get rare drops.");
			draw_text(x,y + 80,"Equip cost: 2 Stamina.");
			break;
		case spr_JingleAcornbell:
			draw_text(x,y - 75,"JINGLE ACORNBELL");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Created with the rarest materials");
			draw_text(x,y - 20,"found in the Forest of Whispers. Attracts");
			draw_text(x,y,     "the most dangerous creatures in the forest.");
			draw_text(x,y + 40,"Added Effect.");
			draw_text(x,y + 60,"Always find boss variants in Forest of Whispers.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_TritonsTrumpet:
			draw_text(x,y - 75,"TRITONS' TRUMPET");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Created with the rarest materials");
			draw_text(x,y - 20,"found in Dragian Bay. Attracts the");
			draw_text(x,y,     "most dangerous creatures in the bay.");
			draw_text(x,y + 40,"Always find boss variants in Dragian Bay.");
			draw_text(x,y + 60,"Primary weapons damage slows enemies when idle.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_Dreamcatcher:
			draw_text(x,y - 75,"DREAMCATCHER");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Created with the rarest materials");
			draw_text(x,y - 20,"found in Ruins of Dragian Empire. Attracts");
			draw_text(x,y,     "the most dangerous creatures in the ruins.");
			draw_text(x,y + 40,"Always find boss variants in Ruins of Dragian.");
			draw_text(x,y + 60,"Empire. All primary weapons destroys proyectiles.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_VoidCompass:
			draw_text(x,y - 75,"VOID COMPASS");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Created with the rarest materials");
			draw_text(x,y - 20,"found in the Necropolis. Attracts the");
			draw_text(x,y,     "most dangerous creatures in the Necropolis.");
			draw_text(x,y + 40,"Always find boss variants in the Necropolis.");
			draw_text(x,y + 60,"Primary weapons damage refills Void Ripples.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_DragianHorn:
			draw_text(x,y - 75,"DRAGIAN HORN");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Created with the rarest materials");
			draw_text(x,y - 20,"found in Twinpeaks. Attracts the");
			draw_text(x,y,     "most dangerous creatures in the mountains.");
			draw_text(x,y + 40,"Always find boss variants in Twinpeaks.");
			draw_text(x,y + 60,"Primary weapons damage burns enemy Special bar.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_AbundanceSigil:
			draw_text(x,y - 75,"ABUNDANCE SIGIL");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A sigil relik that rewards abundance.");
			draw_text(x,y,     "Full Life Orbs increases Primary Weapons'");
			draw_text(x,y + 20,"damge. Bonus damage the more you fill your");
			draw_text(x,y + 40,"special bars, DMG Tanks and equipped");
			draw_text(x,y + 60,"throwing weapon ammo.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_Gnome_Figurine:
			draw_text(x,y - 75,"GNOME FIGURINE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A funny figurine of a gnome pointing");
			draw_text(x,y - 20,"as if he found something important.");
	//		draw_text(x,y,     "most dangerous creatures in the mountains.");
			draw_text(x,y + 40,"In explorations, helps you find gnomes.");
			draw_text(x,y + 60,"Increases money gain by 20%.");
			draw_text(x,y + 90,"Equip cost: 2 Stamina.");
			break;
		case spr_ShadowPearl:
			draw_text(x,y - 75,"SHADOW PEARL");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Former Rainbow Pearl corrupted by");
			draw_text(x,y - 20,"void devices. Instead of speed increment");
			draw_text(x,y,     "spawns shadow figures that mimics the user.");
			draw_text(x,y + 40,"Creates afterimages that attacks when");
			draw_text(x,y + 60,"dashing, dashes restarts your Regeneration.");
			draw_text(x,y + 90,"Equip cost: 4 Stamina.");
			break;
		case spr_DMG_Flask:
			draw_text(x,y - 75,"DMG FLASK");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A special flask that drains the");
			draw_text(x,y - 20,"equipped throwing weapon's special");
			draw_text(x,y,     "to produce a healing substance.");
			draw_text(x,y + 40,"DMG Tanks becomes flasks and can't be used");
			draw_text(x,y + 60,"for specials. Each flask heals 1 Life Orb.");
			draw_text(x,y + 90,"Equip cost: 2 Stamina.");
			break;
		case spr_Healing_Poison:
			draw_text(x,y - 75,"HEALING POISON");
			draw_set_font(Antares);
			draw_text(x,y - 40,"This flask mixes the DMG Tanks");
			draw_text(x,y - 20,"with poisons to create a healing substance.");
			draw_text(x,y + 20,"DMG Tanks becomes flasks and can't be used");
			draw_text(x,y + 40,"for specials. Each flask heals 1 Life Orb.");
			draw_text(x,y + 60,"You'll be poisoned for a short time.");
			draw_text(x,y + 90,"Equip cost: 1 Stamina.");
			break;
		case spr_Elemental_Scorpion:
			draw_text(x,y - 75,"ELEMENTAL SCORPION");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Made with three elemental");
			draw_text(x,y - 20,"medallions linked by alien metal");
			draw_text(x,y,     "that limits your Scorpion Ring's power.");
			draw_text(x,y + 40,"Specials won't fill over time. Mirage, Mjolnir");
			draw_text(x,y + 60,"and Whisperer starts with 40% special bar.");
			draw_text(x,y + 90,"Equip cost: 2 Stamina.");
			break;
		case spr_VenomlordScalemail:
			draw_text(x,y - 80,"VENOMLORD SCALEMAIL");
			draw_set_font(Antares);
			draw_text(x,y - 50,"Created with materials from");
			draw_text(x,y - 30,"Venomlords. Inflicts Dragian Venom that");
			draw_text(x,y - 10,"can't be healed in combat or explorations, but");
			draw_text(x,y + 10,"in exchange gives powerful defensive properties.");
			draw_text(x,y + 50,"Dragian Venom status. Can't be fliched when");
			draw_text(x,y + 70,"damaged. Damage reduced to 1 and sometimes 0.");
			draw_text(x,y + 100,"Equip cost: 5 Stamina.");
			break;
		case spr_GallantCrest:
			draw_text(x,y - 75,"GALLANT CREST");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Extols the beauty and swiftless present");
			draw_text(x,y - 20,"in the art of wielding swords and lances.");
			draw_text(x,y + 20,"Swords/lances Specials fills 20% faster.");
			draw_text(x,y + 40,"Other specials fills 10% slower.");
			draw_text(x,y + 60,"Equip the 3 Crests to power-up most Specials.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_RoughCrest:
			draw_text(x,y - 75,"ROUGH CREST");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Extols the strength needed in");
			draw_text(x,y - 20,"the art of wielding axes and maces.");
			draw_text(x,y + 20,"Axes/maces Specials fills 20% faster.");
			draw_text(x,y + 40,"Other specials fills 10% slower.");
			draw_text(x,y + 60,"Equip the 3 Crests to power-up most Specials.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_SkillCrest:
			draw_text(x,y - 75,"SKILL CREST");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Extols the precision needed in");
			draw_text(x,y - 20,"the art of wielding throwing weapons.");
			draw_text(x,y + 20,"Throwing weapons Specials fills 20% faster.");
			draw_text(x,y + 40,"Other specials fills 10% slower.");
			draw_text(x,y + 60,"Equip the 3 Crests to power-up most Specials.");
			draw_text(x,y + 90,"Equip cost: 3 Stamina.");
			break;
		case spr_SkullOfAgility:
			draw_text(x,y - 75,"SKULL OF AGILITY");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Grim and super strong Relik ");
			draw_text(x,y - 20,"that gives incredible agility to the");
			draw_text(x,y,     "the owner, but at a really high cost.");
			draw_text(x,y + 40,"Brakes half of your Life Orbs.");
			draw_text(x,y + 60,"Swords and Axes attack speed increased by 50%.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_SkullOfDexterity:
			draw_text(x,y - 75,"SKULL OF DEXTERITY");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Grim and super strong Relik ");
			draw_text(x,y - 20,"that gives incredible dexterity to");
			draw_text(x,y,     "the owner, but at a really high cost.");
			draw_text(x,y + 40,"Increases effective damage by 50%.");
			draw_text(x,y + 60,"Can't use Special attacks.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_VoidSigil:
			draw_text(x,y - 75,"VOID SIGIL");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Wicked sigil that rewards scarceness.");
			draw_text(x,y,     "Life Orbs breaks on damage. Broken Life");
			draw_text(x,y + 20,"Orbs increases Primary Weapons' damage.");
			draw_text(x,y + 40,"Bonus damage the more empty you have");
			draw_text(x,y + 60,"Special Bars, DMG Tanks and current ammo.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_SkullOfPower:
			draw_text(x,y - 75,"SKULL OF POWER");
			draw_set_font(Antares);
			draw_text(x,y - 40,"Grim and super strong Relik ");
			draw_text(x,y - 20,"that gives incredible power to");
			draw_text(x,y,     "the owner, but at a really high cost.");
			draw_text(x,y + 40,"Increases damage by 50%.");
			draw_text(x,y + 60,"Breaks all Life Orbs except 2.");
			draw_text(x,y + 90,"Equip cost: 5 Stamina.");
			break;
		case spr_Antares_Blessing:
			draw_text(x,y - 75,"ANTARES' FORCE");
			draw_set_font(Antares);
			draw_text(x,y - 40,"A Relik that channels the strength");
			draw_text(x,y - 20,"of Antares to your Scorpion Ring.");
			draw_text(x,y,     "Equipped Primary Weapons special's starts");
			draw_text(x,y + 40,"at 100%. 1 DMG Tank filled. 10 seconds of");
			draw_text(x,y + 60,"infinite ammo and invinsibility.");
			draw_text(x,y + 90,"Equip cost: 7 Stamina.");
			break;
	}


}
