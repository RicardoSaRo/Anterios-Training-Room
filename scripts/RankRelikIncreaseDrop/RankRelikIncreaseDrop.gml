// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function RankRelikIncreaseDrop(){
	
	var rateIncreasedTotal = 0;
	var LIFErateInc = 0;
	var TIMErateIncreased = 0;
	var OVRKLLrateIncreased = 0;
	
	//-->> LIFE RANK CALCS
	switch(global.results[results.life,lifeOrbs.full])
	{
		case 16: break;
	}
	
	if (global.results[results.life,lifeOrbs.perfect])
	{
		sprite_index = (global.results[results.life,lifeOrbs.broken] > 0) ? spr_Rank_GoldSS : spr_Rank_GoldSSS;
		shckwvColor = c_yellow;
		obj_Results_manager.Scorpionskey1 = true;
		obj_Results_manager.Scorpionskey2 = true;
	}
	
	//-->> TIME RANK CALC
	var rTime = global.results[results.time,0];
	
	if (rTime < 30) OVRKLLrateIncreased += 0;
		
	//-->> OVERKILL RANK CALC
	var redRank = global.results[results.overkill,ovrkll.girtabfinish];
	var overkilldmg = global.results[results.overkill,ovrkll.okdmg];
	
	if (overkilldmg >= 6000) OVRKLLrateIncreased += 0;
	
}