// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Sphere(){
	if (!switch_var)
	{
		if (instance_exists(obj_timedInvin))
		{
			state = Phoenisk_Fireballs;
			exit;
		}
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_RainbowS_start : spr_VioletP_RainbowS_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		switch_var = true;
	}
	
	switch(sprite_index)
	{
		case spr_RadiantP_RainbowS_start: case spr_VioletP_RainbowS_start:
			if (image_index >= 1.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_RainbowS : spr_VioletP_RainbowS;
			}
			break;
			
		case spr_RadiantP_RainbowS: case spr_VioletP_RainbowS:
			if (!switch_var2)
			{
				interact_instance = instance_create_layer(x+(110*image_xscale),y-25,"Enemy_Layer",obj_BossEnemy);
				
				with(interact_instance)
				{
					sprite_index = spr_RainbowShroud_create;
					image_speed = 1;
					enemy_name = "rainbowSphere";
					defeated_spr = spr_RainbowShroud_destroy;
					interact_instance = other;
					image_xscale = 0.4 * sign(other.image_xscale);
					image_yscale = 0.4;
					state = Rainbow_Sphere;
					interact_instance2 = global.currentManager;
					alarm[0] = 1;
					switch(global.difficulty)
					{
						case "casual":	hit_points = 1200; alarm[1] = 380; break;
						case "normal":	hit_points = 1500; alarm[1] = 300; break;
						case "hard":	hit_points = 1700; alarm[1] = 280; break;
						case "lunatic":	hit_points = 2000; alarm[1] = 240; break;
					}
				}
				
				switch_var2 = true;
			}
			break;
		
		case spr_RadiantP_Rainbow_end: case spr_VioletP_RainbowS_end:
			if (interact_instance == self) if (image_index >= 0.4)&&(image_index < 1) image_index = 5;
			if (image_index >= 5.6) switch_var3 = true;
			break;
	}

	if (switch_var3)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		interact_instance = self;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}