function specialTW_Manticore() {
	// Throwing Weapons Specials
	if (global.RELIKS[49,2]) exit;
	
	if (global.RELIKS[41,2]) || (global.RELIKS[42,2])
	{
		//-->> Healing
		var healamount = global.RELIKS[41,2] + global.RELIKS[42,2];
		Manticore_hp += healamount;
		Manticore_hp = clamp(Manticore_hp,0,global.player_max_health);
		//-->> SND FX
		if (!audio_is_playing(snd_HealLO)) 
		{
			audio_sound_pitch(snd_HealLO,1);
			audio_play_sound(snd_HealLO,5,0);
		}
		//-->> Heal FX
		var healpuff = instance_create_layer(x,y,"FX_Layer",obj_centerXYfollow);
		healpuff.sprite_index = spr_Life_Orb_exp;
		healpuff.image_xscale = 2;
		healpuff.image_yscale = 2;
		healpuff.obj_2_follow = obj_Manticore;
		healpuff.anim_end = 7.6;
		healpuff.glow = true;
		repeat(15)
		{
			var cc_spawnX = x + random_range(-50,50);
			var cc_spawnY = x + random_range(-50,50);
			var spawn_layer = choose("Back_FX_Layer","Info_Layer");
			var chispa = instance_create_layer(cc_spawnX,cc_spawnY,spawn_layer,obj_charge_chispa);
			chispa.sprite_index = spr_starShine;
			chispa.angl = choose(45);
			var clr1 = (global.RELIKS[42,2]) ? c_fuchsia : c_red;
			var clr2 = (global.RELIKS[42,2]) ? merge_colour(c_purple,c_fuchsia,0.7) : merge_colour(c_red,c_orange,0.3);
			if (global.RELIKS[24,2]) chispa.image_blend = choose(clr1,clr2);
		}
		if (global.RELIKS[42,2]) //-->> Poisons if Healing Poison Relik is equipped
		{
			healpuff.image_blend = c_fuchsia;
			var mntcrManager = global.currentManager;
			artist.poisonFX = true;
			with (mntcrManager)
			{
				ManticorePoisoned = true;
				if (!mntcrManager.ManticoreVenomed) alarm[2] = 420; break;
			}
		}
		
		consumeTWT();
		state = move_Manticore;
		exit;
	}
	
	state = specialTW_Manticore;

	var TWSPkey_a = max(keyboard_check_pressed(ord("A")),gamepad_button_check_pressed(global.controller_type,gp_shoulderrb),0);

	sptw_onFloor = place_meeting(x,y+1,obj_Wall);

	sptw_onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) sptw_onPlatform = true;
	else sptw_onPlatform = false;

	image_speed = 1;

	if (image_index > 0) && (image_index < 1) //--> Scorpion FX
	{
		if (!instance_exists(obj_FX_Scorpion))
		{
			with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
			{
				sprite_index = spr_ScorpionTail;
				image_xscale = other.image_xscale;
				if (image_xscale < 0) image_xscale = -0.1;
				else image_xscale = 0.1;
				image_yscale = 0.1;
			
				bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_yellow,1);
			}
		}
	}

	if (equipped_tw == 1) //--> MEGA CHAKRAM
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (image_index >= 1)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var size = (global.TWs[1,3]==0) ? 0.5 : ((global.TWs[1,3]==1) ? 0.65 : ((global.TWs[1,3]==2) ? 0.8 : 1));
				var MChakram = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
				MChakram.sprite_index = spr_ChakramSP;
				MChakram.image_blend = (global.TWs[1,3]==0) ? c_white : ((global.TWs[1,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
									   ((global.TWs[1,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)))
				MChakram.image_xscale = image_xscale * size;
				MChakram.image_yscale = size;
				MChakram.alarm[1] = 300;
				MChakram.alarm[0] = 3;
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 2) //--> KUNAI RAIN
	{
		sprite_index = spr_Manticore_TWSP3;
	
		if (image_index >= 6.6)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var dist = (global.TWs[2,3]==0) ? 150 : ((global.TWs[2,3]==1) ? 125 : ((global.TWs[2,3]==2) ? 100 : 75));
				var Kunai = instance_create_layer(x+(dist*image_xscale),y-200,"Info_Layer",obj_TWSpecial);
				Kunai.sprite_index = spr_KunaiSP;
				Kunai.image_xscale = image_xscale;
				Kunai.image_alpha = 0.1;
				Kunai.switch_var = 1;
			
				audio_play_sound(snd_smallExplosion,20,0);
				audio_play_sound(snd_Assegai_SP,20,0);
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 3) //--> GRENADE CLUSTER
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (!instance_exists(obj_TWSpecial))
		{
			var GCX = x + random_range(-600,600);
			var GCY = y + random_range(-250,250);
			if (GCX < Boundary_minX) GCX = (Boundary_minX + random_range(0,250));
			if (GCX > Boundary_maxX) GCX = (Boundary_maxX - random_range(0,250));
			if (GCY < Boundary_minY) GCY = (Boundary_minY + random_range(0,150));
			if (GCY > Boundary_maxY) GCY = (Boundary_maxY - random_range(0,150));
			var GCluster = instance_create_layer(GCX,GCY,"Info_Layer",obj_TWSpecial);
			GCluster.sprite_index = spr_Explosion_grnd;
			GCluster.image_xscale = 1.5;
			GCluster.image_yscale = 1.5;
			GCluster.alarm[0] = 10 - global.TWs[3,3];
			GCluster.amount = (TWammo[3,0]<9) ? TWammo[3,0] : 9;
		
			consumeTWT();
		}

		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 4) //--> MEGA BLAST
	{
		if (sprite_index != spr_Manticore_ExpUpD_prep) sprite_index = spr_Manticore_TWSP2;
		else
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var MBlast = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
				MBlast.sprite_index = spr_BombSP;
			
				var slash1 = instance_create_layer(x,y-50,"Info_Layer",obj_fadeAway);
				slash1.sprite_index = spr_ExpSlash;
				slash1.fadeSpd = 0.05;
				var slash2 = instance_create_layer(x,y-50,"Info_Layer",obj_fadeAway);
				slash2.sprite_index = spr_ExpSlash;
				slash2.image_xscale = -1;
				slash2.fadeSpd = 0.05;
			
				consumeTWT();
			}
		}
	
		with (obj_Manticore)
		{
			alarm[11] = 10;
			image_alpha = 1;
		}
	
		if (image_index >= 8.6) && (sprite_index == spr_Manticore_TWSP2) sprite_index = spr_Manticore_ExpUpD_prep;
	}

	if (equipped_tw == 5) //--> MEGA GUILLOTINE
	{
		sprite_index = spr_Manticore_TWSP1;
	
		if (image_index >= 3)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				for (var i=1; i<=6; i++)
				{
					var size = (global.TWs[5,3]==0) ? 0.5 : ((global.TWs[5,3]==1) ? 0.6 : ((global.TWs[5,3]==2) ? 0.7 : 0.8));
					var MGuillo = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
					MGuillo.sprite_index = spr_GuillotineSP;
					MGuillo.image_xscale = (i==1)||(i==3)||(i==5) ? image_xscale*size : -image_xscale*size;
					MGuillo.image_yscale = size;
					MGuillo.hsp = (i==1)||(i==2) ? 2 : ((i==3)||(i==4) ? 10 : 18);
					MGuillo.vsp = (i==1)||(i==2) ? 0.6 : ((i==3)||(i==4) ? 0.8 : 1);
					MGuillo.alarm[0] = 3;
				}
			
				consumeTWT();
			}
		}
	
		if (image_index >= 6.6) state = move_Manticore;
	}

	if (equipped_tw == 6) //--> MEGA SHURIKEN
	{
		sprite_index = spr_Manticore_TWSP1;
	
		if (image_index >= 3)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var size = (global.TWs[6,3] / 10);
				var MShuriken = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
				MShuriken.sprite_index = spr_ShurikenSP;
				MShuriken.image_xscale = image_xscale + (image_xscale * size * 3);
				MShuriken.image_yscale = image_yscale + (size * 3);
				MShuriken.image_blend = (global.TWs[6,3]==0) ? c_white : ((global.TWs[6,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
										((global.TWs[6,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
				MShuriken.amount = 4;
				MShuriken.vsp = 23;
				MShuriken.alarm[0] = 3;
			
				var MShuriken2 = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
				MShuriken2.sprite_index = spr_ShurikenSP;
				MShuriken2.image_xscale = image_xscale + (image_xscale * size * 3);
				MShuriken2.image_yscale = image_yscale + (size * 3);
				MShuriken2.image_blend = (global.TWs[6,3]==0) ? c_white : ((global.TWs[6,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
										 ((global.TWs[6,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)));
				MShuriken2.amount = 4;
				MShuriken2.switch_var = true;
				MShuriken2.vsp = -23;
				MShuriken2.alarm[0] = 3;
			
				consumeTWT();
			}
		}
	
		if (image_index >= 6.6) state = move_Manticore;
	}

	if (equipped_tw == 7) //--> TRIBLADER SHIELD
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (image_index >= 1)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				for (var i = 1; i <= 3; i++)
				{
					var size = (global.TWs[7,3]==0) ? 0.5 : ((global.TWs[7,3]==1) ? 0.6 : ((global.TWs[7,3]==2) ? 0.7 : 0.8));
					var TBlade1 = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
					TBlade1.image_blend = (global.TWs[7,3]==0) ? c_white : ((global.TWs[7,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
										  ((global.TWs[7,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)))
					TBlade1.sprite_index = spr_TribladerSP;
					TBlade1.image_xscale = image_xscale * size;
					TBlade1.image_yscale = size;
					TBlade1.angle = 45 + i * 120;
					TBlade1.amount = i;
					TBlade1.alarm[0] = 3;
					TBlade1.alarm[1] = 500;
				}
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 8) //--> STARBLADES MAYHEM
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (image_index >= 1)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				for (var i = 1; i <= 6; i++)
				{
					var size = 0.5;
					var SBlade1 = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
					SBlade1.image_blend = (global.TWs[8,3]==0) ? c_white : ((global.TWs[8,3]==1) ? merge_color(c_aqua,c_blue,0.5) :
										  ((global.TWs[8,3]==2) ? merge_color(c_red,c_orange,0.5) : merge_color(c_white,c_yellow,0.5)))
					SBlade1.sprite_index = spr_StarbladesSP;
					SBlade1.image_xscale = image_xscale * size;
					SBlade1.image_yscale = size;
					SBlade1.angle = i * 60;
					SBlade1.amount = i;
					SBlade1.alarm[0] = 3;
					SBlade1.alarm[1] = 90;
					SBlade1.alarm[2] = 60;
				}
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 9) //--> MEGA ASSEGAI
	{
		sprite_index = spr_Manticore_TWSP3;
	
		if (image_index >= 6.6)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				for (var i=1; i <= 5; i++)
				{
					var size = (global.TWs[9,3]==0) ? 1 : ((global.TWs[9,3]==1) ? 1.15 : ((global.TWs[9,3]==2) ? 1.30 : 1.5));
					var MAssegai = instance_create_layer(x+(100*-image_xscale),y-(75*i),"Info_Layer",obj_TWSpecial);
					MAssegai.sprite_index = spr_AssegaiSP;
					MAssegai.image_xscale = image_xscale * size;
					MAssegai.image_yscale = size;
					MAssegai.alarm[1] = 200;
					MAssegai.image_alpha = 0;
					var spd = (i==1) ? 1 : ((i==2) ? 1.5 : ((i==3) ? 2 : ((i==4) ? 2.5 : 3)));
					MAssegai.vsp = -15 * spd;
					MAssegai.hsp = (global.TWs[9,3]==0) ? 30 : ((global.TWs[9,3]==1) ? 26 : ((global.TWs[9,3]==2) ? 23 : 20));
				}
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 10)||(equipped_tw == 11)||(equipped_tw == 12)||(equipped_tw == 13) //--> ORB FAMILIARS
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (image_index >= 1)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var OrbSP = instance_create_layer(x,y-100,"Info_Layer",obj_TWSpecial);
				OrbSP.sprite_index = (global.TWs[equipped_tw,3]<1) ? spr_1OrbSP : spr_2OrbsSP;
				OrbSP.Consum = false;
				OrbSP.image_blend = (equipped_tw==10) ? c_white : ((equipped_tw==11) ? c_lime : ((equipped_tw==12) ? c_orange : c_aqua));
				OrbSP.image_xscale = image_xscale;
				OrbSP.orbTyp = (equipped_tw==10) ? "T" : ((equipped_tw==11) ? "W" : ((equipped_tw==12) ? "F" : "I"));
				OrbSP.orbLvl = global.TWs[equipped_tw,3];
			
				if (instance_exists(obj_Throwing_Wpn))
				{
					with(obj_Throwing_Wpn)
					{
						if ((orb_type == "Thunder") && (obj_Manticore.equipped_tw == 10)) ||
						   ((orb_type == "Wind") && (obj_Manticore.equipped_tw == 11)) ||
						   ((orb_type == "Fire") && (obj_Manticore.equipped_tw == 12)) ||
						   ((orb_type == "Ice") && (obj_Manticore.equipped_tw == 13))
						{
							sprite_index = spr_poof;
							image_index = 0;
						}
					}
				}
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}

	if (equipped_tw == 14) //--> REMOTE BOMB
	{
		sprite_index = spr_Manticore_TWSP1;
	
		if (image_index >= 3)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				//var size = (global.TWs[14,3] / 10);
				var RBomb = instance_create_layer(x+(50*image_xscale),y-50,"Info_Layer",obj_TWSpecial);
				RBomb.sprite_index = spr_RemoteExpSP;
				RBomb.image_xscale = image_xscale;
				RBomb.hsp = 15 * image_xscale;
			
				consumeTWT();
			}
		}
	
		if (image_index >= 6.6) state = move_Manticore;
	}

	if (equipped_tw == 15) //--> JACKPOT!!!
	{
		sprite_index = spr_Manticore_CLSP;
	
		if ((TWSPkey_a) || (global.MONEY <= 0)) && (instance_exists(obj_TWSpecial))
		{
			image_index = 7;
			instance_destroy(obj_TWSpecial);
		}
	
		if (image_index >= 2)&&(image_index <= 6.6) 
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var CL = instance_create_layer(x+(39*image_xscale),y-32,"Info_Layer",obj_TWSpecial);
				CL.sprite_index = spr_CoinLauncherSP;
				CL.Consum = false;
				CL.image_xscale = image_xscale;
				CL.alarm[0] = 2;
				CL.alarm[2] = 3;
			
				consumeTWT();
			}
		
			if (image_index >= 6.4) image_index = 2;
		}
		
		if (image_index >= 8) state = move_Manticore;
	}

	if (equipped_tw == 16) //--> VOID SHROUD
	{
		sprite_index = spr_Manticore_TWSP2;
	
		if (image_index >= 1)
		{
			if (!instance_exists(obj_TWSpecial))
			{
				var VoidS = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
				VoidS.sprite_index = spr_VoidRippleSP;
				VoidS.image_xscale = 0.2;
				VoidS.image_yscale = 0.2;
				artist.VoidWorldActive = true;
				artist.BloomFilterActive = false;
				artist.BlurActive = false;
			
				consumeTWT();
			}
		}
	
		if (image_index >= 8.6) state = move_Manticore;
	}


}
