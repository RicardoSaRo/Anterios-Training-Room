function throw_atks_Manticore() {
	var tw_key_shift = max(keyboard_check(vk_shift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	var tw_key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	var tw_key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);

	//--> Cancel Attack Animation into Dash
	if (!tw_key_right) and (!tw_key_left) direc = 0;
	else
	{
		if (tw_key_right) and (!tw_key_left) direc = 1;
		if (tw_key_left) and (!tw_key_right) direc = -1;
	}

	if ((tw_key_shift)&&(direc != 0)) and (alarm[4] <= 0)
	{
		state = move_Manticore;
		exit;
	}

	airDash_used = false; //--> Refresh Air Dash;
	lghtDash_used = false;
	doublejump_used = false; //--> Refresh Double Jump;
	windjump_used = false;

	switch (equipped_tw) //--> Asign Equipped Throwing Weapon Object
	{
		case 1: var current_tw = spr_Chakram;
				var weapon_number = (global.TWs[1,3]==3) ? 3 : 2;
				var throwing_animation = spr_Manticore_throw;
				var twX = x + (82 * image_xscale);
				var twY = y-10;
				var prio = (global.TWs[1,3]<=1) ? 4 : ((global.TWs[1,3]==2) ? 5 : 6);
				break;
		case 2: var current_tw = spr_Kunai;
				var weapon_number = 17;
				var throwing_animation = spr_Manticore_throw;
				var twX = x + (82 * image_xscale);
				var twY = y-17;
				var prio = 2 + global.TWs[2,3];
				break;
		case 3: var current_tw = spr_Granade;
				var weapon_number = (global.TWs[3,3]==3) ? 2 : 1;
				var throwing_animation = spr_Manticore_throw_2;
				var twX = x + (72 * image_xscale);
				var twY = y-57;
				var prio = 6;
				break;
		case 4: var current_tw = choose(spr_Bomb1,spr_Bomb2);
				var weapon_number = (global.TWs[4,3]==3) ? 2 : ((global.TWs[4,3]>=1) ? 1 : 0);
				var throwing_animation = spr_Manticore_throw_2;
				var twX = x + (72 * image_xscale);
				var twY = y-57;
				var prio = 6 + global.TWs[4,3];
				break;
		case 5: var current_tw = spr_Guillotine;
				var weapon_number = (global.TWs[5,3]>=1) ? global.TWs[5,3] : 1;
				var throwing_animation = spr_Manticore_throw_2;
				var twX = x + (62 * image_xscale);
				var twY = y-57;
				var prio = 4 + global.TWs[5,3];
				break;
		case 6: var current_tw = spr_Shuriken;
				var weapon_number = 15;
				var throwing_animation = spr_Manticore_throw;
				var twX = x + (82 * image_xscale);
				var twY = y-10;
				var prio = 2 + global.TWs[6,3];
				break;
		case 7: var current_tw = spr_Triblader;
				var weapon_number = (global.TWs[7,3]==0) ? 2 : 2 + (global.TWs[7,3] - 1);
				var throwing_animation = spr_Manticore_throw;
				var twX = x + (82 * image_xscale);
				var twY = y-10;
				var prio = 7;
				break;
		case 8: var current_tw = spr_Starblades;
				var weapon_number = (global.TWs[8,3]<=2) ? 8 : 11;
				var throwing_animation = spr_Manticore_throw;
				var twX = x + (82 * image_xscale);
				var twY = y-10;
				var prio = (global.TWs[8,3]==0) ? 1 : global.TWs[8,3];
				break;
		case 9: var current_tw = spr_Assegai;
				var weapon_number = (global.TWs[9,3]<=1) ? 0 : 1;
				var throwing_animation = spr_Manticore_throw_2;
				var twX = x + (82 * image_xscale);
				var twY = y-57;
				var prio = 6 + global.TWs[9,3];
				break;
		case 10: case 11: case 12: case 13:
				 var current_tw = spr_Elemental_Orb_OFF;
				 var weapon_number = 0;
				 var prio = 10;
				 switch(equipped_tw)
				 {
					case 10: weapon_number = (global.TWs[10,3]==0) ? 0 : 1;
							 prio = 3 + (global.TWs[10,3] * 2); break;
					case 11: weapon_number = (global.TWs[11,3]==0) ? 0 : 1;
							 prio = 3 + (global.TWs[11,3] * 2); break;
					case 12: weapon_number = (global.TWs[12,3]==0) ? 0 : 1;
							 prio = 3 + (global.TWs[12,3] * 2); break;
					case 13: weapon_number = (global.TWs[13,3]==0) ? 0 : 1;
						     prio = 3 + (global.TWs[13,3] * 2); break;
				 }
				 var throwing_animation = spr_Manticore_throw_2;
				 var twX = x + (72 * image_xscale);
				 var twY = y-57;
				 break;
		case 14: var current_tw = spr_RemoteExp_OFF;
				 var weapon_number = 0;
				 var throwing_animation = spr_Manticore_throw_2;
				 var twX = x + (72 * image_xscale);
				 var twY = y-57;
				 var prio = 4;
				 break;
		case 15: var current_tw = spr_Coin_Launcher;
				 var weapon_number = (global.TWs[15,3]==3) ? 1 : 0;
				 var throwing_animation = spr_Manticore_throw;
				 var twX = x + (82 * image_xscale);
				 var twY = y-10;
				 var prio = 8;
				 break;
		case 16: var current_tw = spr_Void_Ripple;
				 var weapon_number = 2;
				 var throwing_animation = spr_Manticore_VR;
				 var twX = x + (30 * image_xscale);
				 var twY = y-80;
				 var prio = 10;
				 break;
	}

	if (instance_number(obj_Throwing_Wpn) <= weapon_number) //or//--> Prevents TW spam
	{
		tw_onFloor = place_meeting(x,y+1,obj_Wall);
	
		//--> onPlatform Variable check
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
		   (place_meeting(x,y+1,obj_Platform)) tw_onPlatform = true;
		else tw_onPlatform = false;
	
		tw_limit = false;
		state = throw_atks_Manticore;
	
		image_speed = 1;
	
		if (tw_onFloor or tw_onPlatform)
		{
			var tw_key_jump = max(keyboard_check(vk_space),gamepad_button_check(global.controller_type,gp_face1),0);
			//--> interrupts animation to jump and proceeds to air atk
		
			if (tw_key_jump)
			{
				air_attack = true;
				state = doublejump_Manticore;
				vsp = -17;
				exit;
			}
			
			sprite_index = throwing_animation;
		
			if (image_index > 1)and(image_index < 2) //--> Creates TW
			{
				if (current_tw == spr_Starblades)//--> If TW is Starblades, creates 3 of them
				{
					audio_play_sound(snd_Starblades,15,0);
					var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
					Throwing_Wpn.sprite_index = current_tw;
					Throwing_Wpn.image_xscale = image_xscale;
					Throwing_Wpn.priority = prio;
					Throwing_Wpn.starblade_angle = choose(-1,0,1);
					Throwing_Wpn.bomb_vsp = 10;
					Throwing_Wpn.bomb_hsp = 10;
					if (place_meeting(twX,twY,obj_Wall)) Throwing_Wpn.sprite_index = spr_poof;
				}
				else
				{
					if (!place_meeting(twX,twY,obj_Throwing_Wpn)) && (current_tw != spr_Void_Ripple)
					{
						var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
						Throwing_Wpn.sprite_index = current_tw;
						Throwing_Wpn.image_xscale = image_xscale;
						Throwing_Wpn.priority = prio;
						switch(equipped_tw)
						{
							case 2:  if (global.TWs[2,3]>=2)
									 {
										 var Throwing_Wpn2 = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
										 Throwing_Wpn2.sprite_index = current_tw;
										 Throwing_Wpn2.image_xscale = image_xscale;
										 Throwing_Wpn2.image_angle = (image_xscale) ? 45 : 315;
										 Throwing_Wpn2.starblade_angle = -1;
										 Throwing_Wpn2.priority = prio;
										 var Throwing_Wpn3 = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
										 Throwing_Wpn3.sprite_index = current_tw;
										 Throwing_Wpn3.image_xscale = image_xscale;
										 Throwing_Wpn3.image_angle = (image_xscale) ? -45 : -315;
										 Throwing_Wpn3.starblade_angle = 1;
										 Throwing_Wpn3.priority = prio;
									 }
									 audio_sound_pitch(snd_Cut,random_range(1,1.1));
									 audio_play_sound(snd_Cut,15,0);
									 break;
							case 7:  var tw_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
									 var tw_down = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);
									 if (tw_up) Throwing_Wpn.starblade_angle = -1;
									 if (tw_down) Throwing_Wpn.starblade_angle = 1;
									 break;
							case 10: Throwing_Wpn.orb_type = "Thunder";
									 Throwing_Wpn.image_blend = c_white;
									 break;
							case 11: Throwing_Wpn.orb_type = "Wind";
									 Throwing_Wpn.image_blend = c_lime;
									 break;
							case 12: Throwing_Wpn.orb_type = "Fire";
									 Throwing_Wpn.image_blend = c_orange;
									 break;
							case 13: Throwing_Wpn.orb_type = "Ice";
									 Throwing_Wpn.image_blend = c_aqua;
									 break;
							case 15: global.MONEY -= (global.TWs[15,3]==3) ? 500 : ((global.TWs[15,3]==2) ? 400 : ((global.TWs[15,3]==1) ? 300 : 200));
									 audio_play_sound(snd_CoinLauncher,15,0);
									 break;
						}
					}
				}
			}
			//--> Void Ripples
			if ((image_index > 1)and(image_index < 2)) or
			   ((image_index > 3)and(image_index < 4)) or
			   ((image_index > 5)and(image_index < 6))
			{
				if (current_tw == spr_Void_Ripple)
				{
					var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
					Throwing_Wpn.sprite_index = current_tw;
					Throwing_Wpn.priority = prio;
					Throwing_Wpn.x += random_range(-300,300);
					Throwing_Wpn.y += random_range(-300,100);
				}
			}
		
			//--> Depends on throw sprite change state after animation
			if (sprite_index == spr_Manticore_VR) or (sprite_index == spr_Manticore_VR_air)
				var max_sn = 7;
			else
			{
				if (sprite_index == spr_Manticore_throw) or (sprite_index == spr_Manticore_airthrow)
					var max_sn = 3;
				else var max_sn = 5;
			}
		
		
			if (image_index > max_sn)
			{
				var keep_atking = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
				if (keep_atking) state = throw_atks_Manticore;
				else
				{
					state = move_Manticore;
					atk_type = 0;
					attack_number = 1;
				}
			}
		
		}
		else //--> If is on air
		{
				air_attack = true;
				state = doublejump_Manticore;
		}
	}
	else
	{
		tw_limit = true;
		state = move_Manticore;
		atk_type = 0;
		attack_number = 1;
	}


}
