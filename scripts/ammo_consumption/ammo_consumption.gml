// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ammo_consumption(){
	var consum = 0;

	switch(equipped_tw)
	{
		case 1: case 7: case 10: case 11: case 12: case 13: case 15: ammoConsum = noone; break;
		case 14: consum = 5; break;
		case 6: consum = 4; break;
		case 5: consum = 2; break;
		case 2: case 3: case 9: consum = 1; break;
		case 8: consum = 0; break;
		case 16: case 4: consum = obj_Manticore.TWammo[equipped_tw,0]; break;
	}

	GrndCheck = false;
	if (instance_exists(obj_TWSpecial))
	{
		with (obj_TWSpecial)
		{
			if (sprite_index == spr_Explosion_grnd) other.GrndCheck = true;
		}
		if (GrndCheck) consum = 1;
	}

	if (ammoConsum != noone)
	{
		ammoConsum = (obj_Manticore.TWammo[equipped_tw,0] < consum) ? obj_Manticore.TWammo[equipped_tw,0] : consum;
		if (GrndCheck) obj_Manticore.TWammo[3,0] -= consum;
		else obj_Manticore.TWammo[equipped_tw,0] -= consum;
		if (obj_Manticore.TWammo[equipped_tw,0] <= 0) obj_Manticore.TWammo[equipped_tw,0] = 0;
	}
}