// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Breath(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathStart : spr_VioletP_Breath_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 3.6) && (!switch_var2)
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Breath : spr_VioletP_Breath;
		switch_var2 = true;
	}
	
	if (sprite_index == spr_RadiantP_Breath) || (sprite_index == spr_VioletP_Breath)
	{
		var breath = instance_create_layer(x+(80*image_xscale)+irandom_range(-10,10),y+(35*image_yscale)+irandom_range(0,20),"FX_Layer",obj_EnemyBullet);
		breath.sprite_index = spr_StoneBrth;
		breath.destroy_sprite = spr_StoneBrth_destroy;
		breath.image_alpha = random_range(0.4,0.8);
		breath.vsp = irandom_range(5,20);
		breath.NoDamage = true;
		breath.hsp = irandom_range(-5,25) * sign(image_xscale);
		breath.image_speed = choose(0.4,0.6,0.8,1);
		breath.switch_var = true;
		var brthsize = random_range(1,2);
		breath.image_xscale = brthsize;
		breath.image_yscale = brthsize;
		breath.NoDamage = true;
		
		counter++;
		if (counter >= 90)
		{
			image_index = 0;
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathEnd : spr_VioletP_Breath_end;
		}			
	}
	
	if (sprite_index == spr_RadiantP_BreathEnd) || (sprite_index == spr_VioletP_Breath_end)	if (image_index >= 3.6)	switch_var3 = true;
	
	if (switch_var3)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 70; break;
			case "normal": alarm[1] = 60; break;
			case "hard": alarm[1] = 50; break;
			case "lunatic": alarm[1] = 40; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		counter = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}