function money_results_calcs() {
	//-->> Money Results Calcs
	//-->> This scripts gives money based on your performance

	var money = global.results[results.overkill,ovrkll.enemyMaxhp]/10;

	var lifeR = 0;
	var lifeMulti = 0;
	var timeR = 0;
	var timeMulti = 0;
	var ovrkllR = 0;
	var brknLO = 0;
	var ovrkllMulti = 1;

	switch(life_rank.sprite_index)
	{
		case spr_Rank_F:		lifeR += 50; break;
		case spr_Rank_E:		lifeR += 100; break;
		case spr_Rank_D:		lifeR += 150; break;
		case spr_Rank_C:		lifeR += 200; break;
		case spr_Rank_B:		lifeR += 250; break;
		case spr_Rank_A:		lifeR += 300; break;
		case spr_Rank_GoldS:	lifeR += 350; lifeMulti = 1.1; break;
		case spr_Rank_GoldSS:	lifeR += 400; lifeMulti = 1.2; break;
		case spr_Rank_GoldSSS:	lifeR += 500; lifeMulti = 1.3; break;
	}

	switch(time_rank.sprite_index)
	{
		case spr_Rank_F:		timeR += 50; break;
		case spr_Rank_E:		timeR += 100; break;
		case spr_Rank_D:		timeR += 150; break;
		case spr_Rank_C:		timeR += 200; break;
		case spr_Rank_B:		timeR += 250; break;
		case spr_Rank_A:		timeR += 300; break;
		case spr_Rank_GoldS:	timeR += 350; timeMulti = 1.1; break;
		case spr_Rank_GoldSS:	timeR += 400; timeMulti = 1.2; break;
		case spr_Rank_GoldSSS:	timeR += 500; timeMulti = 1.3; break;
	}

	switch(overkill_rank.sprite_index)
	{
		case spr_Rank_F:		ovrkllR += 50; break;
		case spr_Rank_E:		ovrkllR += 100; break;
		case spr_Rank_D:		ovrkllR += 150; break;
		case spr_Rank_C:		ovrkllR += 200; break;
		case spr_Rank_B:		ovrkllR += 250; break;
		case spr_Rank_A:		ovrkllR += 300; break;
		case spr_Rank_RedS:		ovrkllR += 350; ovrkllMulti = 1.1; break;
		case spr_Rank_RedSS:	ovrkllR += 400; ovrkllMulti = 1.2; break;
		case spr_Rank_RedSSS:	ovrkllR += 500; ovrkllMulti = 1.3;break;
		case spr_Rank_GoldS:	ovrkllR += 600; ovrkllMulti = 1.15; break;
		case spr_Rank_GoldSS:	ovrkllR += 800; ovrkllMulti = 1.25; break;
		case spr_Rank_GoldSSS:	ovrkllR += 1000; ovrkllMulti = 1.5; break;
	}

	brknLO = global.results[results.life,lifeOrbs.broken] * 100;

	//--> Final Calcs
	money += lifeR + timeR + ovrkllR;
	money -= brknLO;
	var maxmulti = max(lifeMulti,timeMulti,ovrkllMulti);
	money = floor(money * maxmulti);
	money = clamp(money,0,99999);

	return money;


}
