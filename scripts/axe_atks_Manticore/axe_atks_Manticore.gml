/// @description Manticore Axe/Mace Atks (Grounded)
// This function will include a new one recieving parameters to reuse code and make it more efficient.

function axe_atks_Manticore() {
	state = axe_atks_Manticore;

	ax_key_s = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);
	ax_key_d = max(keyboard_check_pressed(ord("D")),gamepad_button_check_pressed(global.controller_type,gp_face3),0);
	ax_key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);
	ax_key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	ax_key_shift = max(keyboard_check(vk_shift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);

	ax_onFloor = place_meeting(x,y+1,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) ax_onPlatform = true;
	else ax_onPlatform = false;

	//--> Movement to check for Attack Cancel into Dash
	if (!ax_key_right) and (!ax_key_left) direc = 0;
	else
	{
		if (ax_key_right) and (!ax_key_left) direc = 1;
		if (ax_key_left) and (!ax_key_right) direc = -1;
	}

	if equipped_axe == 1 //--> KORUNAX
	{
		if (ax_onFloor or ax_onPlatform)
		{
			image_speed = (global.RELIKS[48,2]) ? 1.5 : 1;
			//--> Korunax 1st strike
			if (sprite_index != spr_Korunax)
			{
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				attack_number = aux_Axe_atk_num;
				if (sprite_index != spr_AirKorunax) image_index = (attack_number == 2) ? 7 : 0;
			}
			sprite_index = spr_Korunax;
			//--> Cancels into Dash
			if ((ax_key_shift)&&(direc != 0)) && (alarm[4] <= 0)
			{
				if ((image_index>=5)&&(image_index<=7))or((image_index>=11)&&(image_index<=13))
				{
					attack_number = 1;
					state = move_Manticore;
					if (image_index>=11) aux_Axe_atk_num = 1;
					exit;
				}
			}
			//-->Hitboxes first swing
			hitbox_duration(0,3,obj_Manticore_hitbox,spr_Korunax_hitbox1);
			hitbox_duration(3,4,obj_Manticore_hitbox,spr_Korunax_hitbox2);
			hitbox_duration(4,6,obj_Manticore_hitbox,spr_Korunax_hitbox3);
			if (!audio_is_playing(snd_KorunaxSwing1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
			{
				audio_play_sound(snd_KorunaxSwing1,15,0);
			}
			if (image_index>=3)and(image_index<=6)
			{
				if (ax_key_s) attack_number = 2;
				if (image_index>=5)
				{
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
				}
				alarm[7] = 120; //--> To continue combo after cancel
				aux_Axe_atk_num = 2;
			}
			if (image_index >= 7) //and (attack_number > 1)
			{
				if (image_index>=12)
				{	
					if (((global.AXES[1,3]>=2)&&(equipped_axe==1))&&((equiped_sword==4)&&(global.SWORDS[equiped_sword,3]>=2)))||
					   (global.AXES[1,3] == 3) || (global.RELIKS[46,2]) || (global.RELIKS[40,2])
					{
						if (ax_key_s) && ((aux_Swd_atk_num==3)||((aux_Swd_atk_num==1)&&(alarm[7]>0))||
						   ((equiped_sword==4)&&(global.SWORDS[equiped_sword,3]>=2)))
						{
							state = strongWpnAtk;
							exit;
						}
					}
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
					aux_Axe_atk_num = 1;
				}
				if (image_index <= 9) if (!ax_key_shift)&&(direc != 0) image_xscale = direc;
				hitbox_duration(7,9,obj_Manticore_hitbox,spr_Korunax_hitbox4);
				hitbox_duration(9,10,obj_Manticore_hitbox,spr_Korunax_hitbox5);
				hitbox_duration(10,11,obj_Manticore_hitbox,spr_Korunax_hitbox6);
				if (!audio_is_playing(snd_KorunaxSwing2)) && (attack_number == 2) //--> Sound FX
				{
					audio_sound_pitch(snd_KorunaxSwing2,1);
					audio_play_sound(snd_KorunaxSwing2,15,0);
				}
				if (attack_number < 2) or (image_index > 14)
				{
					attack_number = 1;
					state = move_Manticore;
				}
			}
		}
		else //--> Air Korunax (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
			if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
		}
	}

	if equipped_axe == 2 //--> MIRAGE
	{
		if (ax_onFloor or ax_onPlatform)
		{
			image_speed = (global.RELIKS[48,2]) ? 1.5 : 1;
			if (sprite_index != spr_Mirage)
			{
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				attack_number = aux_Axe_atk_num;
				if (sprite_index != spr_AirMirage) image_index = (attack_number == 2) ? 7 : 0;
			}
			sprite_index = spr_Mirage;
			if ((ax_key_shift)&&(direc != 0)) && (alarm[4] <= 0) //--> Cancels into Dash
			{
				if ((image_index>=6)&&(image_index<=8))or((image_index>=12)&&(image_index<=13))
				{
					attack_number = 1;
					state = move_Manticore;
					if (image_index>=12) aux_Axe_atk_num = 1;
					exit;
				}
			}
			//--> Hitboxes first swing
			hitbox_duration(0,3,obj_Manticore_hitbox,spr_Mirage_hitbox1);
			hitbox_duration(3,4,obj_Manticore_hitbox,spr_Mirage_hitbox2);
			hitbox_duration(4,6,obj_Manticore_hitbox,spr_Mirage_hitbox3);
			if (!audio_is_playing(snd_MirageSwing1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
			{
				audio_play_sound(snd_MirageSwing1,15,0);
			}
			//--> MIRAGE FLAMES
			if (image_index > 5)and(image_index < 6) //--> First Flame
			{
				var wowX1 = 82 * image_xscale;
				if (!place_meeting(x+wowX1,y,obj_willowisp))
				{
					var willowisp1 = instance_create_depth(x+wowX1,y,0,obj_willowisp);
					willowisp1.image_xscale = image_xscale;
				}
			}
			if (image_index > 9)and(image_index < 10) //--> Second Flame
			{
				var wowX2 = 68 * image_xscale;
				if (!place_meeting(x+wowX2,y-90,obj_willowisp))
				{
					var willowisp2 = instance_create_depth(x+wowX2,y-90,0,obj_willowisp);
					willowisp2.image_xscale = image_xscale;
				}
			}
			if (image_index > 10)and(image_index < 11) //--> Third Flame
			{
				var wowX3 = 3 * image_xscale;
				if (!place_meeting(x+wowX3,y-144,obj_willowisp))
				{
					var willowisp3 = instance_create_layer(x+wowX3,y-144,"FX_Layer",obj_willowisp);
					willowisp3.image_xscale = image_xscale;
				}
			}
			if (image_index>=3)and(image_index<=6)
			{
				if (ax_key_s) attack_number = 2;
				if (image_index>=5)
				{
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
				}
				alarm[7] = 120; //--> To continue combo after cancel
				aux_Axe_atk_num = 2;
			}
			if (image_index >= 7) //and (attack_number > 1)
			{
				if (image_index>=13)
				{	
					if (global.AXES[2,3] == 3) || (global.RELIKS[46,2]) || (global.RELIKS[40,2])
					{
						if (ax_key_s) && ((aux_Swd_atk_num==3)||((aux_Swd_atk_num==1)&&(alarm[7]>0)))
						{
							state = strongWpnAtk;
							exit;
						}
					}
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
					aux_Axe_atk_num = 1;
				}
				if (image_index <= 9) if (!ax_key_shift)&&(direc != 0) image_xscale = direc;
				//--> Hitboxes second swing
				hitbox_duration(7,9,obj_Manticore_hitbox,spr_Mirage_hitbox4);
				hitbox_duration(9,10,obj_Manticore_hitbox,spr_Mirage_hitbox5);
				hitbox_duration(10,11,obj_Manticore_hitbox,spr_Mirage_hitbox6);
				if (!audio_is_playing(snd_MirageSwing2)) && (attack_number == 2) //--> Sound FX
				{
					audio_play_sound(snd_MirageSwing2,15,0);
				}
				if (attack_number < 2) or (image_index > 14)
				{
					attack_number = 1;
					state = move_Manticore;
				}
			}
		}
		else //--> Air Mirage (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
			if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
		}
	}
	if equipped_axe == 3 //--> MJOLNIR
	{
		if (ax_onFloor or ax_onPlatform)
		{
			image_speed = 1;
			if (sprite_index != spr_Mjolnir)
			{
				attack_number = aux_Axe_atk_num;
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				if (sprite_index != spr_AirMjolnir) image_index = (attack_number == 2) ? 7 : 0;
			}
			sprite_index = spr_Mjolnir;
			//--> Cancels into Dash
			if ((ax_key_shift)&&(direc != 0)) && (alarm[4] <= 0)
			{
				if ((image_index>=7)&&(image_index<=8))or((image_index>=12)&&(image_index<=13))
				{
					attack_number = 1;
					state = move_Manticore;
					if (image_index>=12) aux_Axe_atk_num = 1;
					exit;
				}
			}
			//--> Lvl1-3 Upgrade Lightning
			if (global.AXES[3,3]>=1)
			{
				var lghtngChance = (global.AXES[3,3]==1) ? irandom(100) : ((global.AXES[3,3]==2) ? irandom(75) : irandom(50));
				var lghtspr = (global.AXES[3,3] == 1) ? spr_relampago : ((global.AXES[3,3] == 2) ? spr_relampago2 : spr_relampago3);
				if (lghtngChance==7)
				{
					if (distance_to_object(obj_BossEnemy)<300)
					{
						if (!place_meeting(x,y,obj_relampago))
						{
							var relampago = instance_create_layer(x,y,"Back_FX_Layer",obj_relampago);
							relampago.sprite_index = lghtspr;
							relampago.image_angle = point_direction(x,y,obj_BossEnemy.x,obj_BossEnemy.y)
						}
					}
				}
			}
			//-->Hitboxes first swing
			hitbox_duration(0,3,obj_Manticore_hitbox,spr_Mjolnir_hitbox1);
			hitbox_duration(3,4,obj_Manticore_hitbox,spr_Mjolnir_hitbox2);
			hitbox_duration(4,6,obj_Manticore_hitbox,spr_Mjolnir_hitbox3);
			if (!audio_is_playing(snd_MjolnirClap)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
			{
				audio_play_sound(snd_MjolnirClap,15,0);
			}
			var FX = choose(snd_MjolnirSwing1,snd_MjolnirSwing3,snd_MjolnirSwing5,snd_MjolnirSwing7,snd_MjolnirSwing9);
			if ((!audio_is_playing(snd_MjolnirSwing1))&(!audio_is_playing(snd_MjolnirSwing3))&&
			    (!audio_is_playing(snd_MjolnirSwing5))&&(!audio_is_playing(snd_MjolnirSwing7))&&
				(!audio_is_playing(snd_MjolnirSwing9))) && (attack_number == 1) && (image_index < 1)//--> Sound FX
			{
				audio_play_sound(FX,15,0);
			}
			if (image_index mod 2 = 0) //--> Electric effect
			{
				image_blend = c_white;
				image_alpha = 0.05;
			}
			if (image_index>=3)and(image_index<=6)
			{
				if (ax_key_s) attack_number = 2;
				if (image_index>=5)
				{
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
				}
				alarm[7] = 120; //--> To continue combo after cancel
				aux_Axe_atk_num = 2;
			}
			if (image_index >= 7) //and (attack_number > 1)
			{
				if (image_index>=13)
				{	
					if (global.AXES[3,3] == 3) || (global.RELIKS[46,2]) || (global.RELIKS[40,2])
					{
						if (ax_key_s) && ((aux_Swd_atk_num==3)||((aux_Swd_atk_num==1)&&(alarm[7]>0)))
						{
							state = strongWpnAtk;
							exit;
						}
					}
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
					aux_Axe_atk_num = 1;
				}
				if (image_index <= 9) if (!ax_key_shift)&&(direc != 0) image_xscale = direc;
				hitbox_duration(7,9,obj_Manticore_hitbox,spr_Mjolnir_hitbox4);
				hitbox_duration(9,10,obj_Manticore_hitbox,spr_Mjolnir_hitbox5);
				hitbox_duration(10,13,obj_Manticore_hitbox,spr_Mjolnir_hitbox6);
				if (!audio_is_playing(snd_MaceSwing1)) && (attack_number == 2) && (image_index < 10)//--> Sound FX
				{
					audio_play_sound(snd_MaceSwing2,15,0);
				}
				var FX = choose(snd_MjolnirSwing2,snd_MjolnirSwing4,snd_MjolnirSwing6,snd_MjolnirSwing8,snd_MjolnirSwing10);
				if (!audio_is_playing(snd_MjolnirSwing2)&&!audio_is_playing(snd_MjolnirSwing4)&&
				    !audio_is_playing(snd_MjolnirSwing6)&&!audio_is_playing(snd_MjolnirSwing8)&&
					!audio_is_playing(snd_MjolnirSwing10)) && (attack_number == 2) && (image_index < 10)//--> Sound FX
				{
						audio_play_sound(FX,15,0);
				}
				if (attack_number < 2) or (image_index > 14)
				{
					attack_number = 1;
					state = move_Manticore;
				}
			}
		}
		else //--> Air Mjolnir (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
			if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
		}
	}

	if equipped_axe == 4 //--> WHISPERER
	{
		if (ax_onFloor or ax_onPlatform)
		{
			image_speed = 1;
			if (sprite_index != spr_Whisperer)
			{
				attack_number = aux_Axe_atk_num;
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				if (sprite_index != spr_AirWhisperer) image_index = (attack_number == 2) ? 7 : 0;
			}
			sprite_index = spr_Whisperer;
			//--> Cancels into Dash
			if ((ax_key_shift)&&(direc != 0)) && (alarm[4] <= 0)
			{
				if ((image_index>=6)&&(image_index<=7))or((image_index>=11)&&(image_index<=12))
				{
					attack_number = 1;
					state = move_Manticore;
					if (image_index>=11) aux_Axe_atk_num = 1;
					exit;
				}
			}
			//--> First Swing Hitboxes
			hitbox_duration(0,3,obj_Manticore_hitbox,spr_Whisperer_hitbox1);
			hitbox_duration(3,4,obj_Manticore_hitbox,spr_Whisperer_hitbox2);
			hitbox_duration(4,6,obj_Manticore_hitbox,spr_Whisperer_hitbox3);
			if (!audio_is_playing(snd_MaceSwing1)) && (attack_number == 1) && (image_index < 1) //--> Sound FX
			{
				audio_sound_pitch(snd_MaceSwing1,random_range(0.8,1.1));
				audio_play_sound(snd_MaceSwing1,15,0);
			}
			if (!audio_is_playing(snd_WhispererSwing1)) && (attack_number == 1) && (image_index < 1) //--> Sound FX
			{
				audio_sound_pitch(snd_WhispererSwing1,random_range(0.8,1.1));
				audio_play_sound(snd_WhispererSwing1,16,0);
			}
			if (image_index > 5)and(image_index < 7) //--> First Windslash
			{
				var wowX1 = 82 * image_xscale;
				if (!place_meeting(x+wowX1,y,obj_windslash))
					instance_create_layer(x+wowX1,y,"FX_Layer",obj_windslash);
			}
			if (image_index>=3)and(image_index<=6)
			{
				if (ax_key_s) attack_number = 2;
				if (image_index>=5)
				{
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
				}
				alarm[7] = 120; //--> To continue combo after cancel
				aux_Axe_atk_num = 2;
			}
			if (image_index >= 7)
			{
				if (image_index>=12)
				{	
					if (((global.AXES[4,3]>=2)&&(equipped_axe==1))&&((equiped_sword==2)&&(global.SWORDS[equiped_sword,3]>=2)))||
					   (global.AXES[4,3] == 3) || (global.RELIKS[46,2]) || (global.RELIKS[40,2])
					{
						if (ax_key_s) && ((aux_Swd_atk_num==3)||((aux_Swd_atk_num==1)&&(alarm[7]>0))||
						   ((equiped_sword==2)&&(global.SWORDS[equiped_sword,3]>=2)))
						{
							state = strongWpnAtk;
							exit;
						}
					}
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
					aux_Axe_atk_num = 1;
				}
				if (image_index <= 9) if (!ax_key_shift)&&(direc != 0) image_xscale = direc;
				hitbox_duration(7,9,obj_Manticore_hitbox,spr_Whisperer_hitbox4);
				hitbox_duration(9,10,obj_Manticore_hitbox,spr_Whisperer_hitbox5);
				hitbox_duration(10,11,obj_Manticore_hitbox,spr_Whisperer_hitbox6);
				if (!audio_is_playing(snd_MaceSwing2)) && (attack_number == 2) //--> Sound FX
				{
					audio_sound_pitch(snd_MaceSwing2,random_range(0.8,1.1));
					audio_play_sound(snd_MaceSwing2,15,0);
				}
				if (!audio_is_playing(snd_WhispererSwing2)) && (attack_number == 2) //--> Sound FX
				{
					audio_sound_pitch(snd_WhispererSwing2,random_range(0.8,1.1));
					audio_play_sound(snd_WhispererSwing2,16,0);
				}
				if (image_index > 10)and(image_index < 11) //--> Windslash combo
				{
					var wowX3 = 30 * image_xscale;
					instance_create_layer(x+wowX3,y-100,"FX_Layer",obj_windslash);
				}
				if (attack_number < 2) or (image_index > 14)
				{
					attack_number = 1;
					state = move_Manticore;
				}
			}
		}
		else //--> Air Whisperer (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
			if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
		}
	}


}
