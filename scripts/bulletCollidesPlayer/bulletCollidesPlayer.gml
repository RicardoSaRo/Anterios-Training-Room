// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information

//--> Manage damage and status ailments the player wiil get
//--> when hit by an Enemy Bullet
function bulletCollidesPlayer(lowDmg,highDmg,manager,poison,burn,freeze,venom){
	if (obj_Manticore.alarm[11] <= 1)&&(obj_Manticore.state != defeated_Manticore)
	{
//		if (!counter)
//		{
			if (obj_Manticore.alarm[10] <= 0)
			{
				var MHP = obj_Manticore.Manticore_hp;
				switch(global.difficulty)//--> Invulnerability
				{
					case "casual":	obj_Manticore.alarm[11] = (global.RELIKS[33,2]) ? 200 : 100; break;
					case "normal":	obj_Manticore.alarm[11] = (global.RELIKS[33,2]) ? 160 : 80; break;
					case "hard":	obj_Manticore.alarm[11] = (global.RELIKS[33,2]) ? 120 : 60; break;
					case "lunatic": obj_Manticore.alarm[11] = (global.RELIKS[33,2]) ? 80 : 40; break;
				}
				obj_Manticore.alarm[10] = 10;
				if (global.RELIKS[44,2])
				{
					var damage = choose(1,1,0);
					if (damage == 0)
					{
						if (!audio_is_playing(snd_Parry)) audio_play_sound(snd_Parry,3,0);
						var parry = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_centerXYfollow);
						parry.obj_2_follow = obj_Manticore;
						parry.sprite_index = spr_clank;
						parry.brightness = true;
						parry.image_blend = c_fuchsia;
						parry.glow = true;
						parry.image_xscale = 2;
						parry.image_yscale = 2;
						parry.image_angle = random(360);
						parry.anim_end = 0.6;
						obj_Manticore.image_blend = c_fuchsia;
					}
					else if (!audio_is_playing(snd_ManticoreDmg1)) && (!audio_is_playing(snd_ManticoreDmg2)) && (!audio_is_playing(snd_ManticoreDmg3))
						audio_play_sound(choose(snd_ManticoreDmg1,snd_ManticoreDmg2,snd_ManticoreDmg3),2,0);
					MHP -= damage;
				}
				else
				{
					obj_Manticore.state = damage_Manticore; //--> Condition damage state with Venom Scalemail
					if (poison)	MHP -= (manager.ManticorePoisoned) ? highDmg : lowDmg;
					else MHP -= lowDmg;
				}
				MHP = clamp(MHP, 0, global.player_max_health);
				obj_Manticore.Manticore_hp = MHP;
//				counter = true;
			}
//		}
//		else counter = false;
	}
	
	if (venom) //--> Attack envenoms
	{
		artist.venomFX = true;
		manager.ManticoreVenomed = true;
		with (obj_Life_Orb)
		{
			if (sprite_index == spr_Life_Orb) sprite_index = spr_Life_Orb_vnmd;
			if (sprite_index == spr_Life_Orb_empty) sprite_index = spr_Life_Orb_vnmdempty;
		}
	}
	
	//--> The attack poisons
	if (poison)
	{
		artist.poisonFX = true;
	
		with (manager)
		{
			ManticorePoisoned = true;
			if (!manager.ManticoreVenomed)
			{
				switch(global.difficulty)
				{
					case "casual":	alarm[2] = 240; break;
					case "normal":	alarm[2] = 300; break;
					case "hard":	alarm[2] = 360; break;
					case "lunatic":	alarm[2] = 420; break;
				}
			}
		}
	}
	
	//--> attacks freezes
	if (burn)
	{
		artist.burnFX = true;
		
		with(manager)
		{
			ManticoreBurn = true;
			switch(global.difficulty)
			{
				case "casual":	alarm[4] = 240;	break;
				case "normal":	alarm[4] = 300; break;
				case "hard":	alarm[4] = 360; break;
				case "lunatic":	alarm[4] = 420; break;
			}
		}
	}
	
	//--> attacks freezes
	if (freeze)
	{
		artist.freezeFX = true;
		
		with(manager)
		{
			ManticoreFreeze = true;
			switch(global.difficulty)
			{
				case "casual":	alarm[3] = 300;	break;
				case "normal":	alarm[3] = 360; break;
				case "hard":	alarm[3] = 420; break;
				case "lunatic":	alarm[3] = 480; break;
			}
		}
	}
}