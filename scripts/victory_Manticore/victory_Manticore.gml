function victory_Manticore() {
	//--> VICTORY STATE

	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
	if (instance_exists(obj_EnemyBullet)) instance_destroy(obj_EnemyBullet);
	if (instance_exists(obj_Throwing_Wpn)) instance_destroy(obj_Throwing_Wpn);

	if (instance_exists(obj_Manticore_hitbox))
	{
		with (obj_Manticore_hitbox) instance_destroy();
	}

	if (instance_exists(obj_hit_FX))
	{
		with (obj_hit_FX) instance_destroy();
	}

	//-->> Variables for Results
	if (global.player_max_health < 16) global.results[results.life,lifeOrbs.broken] = 16 - global.player_max_health;
	if (Manticore_hp < global.player_max_health) global.results[results.life,lifeOrbs.empty] = global.player_max_health - Manticore_hp;
	global.results[results.life,lifeOrbs.full] = Manticore_hp;
	if (global.currentManager != noone)
	{
		var crrntMngr = global.currentManager;
		crrntMngr.alarm[1] = noone;
		global.results[results.time,0] = crrntMngr.timerSecs;
	}

	if (direc == 0) speed = 0; //--> Prevents Shaking

	hsp = 0; //--> Horizontal Speed

	if grv != 1 grv = 1; //--> Returns Gravity to normal

	vsp = vsp + grv; //--> Vertical Speed
	
	onFloor = place_meeting(x,y+1,obj_Wall);

	onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) onPlatform = true;
	else onPlatform = false;

	//--> Platform Collision Check
	 if place_meeting(x,y+vsp,obj_Platform)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) vsp = 0;
	}

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	//--> Vertical Collision (Wall)
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}

	y = y + vsp; //--> Vertical Movement
	
	regen_stop = true; //--> Stops Regen
	
	if (onFloor or onPlatform)
	{
		if (!switch_var)
		{
			image_index = 0;
			switch_var = true;
			image_speed = 1;
		}
	
		if (instance_exists(obj_AntaresOrb))
		{
			if (x > obj_AntaresOrb.x) image_xscale = -1;
			else image_xscale = 1;
		}
	
		sprite_index = spr_Manticore_victory;
	
		if ((image_index >= 26)&&(image_index <= 26.6)) && (!instance_exists(obj_V_lightning))
		{
			if (!place_meeting(x,y,obj_V_lightning))
			{
				repeat(3)
				{
					var V_lghtnng = instance_create_layer(x,y-218,"FX_Layer",obj_V_lightning);
					with (V_lghtnng)
					{
						image_yscale = 3;
						if (instance_exists(obj_AntaresOrb))
							image_angle = point_direction(x,y,obj_AntaresOrb.x,obj_AntaresOrb.y);
					}
				}
			}
		}
	
	}

	if (image_index >= 42)
	{
		image_speed = 0;
		image_index = 42;
	}


}
