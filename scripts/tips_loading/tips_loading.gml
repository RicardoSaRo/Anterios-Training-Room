/// @description tips_loading(category)
/// @param category --> Which category to extract the tip
function tips_loading(argument0) {

	// This script returns a tip for the category needed

	var category = argument0;
	var tip = "";

	//CATEGORY 0 = MOVEMENT
	if (category == 0)
	{
		tip = irandom(12) //--> Max number of tips for this category
	
		switch(tip)
		{
			case 0: tip =	@"To do a Downward Dash press your Dash key + down in the air
							  when your character starts to fall."
					break;
				
			case 1: tip =	@"To do an Upward Dash press your Dash key + up in the air."
					break;
		
			case 2: tip =	@"If your Dash ends on the air, you'll wont be able to Dash again
							  until you reach the ground or get damaged."
					break;
				
			case 3: tip =	@"You can jump in midair by pressing the Jump key while airborne
							  and can be done again after you reach the ground or get damaged."
					break;
				
			case 4: tip =	@"To do a Back Dash press your Dash key + the opposite direction
							  you are facing. This kind of dash is useful to attack pursuers
							  or defend yourself against bullets."
					break;
				
			case 5: tip =	@"Crouch by holding down while grounded. You will use your current
							  equipped Axe to shield against most enemy proyectiles. Each proyectile
							  shielded will reduce your Axe special. The shield icons are the times
							  you can stop proyectiles per crouch."
					break;
		
			case 6: tip =	@"Press left or right against a wall in the air to grab the wall.
							  You can can hold in there for a moment, then you'll fall. You can
							  regrab the wall after a second though."
					break;
				
			case 7: tip =	@"Hold your Special key while grounded to charge your specials faster.
							  You'll be open for attacks while doing this, but you can cancel the
							  action by releasing the Special key or pressing the Jump key."
					break;
				
			case 8: tip =	@"While airborne, if you dash against a wall you'll grab it immediately."
					break;
				
			case 9: tip =	@"If you keep the Charge key pressed while moving airborne or grabbing
							  walls, you sometimes charge the Special Bars for the tiniest bit."
					break;
						  
			case 10: tip =	@"Press the Jump button if you are grabbing a wall to perform a walljump."
					break;
		
			case 11: tip =	@"Forward Dash is performed by pressing Dash key + the direction you are
							  facing. You can evade some enemy attacks because it makes you a bit
							  harder to hit."
		
			case 12: tip =	@"You can drop from platforms by crouching and pressing the Jump key.
							  Also, you can pass through them with Downward Dashes."
					break;
		}
	}

	//CATEGORY 1 = STATUS
	if (category == 1)
	{
		tip = irandom(4) //--> Max number of tips for this category
	
		switch(tip)
		{
			case 0: tip =	@"If you gain Invulnerability you can't be damaged. But be wary...
							  You can still be stunned, grabbed or One Hit KO'ed by
							  some enemy Specials."
					break;
				
			case 1: tip =	@"If you are Poisoned your Regeneration will stop or slow down.
							  Also, if you are hit by a poison attack while Poisoned the
							  damage you take will be doubled."
					break;
		
			case 2: tip =	@"If you are Burn your Special Bars will slowly decrease.
							  Also, all damage you do will be 'uneffective' until your Burn heals."
					break;
				
			case 3: tip =	@"If you are Freezed your ground and air movement will be slowed.
							  Also, your Regeneration will stop."
					break;
				
			case 4: tip =	@"If you are Stunned you cannot control your character until the stun
							  ends. Some enemy attacks grabs you or locks you in place, this kind
							  of attacks are considered stuns too."		  
					break;
		}
	}

	//CATEGORY 2 = GAME MECHANICS
	if (category == 2)
	{
		tip = irandom(11) //--> Max number of tips for this category
	
		switch(tip)
		{
			case 0: tip =	@"The Scorpion Ring relik you always have equipped fills slowly
							  your Sword and Axe Special Bars if you are hunting an enemy
							  many times stronger than you."
					break;
				
			case 1: tip =	@"The Scorpion Ring relik you always have equipped gives you Regeneration
							  if you are hunting. If you don't get hit, the Regeneration will slowly
							  fill until it refills one Life Orb."
					break;
		
			case 2: tip =	@"Life Orbs are your health. They'll empty if you get damaged.
							  If a Life Orb breaks, it can't be refilled until the next battle."
					break;
				
			case 3: tip =	@"To enter some areas you'll need items to protect you from the enviroment.
							  You have a limited amount of tries in each Hunt or Exploration, so do
							  your best on every atempt!"
					break;
				
			case 4: tip =	@"Enemy proyectiles have priority as well as your Throwing Weapons.
							  If there is a collision between them, the highest priority wins."
					break;
				
			case 5: tip =	@"After you successfully regenerate a Life Orb, the Regeneration will
							  speed up until you get damaged."
					break;
		
			case 6: tip =	@"After damage, you'll be Invulnerable to damage. You can still be
							  stunned, KO'ed by most Specials or inflicted with status ailments.
							  Invulnerability time depends on the difficulty level you are playing."
					break;
				
			case 7: tip =	@"The game will pause by opening the Weapon's Menu or the Main Menu."
					break;
				
			case 8: tip =	@"If your Axe or Sword Special Bar is filled to 50% or higher, press
							  the Special key + Sword or Axe to unleash the equipped weapon's special
							  attack. Your special bar will be depleated after this."
					break;
				
			case 9: tip =	@"Sword's and Axe's specials can be unleashed when their bars reaches 50%.
							  But the more it fills after 50%, the better it gets. Specials at 100%
							  gets some bonuses for reaching its limit."
					break;
						  
			case 10: tip =	@"The DMG Tanks stores damage done to your enemies. It can be used to unleash
							  your equipped Throwing Weapon's special attack by pressing the Special key +
							  Throwing Weapon key when a tank is filled."
					break;
		
			case 11: tip =	@"DMG Tanks fills with every 5000 damage you do to your enemies. The more
							  upgrades your equipped Throwing Weapon have, the more tanks you can fill."
					break;
		}
	}

	return tip;


}
