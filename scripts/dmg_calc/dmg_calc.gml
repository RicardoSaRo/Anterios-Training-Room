/// @description dmg_calc(hit_used,wpn_sprite)
/// @param hit_used
/// @param wpn_sprite
function dmg_calc(argument0, argument1) {

	//--> DAMAGE CALCULATION
	//--> Checks for strenghts and weaknesses to inflict damage,
	//--> creates object to show damage/clank in the right position

	var hit = argument0;
	var wpn_sprite = argument1;
	var instNum = 1;

	if (wpn_sprite == spr_Elemental_Orb_OFF)||(wpn_sprite == spr_Elemental_Orb_ON)||(wpn_sprite == spr_RemoteBomb)||
	   (wpn_sprite == spr_RemoteExp_low)||(wpn_sprite == spr_RemoteExp_mid)||(wpn_sprite == spr_RemoteExp_high) exit;

	with (instance_place(x,y,obj_BossEnemy))
	{
	
		//--> If Enemy is Defeated, exits the Script
		if (self.hit_points <= 0) exit;
	
		if (hit == false)
		{
			//--> Calculates Damage and shows it
			var dmg_info_array = damage_info(self,wpn_sprite,other.sprite_index);
			var wknss = weakness_check(self,wpn_sprite);
		
			var dmg = dmg_info_array[1];
		
			switch (wknss)//--> Hitbox damages target, modifying damage with strenghts and weaknesses
			{
				case 1: var SOP = (global.RELIKS[50,2]) ? 0.5 : 0; //--> Damage increase if Skull of Power equipped
						dmg *= (global.RELIKS[49,2]) ? 2+SOP : 1.5+SOP;
					
						if (!audio_is_playing(grunt1))&&(!audio_is_playing(grunt2)) //--> Grunt sound when dmg is effective.
							audio_play_sound(choose(grunt1,grunt2),5,0);
						//--> THis code is to prevent volume increase when the same sound plays multiple times at once 
	/*					if (instance_exists(obj_show_damage)) instNum = instance_number(obj_show_damage);
						if (instNum <= 3)
						{
							audio_sound_gain(snd_EffectiveDmg,1/instNum,0);
							audio_play_sound(snd_EffectiveDmg,10,0);
						}*/
						self.dmgColor = choose(c_yellow,merge_color(c_yellow,c_orange,0.3));
						break;
				case 0: self.dmgColor = c_white; break;
				case -1: dmg *= 0.5; self.dmgColor = choose(c_white,c_ltgray); break;
				case noone: dmg = 0; self.dmgColor = choose(c_black,merge_color(c_black,c_dkgray,0.3)); break;
			}
		
			if (global.RELIKS[21,2]) && ((wpn_sprite==spr_Dyrnwyn)||(wpn_sprite==spr_DyrnwynMix)||
			   (wpn_sprite==spr_AirDyrnwyn)||(wpn_sprite==spr_AirDyrnwynMix)) && (dmg > 0)
			{
				var dmgincr = obj_Manticore.special_Dyrnwyn * 15;
				obj_Manticore.special_Dyrnwyn = 0;
				dmg += dmgincr;
			}
		
			if (global.RELIKS[22,2]) && ((wpn_sprite==spr_Argenta)||(wpn_sprite==spr_ArgentaMix)||
			   (wpn_sprite==spr_AirArgenta)||(wpn_sprite==spr_AirArgentaMix)) && (dmg > 0)
			{
				var dmgincr = obj_Manticore.special_Argenta * 15;
				obj_Manticore.special_Argenta = 0;
				dmg += dmgincr;
			}
		
			dmg = round(dmg);
		
			//--> Middle point between Hitbox and enemy to display damage/clank
		
			var hit_y = clamp(other.y,bbox_top,bbox_bottom);
			var hit_x = clamp(other.x,bbox_left,bbox_right);
			if (other.image_xscale == 1) var dir = random_range(-10,50);
			else var dir = random_range(-10,-50);
		
			//--> Hit effect
			if (dmg != 0)
			{
				with instance_create_layer(hit_x + dir,hit_y,"FX_Layer",obj_hit_FX)
				{
					hit_FX = dmg_info_array[2];
					hit_FX_angle = dmg_info_array[3];
				
					if instance_exists(obj_relampago)
					{
						x = obj_BossEnemy.x;
						y = obj_BossEnemy.y;
					}
				
					if (hit_FX == spr_spark) or (wpn_sprite == spr_windslash) or (hit_FX == spr_redspark) or
					   (wpn_sprite == spr_TornadoINNER_front) or (wpn_sprite == spr_TornadoINVERSE_back) or
					   (wpn_sprite == spr_TornadoINVERSE_front) or (wpn_sprite == spr_TornadoINNER_back)
					{
						y -= irandom_range(-60,60);
						x += irandom_range(-60,60);
					}
				
					if (wpn_sprite == spr_Korunax) or (wpn_sprite == spr_AirKorunax)
					{
						for (var i=1; i<4; i++)
						{
							instance_create_layer(hit_x,hit_y,"FX_Layer",obj_rockice_FX);
						}
					}
				
					if (wpn_sprite == spr_Frostbiter)or(wpn_sprite == spr_AirFrostbiter)or(wpn_sprite == spr_FrostBulwark)||
					   (wpn_sprite == spr_FrostbiterMix)or(wpn_sprite == spr_AirFrostbiterMix)or
					   (wpn_sprite == spr_Argenta_DashStab)or((global.RELIKS[34,2])&&(primaryWpnSpr(wpn_sprite)))
					{
						var icy = instance_create_layer(hit_x,hit_y,"FX_Layer",obj_rockice_FX);
						icy.sprite_index = spr_icys_FX;
					
						//--> Checks if Enemy can be slowed down
						var slowAmount = (wpn_sprite == spr_Argenta_DashStab) ? 90: 0.5 + global.SWORDS[4,3]/3;
						var Slow = frostbiterSlow(other,slowAmount);
						if (wpn_sprite == spr_Argenta_DashStab) Slow = false;
						//--> Snowflake FX if Slow is inflicted
						var showFX = irandom(4 - global.SWORDS[4,3]);
						if (showFX == 1) && (Slow)
						{
							var slowFX = instance_create_layer(hit_x,hit_y,"FX_Layer",obj_GrassLeaf);
							slowFX.sprite_index = spr_snowflake;
							slowFX.xmov = irandom_range(-5,5);
							slowFX.rotatin = 1;
							slowFX.image_index = irandom(5);
							slowFX.image_alpha = choose(0.7,1);
							slowFX.alarm[0] = choose(30,40);
							slowFX.image_speed = 0;
							slowFX.image_angle = random(360);
							slowFX.image_xscale = random_range(1,2) * choose(-1,1);
							slowFX.image_yscale = random_range(1,2);
						}
						//--> Freeze SndFX
						if (!audio_is_playing(snd_freeze1))&&(!audio_is_playing(snd_freeze2))&&
						   (!audio_is_playing(snd_freeze3))&&(!audio_is_playing(snd_freeze4))
						{
							var sndFX = choose(snd_freeze1,snd_freeze2,snd_freeze3,snd_freeze4);
							audio_play_sound(sndFX,18,0);
						}
					}
				
					if (wpn_sprite == spr_AirArgenta) or (wpn_sprite == spr_AirArgentaMix) or
					   (wpn_sprite == spr_AirDyrnwyn) or (wpn_sprite == spr_AirDyrnwynMix)
					{
						with (obj_Manticore)//-->Makes Manticore floaty with Air Sword dmg and resets DJ and Dash
						{
							if (vsp >= 0)
							{
								vsp = clamp(vsp,-7,choose(3,3,5,7));
								if (image_index >= 6.6)
								{
									lghtDash_used = false;
									if (doublejump_used) airDash_used = false;
									doublejump_used = false;
									windjump_used = false;
								}
							}
						}
					}
				}
			}
		
			//--> Creates damage/clank object FX
			with (instance_create_layer(hit_x + dir,hit_y,"Info_Layer",obj_show_damage))
			{
				damage_display = dmg;
				if (dmg == 0) wknss = noone;
				weakness = wknss;
			}
		
			//--> Overkill Results
			if ((hit_points-dmg) <= 0) && (enemy_name != "normalVine") && (global.results[results.overkill,ovrkll.okdmg] == 0)
			{
				global.results[results.overkill,ovrkll.okdmg] = abs(dmg - hit_points);
				global.results[results.overkill,ovrkll.girtabfinish] = (wpn_sprite == spr_Special_GirTab) ? true : false;
			}
		
			hit_points -= dmg; //--> Damage to enemy HP
			//with (self) hit_points -= dmg; //--> Damage only the enemy being hit
			//with (obj_BossEnemy) hit_points -= dmg; //--> All instances takes the damage
			
			var isSP = spAtkSpr(wpn_sprite,other); //-->> Checks if the attack sprite is from a SpAtk
			var DHequip = (global.RELIKS[25,2]) ? true : false;
			if (!isSP) || ((isSP)&&(DHequip))
			{
				var maxTank = (DHequip) ? 7000 : 5000;
				var tank2fill = (obj_Manticore.TWTank[1,0] < maxTank) ? 1 : ((obj_Manticore.TWTank[2,0] < maxTank) ? 2 : ((obj_Manticore.TWTank[3,0] < maxTank) ? 3 : 4));
				obj_Manticore.TWTank[tank2fill,0] += dmg;
				obj_Manticore.TWTank[tank2fill,0] = clamp(obj_Manticore.TWTank[tank2fill,0],0,maxTank);
			}
		
			if (global.RELIKS[36,2]) //-->> Fills Void Ripples with Dreamcatcher equipped
			{
				obj_Manticore.VRcounter += dmg;
				//var maxVR = (global.RELIKS[36,2]) ? 1000 : 2000;
				if (obj_Manticore.VRcounter > 1000)
				{
					obj_Manticore.VRcounter = 0;
					obj_Manticore.TWammo[16,0] += 1;
					obj_Manticore.TWammo[16,0] = clamp(obj_Manticore.TWammo[16,0],0,obj_Manticore.TWammo[16,1]);
				}
			}
		
			if (global.RELIKS[4,2])
			{
				var pwdmg = primaryWpnSpr(wpn_sprite);
				if (pwdmg)
				{
					if (obj_Manticore.Manticore_hp != global.player_max_health) //--> Increase Regen
					{
						var dmgmngr = global.currentManager;
						obj_Manticore.alarm[5] -= (dmgmngr) ? (dmg / 20) * -1 : dmg / 15;
						obj_Manticore.alarm[5] = clamp(obj_Manticore.alarm[5],0,artist.max_Regen);
					}
				}
			}
			
			if (global.RELIKS[19,2])
			{
				var pwdmg = primaryWpnSpr(wpn_sprite);
				if (pwdmg)
				{
					moneyGain = round(dmg/100);
					if (moneyGain > 0)
					{
						global.MONEY += moneyGain;
						var coin = instance_create_layer(x,y,"FX_Layer",obj_rockice_FX)
						coin.sprite_index = spr_coin;
					}
				}
			}
		}
	}


}
