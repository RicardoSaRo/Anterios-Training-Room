function save_videoOptns() {
	switch(selecWinMode)
	{
		case 1:	window_set_fullscreen(true);
				alarm[0] = 1;
				break;
	
		case 2: if (window_get_fullscreen()) window_set_fullscreen(false);
				alarm[0] = 1;
				break;
	
		case 3: if (window_get_fullscreen()) window_set_fullscreen(false);
				window_set_size(display_get_width(),display_get_height());
				alarm[0] = 1;
				break;
	}

	var aa = (display_aa < 2) ? 0 : ((display_aa < 4) ? 2 : ((display_aa < 8) ? 4 : 8));
	switch(selecVsync)
	{
		case 1: display_reset(aa,false); break;
		case 2: display_reset(aa,true); break;
	}

	switch(selecAlfa)
	{
		case 1: gpu_set_alphatestenable(true);
				gpu_set_alphatestref(1);
				break;
			
		case 2: gpu_set_alphatestenable(true);
				gpu_set_alphatestref(32);
				break;
			
		case 3:	gpu_set_alphatestenable(true);
				gpu_set_alphatestref(64);
				break;
	}

	switch(selecRes)
	{
		case 1: ResW = 960; ResH = 540; break;
		case 2: ResW = 1024; ResH = 576; break;
		case 3: ResW = 1280; ResH = 720; break;
		case 4: ResW = 1366; ResH = 768; break;
		case 5: ResW = 1600; ResH = 900; break;
	}

	ini_open("Options.ini")

	ini_write_real("VideoOptions","WindowMode",selecWinMode);
	ini_write_real("VideoOptions","Vsync",selecVsync);
	ini_write_real("VideoOptions","AlfaBlend",selecAlfa);
	ini_write_real("VideoOptions","ResWidth",ResW);
	ini_write_real("VideoOptions","ResHeight",ResH);

	ini_close();


}
