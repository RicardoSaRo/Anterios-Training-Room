//--> Used for the lights on the Title Screen

function ttl_stronger_light() {
	//--> Searchs for the strongest light in the background title screen

	var strongest_light

	//--> Light1
	if (lgt1_alfa >= lgt2_alfa) strongest_light = lgt1_alfa;
	else strongest_light = lgt2_alfa;
	if (lgt1_alfa >= lgt3_alfa) strongest_light = lgt1_alfa;
	else strongest_light = lgt3_alfa;
	if (lgt1_alfa >= lgt4_alfa) strongest_light = lgt1_alfa;
	else strongest_light = lgt4_alfa;
	if (lgt1_alfa >= lgt5_alfa) strongest_light = lgt1_alfa;
	else strongest_light = lgt5_alfa;

	//--> Light2
	if (lgt2_alfa >= lgt3_alfa) strongest_light = lgt2_alfa;
	else strongest_light = lgt3_alfa;
	if (lgt2_alfa >= lgt4_alfa) strongest_light = lgt2_alfa;
	else strongest_light = lgt4_alfa;
	if (lgt2_alfa >= lgt5_alfa) strongest_light = lgt2_alfa;
	else strongest_light = lgt5_alfa;

	//--> Light3
	if (lgt3_alfa >= lgt4_alfa) strongest_light = lgt3_alfa;
	else strongest_light = lgt4_alfa;
	if (lgt3_alfa >= lgt5_alfa) strongest_light = lgt3_alfa;
	else strongest_light = lgt5_alfa;

	//--> Light4
	if (lgt4_alfa >= lgt5_alfa) strongest_light = lgt4_alfa;
	else strongest_light = lgt5_alfa;

	return strongest_light;
}
