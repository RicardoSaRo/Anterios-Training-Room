function Zefirius() {
	//--> Thunder HoloFamiliar - Zefirius

	if (image_alpha < 1) image_alpha += 0.05;
	
	if (sprite_index == spr_WindFamiliar_Standing)
	{
		var ManticoreX = obj_Manticore.x + (45 * obj_Manticore.image_xscale);
		var ManticoreY = obj_Manticore.y - 150;

		//--> Speed to chatch up player
		var shieldSPD = hsp;
		var max_acc = 0.020;
		hsp += 0.0050;
		hsp = clamp(hsp,0,max_acc);
		shieldSPD = hsp;
	
		x = lerp(x,ManticoreX,shieldSPD);
		y = lerp(y,ManticoreY,0.025);
	
		if (image_index >= 5.6)
		{
			image_index = 0;
		
			if (obj_BossEnemy.x >= x)
			{
				if (image_xscale == -1.5)
				{
					image_xscale = 1.5;
					image_alpha = 0;
				}
			}
			else
			{
				if (image_xscale == 1.5)
				{
					image_xscale = -1.5;
					image_alpha = 0;
				}
			}
		}
	}

	if ((sprite_index == spr_WindFamiliar_Attack) && (image_index >= 5.6)) ||
	   ((sprite_index == spr_WindFamiliar_Create) && (image_index >= 5.6))
	{
		sprite_index = spr_WindFamiliar_Standing;
		image_index = 0;
	}

	if (alarm[0] <= 0) && (!switch_var)
	{
		var distance = distance_to_object(obj_BossEnemy);
	
		var FamAttk = (distance<150) ? 0 : ((distance<350) ? 1 : 2);
		var playerhp = obj_Manticore.Manticore_hp;
		if (playerhp < global.player_max_health)
		{
			var healing = (irandom(playerhp)==1) ? 1 : 0;
			if (healing) FamAttk = 3;
		}

		switch(FamAttk)
		{
			case 2:
				for(var i=1; i<=20; i++)
				{
					var WS = instance_create_layer(x,y,"Back_FX_Layer",obj_windslash);
					WS.ws_movementX = random_range(7,20) * image_xscale;
					WS.ws_movementY1 = random_range(-30,-15);
					WS.ws_movementY2 = random_range(15,30);
					WS.source = "WindOrb";
					WS.image_xscale = sign(WS.ws_movementX);
					WS.image_yscale = sign(choose(WS.ws_movementY1,WS.ws_movementY2));
				}
				break;
			
			case 1:
				for(var i=1; i<=3; i++)
				{
					var ww = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
					ww.sprite_index = spr_Whirlwind;
					ww.Consum = false;
					ww.image_xscale = image_xscale;
					ww.angle = i * 120;
					ww.instnc = self;
					ww.alarm[0] = 40;
					ww.alarm[1] = 20;
					ww.alarm[2] = 3;
				}
				break;
			
			case 0:
				var hurricane = instance_create_layer(x,y-80,"FX_layer",obj_TWSpecial);
				hurricane.sprite_index = spr_TornadoINNER_front;
				hurricane.Consum = false;
				hurricane.image_alpha = 0;
				hurricane.image_yscale = -1;
				hurricane.alarm[0] = 6 - global.AXES[4,3];
				break;
			
			case 3:
				var heal = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"Back_FX_Layer",obj_FX_Scorpion);
				heal.sprite_index = spr_Healing;
				heal.image_blend = c_white;
				obj_Manticore.Manticore_hp += 1;
				obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
			
				repeat(10)
				{
					var cc_spawnX = obj_Manticore.x + random_range(-70,70);
					var spawn_layer = choose("Back_FX_Layer","Info_Layer");
					var chispa = instance_create_layer(cc_spawnX,obj_Manticore.bbox_bottom,spawn_layer,obj_charge_chispa);
					chispa.image_xscale = 5;
					chispa.image_yscale = 5;
					chispa.angl = choose(45);
				}
				break;
		}
	
		alarm[0] = 180;
		sprite_index = spr_WindFamiliar_Attack;
		image_index = 0;
	}

	if (alarm[1] <= 0) && (!switch_var)
	{
		sprite_index = spr_WindFamiliar_Crumbles;
		image_index = 0;
		switch_var = true;
	}

	if (place_meeting(x,y,obj_EnemyBullet))
	{
		var instnear = instance_nearest(x,y,obj_EnemyBullet);
		if (instnear.priority < 10) && (instnear.sprite_index != instnear.destroy_sprite)
			alarm[1] -= 30;
	}

	if (sprite_index == spr_WindFamiliar_Crumbles) && (image_index >= 6.6) instance_destroy();


}
