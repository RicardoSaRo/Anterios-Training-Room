// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Rainbow_Sphere(){
	image_blend = interact_instance2.rainbow_blend;
	
	if (image_yscale < 1)
	{
		image_yscale += 0.01;
		image_xscale += 0.01 * sign(image_xscale);
	}
	
	if ((interact_instance.sprite_index == spr_RadiantP_Rainbow_end)||(interact_instance.sprite_index == spr_VioletP_RainbowS_end))&&
	   (sprite_index == spr_RainbowShroud)
	{
		if (interact_instance.image_index >= 0) && (interact_instance.image_index < 1)
		{
			x = interact_instance.x + (110*interact_instance.image_xscale);
			y = interact_instance.y - (15*interact_instance.image_yscale);
		}
		if (interact_instance.image_index >= 1) && (interact_instance.image_index < 2)
		{
			x = interact_instance.x + (85*interact_instance.image_xscale);
			y = interact_instance.y;
		}
		if (interact_instance.image_index >= 2)&&(sprite_index != spr_RainbowShroud_destroy)
		{
			if (!switch_var)
			{
				image_index = 0;
				switch_var = true;
				fshockwave_create_layer(x,y,"Instances",view_camera[0],48,208,2,24,70,1,interact_instance2.rainbow_blend,1);
			}
			defeated_spr = spr_RainbowShroud_absorb;
			sprite_index = spr_RainbowShroud_absorb;
			x = interact_instance.x + (65*interact_instance.image_xscale);
			y = interact_instance.y + (15*interact_instance.image_yscale);
		}
	}
	
	if (alarm[1] > 60)
	{
		if (instance_number(obj_Absorb) < 15)
		{
			var rndXplus = random_range(x+400,x+600);
			var rndXminus = random_range(x-400,x-600);
			var rndYplus = random_range(y+400,y+600);
			var rndYminus = random_range(y-400,y-600);
			var absorb = instance_create_layer(random_range(rndXplus,rndXminus),random_range(rndYplus,rndYminus),"Back_FX_Layer",obj_Absorb);
			absorb.alfaplus = random_range(0.03,0.04);
			absorb.obj2follow = self;
			var rndmSpd = random_range(35,45);
			absorb.XYspd = rndmSpd;
			var rndmScale = random_range(0.3,0.8);
			absorb.image_xscale = rndmScale;
			absorb.image_yscale = rndmScale;
			absorb.image_blend = choose(c_orange,c_yellow,c_aqua,c_lime,c_fuchsia,interact_instance2.rainbow_blend);
		//	absorb.glow = choose(0,1);
		//	absorb.bright = choose(0,1);
			absorb.starFX = choose(false,true);
			absorb.twnklSpd = random_range(0.5,2.5);
		}
	}
	
	switch(sprite_index)
	{
		case spr_RainbowShroud_create:
			if (image_index >= 6.6)
			{
				image_index = 0;
				sprite_index = spr_RainbowShroud;
			}
			break;
			
		case spr_RainbowShroud:
			if (hit_points <= 0)
			{
				image_index = 0;
				sprite_index = defeated_spr;
				interact_instance.image_index = 0;
				interact_instance.sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Rainbow_end : spr_VioletP_RainbowS_end;
				interact_instance.interact_instance = interact_instance;
			}
			if (alarm[1] <= 0)&&(!switch_var2)
			{
				interact_instance.image_index = 0;
				interact_instance.sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Rainbow_end : spr_VioletP_RainbowS_end;
				switch_var2 = true;
			}
			break;
			
		case spr_RainbowShroud_destroy:
			y--;
			if (image_index >= 6.6)
			{
				interact_instance.interact_instance = interact_instance;
				instance_destroy();
			}
			break;
			
		case spr_RainbowShroud_absorb:
			if (image_index >= 2.6)
			{
				with(interact_instance)
				{
					if (hit_points < max_hit_points)
					{
						var healAmount = 0;
	
						switch(global.difficulty)
						{
							case "casual": healAmount = 2000; break;
							case "normal": healAmount = 4000; break;
							case "hard": healAmount = 6000; break;
							case "lunatic": healAmount = 8000; break;
						}
		
						healAmount = clamp(healAmount,0,max_hit_points - hit_points);
						hit_points += healAmount;
		
						var numbers = instance_create_layer(other.x,other.y,"FX_Layer",obj_show_damage)
						numbers.font_color = c_lime;
						numbers.damage_display = healAmount;
						numbers.plus_or_minus = "+";
						numbers.y_speed = 1;
						
					}
				}
				instance_create_layer(x,y,"Info_Layer",obj_timedInvin);
				interact_instance.interact_instance = interact_instance;
				instance_destroy();
			}
			break;
	}
}