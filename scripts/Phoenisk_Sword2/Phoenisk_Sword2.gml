// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Sword2(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Sword_start : spr_VioletP_Sword_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 1.6) && (!switch_var2)
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Sworddwn : spr_VioletP_Sworddwn;
		switch_var2 = true;
	}
	
	if (image_index >= 1) && (!switch_var3)
	{
		var htbx = instance_create_layer(x+(128*image_xscale),y+(82*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx.sprite_index = spr_PhoeniskSwd2_htbx;
		htbx.image_xscale = image_xscale;
		htbx.image_yscale = image_yscale;
		
		switch_var3 = true;
	}
	
	if (image_index >= 3) && (!counter)
	{
		if (instance_exists(obj_Enemy_hitbox)) instance_destroy(obj_Enemy_hitbox);
		
		var tailexpl = instance_create_layer(x+(235*image_xscale),y+(165*image_yscale),"FX_Layer",obj_EnemyBullet);
		tailexpl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_grnd : spr_Explosion_blue;
		if (global.BossVariant == 1) tailexpl.image_blend = c_fuchsia;
		tailexpl.NoDamage = true;
		tailexpl.bounce = true;
		
		counter = true;
	}
	
	if (image_index >= 4.6)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		counter = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}