function Pyrneth() {
	//--> Fire HoloFamiliar - Pyrneth

	if (image_alpha < 1) image_alpha += 0.05;
	
	if (sprite_index == spr_FireFamiliar_Standing) || (sprite_index == spr_FireFamiliar_Onfire)
	{
		var ManticoreX = obj_Manticore.x + (45 * obj_Manticore.image_xscale);
		var ManticoreY = obj_Manticore.y - 150;

		//--> Speed to chatch up player
		var shieldSPD = hsp;
		var max_acc = 0.010;
		hsp += 0.0025;
		hsp = clamp(hsp,0,max_acc);
		shieldSPD = hsp;
	
		x = lerp(x,ManticoreX,shieldSPD);
		y = lerp(y,ManticoreY,0.025);
	
		if (sprite_index == spr_FireFamiliar_Onfire) 
		{
			if (place_meeting(x,y,obj_BossEnemy))
			{
				var burn = choose(0,0,1);
				if (burn) dmg_calc(sptw_hit_used,sprite_index);
			}
		}
	
		if (image_index >= 3.6)
		{
			sprite_index = choose(spr_FireFamiliar_Standing,spr_FireFamiliar_Onfire);
			image_index = 0;
		
			if (obj_BossEnemy.x >= x)
			{
				if (image_xscale == -1.5)
				{
					image_xscale = 1.5;
					image_alpha = 0;
				}
			}
			else
			{
				if (image_xscale == 1.5)
				{
					image_xscale = -1.5;
					image_alpha = 0;
				}
			}
		}
	}

	if ((sprite_index == spr_FireFamiliar_Attack) && (image_index >= 3.6)) ||
	   ((sprite_index == spr_FireFamiliar_Create) && (image_index >= 11.6))
	{
		if (sprite_index == spr_FireFamiliar_Attack)
		{
			if (place_meeting(x,y,obj_BossEnemy))
			{
				var burn = choose(0,0,1);
				if (burn) dmg_calc(sptw_hit_used,sprite_index);
			}
		}
	
		sprite_index = choose(spr_FireFamiliar_Standing,spr_FireFamiliar_Onfire);
		image_index = 0;
	}

	if (alarm[0] <= 0) && (!switch_var)
	{
		var distance = distance_to_object(obj_BossEnemy);
	
		var FamAttk = (distance<300) ? 0 : ((distance<600) ? 1 : 2);

		switch(FamAttk)
		{
			case 1:
				var mirageUp = (global.AXES[2,3]<=2) ? 2 : 3;
				repeat(mirageUp)
				{
					for(var i=1; i<=8; i++)
					{
						var Fireball = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
						Fireball.Consum = false;
						Fireball.sprite_index = spr_Fireball;
						Fireball.hsp = random_range(-10,-20);
						Fireball.hsp *= (i<=3) ? image_xscale : -image_xscale;
						Fireball.vsp = random_range(-10,-50);
						Fireball.image_alpha = 0.7;
						Fireball.speed = 10;
						Fireball.speed *= (i<=5) ? image_xscale : -image_xscale;
					}
				}
				break;
			
			case 0:
				var FT = instance_create_layer(x,y,"Back_FX_Layer",obj_TWSpecial);
				FT.sprite_index = spr_flametongues;
				FT.Consum = false;
				FT.image_xscale = image_xscale;
				FT.alarm[0] = 3;
				FT.alarm[1] = 180;
				break;
			
			case 2:
				for(var i=1; i<=5; i++)
				{
					var yDist = 60 - (20 * i);
					var xDist = (i<=3) ? -60 + (20 * i) : ((i==4) ? -20 : -40);
					var HFB = instance_create_layer(x+xDist,y+yDist,"Back_FX_Layer",obj_Hmmng_Fireball);
					HFB.wmov = true;
				}
				var FX = instance_create_layer(x,y,"Fx_Layer",obj_centerXYfollow);
				FX.sprite_index = spr_firering_FX;
				FX.obj_2_follow = other;
				FX.anim_end = 3;
				FX.brightness = true;
				FX.glow = true;
				FX.g_amount = 20;
				FX.image_xscale = 2;
				FX.image_yscale = 2;	
				break;
		}
	
		alarm[0] = 120;
		sprite_index = spr_FireFamiliar_Attack;
		image_index = 0;
	}

	if (alarm[1] <= 0) && (!switch_var)
	{
		sprite_index = spr_FireFamiliar_Crumbles;
		image_index = 0;
		switch_var = true;
	}

	if (place_meeting(x,y,obj_EnemyBullet))
	{
		var instnear = instance_nearest(x,y,obj_EnemyBullet);
		if (instnear.priority < 10) && (instnear.sprite_index != instnear.destroy_sprite)
			alarm[1] -= 30;
	}

	if (sprite_index == spr_FireFamiliar_Crumbles) && (image_index >= 18.6) instance_destroy();


}
