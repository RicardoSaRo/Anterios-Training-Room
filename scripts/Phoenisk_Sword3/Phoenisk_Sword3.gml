// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Sword3(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Sword_start : spr_VioletP_Sword_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 1.6) && (!switch_var2)
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Swordfrnt : spr_VioletP_Swordfrnt;
		switch_var2 = true;
	}
	
	if (image_index >= 3) && (!switch_var3)
	{
		var htbx = instance_create_layer(x+(205*image_xscale),y+(25*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx.sprite_index = spr_PhoeniskSwd3_htbx;
		htbx.image_xscale = image_xscale;
		htbx.image_yscale = image_yscale;
		
		switch_var3 = true;
	}
	
	if (image_index > 5) && (!counter)
	{
		var tailexpl = instance_create_layer(x+(355*image_xscale),y+(25*image_yscale),"FX_Layer",obj_EnemyBullet);
		tailexpl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_grnd : spr_Explosion_blue;
		if (global.BossVariant == 1) tailexpl.image_blend = c_fuchsia;
		tailexpl.image_angle = (sign(image_xscale)) ? 270 : 90;
		tailexpl.NoDamage = true;
		
		for (var i=1;i<=3;i++)
		{
			var vSmallB = instance_create_layer(x+(355*image_xscale),y+(25*image_yscale),"FX_Layer",obj_EnemyBullet);
			vSmallB.sprite_index = spr_Rainbow_Fireball;
			vSmallB.destroy_sprite = spr_RainbowExplosion;
			vSmallB.NoDamage = true;
			vSmallB.image_angle = (sign(image_xscale)) ? 0 : 180;
			vSmallB.direction = (sign(image_xscale)) ? 0 : 180;
			vSmallB.alarm[0] = random_range(12,25);
			vSmallB.alarm[1] = 3;
			vSmallB.speed = 10;
			vSmallB.priority = 10;
			switch(image_xscale)
			{
				case 2:	case 1:
					switch(i)
					{
						case 1:	vSmallB.direction = 0;
								vSmallB.image_angle = 0;
								break;
						case 2:	vSmallB.direction = 30;
								vSmallB.image_angle = 30;
								break;
						case 3:	vSmallB.direction = 330;
								vSmallB.image_angle = 330;
								break;
					}
					break;
				
				case -2: case -1:
					switch(i)
					{
						case 1:	vSmallB.direction = 180;
								vSmallB.image_angle = 180;
								break;
						case 2:	vSmallB.direction = 210;
								vSmallB.image_angle = 210;
								break;
						case 3:	vSmallB.direction = 150;
								vSmallB.image_angle = 150;
								break;
					}
					break;
			}
		}
		
		counter = true;
	}
	
	if (image_index >= 5) if (instance_exists(obj_Enemy_hitbox)) instance_destroy(obj_Enemy_hitbox);
	
	if (image_index >= 6.6)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		counter = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}