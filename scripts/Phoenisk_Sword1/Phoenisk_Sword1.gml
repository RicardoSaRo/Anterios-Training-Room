// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Sword1(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Sword_start : spr_VioletP_Sword_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 1.6) && (!switch_var2)
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Swordslash : spr_VioletP_Swordslash;
		switch_var2 = true;
	}
	
	if (image_index >= 3) && (!switch_var3)
	{
		var htbx1 = instance_create_layer(x+(145*image_xscale),y+(45*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx1.sprite_index = spr_PhoeniskSwd1_htbx1;
		htbx1.image_xscale = image_xscale;
		htbx1.image_yscale = image_yscale;
		
		var htbx2 = instance_create_layer(x+(92*image_xscale),y-(55*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx2.sprite_index = spr_PhoeniskSwd1_htbx2;
		htbx2.image_xscale = image_xscale;
		htbx2.image_yscale = image_yscale;
		
		var htbx3 = instance_create_layer(x+(5*image_xscale),y-(130*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx3.sprite_index = spr_PhoeniskSwd1_htbx2;
		htbx3.image_xscale = image_xscale;
		htbx3.image_yscale = image_yscale;
		
		repeat(7)
		{
			var DX = choose(htbx1,htbx2,htbx3);
			var Fireball = instance_create_layer(DX.x,DX.y,"FX_Layer",obj_EnemyBullet);
			Fireball.sprite_index = spr_Fireball;
			Fireball.image_blend = (global.BossVariant == 0) ? c_white : c_purple;
			Fireball.destroy_sprite = spr_Explosion_grnd;
			Fireball.priority = 1;
			Fireball.hsp = random_range(-20,10) * sign(image_xscale);
			Fireball.vsp = random_range(-10,-30);
			Fireball.speed = 10 * sign(image_xscale);
		}
		
		switch_var3 = true;
	}
	
	if (image_index >= 5) if (instance_exists(obj_Enemy_hitbox)) instance_destroy(obj_Enemy_hitbox);
	
	if (image_index >= 6.6)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}