// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Cluster(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathStart : spr_VioletP_Breath_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 3.6) && (!switch_var2)
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Breath : spr_VioletP_Breath;
		switch_var2 = true;
	}
	
	if (sprite_index == spr_RadiantP_Breath) || (sprite_index == spr_VioletP_Breath)
	{
		if (!switch_var3)
		{
			var implosion = instance_create_layer(x+(80*image_xscale),y+(35*image_yscale),"FX_Layer",obj_EnemyBullet);
			implosion.sprite_index = spr_Rainbow_implosion;
			implosion.switch_var2 = (enemy_name == "PhoeniskR") ? 0 : 1; //--> reference for Burn/Poison
			implosion.image_xscale = sign(image_xscale);
			implosion.image_angle = random(360);
			implosion.priority = 10;
			implosion.ownership = global.currentManager;
			implosion.NoDamage = true;
			soundGain_by_distance(obj_Manticore,200,0.7,snd_DL_Implosion,irandom_range(30,60),0);
			
			switch_var3 = true;
		}
		
		if (image_index >= 2.6)
		{
			image_index = 0;
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathEnd : spr_VioletP_Breath_end;
		}
	}
	
	if (sprite_index == spr_RadiantP_BreathEnd) || (sprite_index == spr_VioletP_Breath_end)
	{
		if (image_index >= 3.6)
		{
			//--> Ends State and goes to Standing
			switch(global.difficulty)
			{
				case "casual": alarm[1] = 90; break;
				case "normal": alarm[1] = 80; break;
				case "hard": alarm[1] = 70; break;
				case "lunatic": alarm[1] = 60; break;
			}
	
			if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
			else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
			image_blend = c_white;
			image_index = 0;
			switch_var = false;
			switch_var2 = false;
			switch_var3 = false;
			state = Phoenisk_Standing;
	
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
			AtkTurn += 1;
		}
	}
}