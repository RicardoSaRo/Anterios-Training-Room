function snd_Stop_Manticore() {
	//--> Interrupts all audio the object Manticore is playing.
	if (state != dash_Manticore) audio_stop_sound(snd_Dash);
	audio_stop_sound(snd_RunningStone);
	audio_stop_sound(snd_ArgentaSwing1);
	if (atk_type != 1) audio_stop_sound(snd_ArgentaSwing2);
	audio_stop_sound(snd_ArgentaSwing3);

	exit;


}
