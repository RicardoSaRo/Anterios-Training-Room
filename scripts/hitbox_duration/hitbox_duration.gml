/// @description hitbox_duration(index1,index2,obj,sprite)
/// @param index1
/// @param index2
/// @param obj
/// @param sprite
function hitbox_duration(argument0, argument1, argument2, argument3) {

	if (image_index >= argument0) and (image_index <= argument1)
	{
		if (instance_number(argument2)<1)
		{
			with (instance_create_depth(x,y,0,argument2))
			{
				sprite_index = argument3;
				image_xscale = other.image_xscale;
			}
		}
	}


}
