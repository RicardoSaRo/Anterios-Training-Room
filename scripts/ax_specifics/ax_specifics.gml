//--> Function on the works. Will be called by "axe_atks_Manticore" and the goal is to quit code repetition on that Function.
/*
function ax_specifics(){
	//--> Sprite assignation for equipped Axe
	var airSprite = (equipped_axe == 1) ? spr_AirKorunax : (equipped_axe == 2) ? spr_AirMirage : (equipped_axe == 3) ? spr_AirMjolnir : spr_AirWhisperer;
	var axSprite = (equipped_axe == 1) ? spr_Korunax : (equipped_axe == 2) ? spr_Mirage : (equipped_axe == 3) ? spr_Mjolnir : spr_Whisperer;
	
	if (ax_onFloor or ax_onPlatform)
		{
			if (equipped_axe == 1) || (equipped_axe == 2) image_speed = (global.RELIKS[48,2]) ? 1.5 : 1;
			else image_speed = 1;
			//--> First Swing
			if (sprite_index != axSprite)
			{
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				attack_number = aux_Axe_atk_num;
				if (sprite_index != airSprite) image_index = (attack_number == 2) ? 7 : 0;
			}
			sprite_index = axSprite;
			
			//--> Cancels into Dash
			//-->Index variables for specific frames in the animations to cancel into dash.
			var indx1 = (equipped_axe == 1) ? 5 : (equipped_axe == 3) ? 7 : 6;
			var indx2 = ((equipped_axe == 1)||(equipped_axe == 4)) ? 7 : 8;
			var indx3 = ((equipped_axe == 1)||(equipped_axe == 4)) ? 11 : 12;
			var indx4 = (equipped_axe == 4) ? 12 : 13;
			var cnclIndx = ((equipped_axe == 2)||(equipped_axe == 3)) ? 12 : 11;
			if ((ax_key_shift)&&(direc != 0)) && (alarm[4] <= 0)
			{
				if ((image_index>=indx1)&&(image_index<=indx2))or((image_index>=indx3)&&(image_index<=indx4))
				{
					attack_number = 1;
					state = move_Manticore;
					if (image_index>=cnclIndx) aux_Axe_atk_num = 1;
					exit;
				}
			}
			//-->Hitboxes first swing
			hitbox_duration(0,3,obj_Manticore_hitbox,spr_Korunax_hitbox1);
			hitbox_duration(3,4,obj_Manticore_hitbox,spr_Korunax_hitbox2);
			hitbox_duration(4,6,obj_Manticore_hitbox,spr_Korunax_hitbox3);
			
			//--> Sounds for first Swing
			var sound1 = (equipped_axe == 1) ? snd_KorunaxSwing1 : (equipped_axe == 2) ? snd_MirageSwing1 : (equipped_axe == 4) ? snd_WhispererSwing1 :
						 choose(snd_MjolnirSwing2,snd_MjolnirSwing4,snd_MjolnirSwing6,snd_MjolnirSwing8,snd_MjolnirSwing10);
			
			switch(equipped_axe)
			{
				case 1: //--> KORUNAX
					if (!audio_is_playing(sound1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
					{
						audio_play_sound(sound1,15,0);
					}
					break;
					
				case 2: //--> MIRAGE
					if (!audio_is_playing(sound1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
					{
						audio_play_sound(sound1,15,0);
					}
					//--> MIRAGE FLAMES
					if (image_index > 5)and(image_index < 6) //--> First Flame
					{
						var wowX1 = 82 * image_xscale;
						if (!place_meeting(x+wowX1,y,obj_willowisp))
						{
							var willowisp1 = instance_create_depth(x+wowX1,y,0,obj_willowisp);
							willowisp1.image_xscale = image_xscale;
						}
					}
					if (image_index > 9)and(image_index < 10) //--> Second Flame
					{
						var wowX2 = 68 * image_xscale;
						if (!place_meeting(x+wowX2,y-90,obj_willowisp))
						{
							var willowisp2 = instance_create_depth(x+wowX2,y-90,0,obj_willowisp);
							willowisp2.image_xscale = image_xscale;
						}
					}
					if (image_index > 10)and(image_index < 11) //--> Third Flame
					{
						var wowX3 = 3 * image_xscale;
						if (!place_meeting(x+wowX3,y-144,obj_willowisp))
						{
							var willowisp3 = instance_create_layer(x+wowX3,y-144,"FX_Layer",obj_willowisp);
							willowisp3.image_xscale = image_xscale;
						}
					}
					break;
				case 3: //--> MJOLNIR
					if ((!audio_is_playing(snd_MjolnirSwing1))&(!audio_is_playing(snd_MjolnirSwing3))&&
				    (!audio_is_playing(snd_MjolnirSwing5))&&(!audio_is_playing(snd_MjolnirSwing7))&&
					(!audio_is_playing(snd_MjolnirSwing9))) && (attack_number == 1) && (image_index < 1)//--> Sound FX
					{
						audio_play_sound(sound1,15,0);
					}
					
					if (image_index mod 2 = 0) //--> Electric effect
					{
						image_blend = c_white;
						image_alpha = 0.05;
					}
					//--> Lvl1-3 Upgrade Lightning
					if (global.AXES[3,3]>=1)
					{
						var lghtngChance = (global.AXES[3,3]==1) ? irandom(100) : ((global.AXES[3,3]==2) ? irandom(75) : irandom(50));
						var lghtspr = (global.AXES[3,3] == 1) ? spr_relampago : ((global.AXES[3,3] == 2) ? spr_relampago2 : spr_relampago3);
						if (lghtngChance==7)
						{
							if (distance_to_object(obj_BossEnemy)<300)
							{
								if (!place_meeting(x,y,obj_relampago))
								{
									var relampago = instance_create_layer(x,y,"Back_FX_Layer",obj_relampago);
									relampago.sprite_index = lghtspr;
									relampago.image_angle = point_direction(x,y,obj_BossEnemy.x,obj_BossEnemy.y)
								}
							}
						}
					}
					break;
					
				case 4: //--> WHISPERER
					if (!audio_is_playing(snd_MaceSwing1)) && (attack_number == 1) && (image_index < 1) //--> Sound FX
					{
						audio_sound_pitch(snd_MaceSwing1,random_range(0.8,1.1));
						audio_play_sound(snd_MaceSwing1,15,0);
					}
					if (!audio_is_playing(sound1)) && (attack_number == 1) && (image_index < 1) //--> Sound FX
					{
						audio_sound_pitch(sound1,random_range(0.8,1.1));
						audio_play_sound(sound1,16,0);
					}
					
					if (image_index > 5)and(image_index < 7) //--> First Windslash
					{
						var wowX1 = 82 * image_xscale;
						if (!place_meeting(x+wowX1,y,obj_windslash))
							instance_create_layer(x+wowX1,y,"FX_Layer",obj_windslash);
					}
					break;
			}
			
			//--> Cancel Axe attack into a Sword attack
			if (image_index>=3)and(image_index<=6)
			{
				if (ax_key_s) attack_number = 2;
				if (image_index>=5)
				{
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
				}
				alarm[7] = 120; //--> To continue combo after cancel
				aux_Axe_atk_num = 2;
			}
			
			
			if (image_index >= 7)
			{
				if (image_index >= cnclIndx+1)
				{	
					if (((global.AXES[1,3]>=2)&&(equipped_axe==1))&&((equiped_sword==4)&&(global.SWORDS[equiped_sword,3]>=2)))||
					   (global.AXES[1,3] == 3) || (global.RELIKS[46,2]) || (global.RELIKS[40,2])
					{
						if (ax_key_s) && ((aux_Swd_atk_num==3)||((aux_Swd_atk_num==1)&&(alarm[7]>0))||
						   ((equiped_sword==4)&&(global.SWORDS[equiped_sword,3]>=2)))
						{
							state = strongWpnAtk;
							exit;
						}
					}
					if (ax_key_d)
					{
						attack_number = 1;
						image_index = 0;
						state = sword_atks_Manticore;
						exit;
					}
					aux_Axe_atk_num = 1;
				}
				if (image_index <= 9) if (!ax_key_shift)&&(direc != 0) image_xscale = direc;
				hitbox_duration(7,9,obj_Manticore_hitbox,spr_Korunax_hitbox4);
				hitbox_duration(9,10,obj_Manticore_hitbox,spr_Korunax_hitbox5);
				hitbox_duration(10,11,obj_Manticore_hitbox,spr_Korunax_hitbox6);
				if (!audio_is_playing(snd_KorunaxSwing2)) && (attack_number == 2) //--> Sound FX
				{
					audio_sound_pitch(snd_KorunaxSwing2,1);
					audio_play_sound(snd_KorunaxSwing2,15,0);
				}
				if (attack_number < 2) or (image_index > 14)
				{
					attack_number = 1;
					state = move_Manticore;
				}
			}
		}
		else //--> Air Korunax (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
			if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
		}
}