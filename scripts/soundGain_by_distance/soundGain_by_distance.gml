/// @description soundGain_by_distance(obj,refDist,maxGain,snd,sndPrio,loop)
/// @param obj --> Object to compare distance with
/// @param refDist --> Distance of reference where the volume begins to decrease
/// @param maxGain --> maxium gain for the sound
/// @param snd ---> sound
/// @param sndPrio ---> sound priority
/// @param loop ---> if loops (true/false)
function soundGain_by_distance(argument0, argument1, argument2, argument3, argument4, argument5) {

	var obj = argument0;
	var refDist = argument1;
	var maxGain = argument2;
	var snd = argument3;
	var sndPrio = argument4;
	var loop = argument5;

	var distMant = distance_to_object(obj);
	distMant = refDist / distMant;
	distMant = clamp(distMant,0,maxGain);
	audio_sound_gain(snd,distMant,0);
	audio_play_sound(snd,sndPrio,loop);


}
