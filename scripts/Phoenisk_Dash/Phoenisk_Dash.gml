// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Dash(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Rush_start : spr_VioletP_Rush_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		//--> Dashes to Player or away of near a wall.
		if (x <= (minX + 300)) if (!sign(image_xscale)) image_xscale *= -1;
		else
		{
			if (x >= (maxX - 300)) if (sign(image_xscale)) image_xscale *= -1;
			else
			{
				if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
				else image_xscale = (global.BossVariant == 2) ? -1 : 2;
			}
		}
		
		counter = 0;
		switch_var = true;
	}
	
	switch(sprite_index)
	{
		case spr_RadiantP_Rush_start: case spr_VioletP_Rush_start:
			if (image_index >= 3.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Dash : spr_VioletP_Dash;
			}
			break;
			
		case spr_RadiantP_Dash: case spr_VioletP_Dash:
			counter++;
			if ((x <= (minX + 300))&&(!sign(image_xscale))) || ((x >= (maxX - 300))&&(sign(image_xscale))) ||
			   (counter > 15)&&(((x <= (obj_Manticore.x))&&(!sign(image_xscale))) || ((x >= (obj_Manticore.x))&&(sign(image_xscale)))) ||
			   (counter >= 30)
			{
				switch_var2 = true;
				//-->FX
				fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],52,512,8,16,83,1,c_white,1);
				
			}
			if (!switch_var2)
			{
				if (!place_meeting(x,y,dash_afterimage))
				{
					var mngr = global.currentManager;
					var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
					afterimage.sprite_index = sprite_index;
					afterimage.image_xscale = image_xscale;
					afterimage.image_yscale = image_yscale;
					afterimage.glowFX = true;
					afterimage.image_alpha = 0.6;
					afterimage.image_blend = mngr.rainbow_blend;
				}
				x += 30 * image_xscale;
			}
			break;
	}
	
	if (switch_var2)
	{
		//--> Ends State and goes to Standing
		if (global.difficulty == "normal")&&(global.ruins_tier <= 3) || (global.difficulty == "casual")
		{
			alarm[1] = 60;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		bounce = 0;
		counter = 0;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
		
		//-->> End Dash FX
		for(var i = 1;i <= 2;i ++)
		{
			var rainbowCircle2 = instance_create_layer(x,y,"Back_FX_Layer",obj_fadeAway);
			rainbowCircle2.sprite_index = sprite_index;
			rainbowCircle2.image_alpha = 0.6;
			rainbowCircle2.fadeSpd = 0.02 * i;
			rainbowCircle2.Ymovement = 0;
			rainbowCircle2.Xmovement = 0;
			rainbowCircle2.sizeMod = 0.05 * (i * sign(image_xscale));
			rainbowCircle2.image_xscale = image_xscale;
			rainbowCircle2.image_yscale = image_yscale;
			alarm[0] = 10;
		}
	
		AtkTurn += 1;
	}
}