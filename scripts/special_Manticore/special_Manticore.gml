///@description special_Manticore(wpn)
function special_Manticore() {
	if (global.RELIKS[49,2]) exit;

	state = special_Manticore;

	sp_onFloor = place_meeting(x,y+1,obj_Wall);

	sp_onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) sp_onPlatform = true;
	else sp_onPlatform = false;

	image_speed = 1;

	if (equiped_sword == 3) && (atk_type == 1)
	{
		if (sp_onFloor)||(sp_onPlatform)
		{
			sprite_index = spr_Special_GirTab;
			//if (!audio_is_playing(snd_GirTabSP1)) audio_play_sound(snd_GirTabSP1,11,0);

			if  (image_index > 14 && image_index < 15)
			{
				if (!instance_exists(obj_FX_Scorpion))
				{
					with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
					{
						sprite_index = spr_ScorpionTail;
						image_xscale = other.image_xscale;
						if (image_xscale < 0) image_xscale = -0.1;
						else image_xscale = 0.1;
						image_yscale = 0.1;
						audio_play_sound(snd_Girtab2,11,0);
						audio_play_sound(snd_FloorFall,11,0);
				
						bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_red,1);
					}
				}
			}
	
			if (image_index > 32) && (image_index < 36) && (sprite_index == spr_Special_GirTab)
			{
				if (!audio_is_playing(snd_GirTabSP3)) audio_play_sound(snd_GirTabSP3,11,0);
				hitbox_duration(32,36,obj_Manticore_hitbox,spr_GirTab_sp_hitbox);
			}
	
			if (image_index > 36)
			{
				special_GirTab = 0;
				alarm[9] = 60;
			}
	
			if (image_index > 43) state = move_Manticore;
		}
		else
		{
			sprite_index = spr_Manticore_TWSP2;
		
			if (!instance_exists(obj_FX_Scorpion))
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
				}
			}
		
			if (image_index >= 6)
			{
				alarm[9] = 60;
				if (!instance_exists(obj_RegenBubble))
				{
					var rbbl = instance_create_layer(x,y,"FX_Layer",obj_RegenBubble);
					rbbl.alarm[0] = (global.RELIKS[40,2]) ? 360 : 300;
					special_GirTab = 0;
					var Plyrmngr = global.currentManager;
					if (Plyrmngr.ManticoreVenomed) alarm[5] = artist.max_Regen;
				}
				else
				{
					if (obj_RegenBubble.permanent) obj_RegenBubble.alarm[0] = 20;
					special_GirTab = 0;
				}
			
				if (image_index >= 8.6)
				{
					image_index = 0;
					state = move_Manticore;
				}
			}
		}
	}

	if (equiped_sword == 4) && (atk_type == 1)
	{
		sprite_index = spr_Frostbiter_sp;
		if (!audio_is_playing(snd_Cut2))
		{
			audio_sound_pitch(snd_Cut2,random_range(0.8,1.3));
			audio_play_sound(snd_Cut2,10,0); //--> SndFX
		}

		if  (image_index > 1 && image_index < 2)
		{
			if (!instance_exists(obj_FX_Scorpion))
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_aqua,1);
				}
			}
		}
		if (image_index > 13) && (image_index < 14)//--> Creates Frost Bulwark
		{
			if (!place_meeting(x,y,obj_FrostBulwark))
			{
				var frstblwrk = instance_create_layer(x+(50*image_xscale),y-9,"Back_FX_Layer",obj_FrostBulwark);
				frstblwrk.image_xscale = image_xscale;			
			}
		}
	
		if (image_index > 20) state = move_Manticore;
	}

	if (equiped_sword == 1) && (atk_type == 1)//--> ARGENTA WAVE
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Argenta_Special_floor;
		else sprite_index = spr_Argenta_Special_air;
	
		if (!instance_exists(obj_FX_Scorpion))
		{
			with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
			{
				sprite_index = spr_ScorpionTail;
				image_xscale = other.image_xscale;
				if (image_xscale < 0) image_xscale = -0.1;
				else image_xscale = 0.1;
				image_yscale = 0.1;
				image_blend = c_white;
				image_alpha = 0.25;
			
				audio_play_sound(snd_ArgentaWave1,15,0);
				bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_silver,1);
			}
		}
		if (image_index > 6)and(image_index < 7) //--> Creates Argenta Wave
		{
			var arg_spX = 100 * image_xscale;
			if (!place_meeting(x+arg_spX,y-30,obj_Argenta_wave))
			{
				var arg_wave = instance_create_depth(x+arg_spX,y-30,0,obj_Argenta_wave);
				arg_wave.image_xscale = image_xscale;			
			}
		
		}
		if (image_index > 10)
		{
			state = move_Manticore;
			vsp = 0;
		}
	}

	if (equiped_sword == 2) && (atk_type == 1)//--> FLARESTAR
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Dyrnwyn_sp_floor;
		else sprite_index = spr_Dyrnwyn_sp_air;
	
		if (image_index > 8)and(image_index < 9) //--> Creates Flarestar
		{
			if (!instance_exists(obj_FX_Scorpion)) //--> Scorpion FX
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
					image_blend = c_white;
					image_alpha = 0.25;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,make_color_rgb(241,100,31),1);
				}
			}
		
			var flrstr_spX = 200 * image_xscale;
			if (!place_meeting(x+flrstr_spX,y-30,obj_Flarestar))
			{
				var Flare_Star = instance_create_layer(x+flrstr_spX,y-30,"Back_FX_Layer",obj_Flarestar);
				Flare_Star.image_xscale = image_xscale;			
			}
		
		}
		if (image_index > 21)
		{
			state = move_Manticore;
			vsp = 0;
		}
	}

	if (equipped_axe == 1) && (atk_type == 2)//--> Granite SHield
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Korunax_sp;
		else sprite_index = spr_Korunax_sp_air;
	
		if (image_index > 1)and(image_index < 2) //--> Creates Granite Shield
		{
			//--> Sound FX
			if (!instance_exists(obj_FX_Scorpion)) //--> Scorpion FX
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
					image_blend = c_white;
					image_alpha = 0.25;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,make_color_rgb(211,151,65),1);
				}
			}
		}
		if (image_index > 2)and(image_index < 3)
		{
			if (!place_meeting(x,y,obj_GraniteShield))
			{
				var Granite_Shield = instance_create_layer(x,y,"Back_FX_Layer",obj_GraniteShield);
				Granite_Shield.image_xscale = image_xscale;
			}
		
		}
		if (sprite_index == spr_Korunax_sp)	if (image_index > 10) state = move_Manticore;
	
		if (sprite_index == spr_Korunax_sp_air)	if (image_index > 9) state = move_Manticore;
	}

	if (equipped_axe == 2) && (atk_type == 2)//--> Spectral Bomb
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Mirage_sp; //--> Needs on air sprite
	
		if (image_index > 7)and(image_index < 8) //--> Creates lightningball
		{
			if (!instance_exists(obj_FX_Scorpion)) //--> Scorpion FX
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
					image_blend = c_white;
					image_alpha = 0.25;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_fuchsia,1);
				}
			}
		}
		if (image_index > 10)and(image_index < 11)
		{
			if (!place_meeting(x,y-30,obj_Spectral_Bomb))
			{
				var spctrlbmbX = 30 * image_xscale;
				var Spectral_Bomb = instance_create_layer(x+spctrlbmbX,y-100,"Back_FX_Layer",obj_Spectral_Bomb);
			}
		
		}
		if (image_index > 20)
		{
			state = move_Manticore;
			vsp = 0;
		}
	}

	if (equipped_axe == 3) && (atk_type == 2)//--> LIGHTNINGBALL
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Mjolnir_sp; //--> Needs on air sprite
	
		if (image_index > 5)and(image_index < 6) //--> Creates lightningball
		{
			if (!instance_exists(obj_FX_Scorpion)) //--> Scorpion FX
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
					image_blend = c_white;
					image_alpha = 0.25;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,make_color_rgb(39,100,205),1);
				}
			}
		
			if (!place_meeting(x,y-30,obj_Lightningball))
			{
				var lgtbll_spX = 30 * image_xscale;
				var Lightning_Ball = instance_create_layer(x+lgtbll_spX,y-170,"Back_FX_Layer",obj_Lightningball);
				Lightning_Ball.image_xscale = image_xscale;			
			}
		
		}
		if (image_index > 20)
		{
			state = move_Manticore;
			vsp = 0;
		}
	}

	if (equipped_axe == 4) && (atk_type == 2)//--> TORNADO
	{
		if (sp_onFloor or sp_onPlatform) sprite_index = spr_Whisperer_sp; //--> Needs on air sprite
	
		if (image_index > 7)and(image_index < 8) //--> Creates lightningball
		{
			if (!instance_exists(obj_FX_Scorpion)) //--> Scorpion FX
			{
				with (instance_create_layer(other.x,other.y,"Back_FX_Layer",obj_FX_Scorpion))
				{
					sprite_index = spr_ScorpionTail;
					image_xscale = other.image_xscale;
					if (image_xscale < 0) image_xscale = -0.1;
					else image_xscale = 0.1;
					image_yscale = 0.1;
					image_blend = c_white;
					image_alpha = 0.25;
				
					bshockwave_create_layer(x,y,"FX_Layer",view_camera[0],72,160,3,4,-75,1,c_lime,1);
				}
			}
			if (!place_meeting(x,bbox_bottom,obj_Tornado))
			{
				var Tornado_inverse_back = instance_create_layer(x,bbox_bottom,"Back_FX_Layer",obj_Tornado);
				var Tornado_inverse_front = instance_create_layer(x,bbox_bottom,"FX_Layer",obj_Tornado);
				var Tornado_inner_back = instance_create_layer(x,bbox_bottom,"Back_FX_Layer",obj_Tornado);
				var Tornado_inner_front = instance_create_layer(x,bbox_bottom,"FX_Layer",obj_Tornado);
				Tornado_inner_back.image_xscale = image_xscale;
				Tornado_inner_back.sprite_index = spr_TornadoINNER_back;
				Tornado_inner_front.image_xscale = image_xscale;
				Tornado_inner_front.sprite_index = spr_TornadoINNER_front;
				Tornado_inverse_back.image_xscale = image_xscale;
				Tornado_inverse_back.sprite_index = spr_TornadoINVERSE_back;
				Tornado_inverse_front.image_xscale = image_xscale;
				Tornado_inverse_front.sprite_index = spr_TornadoINVERSE_front;
			}
		
		}
		if (image_index > 20)
		{
			state = move_Manticore;
			vsp = 0;
		}
	}
}
