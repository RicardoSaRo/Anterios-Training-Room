// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Blaze(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawRaise : spr_VioletP_ClawRaise;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		switch_var = true;
	}
	
	switch(sprite_index)
	{
		case spr_RadiantP_ClawRaise: case spr_VioletP_ClawRaise:
			if (image_index >= 3.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawUP : spr_VioletP_ClawUP;
			}
			break;
		
		case spr_RadiantP_ClawUP: case spr_VioletP_ClawUP:
			if (image_index >= 2.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Shock : spr_VioletP_Shock;
			}
			break;
			
		case spr_RadiantP_Shock: case spr_VioletP_Shock:
			if (image_index >= 2.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Landing : spr_VioletP_Landing;
			}
			break;
		
		case spr_RadiantP_Landing: case spr_VioletP_Landing:
			if (!switch_var2)
			{
				global.camera_shake = true;
				global.cam_shake_amount = 5;
				var mng = global.currentManager;
				audio_play_sound(snd_Grenade3,10,0);
				audio_play_sound(snd_FlareStar_destroy,10,0);
				mng.alarm[0] = 20;
				with (obj_Wall)
				{
					if (y == 1152)
					{
						var explo = instance_create_layer(x,y-28,"FX_Layer",obj_EnemyBullet);
						explo.sprite_index = spr_RainbowExplosion64x;
						explo.image_xscale = choose(-2,-1.5,-1,1,1.5,2);
						explo.image_yscale = choose(1,1.5,2);
						explo.image_speed = random_range(0.6,1);
						explo.NoDamage = true;
						explo.priority = 5;
						explo.ownership = global.currentManager;
					}
				}
				
				with(obj_Platform)
				{
					var explo = instance_create_layer(x,y-28,"FX_Layer",obj_EnemyBullet);
					explo.sprite_index = spr_RainbowExplosion64x;
					explo.image_xscale = choose(-2,-1.5,-1,1,1.5,2);
					explo.image_yscale = choose(1,1.5,2);
					explo.image_speed = random_range(0.6,1);
					explo.NoDamage = true;
					explo.priority = 5;
					explo.ownership = global.currentManager;
				}
				switch_var2 = true;
			}
			if (image_index >= 4.6)	switch_var3 = true;			
			break;
	}
	
	if (switch_var3)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}