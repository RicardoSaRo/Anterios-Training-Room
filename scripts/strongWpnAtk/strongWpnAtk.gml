function strongWpnAtk() {
	state = strongWpnAtk;

	switch(sprite_index)
	{
		case spr_Argenta_DashStab:
			hitbox_duration(1,3,obj_Manticore_hitbox,spr_ArgentaSTR_hitbox);
			if (global.RELIKS[22,2])
			{
				with (obj_Manticore)
				{
					if !(alarm[11] > 3)
					{
						alarm[11] = 3;
						image_blend = c_yellow;
					}
				}
			}
			if (image_index >= 5.6)
			{
				state = move_Manticore;
				image_index = 0;
				exit;
			}
			break;
		
		case spr_Dyrnwyn_DashStab:
			var chispa = instance_create_layer(x,y,"Back_FX_Layer",obj_chispa);
			with(chispa)
			{
				x += random_range(40,180) * obj_Manticore.image_xscale;
				y += random_range(-50,40);
			}
			hitbox_duration(1,3,obj_Manticore_hitbox,spr_DyrnwynSTR_hitbox);
			if (global.RELIKS[21,2])
			{
				with (obj_Manticore)
				{
					if !(alarm[11] > 3)
					{
						alarm[11] = 3;
						image_blend = c_yellow;
					}
				}
				if (image_index > 4)&&(image_index < 5)
				{
					var Explo = instance_create_layer(x+(100*image_xscale)+random_range(-80,80),(y-20)+random_range(-60,60),"FX_Layer",obj_Throwing_Wpn);
					Explo.sprite_index = spr_Explosion_hmmgF;
					Explo.image_alpha = choose(0.7,0.8,0.9);
					Explo.image_angle = random(360);
				}
			}
			if (image_index >= 5.6)
			{
				state = move_Manticore;
				image_index = 0;
				exit;
			}
			break;
	
		case spr_AxeStrongAtk:
			if (instance_exists(obj_StrAtk))
			{
				switch(obj_StrAtk.sprite_index)
				{
					case spr_StrAtk_Korunax1: case spr_StrAtk_Korunax2: case spr_StrAtk_KorunaxFrostbiter:
					case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2: case spr_StrAtk_Whisperer1:
					case spr_StrAtk_Whisperer2: case spr_StrAtk_WhispererDyrnwyn:
						if (image_index >= 3)
						{
							obj_StrAtk.start_anim = true;
							obj_StrAtk.start_draw = true;
						}
						if (image_index >= 5) obj_StrAtk.active_dmg = true;
						break;
				
					case spr_StrAtk_Mjolnir1: case spr_StrAtk_Mjolnir2:
						if (image_index >= 1)
						{
							obj_StrAtk.start_anim = true;
							obj_StrAtk.start_draw = true;
						}
						if (image_index >= 5) obj_StrAtk.active_dmg = true;
						break;
				}
			}
			if (image_index >= 10.6)
			{
				state = move_Manticore;
				image_index = 0;
				exit;
			}
			break;
		
		case spr_Korunax:
			if (global.AXES[1,3]>=2)&&(equiped_sword==4)&&(global.SWORDS[equiped_sword,3]>=2)
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_KorunaxFrostbiter;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.RELIKS[40,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Korunax2;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.AXES[1,3] == 3) || (global.RELIKS[46,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Korunax1;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			sprite_index = spr_AxeStrongAtk;
			image_index = 0;
			break;
	
		case spr_Mirage:
			if (global.RELIKS[40,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Mirage2;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.AXES[2,3] == 3) || (global.RELIKS[46,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Mirage1;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			sprite_index = spr_AxeStrongAtk;
			image_index = 0;
			break;
		
		case spr_Mjolnir:
			if (global.RELIKS[40,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Mjolnir2;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.AXES[3,3] == 3) || (global.RELIKS[46,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Mjolnir1;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			sprite_index = spr_AxeStrongAtk;
			image_index = 0;
			break;
		
		case spr_Whisperer:
			if (global.AXES[4,3]>=2)&&(equiped_sword==2)&&(global.SWORDS[equiped_sword,3]>=2)
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_WhispererDyrnwyn;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.RELIKS[40,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Whisperer2;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			if (global.AXES[4,3] == 3) || (global.RELIKS[46,2])
			{
				if (!instance_exists(obj_StrAtk))
				{
					var StrAtkInst = instance_create_layer(x,y,"FX_Layer",obj_StrAtk);
					StrAtkInst.sprite_index = spr_StrAtk_Whisperer1;
					StrAtkInst.image_xscale = image_xscale;
				}
			}
			sprite_index = spr_AxeStrongAtk;
			image_index = 0;
			break;
	
		case spr_Manticore_fdash:
			switch(equiped_sword)
			{
				case 1: sprite_index = spr_Argenta_DashStab; image_index = 0; break;
				case 2: sprite_index = spr_Dyrnwyn_DashStab; image_index = 0; break;
			}
			break;
	}

	//-->> COLLISIONS!!!
	SWAonFloor = place_meeting(x,y+1,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) SWAonPlatform = true;
	else SWAonPlatform = false;

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	if (!SWAonFloor) && (!SWAonPlatform)
	{
		if (airDash_used == true) || (lghtDash_used == true) //--> this "if" prevents getting on ceilings after dash
		{
			var onepixel = sign(vsp);
			while (place_meeting(x,y-onepixel,obj_Wall )) y += onepixel;
		}
	}


}
