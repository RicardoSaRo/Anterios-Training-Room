function crouchDEF_Manticore() {

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) CDonPlatform = true;
	else CDonPlatform = false;
	
	CDonFloor = place_meeting(x,y+1,obj_Wall); //--> onFloor Variable Check
	
	if (!CDonFloor)&&(!CDonPlatform)
	{
		state = move_Manticore;
		exit;
	}
	
	state = crouchDEF_Manticore;

	cDEF_keydown = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);
	cDEF_keyleft = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),0);
	cDEF_keyright = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),0);
	cDEF_keyjump = max(keyboard_check_pressed(vk_space),gamepad_button_check_pressed(global.controller_type,gp_face1),0);

	air_attack = false; //--> Indicates that an Air Attack is not being performed
	vsp = 0; //--> Prevents jumping while Crouching

	if ((cDEF_keydown) && (cDEF_keyjump)) && (onPlatform)
	{
		state = move_Manticore;
		platfDrop = true;
		y += 2;
		exit;
	}

	switch(obj_Manticore.equipped_axe)
	{
		case 1: sprite_index = spr_CrouchDEF_Korunax; break;
		case 2: sprite_index = spr_CrouchDEF_Mirage; break;
		case 3: sprite_index = spr_CrouchDEF_Mjolnir; break;
		case 4: sprite_index = spr_CrouchDEF_Whisperer; break;
	}

	if (cDEF_keyleft) if (!cDEF_keyright) image_xscale = -1;
	if (cDEF_keyright) if (!cDEF_keyleft) image_xscale = 1;

	if (image_index < 3)
	{
		if (cDEF_keydown)
		{
			if (image_index >= 2)
			{
				image_index = 2;
			
				if (!instance_exists(obj_CrouchDEF))
				{
					var cDef = instance_create_layer(x,y,"Life_n_Bars",obj_CrouchDEF);
				
					switch(equipped_axe)
					{
						case 1: cDef.shieldAmount = (global.AXES[1,3]>=1) ? 4 : 3; break;
						case 2: cDef.shieldAmount = (global.AXES[2,3]==3) ? 5 : (global.AXES[2,3]>=1 ? 4 : 3); break;
						case 3: cDef.shieldAmount = (global.AXES[3,3]==3) ? 6 : (global.AXES[3,3]>=1 ? 5 : 4); break;
						case 4: cDef.shieldAmount = (global.AXES[4,3]>=1) ? 6 : 5; break;
					}
				}
			}
		}
	}
	else
	{
		if (instance_exists(obj_CrouchDEF)) instance_destroy(obj_CrouchDEF);
		state = move_Manticore;
	}



}
