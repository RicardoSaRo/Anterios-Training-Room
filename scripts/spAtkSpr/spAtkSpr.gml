/// @description primaryWpnSpr(sprite_index,obj)
/// @param spr
/// @param obj
function spAtkSpr(argument0,argument1){
	
	var isSP = false;
	
	switch(argument0)
	{
		case spr_FrostBulwark: case spr_TribladerSP: case spr_TornadoINNER_back:
		case spr_TornadoINNER_front: case spr_TornadoINVERSE_front: case spr_TornadoINVERSE_back:
		case spr_StarbladesSP: case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp:
		case spr_Lightball: case spr_wow_bomb: case spr_BigCoin: case spr_BigTripleCoin:
		case spr_Argenta_wave2: case spr_Special_GirTab: case spr_ShurikenSP: case spr_ChakramSP:
		case spr_KunaiSP: case spr_BombSP: case spr_AssegaiSP: case spr_GuillotineSP:
		case spr_flametongues: case spr_wavelightning: case spr_FireFamiliar_Attack:
		case spr_FireFamiliar_Onfire: case spr_NeonLaser: case spr_BigAquaExplosion:
			isSP = true;
			break;
		case spr_willowisp: isSP = (argument1.image_xscale > 1) ? true : false; break;
	}
	
	return isSP;
}