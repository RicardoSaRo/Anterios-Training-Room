//--> Phoenisk_Standing is the Core of this particular boss State Machine
// At this moment, the attacks for each turn are not decided yet, the attacks here are place holders.
function Phoenisk_Standing(){
	if (hit_points <= 0) alarm[1] = 0;
	
	if (alarm[1] <= 0)
	{
		var attack = 1;
		switch_var2 = false;
		switch_var3 = false;
		right_var = 0;
		interact_instance = self;
		interact_instance2 = self;
	
		if (distance_to_object(obj_Manticore)>1000)
		{
			switch(enemy_name)
			{
				case "PhoeniskR":
					switch(AtkTurn)
					{
						case 0:		attack = choose(2,15); break;
						case 1:		attack = choose(2,14); break;
						case 2:		attack = choose(2,15); break;
						case 3:		attack = choose(2,14); break;
						case 4:		attack = choose(2,15); break;
						case 5:		attack = choose(2,15); break;
						case 6:		attack = choose(2,14); AtkTurn = 0; break;
						default:	attack = 5; break;
					}
					break;
						
				case "PhoeniskV":
					switch(AtkTurn)
					{
						case 0:		attack = choose(2,14); break;
						case 1:		attack = choose(2,14); break;
						case 2:		attack = choose(2,14); break;
						case 3:		attack = choose(2,9,10); break;
						case 4:		attack = choose(2,9,10); AtkTurn = 0; break;
						default:	attack = 5; break;
					}
					break;
			}
		}
		else
		{
			if (distance_to_object(obj_Manticore)<400)
			{
				switch(enemy_name)
				{
					case "PhoeniskR":
						switch(AtkTurn)
						{
							case 0:		attack = choose(2,15); break;
							case 1:		attack = choose(2,14); break;
							case 2:		attack = choose(2,15); break;
							case 3:		attack = choose(2,14); break;
							case 4:		attack = choose(2,15); break;
							case 5:		attack = choose(2,15); break;
							case 6:		attack = choose(2,14); AtkTurn = 0; break;
						}
						break;
						
					case "PhoeniskV":
						switch(AtkTurn)
						{
							case 0:		attack = choose(2,5,7,9,10); break;
							case 1:		attack = choose(2,5,7,9,10); break;
							case 2:		attack = choose(2,9,9,9,10); break;
							case 3:		attack = choose(2,5,7,9,10); break;
							case 4:		attack = choose(2,2,5,9,10); break;
							case 5:		attack = choose(2,2,2,7,9,10); break;
							case 6:		attack = choose(2,5,7,9,10); AtkTurn = 0; break;
						}
						break;
				}
			}
			else
			{
				if (distance_to_object(obj_Manticore)<1000)
				{
					switch(enemy_name)
					{
						case "PhoeniskR":
							switch(AtkTurn)
							{
								case 0:		attack = choose(2,15); break;
								case 1:		attack = choose(2,14); break;
								case 2:		attack = choose(2,15); break;
								case 3:		attack = choose(2,15); break;
								case 4:		attack = choose(2,15); break;
								case 5:		attack = choose(2,14); break;
								case 6:		attack = choose(2,14); AtkTurn = 0; break;
							}
							break;
						
						case "PhoeniskV":
							switch(AtkTurn)
							{
								case 0:		attack = choose(2,9,10); break;
								case 1:		attack = choose(2,9,10); break;
								case 2:		attack = choose(2,9,10); break;
								case 3:		attack = choose(2,9,10); break;
								case 4:		attack = choose(2,9,10); AtkTurn = 0; break;
								default:	attack = 5; break;
							}
							break;
					}
				}
			}
		}

		if (canSpecial)
		{
			if (hit_points < max_hit_points/3) && (alarm[3] <= 1) //--> Special Triggers at conditions
			{
				var attack = choose(12);
				if (distance_to_object(obj_Manticore)<400) attack = 13//choose(11,11,12);
				alarm[3] = 800;
			}
			else
			{
				if (hit_points < max_hit_points/2) && (alarm[3] <= 1) //--> Special Triggers at conditions
				{
					var attack = choose(12);
					if (distance_to_object(obj_Manticore)<400) attack = 13//choose(11,11,12);
					alarm[3] = 800;
				}
				else
				{
					if (hit_points < max_hit_points/1.5) && (alarm[3] <= 1) //--> Special Triggers at conditions
					{
						var attack = choose(12);
						if (distance_to_object(obj_Manticore)<400) attack = 13//choose(11,11,12);
						alarm[3] = 800;
					}	
				}
			}
		}

		//--> Defeated state
		if (hit_points <= 0) attack = 1;

		image_index = 0;

		switch(attack)
		{
			case 1:		state = Phoenisk_Fireballs; break;
			case 2:		state = Phoenisk_Dash; break;
			case 3:		state = Phoenisk_Kicks; break;
			case 4:		state = Phoenisk_Blaze; break;
			case 5:		state = Phoenisk_Tail; break;
			case 6:		state = Phoenisk_Sword1; break;
			case 7:		state = Phoenisk_Sword2; break;
			case 8:		state = Phoenisk_Sword3; break;
			case 9:		state = Phoenisk_Breath; break;
			case 10:	state = Phoenisk_Sphere; break;
			case 11:	state = Phoenisk_ComboSP; break;
			case 12:	state = Phoenisk_RushSP; break;
			case 13:	state = Phoenisk_LancesSP; break;
			case 14:	state = Phoenisk_Cluster; break;
			case 15:	state = Phoenisk_RainbowBeam; break;
		}
	}
}