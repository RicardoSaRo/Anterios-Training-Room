function rankType_calcs() {
	switch(rankType)
	{
		case "life":		if (global.results[results.life,lifeOrbs.perfect])
							{
								sprite_index = (global.results[results.life,lifeOrbs.broken] > 0) ? spr_Rank_GoldSS : spr_Rank_GoldSSS;
								shckwvColor = c_yellow;
								obj_Results_manager.Scorpionskey1 = true;
								obj_Results_manager.Scorpionskey2 = true;
							}
							else
							{
								switch(global.results[results.life,lifeOrbs.full])
								{
									case 16:			sprite_index = spr_Rank_GoldS;
														shckwvColor = c_yellow;
														obj_Results_manager.Scorpionskey1 = true;
														break;
											
									case 14: case 15:	sprite_index = spr_Rank_A;
														shckwvColor = c_white;
														break;
								
									case 12: case 13:	sprite_index = spr_Rank_B;
														shckwvColor = c_white;
														break;
													
									case 10: case 11:	sprite_index = spr_Rank_C;
														shckwvColor = c_white;
														break;
													
									case 8: case 9:		sprite_index = spr_Rank_D;
														shckwvColor = c_white;
														break;
													
									case 5: case 6: case 7:		
														sprite_index = spr_Rank_E;
														shckwvColor = c_white;
														break;
													
									case 4: case 3: case 2: case 1: case 0:
														sprite_index = spr_Rank_F;
														shckwvColor = c_white;
														break;
								}
							
							}
							break;
					
		case "time":		//-->> Make this into a scrip for each specific Boss
							var rTime = global.results[results.time,0];
							if (rTime < 30)
							{
								sprite_index = spr_Rank_GoldSSS;
								obj_Results_manager.Scorpionskey1 = true;
								obj_Results_manager.Scorpionskey2 = true;
							}
							else
							{
								if (rTime < 45)
								{
									sprite_index = spr_Rank_GoldSS;
									obj_Results_manager.Scorpionskey2 = true;
								}
								else
								{
									if (rTime < 60)
									{
										sprite_index = spr_Rank_GoldS;
										obj_Results_manager.Scorpionskey2 = true;
									}
									else
									{
										if (rTime < 75) sprite_index = spr_Rank_A;
										else
										{
											if (rTime < 90) sprite_index = spr_Rank_B;
											else
											{
												if (rTime < 105) sprite_index = spr_Rank_C;
												else
												{
													if (rTime < 120) sprite_index = spr_Rank_D;
													else
													{
														if (rTime < 135) sprite_index = spr_Rank_E;
														else sprite_index = spr_Rank_F;
													}
												}
											}
										}
									}
								}
							}
							shckwvColor = (global.results[results.time,0] < 60) ? c_yellow : c_white;
							break;
					
		case "overkill":	var redRank = global.results[results.overkill,ovrkll.girtabfinish];
							var overkilldmg = global.results[results.overkill,ovrkll.okdmg];
							if (overkilldmg >= 6000)
							{
								if (redRank)
								{
									if (obj_Results_manager.Scorpionskey2)||(obj_Results_manager.Scorpionskey1)
										obj_Results_manager.scorpBorder = spr_ResultsScorpRG;
									else obj_Results_manager.scorpBorder = spr_ResultsScorpR;
								}
								else
								{
									if (!obj_Results_manager.Scorpionskey2)&&(!obj_Results_manager.Scorpionskey1)
										obj_Results_manager.scorpBorder = spr_ResultsScorpG;
								}
							}
							if (overkilldmg >= 10000)
							{
								sprite_index = (redRank) ? spr_Rank_RedSSS : spr_Rank_GoldSSS;
								shckwvColor = (redRank) ? c_red : c_yellow;
								obj_Results_manager.Scorpionskey2 = true;
								obj_Results_manager.Scorpionskey3 = true;
							}
							else
							{
								if (overkilldmg >= 7500)
								{
									sprite_index = (redRank) ? spr_Rank_RedSS : spr_Rank_GoldSS;
									shckwvColor = (redRank) ? c_red : c_yellow;
									obj_Results_manager.Scorpionskey3 = true;
								}
								else
								{
									if (overkilldmg >= 5500)
									{
										sprite_index = (redRank) ? spr_Rank_RedS : spr_Rank_GoldS;
										shckwvColor = (redRank) ? c_red : c_yellow;
										obj_Results_manager.Scorpionskey3 = true;
									}
									else
									{
										if (overkilldmg >= 5000) sprite_index = spr_Rank_A;
										else
										{
											if (overkilldmg >= 4500) sprite_index = spr_Rank_B;
											else
											{
												if (overkilldmg > 4000) sprite_index = spr_Rank_C;
												else
												{
													if (overkilldmg > 3000) sprite_index = spr_Rank_D;
													else
													{
														if (overkilldmg > 2000) sprite_index = spr_Rank_E;
														else
														{
															if (overkilldmg > 1000) sprite_index = spr_Rank_F;
															else sprite_index = spr_Rank_NA;
														}
													}
												}
											}
										}
										shckwvColor = c_white;
									}
								}
							}		
							break;
	}


}
