// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Fireballs(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawRaise : spr_VioletP_ClawRaise;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		switch_var = true;
	}
	
	switch(sprite_index)
	{
		case spr_RadiantP_ClawRaise: case spr_VioletP_ClawRaise:
			if (image_index >= 3.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawUP : spr_VioletP_ClawUP;
			}
			break;
		
		case spr_RadiantP_ClawUP: case spr_VioletP_ClawUP:
			if (image_index >= 2.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawExtend : spr_VioletP_ClawExtend;
			}
			break;
			
		case spr_RadiantP_ClawExtend: case spr_VioletP_ClawExtend:
			if (image_index >= 2)
			{
				if (!switch_var2)
				{
					//--> Shot FX
					fshockwave_create_layer(x+(110*image_xscale),y-(10*image_yscale),"FX_Layer",view_camera[0],52,512,8,16,83,1,c_white,1);
					var rainbowCircle2 = instance_create_layer(x+(110*image_xscale),y-(10*image_yscale),"FX_Layer",obj_GrassLeaf);
					rainbowCircle2.sprite_index = spr_rainbowCircle2;
					rainbowCircle2.raise = 0;
					rainbowCircle2.xmov = 0;
					rainbowCircle2.image_xscale = 2;
					rainbowCircle2.image_yscale = 2;
					
					for (var i=1;i<=10;i++)
					{
						var vSmallB = instance_create_layer(x+(110*image_xscale),y-(10*image_yscale),"FX_Layer",obj_EnemyBullet);
						vSmallB.sprite_index = spr_Rainbow_Fireball;
						vSmallB.destroy_sprite = spr_RainbowExplosion;
						vSmallB.NoDamage = true;
						vSmallB.alarm[0] = random_range(12,25);
						vSmallB.alarm[1] = 3;
						vSmallB.speed = 8;
						switch(image_xscale)
						{
							case 2:	switch(i)
										{
											case 1:	vSmallB.direction = 240;
													vSmallB.image_angle = 240;
													break;
											case 2:	vSmallB.direction = 60;
													vSmallB.image_angle = 60;
													break;
											case 3:	vSmallB.direction = 40;
													vSmallB.image_angle = 40;
													break;
											case 4:	vSmallB.direction = 20;
													vSmallB.image_angle = 20;
													break;
											case 5:	vSmallB.direction = 0;
													vSmallB.image_angle = 0;
													break;
											case 6:	vSmallB.direction = 340;
													vSmallB.image_angle = 340;
													break;
											case 7:	vSmallB.direction = 320;
													vSmallB.image_angle = 320;
													break;
											case 8:	vSmallB.direction = 300;
													vSmallB.image_angle = 300;
													break;
											case 9:	vSmallB.direction = 280;
													vSmallB.image_angle = 280;
													break;
											case 10: vSmallB.direction = 260;
													 vSmallB.image_angle = 260;
													 break;
										}
										break;
							case -2:		switch(i)
										{
											case 1:	vSmallB.direction = 260;
													vSmallB.image_angle = 260;
													break;
											case 2:	vSmallB.direction = 240;
													vSmallB.image_angle = 240;
													break;
											case 3:	vSmallB.direction = 220;
													vSmallB.image_angle = 220;
													break;
											case 4:	vSmallB.direction = 200;
													vSmallB.image_angle = 200;
													break;
											case 5:	vSmallB.direction = 180;
													vSmallB.image_angle = 180;
													break;
											case 6:	vSmallB.direction = 160;
													vSmallB.image_angle = 160;
													break;
											case 7:	vSmallB.direction = 140;
													vSmallB.image_angle = 140;
													break;
											case 8:	vSmallB.direction = 120;
													vSmallB.image_angle = 120;
													break;
											case 9:	vSmallB.direction = 100;
													vSmallB.image_angle = 100;
													break;
											case 10: vSmallB.direction = 80;
													 vSmallB.image_angle = 80;
													 break;
										}
										break;
						}
					//	vSmallB.image_xscale = 2;
					//	vSmallB.image_yscale = 2;
						vSmallB.priority = 10;
						vSmallB.direc = 0;
						if (global.BossVariant != 0)||(hit_points <= max_hit_points/2) vSmallB.bounce = 0.5;
						else vSmallB.bounce = 0;
					}
					
					switch_var2 = true;
				}
			}
			if (image_index >= 5.6)	switch_var3 = true;
			break;
	}
	
	if (switch_var3)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}