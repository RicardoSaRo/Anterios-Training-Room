function ini_Optns() {
	if (!file_exists("Options.ini"))
	{

		ini_open("Options.ini");

		//--> Vide Options Inicialize
		ini_write_real("VideoOptions","WindowMode",1);
		ini_write_real("VideoOptions","Vsync",0);
		ini_write_real("VideoOptions","AlfaBlend",1);
		ini_write_real("VideoOptions","ResWidth",960);
		ini_write_real("VideoOptions","ResHeight",540);

		//--> Keyboard Control Options Inicialize
		ini_write_string("Controls","kb_Sword","D");
		ini_write_string("Controls","kb_Axe","S");
		ini_write_string("Controls","kb_TW","A");
		ini_write_string("Controls","kb_Charge","Q");
		ini_write_string("Controls","kb_Jump","vk_space");
		ini_write_string("Controls","kb_Right","vk_right");
		ini_write_string("Controls","kb_Left","vk_left");
		ini_write_string("Controls","kb_Up","vk_up");
		ini_write_string("Controls","kb_Down","vk_down");
		ini_write_string("Controls","kb_Dash","vk_shift");
		ini_write_string("Controls","kb_MainMenu","vk_escape");
		ini_write_string("Controls","kb_WpnsMenu","vk_enter");
		ini_write_string("Controls","kb_SwordQA","E");
		ini_write_string("Controls","kb_AxeQA","W");
		ini_write_string("Controls","kb_TWQA","vk_numpad0");

		ini_close();
	}


}
