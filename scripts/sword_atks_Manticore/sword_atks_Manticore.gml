function sword_atks_Manticore() {
	state = sword_atks_Manticore;

	//--> MOVE CAPTURE
	atk_key_jump = max(keyboard_check(vk_space),gamepad_button_check(global.controller_type,gp_face1),0); 
	atk_key_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
	atk_key_s = max(keyboard_check_pressed(ord("S")),gamepad_button_check_pressed(global.controller_type,gp_face2),0);
	atk_key_shift = max(keyboard_check(vk_lshift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	atk_key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	atk_key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);
	atk_key_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);

	onFloor = place_meeting(x,y+1,obj_Wall);

	//--> switches for attacks
	spriteSwitch = false;
	turnSwitch = false;

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) swrd_onPlatform = true;
	else swrd_onPlatform = false;

	//--> Cancel Attack Animation into Dash

	if (!atk_key_right) and (!atk_key_left) direc = 0;
	else
	{
		if (atk_key_right) and (!atk_key_left) direc = 1;
		if (atk_key_left) and (!atk_key_right) direc = -1;
	}

	if ((atk_key_shift)&&(direc != 0)) and (alarm[4] <= 0)
	{
		attack_number = 1;
		state = move_Manticore;
		if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
		exit;
	}

	//--> SWORD ATTACKS
	switch (equiped_sword) //--> ARGENTA
	{
	case 1:	//--> Argenta
		if (onFloor or swrd_onPlatform)
		{
			if (sprite_index == spr_AirArgenta)||(sprite_index == spr_AirArgentaMix) state = move_Manticore;
			else
			{
				//--> Animation speed increases with upgrades
				image_speed = (global.SWORDS[1,3]==0) ? 1 : ((global.SWORDS[1,3]==1) ? 1.1 : ((global.SWORDS[1,3]==2) ? 1.3 : 1.5));
				if (global.RELIKS[48,2]) image_speed += 0.5;
				//-->Assign sprites
				if (sprite_index != spr_Argenta) && (sprite_index != spr_ArgentaMix)
				{
					attack_number = aux_Swd_atk_num;
					if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
					sprite_index = choose(spr_Argenta,spr_ArgentaMix);
					image_index = (attack_number == 2) ? 5 : ((attack_number == 3) ? 11 : 0);
				}
				if (atk_key_jump)
				{
					air_attack = true;
					state = doublejump_Manticore;
					vsp = -20;
					aux_Swd_atk_num = attack_number;
					alarm[7] = 90;
					exit;
				}
				//--> Hitboxes
				hitbox_duration(0,3,obj_Manticore_hitbox,spr_Argenta_hitbox1);
				if (image_index>=3)and(image_index<=5)
				{
					if (atk_key_s)
					{
						attack_number = 1;
						image_index = 0;
						state = axe_atks_Manticore;
						exit;
					}
					if (atk_key_d)
					{
						if (attack_number == 1)	spriteSwitch = true;
						attack_number = 2;
						if (image_index>=4) if (!atk_key_shift)&&(direc != 0) image_xscale = direc;
					}
					aux_Swd_atk_num = 2;
					alarm[7] = 90; //--> To continue combo after cancel
				}
				if (!audio_is_playing(snd_ArgentaSwing1)) && (attack_number == 1) //--> Sound FX
				{
					audio_sound_pitch(snd_ArgentaSwing1,random_range(1,1.1))
					audio_play_sound(snd_ArgentaSwing1,15,0);
				}
				if (image_index >= 5) //and (attack_number > 1)
				{
					hitbox_duration(7,9,obj_Manticore_hitbox,spr_Argenta_hitbox2);
					if (attack_number < 2) state = move_Manticore;
					if (image_index>=7)and(image_index<=11)
					{
						aux_Swd_atk_num = 3;
						alarm[7] = 90;
						if (atk_key_s)
						{
							attack_number = 1;
							image_index = 0;
							state = axe_atks_Manticore;
							exit;
						}
						if (atk_key_d)
						{
							if (attack_number == 2)	spriteSwitch = true;
							attack_number = 3;
							if (image_index>=8) if (!atk_key_shift)&&(direc != 0) image_xscale = direc;
						}
					}
					if (!audio_is_playing(snd_ArgentaSwing2)) && (attack_number == 2)//--> Sound FX
					{
						audio_sound_pitch(snd_ArgentaSwing2,random_range(0.7,0.9))
						audio_play_sound(snd_ArgentaSwing2,15,0);
					}
					if (image_index >= 10) 
					{
						hitbox_duration(12,15,obj_Manticore_hitbox,spr_Argenta_hitbox3);
						if (attack_number < 3) or (image_index > 17)
						{
							attack_number = 1;
							state = move_Manticore;
						}
						if (!audio_is_playing(snd_ArgentaSwing3)) && (attack_number == 3) //--> Sound FX
						{
							audio_sound_pitch(snd_ArgentaSwing3,0.8)
							audio_play_sound(snd_ArgentaSwing3,15,0);
						}
						if (image_index >= 12) aux_Swd_atk_num = 1;
					}
				}
				if (spriteSwitch)
				{
					if (image_index >= 11)
					{
						sprite_index = choose(spr_Argenta,spr_ArgentaMix);
						spriteSwitch = false;
					}
					else 
					{
						if (image_index >= 5)
						{
							sprite_index = choose(spr_Argenta,spr_ArgentaMix);
							spriteSwitch = false;
						}
					}
				}
			
			}
		}
		else //--> Air Argenta (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
		}
		break;

	case 2: //--> DYRNWYN
		if (onFloor or swrd_onPlatform)
		{
			if (sprite_index == spr_AirDyrnwyn) state = move_Manticore;
			else
			{
				image_speed = (global.SWORDS[2,3]==0) ? 1 : ((global.SWORDS[2,3]==1) ? 1.1 : ((global.SWORDS[2,3]==2) ? 1.3 : 1.5));
				if (global.RELIKS[48,2]) image_speed += 0.5;
				if (sprite_index != spr_Dyrnwyn) && (sprite_index != spr_DyrnwynMix)
				{
					attack_number = aux_Swd_atk_num;
					sprite_index = choose(spr_Dyrnwyn,spr_DyrnwynMix);
					if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
					image_index = (attack_number == 2) ? 5 : ((attack_number == 3) ? 11 : 0);
				}
				if (atk_key_jump)
				{
					air_attack = true;
					state = doublejump_Manticore;
					vsp = -20;
					aux_Swd_atk_num = attack_number;
					alarm[7] = 90;
					exit;
				}
				var chispa = instance_create_layer(x,y,"Back_FX_Layer",obj_chispa);
				with(chispa)
				{
					x += random_range(40,180) * obj_Manticore.image_xscale;
					y += random_range(-50,40);
				}
				//--> Hitboxes
				hitbox_duration(0,3,obj_Manticore_hitbox,spr_Dyrnwyn_hitbox1);
				if (image_index>=3)and(image_index<=5)
				{
					if (atk_key_s)
					{
						attack_number = 1;
						image_index = 0;
						state = axe_atks_Manticore;
						exit;
					}
					if (atk_key_d) 
					{
						if (attack_number == 1)	spriteSwitch = true;
						attack_number = 2;
						if (image_index>=4) if (!atk_key_shift)&&(direc != 0) image_xscale = direc;
					}
					aux_Swd_atk_num = 2;
					alarm[7] = 90; //--> To continue combo after cancel
				}
				if (!audio_is_playing(snd_DyrnwynSwing1)) && (attack_number == 1) //--> Sound FX
				{
					audio_sound_pitch(snd_DyrnwynSwing1,random_range(1,1.3))
					audio_play_sound(snd_DyrnwynSwing1,15,0);
				}
				if (!audio_is_playing(snd_ArgentaSwing1)) && (attack_number == 1) //--> Sound FX
				{
					audio_sound_pitch(snd_ArgentaSwing1,random_range(0.9,1))
					audio_play_sound(snd_ArgentaSwing1,16,0);
				}
				if (image_index >= 5) //and (attack_number > 1)
				{
					hitbox_duration(7,9,obj_Manticore_hitbox,spr_Dyrnwyn_hitbox2);
					if (attack_number < 2) state = move_Manticore;
					if (image_index>=7)and(image_index<=11)
					{
						aux_Swd_atk_num = 3;
						alarm[7] = 90;
						if (atk_key_s)
						{
							attack_number = 1;
							image_index = 0;
							state = axe_atks_Manticore;
							exit;
						}
						if (atk_key_d) 
						{
							if (attack_number == 2)	spriteSwitch = true;
							attack_number = 3;
							if (image_index>=8) if (!atk_key_shift)&&(direc != 0) image_xscale = direc;
						}
					}
					if (!audio_is_playing(snd_DyrnwynSwing2)) && (attack_number == 2)//--> Sound FX
					{
						audio_sound_pitch(snd_DyrnwynSwing2,random_range(0.9,1.3));
						audio_play_sound(snd_DyrnwynSwing2,15,0);
					}
					if (!audio_is_playing(snd_ArgentaSwing2)) && (attack_number == 2)//--> Sound FX
					{
						audio_sound_pitch(snd_ArgentaSwing2,random_range(0.6,0.8))
						audio_play_sound(snd_ArgentaSwing2,16,0);
					}
					if (image_index >= 10) 
					{
						hitbox_duration(12,15,obj_Manticore_hitbox,spr_Dyrnwyn_hitbox3);
						if (attack_number < 3) or (image_index > 17)
						{
							attack_number = 1;
							state = move_Manticore;
						}
						if (!audio_is_playing(snd_DyrnwynSwing3)) && (attack_number == 3) //--> Sound FX
						{
							audio_sound_pitch(snd_DyrnwynSwing3,random_range(0.9,1.3))
							audio_play_sound(snd_DyrnwynSwing3,15,0);
						}
						if (!audio_is_playing(snd_ArgentaSwing3)) && (attack_number == 3) //--> Sound FX
						{
							audio_sound_pitch(snd_ArgentaSwing3,0.7)
							audio_play_sound(snd_ArgentaSwing3,16,0);
						}
						if (image_index >= 12) aux_Swd_atk_num = 1;
					}
				}
				if (spriteSwitch)
				{
					if (image_index >= 11)
					{
						sprite_index = choose(spr_Dyrnwyn,spr_DyrnwynMix);
						spriteSwitch = false;
					}
					else 
					{
						if (image_index >= 5)
						{
							sprite_index = choose(spr_Dyrnwyn,spr_DyrnwynMix);
							spriteSwitch = false;
						}
					}
				}
			}
		}
		else //--> Air Argenta (continues in doublejump_Manticore)
		{
			air_attack = true;
			state = doublejump_Manticore;
		}
		break;

	case 3: //--> GIR-TAB
		if (onFloor or swrd_onPlatform)
		{
			if (atk_key_jump)
			{
				vsp = -20; //--> Full jump and Attack
				air_attack = true;
				state = doublejump_Manticore;
				attack_number = 1;
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				audio_play_sound(snd_Jump,10,0); //--> Sound FX
			}
			if (sprite_index == spr_AirGirTab) or (sprite_index == spr_AirGirTabUP)
			{
				if (atk_key_d)
				{
					sprite_index = (atk_key_up) ? ((global.SWORDS[3,3]==3) ? spr_GirTridentUP : spr_GirTabUP) :
					((global.SWORDS[3,3]==3) ? choose(spr_GirTrident,spr_GirTridentMix) : choose(spr_GirTab,spr_GirTabMix));
					image_index = 3;
				}
				else if (image_index >= 10) state = move_Manticore;
			}
			else
			{
				image_speed = 1;
				if (global.RELIKS[28,2]) //--> Countinuous attacks after dash
				{
					if (alarm[4] > 54) image_index = 3;
				}
				else
				{
					if (alarm[4] > 4) image_index = 3;
				}
				//--> Gir-Tab first double Strike
				if (attack_number == 1)
				{
					sprite_index = (atk_key_up) ? ((global.SWORDS[3,3]==3) ? spr_GirTridentUP : spr_GirTabUP) :
					((global.SWORDS[3,3]==3) ? choose(spr_GirTrident,spr_GirTridentMix) : choose(spr_GirTab,spr_GirTabMix));
					attack_number = 2;
				}
				//--> Hitboxes
				var normORup1 = (atk_key_up) ? spr_GirTabUP_hitbox1 : spr_GirTab_hitbox1;
				var normORup2 = (atk_key_up) ? spr_GirTabUP_hitbox2 : spr_GirTab_hitbox2;
				hitbox_duration(4,5,obj_Manticore_hitbox,normORup1);
				hitbox_duration(6,7,obj_Manticore_hitbox,normORup2);
				if (image_index >= 8) 
				{
					if (atk_key_d)
					{
						if (sprite_index == spr_GirTab)||(sprite_index == spr_GirTabMix)
							sprite_index = choose(spr_GirTab,spr_GirTabMix);
						if (sprite_index == spr_GirTrident)||(sprite_index == spr_GirTridentMix)
							sprite_index = choose(spr_GirTrident,spr_GirTridentMix);
						image_index = 4;
						attack_number = 1;
					}
					else
					{
						if (image_index >= 10)
						{
							state = move_Manticore;
							attack_number = 1;
						}
					}
				}
			}
		}
		if (!onFloor && !swrd_onPlatform) //--> Air Gir-Tab
		{
			air_attack = true;
			attack_number = 1;
			state = doublejump_Manticore;
		}
		break;

	case 4: //--> FROSTBITER
		if (onFloor or swrd_onPlatform)
		{
			if (atk_key_jump)
			{
				vsp = -20; //--> Full jump and Attack
				air_attack = true;
				state = doublejump_Manticore;
				if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
				exit;
			}
			if (sprite_index == spr_AirFrostbiter) or (sprite_index == spr_AirFrostbiterUP)
			{
				if (atk_key_d)
				{
					sprite_index = (atk_key_up) ? spr_FrostbiterUP : spr_Frostbiter;
					image_index = 3;
				}
				else if (image_index >= 10) state = move_Manticore;
			}
			else
			{
				image_speed = 1;
				if (global.RELIKS[28,2]) //--> Countinuous attacks after dash
				{
					if (alarm[4] > 54) image_index = 3;
				}
				else
				{
					if (alarm[4] > 4) image_index = 3;
				}
				//--> Gir-Tab first double Strike
				if (attack_number == 1) sprite_index = (atk_key_up) ? spr_FrostbiterUP : spr_Frostbiter;
				//--> Hitboxes
				var normORup1 = (atk_key_up) ? spr_GirTabUP_hitbox1 : spr_GirTab_hitbox1;
				var normORup2 = (atk_key_up) ? spr_GirTabUP_hitbox2 : spr_GirTab_hitbox2;
				hitbox_duration(4,5,obj_Manticore_hitbox,normORup1);
				hitbox_duration(6,7,obj_Manticore_hitbox,normORup2);
				if (image_index >= 8) 
				{
				
					if (atk_key_d) image_index = 4;
					else if (image_index >= 10) state = move_Manticore;
				}
			}
		}
		if (!onFloor && !swrd_onPlatform) //--> Air Frostbiter
		{
			air_attack = true;
			state = doublejump_Manticore;
		}
		break;
	}


}
