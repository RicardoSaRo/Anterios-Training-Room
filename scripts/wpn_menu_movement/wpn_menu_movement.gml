/// @description wpn_menu_movement(weapon_type,menu_place,movement)
/// @param wpn_type
/// @param menu_place
/// @param movement
function wpn_menu_movement(argument0, argument1, argument2) {

	var wpn_type = argument0;
	var menu_place = argument1;
	var mov = argument2;
	var start_place = menu_place;
	var limit = 0;
	var copyGlobal = global.AXES;
	var foundAvailable = false;

	if (mov = "up")
	{
		//--> Defines a limit to upward movement. It depends of weapon type and the place in the menu
		if (wpn_type == "SWORD") or (wpn_type == "AXE")
		{
			if (wpn_type == "SWORD") copyGlobal = global.SWORDS;
			else copyGlobal = global.AXES;
			limit = 1;
		}
		else
		{
			copyGlobal = global.TWs;
			if (menu_place < 6) limit = 1;
			else
			{
				if (menu_place > 5) && (menu_place < 11) limit = 6;
				else limit = 11;
			}
		}
	
		//--> Looks for an available weapon
		while (menu_place > limit) && (!foundAvailable)
		{
			menu_place -= 1;
			if (copyGlobal[menu_place,1]) foundAvailable = true;		
		}
		menu_place = clamp(menu_place,limit,menu_place);
	
		//--> If reaches limit and founds nothing, then goes to swords or axes depending of position
		if (wpn_type = "TW") && (!foundAvailable)
		{
			if (limit == 1)
			{
				wpn_type = "SWORD";
				menu_place = 5;
				copyGlobal = global.SWORDS;
			}
			else 
			{
				wpn_type = "AXE";
				menu_place = 5;
				copyGlobal = global.AXES;
				limit = 1;
			}
			//--> after re-assigning variables to swords/axes searches for weapon availablility
			while (menu_place > limit) && (!foundAvailable)
			{
				menu_place -= 1;
				if (copyGlobal[menu_place,1]) foundAvailable = true;
			}
		}
	}

	if mov = "down"
	{
		//--> Defines a limit to upward movement. It depends of weapon type and the place in the menu
		if (wpn_type == "SWORD") or (wpn_type == "AXE")
		{
			if (wpn_type == "SWORD") copyGlobal = global.SWORDS;
			else copyGlobal = global.AXES;
			limit = 4;
		}
		else
		{
			copyGlobal = global.TWs;
			if (menu_place < 6) limit = 5;
			else
			{
				if (menu_place > 5) && (menu_place < 11) limit = 10;
				else limit = 16;
			}
		}
	
		//--> Looks for an available weapon
		while (menu_place < limit) && (!foundAvailable)
		{
			menu_place += 1;
			if (copyGlobal[menu_place,1]) foundAvailable = true;
		}
		menu_place = clamp(menu_place,menu_place,limit);
	
		//--> If reaches limit and founds nothing, then goes to TWs depending of position
		if (wpn_type = "SWORD") && (!foundAvailable)
		{
				wpn_type = "TW";
				menu_place = 0;
				copyGlobal = global.TWs;
				limit = 5;
			
				//--> after re-assigning variables to TWs searches for weapon availablility from 1 to 5
				while (menu_place < limit) && (!foundAvailable)
				{
					menu_place += 1;
					if (copyGlobal[menu_place,1]) foundAvailable = true;
				}
		}
		if (wpn_type = "AXE") && (!foundAvailable)
		{
			wpn_type = "TW";
			menu_place = 10;
			copyGlobal = global.TWs;
			limit = 16;
		
			//--> after re-assigning variables to TWs searches for weapon availablility from 11 to 16
			while (menu_place < limit) && (!foundAvailable)
			{
				menu_place += 1;
				if (copyGlobal[menu_place,1]) foundAvailable = true;
			}
		
			//--> If doesn't find wpn available formm 11 to 16, then searches back from 11 to 1
			if (!foundAvailable)
			{
				limit = 1;
				menu_place = 12;
				while (menu_place > limit) && (!foundAvailable)
				{
					menu_place -= 1;
					if (copyGlobal[menu_place,1]) foundAvailable = true;
				}
			}
		}
		if (wpn_type = "TW") && (!foundAvailable) menu_place = start_place;
	}

	if (mov = "left")
	{
		//--> Defines a limit to upward movement (if there is no availability, seeks it upwards)
		if (wpn_type == "AXE") limit = 1;
		else
		{
			copyGlobal = global.TWs;
			if (menu_place > 10) limit = 6;
			else if (menu_place > 5) && (menu_place < 11) limit = 1;
		}
		//--> Left from swords are no options. So, the movement is ignored
		if (wpn_type != "SWORD") or (wpn_type == "TW")
		{
			if (menu_place > 5) or (wpn_type == "AXE")
			{
				if (wpn_type == "AXE")
				{
					wpn_type = "SWORD";
					menu_place += 1;
					copyGlobal = global.SWORDS;
				}
				else
				{
					wpn_type = "TW";
					menu_place -= 4;
					copyGlobal = global.TWs;
				}
				//--> Looks for an available weapon
				while (menu_place > limit) && (!foundAvailable)
				{
					menu_place -= 1;
					if (copyGlobal[menu_place,1]) foundAvailable = true;
				}
				menu_place = clamp(menu_place,limit,menu_place);
				//--> If didn't find availability. Searches again the same column from bottom to top
				if (wpn_type = "TW") && (!foundAvailable)
				{
					menu_place = limit - 1;
					if (limit  == 6) limit = 10;
					else limit = 5;
					while (menu_place < limit) && (!foundAvailable)
					{
						menu_place += 1;
						if (copyGlobal[menu_place,1]) foundAvailable = true;
					}
					menu_place = clamp(menu_place,menu_place,limit);
					//--> If didn't found anything. Assigns Chakram that is the fist option of the first column. Always available.
					if (!foundAvailable) menu_place = 1;
				}
			}
		}
	}

	if (mov = "right")
	{
		//--> Defines a limit to upward movement (if there is no availability, seeks it upwards)
		if (wpn_type == "SWORD") limit = 1;
		else
		{
			if (menu_place < 6) limit = 6;
			else if (menu_place > 5) && (menu_place < 11) limit = 11;
		}
		//--> Right from axes are no options. So, the movement is ignored
		if (wpn_type != "AXE") or (wpn_type == "TW")
		{
			if (menu_place < 11)
			{
				if (wpn_type == "SWORD")
				{
					wpn_type = "AXE";
					menu_place += 1;
					copyGlobal = global.AXES;
				}
				else
				{
					copyGlobal = global.TWs;
					wpn_type = "TW";
					menu_place += 6;
				}
				//--> Looks for an available weapon
				while (menu_place > limit) && (!foundAvailable)
				{
					menu_place -= 1;
					if (copyGlobal[menu_place,1]) foundAvailable = true;
				}
				menu_place = clamp(menu_place,limit,menu_place);
				//--> If didn't find availability. Searches again the same column from top to bottom
				if (wpn_type == "TW") && (!foundAvailable)
				{
					menu_place = limit-1;
					limit = 16;
					while (menu_place < limit) && (!foundAvailable)
					{
						menu_place += 1;
						if (copyGlobal[menu_place,1]) foundAvailable = true;
					}
					menu_place = clamp(menu_place,menu_place,limit);
				}
			}
		}
		if (wpn_type == "TW") && (!foundAvailable) menu_place = start_place;
	}

	var movOption = array_create(1,0);
	movOption[1,1] = wpn_type;
	movOption[1,2] = menu_place;

	return movOption;


}
