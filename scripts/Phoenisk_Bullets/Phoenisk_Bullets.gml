// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Bullets(argument0){
	
	var spr_index = argument0;
	
	switch(spr_index)
	{
		case spr_Rainbow_Fireball:
			var mngr = global.currentManager;
			image_blend = mngr.rainbow_blend;
			if (alarm[0] <= 0)
			{
				var rndmpos = irandom_range(-20,20);
				var rainbowCircle2 = instance_create_layer(x+(20*image_xscale),y+rndmpos,"FX_Layer",obj_fadeAway);
				rainbowCircle2.sprite_index = choose(spr_rainbowCircle1,spr_rainbowCircle3);
				//rainbowCircle2.image_alpha = random_range(0.9,1);
				rainbowCircle2.fadeSpd = random_range(0.01,0.03);
				rainbowCircle2.Ymovement = 0;
				rainbowCircle2.Xmovement = 0;
				rainbowCircle2.sizeMod = random_range(0.01,0.05);
				var circSiz = random_range(0.5,2.5);
				rainbowCircle2.image_xscale = circSiz;
				rainbowCircle2.image_yscale = circSiz;
				alarm[0] = random_range(12,25);
			}
			if (playerCollision)
			{
				if (global.BossVariant == 0) bulletCollidesPlayer(2,2,mngr,0,1,0,0);
				else bulletCollidesPlayer(2,4,mngr,1,1,0,0);
			}
			if (place_meeting(x,y,obj_Wall))
			{
				var wallnear = instance_place(x,y,obj_Wall);
				soundGain_by_distance(obj_Manticore,150,1,snd_ExplosionUpDash,32,0);
				image_index = 0;
				sprite_index = destroy_sprite;
				//--> Explosion Angles
				if (wallnear.y == 1152)||(wallnear.y == 0) image_angle = (wallnear.y == 0) ? 180 : 0;
				else image_angle = (wallnear.x == 384) ? 270 : 90;
				image_xscale = choose(1,-1);
				bounce = false;
				speed = 0;
			}
			//--> FX
			if (!place_meeting(x,y,dash_afterimage)) && (alarm[1] <= 0)
			{
				var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_angle = image_angle;
				afterimage.image_index = irandom(5);
				afterimage.glowFX = true;
				afterimage.image_alpha = 0.6;
				afterimage.image_blend = mngr.rainbow_blend;
				alarm[1] = 3;
			}
			if (x <= 0) or (x >= room_width) instance_destroy();
			break;
		
		case spr_StunWave:
			if (image_angle != 0)
			{
				speed = 3;
				image_alpha -= 0.03;
			}
			else
			{
				speed = 7;
				image_alpha -= 0.03;
			}
			if (image_alpha >= 0.4)
			{
				if (place_meeting(x,y,obj_Manticore))
				{
					if (!counter)
					{
						with (obj_Manticore)
						{
							//global.player_max_health = Manticore_hp;
							alarm[6] = (other.direc == 1) ? 25 : 60;
							state = stun_Manticore;
							vsp = (other.switch_var) ? -20 * other.direc : 0;
							hsp = (other.switch_var) ? 0 : 20 * sign(other.image_xscale);
							y += (other.switch_var) ? -1 : 0;
						}
						counter = true;
					}
				}
			}
			if (image_alpha <= 0) instance_destroy();
			break;
			
		case spr_RainbowExplosion64x:
			image_blend = ownership.rainbow_blend;
			if (playerCollision)
			{
				if (global.BossVariant == 0) bulletCollidesPlayer(2,2,global.currentManager,0,1,0,0);
				else bulletCollidesPlayer(2,4,global.currentManager,1,1,0,0);
			}
			if (image_index >= 8.6) instance_destroy();
			break;
		
		case spr_Explosion_grnd: case spr_Explosion_blue:
			if (bounce)
			{
				image_xscale += 0.02;
				image_yscale += 0.02;
			}
			if (image_index <= 8)
			{
				if (playerCollision)
				{
					if (global.BossVariant == 0) bulletCollidesPlayer(3,3,global.currentManager,0,1,0,0);
					else bulletCollidesPlayer(3,6,global.currentManager,1,1,0,0);
				}
			}
			if (image_index >= 12.6) instance_destroy();
			break;
			
		case spr_Fireball:
			vsp += grv;
			image_speed = 0.8;
			if (image_index >= 5) image_index = 5;
			if (place_meeting(x,y,obj_Wall)) or
			   ((place_meeting(x,y,obj_Manticore_hitbox))&&(obj_Manticore.atk_type == 2)) or
			   ((x < obj_BossEnemy.minX)or(x > obj_BossEnemy.maxX))
				sprite_index = destroy_sprite;
			else
			{
				x += hsp;
				y += vsp;
				image_angle += hsp/3;
			}						
			if (instance_number(obj_charge_chispa)<100)
			{
				var spark = instance_create_layer(x+random_range(-30,30),y+random_range(-30,30),"FX_Layer",obj_charge_chispa)
				spark.coloroption1 = c_orange;
				spark.coloroption2 = c_red;	
			}
			break;
		
		case spr_StoneBrth:
			if (switch_var) || (playerCollision) sprite_index = destroy_sprite;
			break;
		
		case spr_Explosion_bmb: case spr_BigAquaExplosion:
			if (image_index >= 4) && (image_index <= 7) && (playerCollision)//--> ONE HIT KILL!!!! + FXs
			{
				consecutive_dmg = -1;
				repeat(global.player_max_health - 1) 
				{
					consecutive_dmg += 1;
					var lifeorb = instance_find(obj_Life_Orb,consecutive_dmg);
					lifeorb.sprite_index = spr_Life_Orb_empty;
					lifeorb.dmg = consecutive_dmg+1;
				}
				obj_Manticore.Manticore_hp -= global.player_max_health;
				obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
			}
			if (image_index >= 14) instance_destroy();
			break;
			
		case spr_RainbowLance_ball:
			image_blend = ownership.rainbow_blend;
			if (alarm[0] <= 0) && (counter > 0)
			{
				if (visible == true) visible = false;
				repeat(5)
				{
					var light = instance_create_layer(x,y,"Back_FX_Layer",obj_GrassLeaf);
					light.sprite_index = choose(spr_rainbowLight,spr_whiteLight);
					light.image_blend = choose(c_white,c_white,c_white,c_orange,c_yellow,c_aqua,c_lime,c_fuchsia,ownership.rainbow_blend);
					light.image_alpha = random_range(0.9,0.7);
					light.alarm[0] = choose(15,20,25,30);
					light.raise = 0;
					light.xmov = 0;
					light.rotatin = choose(10,20)//,30,40,50,60);
					light.sizeMod = random_range(0.01,0.05);
					var circSiz = choose(0.5,1,1.5,2,2.5,3);
					light.image_xscale = circSiz;
					light.image_yscale = circSiz;
				}
				var lance = instance_create_layer(x,y,"FX_Layer",obj_EnemyBullet);
				lance.sprite_index = spr_RainbowLance_create;
				lance.switch_var = false; //--> Not final lance attack
				lance.ownership = global.currentManager;
				
				fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,208,4,24,70,1,ownership.rainbow_blend,1);
				
				switch(global.difficulty) //--> Shot intervals
				{
					case "casual":	alarm[0] = 80; break;
					case "normal":	alarm[0] = 60; break;
					case "hard":	alarm[0] = 50; break;
					case "lunatic":	alarm[0] = 40; break;
				}
				counter -= 1;
			}
			if (image_yscale <= 1)
			{
				image_yscale += 0.05;
				image_xscale += 0.05;
				//--> Absorb FX
				if (instance_number(obj_Absorb) < 15)
				{
					var rndXplus = random_range(x+400,x+600);
					var rndXminus = random_range(x-400,x-600);
					var rndYplus = random_range(y+400,y+600);
					var rndYminus = random_range(y-400,y-600);
					var absorb = instance_create_layer(random_range(rndXplus,rndXminus),random_range(rndYplus,rndYminus),"Back_FX_Layer",obj_Absorb);
					absorb.alfaplus = random_range(0.03,0.04);
					absorb.obj2follow = self;
					var rndmSpd = random_range(35,45);
					absorb.XYspd = rndmSpd;
					var rndmScale = random_range(0.3,0.8);
					absorb.image_xscale = rndmScale;
					absorb.image_yscale = rndmScale;
					absorb.image_blend = choose(c_orange,c_yellow,c_aqua,c_lime,c_fuchsia,ownership.rainbow_blend);
					absorb.starFX = choose(false,true);
					absorb.twnklSpd = random_range(0.5,2.5);
				}
			}
			if (counter <= 0) && (alarm[0] <= 0) switch_var = true;
			if (switch_var) //--> Final Lance
			{
				var moreAngl = irandom(360);
				//--> Creates Nova
				for (var i=0;i<=18;i++)
				{
					var flance = instance_create_layer(x,y,"FX_Layer",obj_EnemyBullet);
					flance.sprite_index = spr_RainbowLance_create;
					flance.ownership = global.currentManager;
					flance.alarm[0] = (i mod 2 == 0) ? 50 : 20;
					flance.switch_var = true;
					flance.direction = (i * 30) + moreAngl;
					flance.image_speed *= (i mod 2 == 0) ? 2 : 1;
					flance.image_angle = (i * 30) + moreAngl;
					flance.image_yscale = 0.5;
					flance.NoDamage = true;
					flance.direc = 0;
				}
				obj_BossEnemy.interact_instance = obj_BossEnemy;
				fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],52,512,8,16,83,1,ownership.rainbow_blend,1);
				instance_destroy();
			}
			break;
			
		case spr_RainbowLance_create:
			image_blend = ownership.rainbow_blend;
			if (switch_var)
			{
				speed = 5;
			}
			else
			{
				direction = point_direction(x,y,obj_Manticore.x,obj_Manticore.y);
				image_angle = direction;
			}
			if (image_index >= 12.6)
			{
				image_index = 0;
				sprite_index = spr_RainbowLance;
				speed = (switch_var) ? 45 : 40;
				var rndmpos = irandom_range(-20,20);
				var rainbowCircle2 = instance_create_layer(x+(20*image_xscale),y+rndmpos,"FX_Layer",obj_fadeAway);
				rainbowCircle2.sprite_index = choose(spr_rainbowCircle1,spr_rainbowCircle3);
				//rainbowCircle2.image_alpha = random_range(0.9,1);
				rainbowCircle2.fadeSpd = random_range(0.01,0.03);
				rainbowCircle2.Ymovement = 0;
				rainbowCircle2.Xmovement = 0;
				rainbowCircle2.sizeMod = random_range(0.01,0.05);
				var circSiz = random_range(0.5,2.5);
				rainbowCircle2.image_xscale = circSiz;
				rainbowCircle2.image_yscale = circSiz;
				if (switch_var)
				{
					alarm[0] = 25;
					obj_BossEnemy.switch_var2 = true;
				}
			}
			break;
			
		case spr_RainbowLance:
			if (speed >= 0)
			{
				//--> FX
				if (!place_meeting(x,y,dash_afterimage)) && (alarm[1] <= 0)
				{
					var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
					afterimage.sprite_index = sprite_index;
					afterimage.image_angle = image_angle;
					afterimage.image_index = image_index;
					afterimage.glowFX = true;
					afterimage.image_alpha = 0.6;
					afterimage.image_blend = ownership.rainbow_blend;
				}
			}
			//--> Lance Speed
			if (alarm[0] > 0) speed = 0;
			else
			{
				if (place_meeting(x,y,obj_Wall))
				{
					speed = 0;
					image_alpha -= 0.05;
					if (!switch_var2)
					{
						if (instance_number(obj_EnemyBullet)<=3)
						{
							for(var i = 1;i <= 2;i ++)
							{
								var rainbowCircle2 = instance_create_layer(x,y,"Back_FX_Layer",obj_fadeAway);
								rainbowCircle2.sprite_index = sprite_index;
								rainbowCircle2.image_alpha = 0.6;
								rainbowCircle2.fadeSpd = 0.05 * i;
								rainbowCircle2.Ymovement = 0;
								rainbowCircle2.Xmovement = 0;
								rainbowCircle2.sizeMod = 0.05 * (i * sign(image_xscale));
								rainbowCircle2.image_xscale = image_xscale;
								rainbowCircle2.image_yscale = image_yscale;
							}
							fshockwave_create_layer(x,y,"FX_Layer",view_camera[0],48,208,4,24,70,1,ownership.rainbow_blend,1);
						}
						switch_var2 = true;
					}
				}
				else speed = (switch_var) ? 45 : 40;
			}
			if (image_alpha >= 0.4)
			{
				if (playerCollision)//--> ONE HIT KILL!!!! + FXs
				{
					consecutive_dmg = -1;
					repeat(global.player_max_health - 1) 
					{
						consecutive_dmg += 1;
						var lifeorb = instance_find(obj_Life_Orb,consecutive_dmg);
						lifeorb.sprite_index = spr_Life_Orb_empty;
						lifeorb.dmg = consecutive_dmg+1;
					}
					obj_Manticore.Manticore_hp -= global.player_max_health;
					obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
				}
			}
			if (speed > 0)
			{
				var rndm = irandom_range(1,25);
				if (rndm)
				{
					var rndmpos = irandom_range(-20,20);
					var rainbowCircle2 = instance_create_layer(x+(20*image_xscale),y+rndmpos,"FX_Layer",obj_fadeAway);
					rainbowCircle2.sprite_index = choose(spr_rainbowCircle1,spr_rainbowCircle3);
					//rainbowCircle2.image_alpha = random_range(0.9,1);
					rainbowCircle2.fadeSpd = random_range(0.01,0.03);
					rainbowCircle2.Ymovement = 0;
					rainbowCircle2.Xmovement = 0;
					rainbowCircle2.sizeMod = random_range(0.01,0.05);
					var circSiz = random_range(0.5,2.5);
					rainbowCircle2.image_xscale = circSiz;
					rainbowCircle2.image_yscale = circSiz;
				}
			}
			if (image_alpha <= 0)||(x <= 0)||(x >= room_width) instance_destroy();
			break;
			
		case spr_Rainbow_implosion:
			image_blend = ownership.rainbow_blend;
			if (image_index >= 3) && (!switch_var)
			{
				if ((x >= 0)&&(x <= room_width)) && ((y >= 0)&&(y <= room_height))
				{
					var movX = x + (random_range(200,250)*image_xscale);
					var mntcrY = obj_Manticore.y;
					if (mntcrY >= y) mntcrY += random_range(150,-250);
					else mntcrY -= random_range(150,250);
					var newCluster = instance_create_layer(movX,mntcrY,"FX_Layer",obj_EnemyBullet);
					newCluster.sprite_index = spr_Rainbow_implosion;
					newCluster.switch_var2 = (obj_BossEnemy.enemy_name == "PhoeniskR") ? 0 : 1; //--> reference for the next cluster
					newCluster.ownership = global.currentManager;
					newCluster.image_xscale = image_xscale;
					newCluster.image_angle = random(360);
					newCluster.priority = 10;
					newCluster.NoDamage = true;
					soundGain_by_distance(obj_Manticore,200,0.7,snd_DL_Implosion,irandom_range(30,60),0);
				}
				switch_var = true;
			}
			if (image_index >= 8)
			{
				image_index = 0;
				sprite_index = spr_BigRainbow_Explosion;
			}
			break;
		
		case spr_BigRainbow_Explosion:
			image_blend = ownership.rainbow_blend;
			image_xscale += 0.01 * sign(image_xscale);
			image_yscale += 0.01;
			if (!bounce)//--> For soundFX
			{
				soundGain_by_distance(obj_Manticore,200,0.7,snd_Golem_punch,irandom_range(30,60),0);
				bounce = 1;
			}
			if (playerCollision)
			{
				if (!switch_var2) bulletCollidesPlayer(2,2,global.currentManager,0,1,0,0);
				else bulletCollidesPlayer(2,4,global.currentManager,1,1,0,0);
			}
			if (image_index < 5)
			{
				var rainbowCircle2 = instance_create_layer(x+random_range(-160,160),y+random_range(-160,160),"FX_Layer",obj_fadeAway);
				rainbowCircle2.sprite_index = choose(spr_rainbowCircle1,spr_rainbowCircle3);
				rainbowCircle2.fadeSpd = random_range(0.01,0.03);
				rainbowCircle2.Ymovement = 0;
				rainbowCircle2.Xmovement = 0;
				rainbowCircle2.sizeMod = random_range(0.01,0.05);
				var circSiz = random_range(0.5,2.5);
				rainbowCircle2.image_xscale = circSiz;
				rainbowCircle2.image_yscale = circSiz;
			}
			var afterimage = instance_create_layer(x+random_range(-10,10),y+random_range(-10,10),"FX_Layer",dash_afterimage);
			afterimage.sprite_index = sprite_index;
			afterimage.image_index = image_index;
			afterimage.image_xscale = sign(image_xscale) * 2;
			afterimage.image_yscale = 2;
			afterimage.image_blend = ownership.rainbow_blend;
			afterimage.image_angle = image_angle;
			afterimage.image_alpha = random_range(0.2,0.6);
			if (image_index >= 14) instance_destroy();
			break;
			
		case spr_RainbowBeam_standing:
			image_blend = amount.rainbow_blend;
			if (alarm[0] mod 6 = 0)
			{
				var afterimage = instance_create_layer(x,y,"FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_index = image_index;
				afterimage.image_xscale = 2;
				afterimage.image_yscale = 2;
				afterimage.image_blend = image_blend;
				afterimage.image_angle = image_angle;
				afterimage.image_alpha = 0.7;
			}
			if (image_xscale < 1)
			{
				image_xscale += 0.05;
				image_yscale += 0.05;
				alarm[0] = 20;
				if (instance_number(obj_Absorb) <= 40)
				var rndXplus = random_range(x+200,x+250);
				var rndXminus = random_range(x-200,x-250);
				var rndYplus = random_range(y+200,y+250);
				var rndYminus = random_range(y-200,y-250);
				var absorb = instance_create_layer(random_range(rndXplus,rndXminus),random_range(rndYplus,rndYminus),"FX_Layer",obj_Absorb);
				absorb.alfaplus = random_range(0.03,0.04);
				absorb.obj2follow = self;
				var rndmSpd = random_range(40,50);
				absorb.XYspd = rndmSpd;
				var rndmScale = random_range(0.3,0.5);
				absorb.image_blend = choose(c_fuchsia,c_aqua,c_orange,c_yellow,c_lime,c_white);
				absorb.image_xscale = rndmScale;
				absorb.image_yscale = rndmScale;
				absorb.starFX = true;
				absorb.twnklSpd = random_range(0.5,2.5);
			}
			else
			{
				if (alarm[0] <= 0)
				{
					if (sprite_index != spr_RainbowBeam_shoot)
					{
						image_index = 0;
						sprite_index = spr_RainbowBeam_shoot;
					}
				}
			}
			break;
			
		case spr_RainbowBeam_shoot:
			image_blend = amount.rainbow_blend;
			if (alarm[0] mod 6 = 0)
			{
				var afterimage = instance_create_layer(x,y,"FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_index = image_index;
				afterimage.image_xscale = 2;
				afterimage.image_yscale = 2;
				afterimage.image_blend = image_blend;
				afterimage.image_angle = image_angle;
				afterimage.image_alpha = 0.7;
			}
			if (image_index >= 4.6)
			{
				image_index = 0;
				sprite_index = spr_RainbowBeam;
				alarm[0] = 60;
				direction = point_direction(x,y,obj_Manticore.x,obj_Manticore.y);
				image_angle = direction;
				speed = 20;
				if (!switch_var)
				{
					ownership.switch_var2 = false;
					switch_var = true;
				}
			}
			break;
			
		case spr_RainbowBeam:
			image_blend = amount.rainbow_blend;
			if (alarm[0] mod 6 = 0)
			{
				var afterimage = instance_create_layer(x,y,"FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_index = image_index;
				afterimage.image_xscale = 2;
				afterimage.image_yscale = 2;
				afterimage.image_blend = image_blend;
				afterimage.image_angle = image_angle;
				afterimage.image_alpha = 0.7;
			}
			if (playerCollision)
			{
				var variant = (global.BossVariant == 0) ? 0 : 1;
				bulletCollidesPlayer(1,2,global.currentManager,variant,1,0,0);
			}
			if (alarm[0] <= 30)
			{
				image_index = 0;
				sprite_index = spr_RainbowBeam_stop;
				speed = 0;
			}
			break;
			
		case spr_RainbowBeam_stop:
			image_blend = amount.rainbow_blend;
			if (alarm[0] mod 6 = 0)
			{
				var afterimage = instance_create_layer(x,y,"FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_index = image_index;
				afterimage.image_xscale = 2;
				afterimage.image_yscale = 2;
				afterimage.image_blend = image_blend;
				afterimage.image_angle = image_angle;
				afterimage.image_alpha = 0.7;
			}
			if (image_index >= 4.6)
			{
				counter++;
				if (counter >= bounce)
				{
					image_index = 4;
					sprite_index = spr_Rainbow_implosion;
					ownership = global.currentManager;
					switch_var = true;
				}
				else
				{
					image_index = 0;
					sprite_index = spr_RainbowBeam_standing;
				}
			}
			break;
	}
}