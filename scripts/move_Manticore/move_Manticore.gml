//--> move_Manticore is the core function for the character's State Machine

function move_Manticore() {
	state = move_Manticore;

	//snd_Stop_Manticore();

	//--> Prevents Stucks on jump animation if lands after some attacks
	atk_type = 0;
	attack_number = 1;

	//--> Movement Capture
	key_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
	key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);
	key_jump = max(keyboard_check_pressed(vk_space),gamepad_button_check_pressed(global.controller_type,gp_face1),0);
	key_minijump = max(keyboard_check_released(vk_space),gamepad_button_check_released(global.controller_type,gp_face1),0);
	key_lshift = max(keyboard_check(vk_shift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	parry_check = max(keyboard_check_pressed(vk_shift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	key_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
	key_s = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);
	key_a = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
	key_q = max(keyboard_check(ord("Q")),gamepad_button_check(global.controller_type,gp_face4),0);
	key_down = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);

	if (global.RELIKS[49,2]) key_q = false; //--> Relik specific restriction

	if (global.RELIKS[45,2]) //--> Dash Attack
	{
		if (key_d) && (sprite_index == spr_Manticore_fdash)
		{
			strongWpnAtk();
			exit;
		}
	}

	//--> Movement Calcs
	if (!key_right) and (!key_left) direc = 0;
	else
	{
		if (key_right) and (!key_left) direc = 1;
		if (key_left) and (!key_right) direc = -1;
	}

	if (direc == 0) speed = 0; //--> Prevents Shaking

	if (artist.freezeFX) walksp = 6; //--> If freeze stat, slows the walk speed
	else walksp = (global.RELIKS[29,2]) ? 12 : 9;
	hsp = direc * walksp; //--> Horizontal Speed

	if grv != 1 grv = 1; //--> Returns Gravity to normal

	if (vsp >= 25) prevVSP = true;
	if (vsp <= -1) prevVSP = false;

	vsp = vsp + grv; //--> Vertical Speed
	vsp = clamp(vsp,-20,25); //--> Prevents ridiculous speeds when player falls
	
	onFloor = place_meeting(x,y+1,obj_Wall);

	onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> Platform Collision Check
	if (!platfDrop)
	{
		if place_meeting(x,y+vsp,obj_Platform)
		{
			if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
			{
				y = instance_nearest(x,y,obj_Platform).bbox_top-56;
				vsp = 0;
			}
		}
	}

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) onPlatform = true;
	else onPlatform = false;
	
	//--> Switches platfDrop variable back to false, in case it was set true in "crouch defense" script
	platfDrop = false;

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	x = x + hsp; //--> Horizontal Movement

	//--> Vertical Collision (Wall)
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}

	y = y + vsp; //--> Vertical Movement

	//--> Double Jump
	if (!onFloor) && (!onPlatform) && (key_jump)
	{
		if (!doublejump_used)
		{
			instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> Doublejump halo effect
			doublejump_Manticore();
		}
		else
		{
			if (global.RELIKS[16,2]) //--> Wind Jump (Triple Jump)
			{
				if (!windjump_used) && (special_Whisperer >= 10)
				{
					if (!audio_is_playing(snd_WindJump)) audio_play_sound(snd_WindJump,17,0);
					repeat(6)
					{
						var wndjmpFX = instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
						wndjmpFX.sprite_index = spr_windjmp_fx;
						wndjmpFX.image_blend = c_white;
					}
					special_Whisperer -= 10;
					special_Whisperer = clamp(special_Whisperer,0,100);
					windjump_used = true;
					doublejump_Manticore();
				}
			}
		}
	}

	//--> Short hop
	if (!onFloor && !onPlatform) and (key_minijump) and (vsp < -7)	vsp = -7;

	//--> Jump
	if (onFloor or onPlatform) and (key_jump)	
	{
		if (key_d) key_d = false; //--> Prevents weird jump buffers after attacks
		if (key_s) key_s = false;	
		if (key_a) key_a = false;
		vsp = -20;
	//	audio_sound_gain(snd_Jump,0.4,0);
		audio_play_sound(snd_Jump,10,0);
	}

	//--> Jump animation, if is touching the floor
	if (!onFloor) && (!onPlatform)
	{
		//--> Wallcling
		if (onWall) and (sign(vsp) > 0) and (hsp == 0) and (wallcling_cooldown == false)
		{
			if (!global.RELIKS[15,2]) alarm[3] = 60; //--> Forces wallcling release
			wallcling_Manticore();
		}
		else //--> Asigns images for jumping up and falling down
		{
			if (sprite_index != spr_Manticore_parry) || (sprite_index != spr_Manticore_parryAIR)
			{
				sprite_index = spr_Manticore_jump;
				image_speed = 0;
				if (sign(vsp) > 0) image_index = 1;	else image_index = 0;
			}
		
			if (airDash_used == true) || (lghtDash_used == true) //--> this "if" prevents getting on ceilings after dash
			{
				var onepixel = sign(vsp);
				while (place_meeting(x,y-onepixel,obj_Wall )) y += onepixel;
			}
		}
		//--> Stops steps audio
		audio_stop_sound(stepsSndFX);
		audio_stop_sound(stepsPlatSndFX);
	
	}

	if (onFloor)||(onPlatform)//--> Idle or Running animation AND AirDash/DoubleJump Refreshes
	{
		image_speed = 1;
	
		if (sprite_index == spr_Manticore_jump)
		{
			if (prevVSP) sprite_index = spr_Manticore_landing;
			else sprite_index = spr_Manticore;

			audio_sound_gain(snd_FloorFall,0.4,0);
			audio_play_sound(snd_FloorFall,20,0);
		}
	
		if (hsp != 0) sprite_index = spr_Manticore_run; //--> Runs
		else
		{
			if (prevVSP) || (sprite_index == spr_Manticore_landing) sprite_index = spr_Manticore_landing;
			else sprite_index = (sprite_index == spr_Manticore_parry) ? spr_Manticore_parry : spr_Manticore;
		}

	
		if (sprite_index == spr_Manticore) or (sprite_index == spr_Manticore_run) || (sprite_index == spr_Manticore_landing) || (sprite_index == spr_Manticore_parry)
		{
			airDash_used = false; //--> Refreshes Air Dash
			lghtDash_used = false;
			doublejump_used = false; //--> Refreshes Doublejump
			windjump_used = false;
			air_attack = false; //--> Indicates that an Air Attack is not being performed
			prevVSP = false;
			if (sprite_index == spr_Manticore_run) //--> Running Sound FX
			{
				if (onFloor)
				{
					if (!audio_is_playing(stepsSndFX))
					{
						audio_sound_pitch(stepsSndFX,random_range(0.8,1));
						audio_sound_gain(stepsSndFX,0.4,0);
						audio_play_sound(stepsSndFX,20,0);
					}
				}
				if (onPlatform)
				{
					if (!audio_is_playing(stepsPlatSndFX))
					{
						audio_sound_pitch(stepsPlatSndFX,random_range(0.8,1));
						audio_sound_gain(stepsPlatSndFX,0.4,0);
						audio_play_sound(stepsPlatSndFX,20,0);
					}
				}
			}
		}
	}
	
	//--> Checks the sword and lance special thats charging currently
	switch(equiped_sword)
	{
		case 1: current_sp_sword = special_Argenta; break;
		case 2: current_sp_sword = special_Dyrnwyn; break;
		case 3: current_sp_sword = special_GirTab; break;
		case 4: current_sp_sword = special_Frostbiter; break;
	}

	//--> Checks the axe and mace special thats charging currently
	switch(equipped_axe)
	{
		case 1: current_sp_axe = special_Korunax; break;
		case 2: current_sp_axe = special_Mirage; break;
		case 3: current_sp_axe = special_Mjolnir; break;
		case 4: current_sp_axe = special_Whisperer; break;
	}

	//---> Enter Attacks
	if (key_d) and (!onWall)
	{
		atk_type = 1;
	
		//--> Prevents getting stuck using inexistent air specials
		if (!onFloor && !onPlatform) && (((equiped_sword==4)) or (current_sp_sword<50))
			key_q = false;
	
		if (key_q) //--> Enters Sword/Lance Specials
		{
			if (current_sp_sword >= 50)
			{
				image_index = 0;
				special_Manticore();
				exit;
			}
		}
		else
		{
			image_index = 0;
			sword_atks_Manticore();
		}
	}

	if (key_s) and (!onWall)
	{
		atk_type = 2;
	
		//--> Prevents getting stuck using inexistent air specials
		if (!onFloor && !onPlatform) && (((equipped_axe==2)or(equipped_axe==3)or(equipped_axe==4)) or (current_sp_axe<50))
			key_q = false;
	
		if (key_q) //--> Enters Axe/Mace Specials
		{
			if (current_sp_axe >= 50)
			{
				image_index = 0;
				special_Manticore();
				exit;
			}
		}
		else
		{
			image_index = 0;
			axe_atks_Manticore();
		}
	}

	if ((TWammo[global.equippedTW,0] == 0) && (key_a)) ||
	   ((global.equippedTW == 15) && (key_a) && (global.MONEY <= 0))
	{
		if (!audio_is_playing(snd_Wrong)) audio_play_sound(snd_Wrong,3,0);
	}
	else
	{
		var currentAmmo = (TWammo[global.equippedTW,0]==noone) ? 1 : TWammo[global.equippedTW,0]; //--> If infinite, will be always 1
		if (global.equippedTW == 14) && (currentAmmo == 0) && (instance_exists(obj_Throwing_Wpn)) currentAmmo = 1;
		if (instance_exists(obj_TWSpecial)) if (obj_TWSpecial.sprite_index == spr_RemoteExpSP || spr_BombSP) && (global.equippedTW == 14) currentAmmo = 0;
	
		if (key_a) && (!tw_limit) && (!onWall) && (currentAmmo > 0)
		{
			atk_type = 3;
		
			if (key_q) //--> Enters Throwing Weapons Specials
			{
				if (TWTank[1,0]>=5000) || (TWTank[2,0]>=5000) || (TWTank[3,0]>=5000) || (TWTank[4,0]>=5000)
				{
					image_index = 0;
					specialTW_Manticore();
					exit;
				}
			}
			else
			{
				image_index = 0;
				throw_atks_Manticore();
			}
		}
	
	}

	if (key_q) && (!onWall)
	{
		if (onFloor or onPlatform)
		{
			if (current_sp_axe < 100) or (current_sp_sword < 100)
			{
				image_index = 0;
				spCharge_Manticore();
			}
		}
		else spCharge_air();
	}
	
	//-->Parry
	if (alarm[2] <= 0)
	{
		var parrywindow = (((global.RELIKS[7,2]) + (global.RELIKS[8,2])) * 6) + 18;
		if (parry_check) alarm[2] = parrywindow;
		if (sprite_index == spr_Manticore_parry)||(sprite_index == spr_Manticore_parryAIR)
		{
			sprite_index = ((onFloor)||(onPlatform)) ? spr_Manticore : spr_Manticore_jump;
			image_index = 0;
		}
	}
	else
	{
		if (alarm[2] > 10) && (key_lshift) //--> Parry
		{
			var parried = false;
			//-->> Parries Bullets prio<10 and destroys them
			if (place_meeting(x,y,obj_EnemyBullet)) or (place_meeting(x,y,obj_specialBullet))
			{
				var bulleto = (place_meeting(x,y,obj_EnemyBullet)) ? instance_place(x,y,obj_EnemyBullet) : instance_place(x,y,obj_specialBullet);
				if (bulleto.priority < 10) && (bulleto.sprite_index != bulleto.destroy_sprite)
				{
					bulleto.NoDamage = true;
					bulleto.sprite_index = bulleto.destroy_sprite;
					if (bulleto.image_index > 1) bulleto.image_index = 0;
					parried = true;
				}
			}
			
			//-->> Parries EnemyHitboxes and renders them unable to damage
			if (place_meeting(x,y,obj_Enemy_hitbox))
			{
				var EH = instance_place(x,y,obj_Enemy_hitbox);
				if (!EH.EH_hit_used)
				{
					with(obj_Enemy_hitbox) EH_hit_used = true;
					parried = true;
				}
			}
			
			if (parried)
			{
				if (alarm[11] < 20) alarm[11] = 40; //-->> Invulnerability
				alarm[2] = 10; //-->> Ready for parry again
				vsp = 0;
				
				if (sprite_index != spr_Manticore_parry)||(sprite_index != spr_Manticore_parryAIR) //--> Parry FX
				{
					if (!audio_is_playing(snd_Parry)) audio_play_sound(snd_Parry,3,0);
					var parry = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_centerXYfollow);
					parry.sprite_index = spr_Parry;
					//parry.brightness = true;
					//parry.glow = true;
					parry.image_alpha = 0.5;
					var tmnh = choose(2.2,2.3,2.4,2.5);
					parry.image_xscale = tmnh;
					parry.image_yscale = tmnh;
					parry.image_angle = random(360);
					parry.anim_end = 5;
					
					var starplace = 1;
					var starspeed = irandom_range(5,10);
					repeat(8)
					{
						var stars = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
						stars.sprite_index = spr_starShine;
						stars.image_angle = irandom(360);
						switch(starplace)
						{
							case 1:	stars.Xmovement = 0;
									stars.Ymovement = -starspeed;
									break;
							case 2:	stars.Xmovement = -starspeed;
									stars.Ymovement = -starspeed;
									break;
							case 3:	stars.Xmovement = -starspeed;
									stars.Ymovement = 0;
									break;
							case 4:	stars.Xmovement = -starspeed;
									stars.Ymovement = starspeed;
									break;
							case 5:	stars.Xmovement = 0;
									stars.Ymovement = starspeed;
									break;
							case 6:	stars.Xmovement = starspeed;
									stars.Ymovement = starspeed;
									break;
							case 7:	stars.Xmovement = starspeed;
									stars.Ymovement = 0;
									break;
							case 8:	stars.Xmovement = starspeed;
									stars.Ymovement = -starspeed;
									break;
						}
						starplace += 1;
						stars.fadeSpd = 0.01;
						stars.bright = true;
					}
				}
				
				sprite_index = ((onFloor)||(onPlatform)) ? spr_Manticore_parry : spr_Manticore_parryAIR;
				image_index = 0;
			}
		}
	}
	
	//-->  Dashes
	if (((key_lshift) and (direc != 0)) and (alarm[4] <= 0)) or
	   (((key_lshift) and (key_up)) and (alarm[4] <= 0)) or
	   (((key_lshift) and (key_down)) && (alarm[4] <= 0) and (vsp > 5))
	{
		if ((onFloor) or (onPlatform)) or (((!onFloor) && (!onPlatform))&&(airDash_used == false)) ||
		   (((!onFloor) && (!onPlatform))&&(!lghtDash_used)&&(airDash_used)&&(global.RELIKS[14,2]))
		{
			if (airDash_used == false)
			{
				alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
			
				if (global.RELIKS[15,2])&&(special_Mirage >= 10)&&(key_up)
				{
					alarm[0] = 15;
					special_Mirage -= 10;
					special_Mirage = clamp(special_Mirage,0,100);
					ExpUpDash = true;
				}
				if (key_down) if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
				dash_Manticore();
			}
			else
			{
				if (global.RELIKS[14,2]) && (alarm[4] <= 0) && (direc != 0)
				{
					if (!lghtDash_used) && (special_Mjolnir >= 10)
					{
						alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
						special_Mjolnir -= 10;
						special_Mjolnir = clamp(special_Mjolnir,0,100);
						lghtDash_used = true;
						key_up = false;
						dash_Manticore();
					}
				}
			}
		}
	}

	//--> Crouch Defense
	if (key_down) && (!onWall)
	{
		if (onFloor or onPlatform)
		{
			image_index = 0;
			crouchDEF_Manticore();
		}
	}

	//--> Inverse the sprite (if not Back Dash)
	if (sprite_index != spr_Manticore_bdash) if (hsp != 0) image_xscale = sign(hsp);

	//--> Destroys Hitbox is exits
	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);

	equiped_sword = global.equippedSWORD;
	equipped_axe = global.equippedAXE;
	equipped_tw = global.equippedTW;


}
