function stageStart_Manticore() {
	atk_type = 0;
	attack_number = 1;

	if grv != 1 grv = 1; //--> Returns Gravity to normal

	vsp = vsp + grv; //--> Vertical Speed
	vsp = clamp(vsp,-20,25); //--> Prevents ridiculous speeds when player falls
	
	onFloor = place_meeting(x,y+1,obj_Wall);

	onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) onPlatform = true;
	else onPlatform = false;

	//--> Platform Collision Check
	if (!platfDrop)
	{
		if place_meeting(x,y+vsp,obj_Platform)
		{
			if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
			{
				y = instance_nearest(x,y,obj_Platform).bbox_top-56;
				vsp = 0;
			}
		}
	}

	//--> Switches platfDrop variable back to false, in case it was set true in "crouch defense" script
	platfDrop = false;

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	x = x + hsp; //--> Horizontal Movement

	//--> Vertical Collision (Wall)
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}

	y = y + vsp; //--> Vertical Movement

	//--> Sprites
	if (!onFloor)&&(!onPlatform)
	{
		sprite_index = spr_Manticore_cloakFall;
		if (image_index >= 4.6) image_index = 0;
	}
	else
	{
		if (sprite_index == spr_Manticore_cloakFall)
		{
			//image_speed = 1;
			if (image_index < 5) image_index = 5;
		
			if (image_index >= 8.6)
			{
				sprite_index = spr_Manticore_Uncloaks;
				image_index = 0;
				image_speed = 0;
			}
		}
	
		if (sprite_index == spr_Manticore_Uncloaks)
		{
			if (alarm[2] < 0) image_speed = 1;
		
			if (image_index >= 9)&&(!instance_exists(obj_capeFloor))
			{
				var cape_on_floor = instance_create_layer(x,y,"Back_FX_Layer",obj_capeFloor);
				cape_on_floor.image_speed = 0;
				cape_on_floor.image_index = 8;
			}
			
			if (global.RELIKS[29,2])
				if (!instance_exists(obj_Speedflux))
					instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_Speedflux);
					
			if (global.RELIKS[30,2])
				if (!instance_exists(obj_RegenBubble))
					instance_create_layer(obj_Manticore.x,obj_Manticore.y,"FX_Layer",obj_RegenBubble);
					
			if (global.RELIKS[44,2])
			{
				artist.venomFX = true;
				var manager = global.currentManager;
				manager.ManticoreVenomed = true;
				alarm[5] = artist.max_Regen;
			}
			
			if (image_index >= 11.6)
			{
				sprite_index = spr_Manticore;
				image_index = 0;
				state = move_Manticore;
			
				if (global.currentManager != noone)
				{
					var cMngr = global.currentManager;
					audio_sound_gain(cMngr.levelMusic,global.musicVolume,0);
					audio_play_sound(cMngr.levelMusic,2,true);
					cMngr.alarm[1] = 60;
				}
			}
		}
	}
}
