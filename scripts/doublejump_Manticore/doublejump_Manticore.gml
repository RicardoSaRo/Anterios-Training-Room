function doublejump_Manticore() {
	state = doublejump_Manticore;

	//--> Interrupt sounds
	snd_Stop_Manticore();

	//--> Movement Capture
	dj_key_left = max(keyboard_check(vk_left),gamepad_button_check(global.controller_type,gp_padl),gamepad_axis_value(global.controller_type,gp_axislh)*-1,0);
	dj_key_right = max(keyboard_check(vk_right),gamepad_button_check(global.controller_type,gp_padr),gamepad_axis_value(global.controller_type,gp_axislh),0);
	dj_key_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
	dj_key_shift = max(keyboard_check(vk_shift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	dj_key_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
	dj_key_s = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);
	dj_key_q = max(keyboard_check(ord("Q")),gamepad_button_check(global.controller_type,gp_face4),0);
	dj_key_minijump = max(keyboard_check_released(vk_space),gamepad_button_check_released(global.controller_type,gp_face1),0);
	dj_key_atkjmpC = max(keyboard_check_pressed(vk_space),gamepad_button_check_released(global.controller_type,gp_face1),0);
	dj_key_down = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);

	if (global.RELIKS[49,2]) dj_key_q = false;

	//--> Movement Calcs
	if (!dj_key_right) and (!dj_key_left) direc = 0;
	else
	{
		if (dj_key_right) and (!dj_key_left) direc = 1;
		if (dj_key_left) and (!dj_key_right) direc = -1;
	}

	//--> Changes Horizontal Speed for some weapons
	if ((dj_key_d) and (equiped_sword > 2)) or ((dj_key_s) and (equipped_axe > 2))
		hsp = direc * (walksp*1.25);
	else hsp = direc * walksp; //--> Normal Horizontal Speed

	dj_onFloor = place_meeting(x,y+1,obj_Wall);

	dj_onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	if (!air_attack) 
	{
		if (vsp > 0) vsp = 0; //--> Stops falling momentum
		vsp -= (windjump_used) ? 0.3 : 0.2; //--> Gains upward momentum per frame
	}
	else
	{
		if (atk_type == 1) && (equiped_sword > 2) && (dj_key_minijump) && (vsp < -5) vsp = -3;
		if (atk_type == 3) && (dj_key_minijump) && (vsp < -5) vsp = -3;
		if (dj_key_minijump) && (vsp < -7) vsp = -7; //--> Short Hop
		else vsp = vsp + grv; //--> Normal vsp if its an attack instead of a dj
	}

	//--> Platform Collision Check
	if (!key_down)
	{
		if place_meeting(x,y+vsp,obj_Platform)
		{
			if (bbox_bottom <= instance_nearest(x,y,obj_Platform).bbox_top)
			{
				y = instance_nearest(x,y,obj_Platform).bbox_top-56;
				vsp = 0;
			}
		}
	}
	
	//--> onPlatform Variable check
	if (bbox_bottom <= instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) dj_onPlatform = true;
	else dj_onPlatform = false;

	//--> Horizontal Collision Check
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		{
			if (sprite_index == spr_Manticore_doublejmp) //--> Prevents DJ stuck
				hsp = 0;
			else hsp = -onepixel * 2; //--> Pushes further to prevent get stuck on Air Attacks
		}
	}
	x = x + hsp; //--> Horizontal Movement

	//--> Vertical Collision Check
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}
	y = y + vsp; //--> Vertical Movement

	image_speed = 1;

	if (!air_attack)
	{
		if (dj_key_q) spCharge_air();
		
		if (dj_onWall)
		{
			//walljump_used = true;
			if (!global.RELIKS[15,2]) alarm[3] = 60;
			state = wallcling_Manticore;
		}
		else
		{
			if (dj_key_q) spCharge_air();
		
			sprite_index = (windjump_used) ? spr_Manticore_windjmp : spr_Manticore_doublejmp; //--> Asigns Doublejump Sprite
			doublejump_used = true;
			if (!audio_is_playing(snd_DoubleJump))
			{
				audio_play_sound(snd_DoubleJump,10,0);
			}
		
			if (image_index >= 5) //--> Finishes DJ animation and enters move state
			{
				atk_type = 0;
				attack_number = 1;
				state = move_Manticore;
			}
		
			exit;
		}
	}
	else //--> IF its an Air Attack
	{
		if (atk_type == 1)
		{
			switch (equiped_sword)
			{
				//--> Air Argenta
				case 1: image_speed = (global.SWORDS[1,3]*0.1) + 1;
						if (sprite_index != spr_AirArgenta) && (sprite_index != spr_AirArgentaMix)
							sprite_index = choose(spr_AirArgenta,spr_AirArgentaMix);
						else sprite_index = (sprite_index == spr_AirArgenta) ? spr_AirArgenta : spr_AirArgentaMix;
						//--> Cancels air atk with dash
						if (((dj_key_shift) and (direc != 0))||
						   ((dj_key_shift) and (dj_key_up))||
						   ((dj_key_shift) and (dj_key_down)) && (vsp > 5)) and (alarm[4] <= 0)
						{
							if (!airDash_used)
							{
								alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
								air_attack = false;
								airDash_used = true;
								if (dj_key_up)
								{
									if (!global.RELIKS[15,2]) sprite_index = spr_Manticore_UpD_prep;
									else
									if (special_Mirage >= 10)
									{
										alarm[0] = 15;
										special_Mirage -= 10;
										special_Mirage = clamp(special_Mirage,0,100);
										ExpUpDash = true;
									}
								}
								else
								{
									if (dj_key_down)
									{
										sprite_index = spr_Manticore_spinning;
										if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
									}
								}
								state = dash_Manticore;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
							else
							{
								if (global.RELIKS[14,2]) && (alarm[4] <= 0)
								{
									if (!lghtDash_used) && (special_Mjolnir >= 10)
									{
										alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
										special_Mjolnir -= 10;
										special_Mjolnir = clamp(special_Mjolnir,0,100);
										lghtDash_used = true;
										air_attack = false;
										state = dash_Manticore;
										if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
										exit;
									}
								}
							}
						}
						//--> Cancel Attack with DJ
						if (!doublejump_used)
						{
							if (dj_key_atkjmpC)
							{
								instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> Doublejump halo effect
								sprite_index = spr_Manticore_doublejmp;
								image_index = 0;
								state = doublejump_Manticore;
								air_attack = false;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
						}
						else
						{
							if (global.RELIKS[16,2]) && (dj_key_atkjmpC)
							{
								if (!windjump_used) && (special_Whisperer >= 10)
								{
									repeat(6)
									{
										var wndjmpFX = instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
										wndjmpFX.sprite_index = spr_windjmp_fx;
										wndjmpFX.image_blend = c_white;
									}
									special_Whisperer -= 10;
									special_Whisperer = clamp(special_Whisperer,0,100);
									windjump_used = true;
									sprite_index = spr_Manticore_doublejmp;
									image_index = 0;
									state = doublejump_Manticore;
									air_attack = false;
									if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
									exit;
								}
							}
						}
						if (!audio_is_playing(snd_AirArgenta)) //--> Sound FX
						{
							audio_sound_pitch(snd_AirArgenta,random_range(1.8,2.2));
							audio_play_sound(snd_AirArgenta,15,0);
						}
						if (direc != 0) image_xscale = direc;
						hitbox_duration(0,1,obj_Manticore_hitbox,spr_AirArgenta_hitbox1);
						hitbox_duration(2,3,obj_Manticore_hitbox,spr_AirArgenta_hitbox2);
						hitbox_duration(4,5,obj_Manticore_hitbox,spr_AirArgenta_hitbox3);
						hitbox_duration(6,7,obj_Manticore_hitbox,spr_AirArgenta_hitbox4);
						if (dj_onFloor) or (dj_onPlatform)
						{
							air_attack = false;
							lghtDash_used = false;
							state = move_Manticore;
							if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
						}
						break;
				//--> Air Dyrnwyn
				case 2: image_speed = 1;
						if (sprite_index != spr_AirDyrnwyn) && (sprite_index != spr_AirDyrnwynMix)
							sprite_index = choose(spr_AirDyrnwyn,spr_AirDyrnwynMix);
						else sprite_index = (sprite_index == spr_AirDyrnwyn) ? spr_AirDyrnwyn : spr_AirDyrnwynMix;
						//--> Cancels air atk with dash
						if (((dj_key_shift) and (direc != 0))||
						   ((dj_key_shift) and (dj_key_up))||
						   ((dj_key_shift) and (dj_key_down)) && (vsp > 5)) and (alarm[4] <= 0)
						{
							if (!airDash_used)
							{
								alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
								air_attack = false;
								airDash_used = true;
								if (dj_key_up)
								{
									if (!global.RELIKS[15,2]) sprite_index = spr_Manticore_UpD_prep;
									else
									if (special_Mirage >= 10)
									{
										alarm[0] = 15;
										special_Mirage -= 10;
										special_Mirage = clamp(special_Mirage,0,100);
										ExpUpDash = true;
									}
								}
								else
								{
									if (dj_key_down)
									{
										sprite_index = spr_Manticore_spinning;
										if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
									}
								}
								state = dash_Manticore;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
							else
							{
								if (global.RELIKS[14,2]) && (alarm[4] <= 0)
								{
									if (!lghtDash_used) && (special_Mjolnir >= 10)
									{
										alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
										special_Mjolnir -= 10;
										special_Mjolnir = clamp(special_Mjolnir,0,100);
										lghtDash_used = true;
										air_attack = false;
										state = dash_Manticore;
										if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
										exit;
									}
								}
							}
						}
						//--> Cancel Attack with DJ
						if (!doublejump_used)
						{
							if (dj_key_atkjmpC)
							{
								instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> Doublejump halo effect
								sprite_index = spr_Manticore_doublejmp;
								image_index = 0;
								state = doublejump_Manticore;
								air_attack = false;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
						}
						else
						{
							if (global.RELIKS[16,2]) && (dj_key_atkjmpC)
							{
								if (!windjump_used) && (special_Whisperer >= 10)
								{
									repeat(6)
									{
										var wndjmpFX = instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
										wndjmpFX.sprite_index = spr_windjmp_fx;
										wndjmpFX.image_blend = c_white;
									}
									special_Whisperer -= 10;
									special_Whisperer = clamp(special_Whisperer,0,100);
									windjump_used = true;
									sprite_index = spr_Manticore_doublejmp;
									image_index = 0;
									state = doublejump_Manticore;
									air_attack = false;
									if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
									exit;
								}
							}
						}
						var chispa = instance_create_layer(x,y,"Back_FX_Layer",obj_chispa);
						with (chispa)
						{
							x += random_range(-90,90);
							y += random_range(-90,90);
						}
						if (!audio_is_playing(snd_AirArgenta)) //--> Sound FX
						{
							audio_sound_pitch(snd_AirArgenta,random_range(1.3,1.7));
							audio_play_sound(snd_AirArgenta,15,0);
						}
						if (!audio_is_playing(snd_DyrnwynSwing3)) //--> Fire Sound FX
						{
							audio_sound_pitch(snd_DyrnwynSwing3,random_range(1.8,2.2));
							audio_play_sound(snd_DyrnwynSwing3,15,0);
						}
						hitbox_duration(0,1,obj_Manticore_hitbox,spr_AirDyrnwyn_hitbox1);
						hitbox_duration(2,3,obj_Manticore_hitbox,spr_AirDyrnwyn_hitbox2);
						hitbox_duration(4,5,obj_Manticore_hitbox,spr_AirDyrnwyn_hitbox3);
						hitbox_duration(6,7,obj_Manticore_hitbox,spr_AirDyrnwyn_hitbox4);
						if (direc != 0) image_xscale = direc;
						if (dj_onFloor or dj_onPlatform)
						{
							air_attack = false;
							lghtDash_used = false;
							airDash_used = false;
							state = move_Manticore;
							if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
						}
						break;
				//--> Air Gir-Tab
				case 3:	if (attack_number == 1)
						{
							sprite_index = (dj_key_up) ? ((global.SWORDS[3,3]==3) ? spr_AirGirTridentUP : spr_AirGirTabUP) :
							((global.SWORDS[3,3]==3) ? choose(spr_AirGirTrident,spr_AirGirTridentMix) : choose(spr_AirGirTab,spr_AirGirTabMix));
							attack_number = 2;
						}
						if (((dj_key_shift) and (direc != 0))||
						   ((dj_key_shift) and (dj_key_up))||
						   ((dj_key_shift) and (dj_key_down)) && (vsp > 5)) and (alarm[4] <= 0)
						{
							if (!airDash_used)
							{
								alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
								air_attack = false;
								airDash_used = true;
								if (dj_key_up)
								{
									if (!global.RELIKS[15,2]) sprite_index = spr_Manticore_UpD_prep;
									else
									if (special_Mirage >= 10)
									{
										alarm[0] = 15;
										special_Mirage -= 10;
										special_Mirage = clamp(special_Mirage,0,100);
										ExpUpDash = true;
									}
								}
								else
								{
									if (dj_key_down)
									{
										sprite_index = spr_Manticore_spinning;
										if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
									}
								}
								state = dash_Manticore;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
							else
							{
								if (global.RELIKS[14,2]) && (alarm[4] <= 0)
								{
									if (!lghtDash_used) && (special_Mjolnir >= 10)
									{
										alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
										special_Mjolnir -= 10;
										special_Mjolnir = clamp(special_Mjolnir,0,100);
										lghtDash_used = true;
										air_attack = false;
										state = dash_Manticore;
										if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
										exit;
									}
								}
							}
						}
						//--> Cancel Attack with DJ
						if (!doublejump_used)
						{
							if (dj_key_atkjmpC)
							{
								instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> Doublejump halo effect
								sprite_index = spr_Manticore_doublejmp;
								image_index = 0;
								state = doublejump_Manticore;
								air_attack = false;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
						}
						else
						{
							if (global.RELIKS[16,2]) && (dj_key_atkjmpC)
							{
								if (!windjump_used) && (special_Whisperer >= 10)
								{
									repeat(6)
									{
										var wndjmpFX = instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
										wndjmpFX.sprite_index = spr_windjmp_fx;
										wndjmpFX.image_blend = c_white;
									}
									special_Whisperer -= 10;
									special_Whisperer = clamp(special_Whisperer,0,100);
									windjump_used = true;
									sprite_index = spr_Manticore_doublejmp;
									image_index = 0;
									state = doublejump_Manticore;
									air_attack = false;
									if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
									exit;
								}
							}
						}
						if (global.RELIKS[28,2]) //--> Countinuous attacks after dash
						{
							if (alarm[4] > 54) image_index = 3;
						}
						else
						{
							if (alarm[4] > 4) image_index = 3;
						}
						//--> Hitboxes
						var normORup1 = (dj_key_up) ? spr_GirTabUP_hitbox1 : spr_GirTab_hitbox1;
						var normORup2 = (dj_key_up) ? spr_GirTabUP_hitbox2 : spr_GirTab_hitbox2;
						hitbox_duration(4,5,obj_Manticore_hitbox,normORup1);
						hitbox_duration(6,7,obj_Manticore_hitbox,normORup2);
						if (image_index >= 8) 
						{
							if (dj_key_d)
							{
								if (sprite_index == spr_AirGirTab)||(sprite_index == spr_AirGirTabMix)
									sprite_index = choose(spr_AirGirTab,spr_AirGirTabMix);
								if (sprite_index == spr_AirGirTrident)||(sprite_index == spr_AirGirTridentMix)
									sprite_index = choose(spr_AirGirTrident,spr_AirGirTridentMix);
								image_index = 4;
								attack_number = 1;
							}
							else
							{
								if (image_index >= 10) 
								{
									//--> Gives jump image preventing wall stuck
									if (!dj_onFloor && !dj_onPlatform) 
									{
										air_attack = false;
										sprite_index = spr_Manticore_jump;
										image_speed = 0;
										if (sign(vsp) > 0) image_index = 1;	else image_index = 0;
									}
									state = move_Manticore;
								}
							}
						}
						if (dj_onFloor or dj_onPlatform) 
						{
							air_attack = false;
							airDash_used = false;
							lghtDash_used = false;
							attack_number = 1;
							if (dj_key_d) state = sword_atks_Manticore;
							else state = move_Manticore;
							if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
						}
						break;
				//--> Air Frostbiter		
				case 4:	sprite_index = (dj_key_up) ? spr_AirFrostbiterUP : spr_AirFrostbiter;
						if (((dj_key_shift) and (direc != 0))||
						   ((dj_key_shift) and (dj_key_up))||
						   ((dj_key_shift) and (dj_key_down)) && (vsp > 5)) and (alarm[4] <= 0)
						{
							if (!airDash_used)
							{
								alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
								air_attack = false;
								airDash_used = true;
								if (dj_key_up)
								{
									if (!global.RELIKS[15,2]) sprite_index = spr_Manticore_UpD_prep;
									else
									if (special_Mirage >= 10)
									{
										alarm[0] = 15;
										special_Mirage -= 10;
										special_Mirage = clamp(special_Mirage,0,100);
										ExpUpDash = true;
									}
								}
								else
								{
									if (dj_key_down)
									{
										sprite_index = spr_Manticore_spinning;
										if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
									}
								}
								state = dash_Manticore;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
							else
							{
								if (global.RELIKS[14,2]) && (alarm[4] <= 0)
								{
									if (!lghtDash_used) && (special_Mjolnir >= 10)
									{
										alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
										special_Mjolnir -= 10;
										special_Mjolnir = clamp(special_Mjolnir,0,100);
										lghtDash_used = true;
										air_attack = false;
										state = dash_Manticore;
										if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
										exit;
									}
								}
							}
						}
						//--> Cancel Attack with DJ
						if (!doublejump_used)
						{
							if (dj_key_atkjmpC)
							{
								instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> Doublejump halo effect
								sprite_index = spr_Manticore_doublejmp;
								image_index = 0;
								state = doublejump_Manticore;
								air_attack = false;
								if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
								exit;
							}
						}
						else
						{
							if (global.RELIKS[16,2]) && (dj_key_atkjmpC)
							{
								if (!windjump_used) && (special_Whisperer >= 10)
								{
									repeat(6)
									{
										var wndjmpFX = instance_create_layer(x,y+50,"Back_FX_Layer",obj_doublejmp_fx); //--> windjump halo effect
										wndjmpFX.sprite_index = spr_windjmp_fx;
										wndjmpFX.image_blend = c_white;
									}
									special_Whisperer -= 10;
									special_Whisperer = clamp(special_Whisperer,0,100);
									windjump_used = true;
									sprite_index = spr_Manticore_doublejmp;
									image_index = 0;
									state = doublejump_Manticore;
									air_attack = false;
									if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
									exit;
								}
							}
						}
						if (global.RELIKS[28,2]) //--> Countinuous attacks after dash
						{
							if (alarm[4] > 54) image_index = 3;
						}
						else
						{
							if (alarm[4] > 4) image_index = 3;
						}
						//if (direc != 0) image_xscale = direc;
						var normORup1 = (dj_key_up) ? spr_GirTabUP_hitbox1 : spr_GirTab_hitbox1;
						var normORup2 = (dj_key_up) ? spr_GirTabUP_hitbox2 : spr_GirTab_hitbox2;
						hitbox_duration(4,5,obj_Manticore_hitbox,normORup1);
						hitbox_duration(6,7,obj_Manticore_hitbox,normORup2);
						if (image_index >= 8) 
						{
							if (dj_key_d) image_index = 4;
							else
							{
								if (image_index >= 10) 
								{
									//--> Gives jump image preventing wall stuck
									if (!dj_onFloor && !dj_onPlatform)
									{
									air_attack = false;
									sprite_index = spr_Manticore_jump;
									image_speed = 0;
									if (sign(vsp) > 0) image_index = 1;	else image_index = 0;
									}
									state = move_Manticore;
								}
							}
						}
						if (dj_onFloor or dj_onPlatform)
						{
							air_attack = false;
							airDash_used = false;
							if (dj_key_d) state = sword_atks_Manticore;
							else state = move_Manticore;
							if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
						}
						break;
			}
		}
		if (atk_type == 2)
		{
			switch (equipped_axe)
			{
				//--> AIR KORUNAX
				case 1: image_speed = (global.RELIKS[48,2]) ? 1.5 : 1;
						sprite_index = spr_AirKorunax;
						hitbox_duration(0,3,obj_Manticore_hitbox,spr_Korunax_hitbox1);
						hitbox_duration(3,4,obj_Manticore_hitbox,spr_Korunax_hitbox2);
						hitbox_duration(4,6,obj_Manticore_hitbox,spr_Korunax_hitbox3);
						if (!audio_is_playing(snd_KorunaxSwingAir)) && (attack_number == 1) //--> Sound FX
						{
							//audio_sound_pitch(snd_KorunaxSwing1,1.2);
							SwingAir = audio_play_sound(snd_KorunaxSwingAir,15,0);
						}
						if (dj_onFloor or dj_onPlatform)
						{
							if (dj_onPlatform) onPlatform = true;
							air_attack = false;
							state = axe_atks_Manticore;
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
							if (audio_is_playing(snd_KorunaxSwingAir))
							{
								var trackPos = audio_sound_get_track_position(SwingAir);
								audio_stop_sound(SwingAir);
								var Swing1 = audio_play_sound(snd_KorunaxSwing1,15,0);
								audio_sound_set_track_position(Swing1,trackPos);
							}
						}
						if (image_index > 7)
						{
							air_attack = false;
							state = move_Manticore;
						}
						break;
				//--> AIR MIRAGE
				case 2: image_speed = (global.RELIKS[48,2]) ? 1.5 : 1;
						sprite_index = spr_AirMirage;
						hitbox_duration(0,3,obj_Manticore_hitbox,spr_Mirage_hitbox1);
						hitbox_duration(3,4,obj_Manticore_hitbox,spr_Mirage_hitbox2);
						hitbox_duration(4,6,obj_Manticore_hitbox,spr_Mirage_hitbox3);
						if (!audio_is_playing(snd_MirageSwingAir)) && (attack_number == 1) //--> Sound FX
						{
							SwingAir = audio_play_sound(snd_MirageSwingAir,15,0);
						}
						if (image_index > 3)and(image_index < 4) //--> Air Flame
						{
							var wowX1 = 72 * image_xscale;
							if (!place_meeting(x+wowX1,y-22,obj_willowisp))
							{
								var willowisp1 = instance_create_depth(x+wowX1,y-22,0,obj_willowisp);
								willowisp1.image_xscale = image_xscale;
							}
						}
						if (dj_onFloor or dj_onPlatform)
						{
							air_attack = false;
							state = axe_atks_Manticore;
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
							if (audio_is_playing(snd_MirageSwingAir))
							{
								var trackPos = audio_sound_get_track_position(SwingAir);
								audio_stop_sound(SwingAir);
								var Swing1 = audio_play_sound(snd_MirageSwingAir,15,0);
								audio_sound_set_track_position(Swing1,trackPos);
							}
						}
						if (image_index > 7)
						{
							air_attack = false;
							state = move_Manticore;
						}
						break;
				//--> AIR MJOLNIR
				case 3: image_speed = 1;
						sprite_index = spr_AirMjolnir;
						hitbox_duration(0,3,obj_Manticore_hitbox,spr_Mjolnir_hitbox1);
						hitbox_duration(3,4,obj_Manticore_hitbox,spr_Mjolnir_hitbox2);
						hitbox_duration(4,6,obj_Manticore_hitbox,spr_Mjolnir_hitbox3);
						if (!audio_is_playing(snd_MjolnirSwing1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
						{
							audio_play_sound(snd_MjolnirSwing1,15,0);
						}
						if (!audio_is_playing(snd_MaceSwing1)) && (attack_number == 1) //--> Sound FX
						{
							//audio_sound_pitch(snd_KorunaxSwing1,1.2);
							SwingAir = audio_play_sound(snd_MaceSwing1,15,0);
						}
						if (image_index mod 2 = 1) //--> Electric effect
						{
							image_blend = c_white;
							image_alpha = 0.05;
						}
						if (dj_onFloor or dj_onPlatform)
						{
							air_attack = false;
							state = axe_atks_Manticore;
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
							if (audio_is_playing(snd_MaceSwing1))
							{
								var trackPos = audio_sound_get_track_position(SwingAir);
								audio_stop_sound(SwingAir);
								var Swing1 = audio_play_sound(snd_MaceSwing1,15,0);
								audio_sound_set_track_position(Swing1,trackPos);
							}
						}
						if (image_index > 5)
						{
							air_attack = false;
							state = move_Manticore;
						}
						break;
				//--> AIR WHISPERER
				case 4: image_speed = 1;
						sprite_index = spr_AirWhisperer;
						hitbox_duration(0,3,obj_Manticore_hitbox,spr_Whisperer_hitbox1);
						hitbox_duration(3,4,obj_Manticore_hitbox,spr_Whisperer_hitbox2);
						hitbox_duration(4,6,obj_Manticore_hitbox,spr_Whisperer_hitbox3);
						if (!audio_is_playing(snd_WhispererSwing1)) && (attack_number == 1) && (image_index < 1)//--> Sound FX
						{
							audio_play_sound(snd_WhispererSwing1,15,0);
						}
						if (!audio_is_playing(snd_MaceSwing1)) && (attack_number == 1) //--> Sound FX
						{
							//audio_sound_pitch(snd_KorunaxSwing1,1.2);
							SwingAir = audio_play_sound(snd_MaceSwing1,15,0);
						}
						if (image_index > 5)and(image_index < 7) //--> First Windslash
						{
							var wowX1 = 82 * image_xscale;
							if (!place_meeting(x+wowX1,y,obj_windslash))
								instance_create_depth(x+wowX1,y,0,obj_windslash);
						}
						if (dj_onFloor or dj_onPlatform)
						{
							air_attack = false;
							state = axe_atks_Manticore;
							audio_sound_gain(snd_FloorFall,0.4,0);
							audio_play_sound(snd_FloorFall,20,0);
							if (audio_is_playing(snd_MaceSwing1))
							{
								var trackPos = audio_sound_get_track_position(SwingAir);
								audio_stop_sound(SwingAir);
								var Swing1 = audio_play_sound(snd_MaceSwing1,15,0);
								audio_sound_set_track_position(Swing1,trackPos);
							}
						}
						if (image_index > 7)
						{
							air_attack = false;
							state = move_Manticore;
						}
						break;
			}
		}
		if (atk_type == 3)
		{
			switch (equipped_tw) //--> Asign Equipped Throwing Weapon Object
			{
				case 1: var current_tw = spr_Chakram;
						var weapon_number = (global.TWs[1,3]==3) ? 3 : 2;
						var throwing_animation = spr_Manticore_airthrow;
						var twX = x + (82 * image_xscale);
						var twY = y-10;
						var prio = (global.TWs[1,3]<=1) ? 4 : ((global.TWs[1,3]==2) ? 5 : 6);
						break;
				case 2: var current_tw = spr_Kunai;
						var weapon_number = 17;
						var throwing_animation = spr_Manticore_airthrow;
						var twX = x + (82 * image_xscale);
						var twY = y-17;
						var prio = 2 + global.TWs[2,3];
						break;
				case 3: var current_tw = spr_Granade;
						var weapon_number = (global.TWs[3,3]==3) ? 2 : 1;
						var throwing_animation = spr_Manticore_airthrow2;
						var twX = x + (72 * image_xscale);
						var twY = y-57;
						var prio = 6;
						break;
				case 4: var current_tw = choose(spr_Bomb1,spr_Bomb2);
						var weapon_number = (global.TWs[4,3]==3) ? 2 : ((global.TWs[4,3]>=1) ? 1 : 0);
						var throwing_animation = spr_Manticore_airthrow2;
						var twX = x + (72 * image_xscale);
						var twY = y-57;
						var prio = 6 + global.TWs[4,3];
						break;
				case 5: var current_tw = spr_Guillotine;
						var weapon_number = (global.TWs[5,3]>=1) ? global.TWs[5,3] : 1;
						var throwing_animation = spr_Manticore_airthrow2;
						var twX = x + (62 * image_xscale);
						var twY = y-57;
						var prio = 4 + global.TWs[5,3];
						break;
				case 6: var current_tw = spr_Shuriken;
						var weapon_number = 15;
						var throwing_animation = spr_Manticore_airthrow;
						var twX = x + (82 * image_xscale);
						var twY = y-10;
						var prio = 2 + global.TWs[6,3];
						break;
				case 7: var current_tw = spr_Triblader;
						var weapon_number = (global.TWs[7,3]==0) ? 2 : 2 + (global.TWs[7,3] - 1);
						var throwing_animation = spr_Manticore_airthrow;
						var twX = x + (82 * image_xscale);
						var twY = y-10;
						var prio = 7;
						break;
				case 8: var current_tw = spr_Starblades;
						var weapon_number = (global.TWs[8,3]<=2) ? 8 : 11;
						var throwing_animation = spr_Manticore_airthrow;
						var twX = x + (82 * image_xscale);
						var twY = y-10;
						var prio = (global.TWs[8,3]==0) ? 1 : global.TWs[8,3];
						break;
				case 9: var current_tw = spr_Assegai;
						var weapon_number = (global.TWs[9,3]<=1) ? 0 : 1;
						var throwing_animation = spr_Manticore_airthrow2;
						var twX = x + (82 * image_xscale);
						var twY = y-57;
						var prio = 6 + global.TWs[9,3];
						break;
				case 10: case 11: case 12: case 13:
						 var current_tw = spr_Elemental_Orb_OFF;
						 var weapon_number = 0;
						 var prio = 10;
						 switch(equipped_tw)
						 {
							case 10: weapon_number = (global.TWs[10,3]==0) ? 0 : 1;
									 prio = 3 + (global.TWs[10,3] * 2); break;
						 	case 11: weapon_number = (global.TWs[11,3]==0) ? 0 : 1;
									 prio = 3 + (global.TWs[11,3] * 2); break;
							case 12: weapon_number = (global.TWs[12,3]==0) ? 0 : 1;
									 prio = 3 + (global.TWs[12,3] * 2); break;
							case 13: weapon_number = (global.TWs[13,3]==0) ? 0 : 1;
								     prio = 3 + (global.TWs[13,3] * 2); break;
						 }
						 var throwing_animation = spr_Manticore_airthrow2;
						 var twX = x + (72 * image_xscale);
						 var twY = y-57;
						 break;
				case 14: var current_tw = spr_RemoteExp_OFF;
						 var weapon_number = 0;
						 var throwing_animation = spr_Manticore_airthrow2;
						 var twX = x + (72 * image_xscale);
						 var twY = y-57;
						 var prio = 4;
						 break;
				case 15: var current_tw = spr_Coin_Launcher;
						 var weapon_number = (global.TWs[15,3]==3) ? 1 : 0;
						 var throwing_animation = spr_Manticore_airthrow;
						 var twX = x + (82 * image_xscale);
						 var twY = y-10;
						 var prio = 8;
						 break;
				case 16: var current_tw = spr_Void_Ripple;
						 var weapon_number = 2;
						 var throwing_animation = spr_Manticore_VR_air;
						 var twX = x + (30 * image_xscale);
						 var twY = y-80;
						 var prio = 10;
						 break;
			}

			image_speed = 1;
			sprite_index = throwing_animation;
		
			if (instance_number(obj_Throwing_Wpn) <= weapon_number) //--> Prevents TW spam
			{
				if (((dj_key_shift) and (direc != 0))||
				   ((dj_key_shift) and (dj_key_up))||
				   ((dj_key_shift) and (dj_key_down)) && (vsp > 5)) and (alarm[4] <= 0) && (!airDash_used)
				{
					alarm[0] = (global.RELIKS[28,2]) ? 20 : 15;
					air_attack = false;
					airDash_used = true;
					if (dj_key_up)
					{
						if (!global.RELIKS[15,2]) sprite_index = spr_Manticore_UpD_prep;
						else
						if (special_Mirage >= 10)
						{
							alarm[0] = 15;
							special_Mirage -= 10;
							special_Mirage = clamp(special_Mirage,0,100);
							ExpUpDash = true;
						}
					}
					else
					{
						if (dj_key_down)
						{
							sprite_index = spr_Manticore_spinning;
							if (place_meeting(x,y+25,obj_Wall)) y -= 25; //--> Prevent DwnDash near floor
						}
					}
					state = dash_Manticore;
					if instance_exists(obj_Manticore_hitbox) instance_destroy(obj_Manticore_hitbox);
					exit;
				}
				tw_limit = false;
				if (image_index > 1)and(image_index < 2) //--> Creates TW
				{
					if (current_tw == spr_Starblades)//--> If TW is Starblades, creates 3 of them
					{
						audio_play_sound(snd_Starblades,15,0);
						var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
						Throwing_Wpn.sprite_index = current_tw;
						Throwing_Wpn.image_xscale = image_xscale;
						Throwing_Wpn.priority = prio;
						Throwing_Wpn.starblade_angle = choose(-1,0,1);
						Throwing_Wpn.bomb_vsp = 10;
						Throwing_Wpn.bomb_hsp = 10;
						if (place_meeting(twX,twY,obj_Wall)) Throwing_Wpn.sprite_index = spr_poof;
					}
					else
					{
						if (!place_meeting(twX,twY,obj_Throwing_Wpn)) && (current_tw != spr_Void_Ripple)
						{
							var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
							Throwing_Wpn.sprite_index = current_tw;
							Throwing_Wpn.image_xscale = image_xscale;
							Throwing_Wpn.priority = prio;
							switch(equipped_tw)
							{
								case 2:  if (global.TWs[2,3]>=2)
										 {
											 var Throwing_Wpn2 = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
											 Throwing_Wpn2.sprite_index = current_tw;
											 Throwing_Wpn2.image_xscale = image_xscale;
											 Throwing_Wpn2.image_angle = (image_xscale) ? 45 : 315;
											 Throwing_Wpn2.starblade_angle = -1;
											 Throwing_Wpn2.priority = prio;
											 var Throwing_Wpn3 = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
											 Throwing_Wpn3.sprite_index = current_tw;
											 Throwing_Wpn3.image_xscale = image_xscale;
											 Throwing_Wpn3.image_angle = (image_xscale) ? -45 : -315;
											 Throwing_Wpn3.starblade_angle = 1;
											 Throwing_Wpn3.priority = prio;
										 }
										 audio_sound_pitch(snd_Cut,random_range(1,1.1));
										 audio_play_sound(snd_Cut,15,0);
										 break;
								case 7:  var tw_up = max(keyboard_check(vk_up),gamepad_button_check(global.controller_type,gp_padu),gamepad_axis_value(global.controller_type,gp_axislv)*-1,0);
										 var tw_down = max(keyboard_check(vk_down),gamepad_button_check(global.controller_type,gp_padd),gamepad_axis_value(global.controller_type,gp_axislv),0);
										 if (tw_up) Throwing_Wpn.starblade_angle = -1;
										 if (tw_down) Throwing_Wpn.starblade_angle = 1;
										 break;
								case 10: Throwing_Wpn.orb_type = "Thunder";
										 Throwing_Wpn.image_blend = c_white;
										 break;
								case 11: Throwing_Wpn.orb_type = "Wind";
										 Throwing_Wpn.image_blend = c_lime;
										 break;
								case 12: Throwing_Wpn.orb_type = "Fire";
										 Throwing_Wpn.image_blend = c_orange;
										 break;
								case 13: Throwing_Wpn.orb_type = "Ice";
										 Throwing_Wpn.image_blend = c_aqua;
										 break;
								case 15: global.MONEY -= (global.TWs[15,3]==3) ? 500 : ((global.TWs[15,3]==2) ? 400 : ((global.TWs[15,3]==1) ? 300 : 200));
										 audio_play_sound(snd_CoinLauncher,15,0);
										 break;
							}
						}
					}
				}
				//--> Void Ripples
				if ((image_index > 1)and(image_index < 2)) or
				   ((image_index > 3)and(image_index < 4)) or
				   ((image_index > 5)and(image_index < 6))
				{
					if (current_tw == spr_Void_Ripple)
					{
						var Throwing_Wpn = instance_create_layer(twX,twY,"Player_Layer",obj_Throwing_Wpn);
						Throwing_Wpn.sprite_index = current_tw;
						Throwing_Wpn.priority = prio;
						Throwing_Wpn.x += random_range(-300,300);
						Throwing_Wpn.y += random_range(-300,100);
					}
				}
			}
			else
			{
				tw_limit = true;
				air_attack = false;
				state = move_Manticore;
				atk_type = 0;
				attack_number = 1;
			}
			if (dj_onFloor or dj_onPlatform)
			{
				air_attack = false;
				state = throw_atks_Manticore;
				audio_sound_gain(snd_FloorFall,0.4,0);
				audio_play_sound(snd_FloorFall,20,0);
				exit;
			}
		
			if (sprite_index == spr_Manticore_VR) or (sprite_index == spr_Manticore_VR_air)
				var max_sn = 7;
			else
			{
				if (sprite_index == spr_Manticore_throw) or (sprite_index == spr_Manticore_airthrow) var max_sn = 3;
				else var max_sn = 5;
			}
		
			if (image_index > max_sn)
			{
				var keep_atking = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
				if (keep_atking) state = throw_atks_Manticore;
				else
				{
					air_attack = false;
					if (!dj_onWall)
					{
						state = move_Manticore;
						atk_type = 0;
						attack_number = 1;
						audio_sound_gain(snd_FloorFall,0.4,0);
						audio_play_sound(snd_FloorFall,20,0);
					}
					else
					{
						if (!global.RELIKS[15,2]) alarm[3] = 60;
						state = wallcling_Manticore;
					}
				}
			}
		}
	}


}
