function spCharge_Manticore() {
	if (global.RELIKS[49,2]) exit;
	
	//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) CHonPlatform = true;
	else CHonPlatform = false;
	
	CHonFloor = place_meeting(x,y+1,obj_Wall); //--> onFloor Variable Check
	
	if (!CHonFloor)&&(!CHonPlatform)
	{
		state = move_Manticore;
		exit;
	}

	var sp_key_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
	var sp_key_s = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);
	var sp_key_a = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
	var sp_key_space = max(keyboard_check(vk_space),gamepad_button_check(global.controller_type,gp_face1),0);

	if (sp_key_space)
	{
		if (sp_key_d) key_d = false; //--> Prevents weird jump buffers after attacks
		if (sp_key_s) key_s = false;	
		if (sp_key_a) key_a = false;
	
		airDash_used = false; //--> Refreshes Air Dash
		lghtDash_used = false;
		doublejump_used = false; //--> Refreshes Doublejump
		windjump_used = false;
	
		vsp = -20;
		state = move_Manticore;
		exit;
	}

	//--> First two switches checks the special bars to be filled and gives a temporary variable
	//--> the appropiate color to use on the numbers that shows the increment of the bar
	equiped_sword = global.equippedSWORD;
	equipped_axe = global.equippedAXE;
	equipped_tw = global.equippedTW;

	switch(equiped_sword)
	{
		case 1: current_sp_sword = special_Argenta;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_silver : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 2: current_sp_sword = special_Dyrnwyn;
				var fontcolor1 = (!global.RELIKS[24,2]) ? make_color_rgb(241,100,31) : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 3: current_sp_sword = special_GirTab;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_red : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 4: current_sp_sword = special_Frostbiter;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_aqua : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
	}

	switch(equipped_axe)
	{
		case 1: current_sp_axe = special_Korunax;
				var fontcolor2 = (!global.RELIKS[24,2]) ? make_color_rgb(211,151,65) : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 2: current_sp_axe = special_Mirage;
				var fontcolor2 = (!global.RELIKS[24,2]) ? c_fuchsia : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 3: current_sp_axe = special_Mjolnir;
				var fontcolor2 = (!global.RELIKS[24,2]) ? make_color_rgb(39,100,205) : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 4: current_sp_axe = special_Whisperer;
				var fontcolor2 = (!global.RELIKS[24,2]) ? c_lime : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
	}

	if (sp_key_d)//--> If sword key is pressed, and special is charged, exits to execute special.
	{
		if (current_sp_sword >= 50)
		{
			atk_type = 1;
			image_index = 0;
			state = special_Manticore;
			exit;
		}
	}

	if (sp_key_s) //--> If axe key is pressed, and special is charged, exits to execute special.
	{
		if (current_sp_axe >= 50)
		{
			atk_type = 2;
			image_index = 0;
			state = special_Manticore;
			exit;
		}
	}

	var SPcurrentAmmo = (TWammo[global.equippedTW,0]==noone) ? 1 : TWammo[global.equippedTW,0]; //--> If infinite, will be always 1
	if (global.equippedTW == 14) && (SPcurrentAmmo == 0) && (instance_exists(obj_Throwing_Wpn)) SPcurrentAmmo = 1;
	
	if (sp_key_a)//--> If TW key is pressed, and special is charged, exits to execute special.
	{
		if (!tw_limit) and (!onWall) and (SPcurrentAmmo > 0)
		{
			if (TWTank[1,0]>=5000) || (TWTank[2,0]>=5000) || (TWTank[3,0]>=5000) || (TWTank[4,0]>=5000)
			{
				atk_type = 3;
				image_index = 0;
				state = specialTW_Manticore;
				exit;
			}
		}
	}

	if (current_sp_sword == 100) && (current_sp_axe == 100) //--> if both bars are filled, exits the script
	{
		state = move_Manticore;
		exit;
	}

	state = spCharge_Manticore;

	//var spCharge_up = keyboard_check_released(vk_up);
	var spCharge_q = max(keyboard_check_released(ord("Q")),gamepad_button_check_released(global.controller_type,gp_face4),0);

	image_speed = 1;
	sprite_index = spr_Manticore_spCharge;

	if (spCharge_q) image_index = 10; //--> checks if Q is released to jump to the end of animation

	if (image_index < 10)
	{
		alarm[9] = spCharge_spd;
		alarm[8] = spCharge_spd;
	
		if ((image_index >= 4) && (image_index < 5))// && (global.HuntRoom != noone)
		{
			//--> If Navaratna Equipped
			if (global.RELIKS[24,2])
			{
				var spChmngr = global.currentManager;
				if (Manticore_hp != global.player_max_health)&&(!spChmngr.ManticoreVenomed) //--> Increase Regen
				{
					obj_Manticore.alarm[5] -= 10;
					obj_Manticore.alarm[5] = clamp(obj_Manticore.alarm[5],0,artist.max_Regen);
				}
			}
		
			if (current_sp_sword < 100)
			{
				with (instance_create_layer(x+random_range(-10,-50),y,"FX_Layer",obj_show_damage))
				{
					font_color = fontcolor1;
					damage_display = 1;
					plus_or_minus = "+";
					y_speed = 1;
				}
			
				if (global.RELIKS[24,2])
				{
					special_Argenta += 1;
					special_Dyrnwyn += 1;
					special_GirTab += 1;
					special_Frostbiter += 1;
				}
				else
				{
					switch(equiped_sword)
					{
						case 1: special_Argenta += 1; break;
						case 2: special_Dyrnwyn += 1; break;
						case 3: special_GirTab += 1; break;
						case 4: special_Frostbiter += 1; break;
					}
				}
			}
		
			if (current_sp_axe < 100)
			{
				with (instance_create_layer(x+random_range(10,50),y,"FX_Layer",obj_show_damage))
				{
					font_color = fontcolor2;
					damage_display = 1;
					plus_or_minus = "+";
					y_speed = 1;
				}
			
				if (global.RELIKS[24,2])
				{
					special_Korunax += 1;
					special_Mirage += 1;
					special_Mjolnir += 1;
					special_Whisperer += 1;
				}
				else
				{
					switch(equipped_axe)
					{
						case 1: special_Korunax += 1; break;
						case 2: special_Mirage += 1; break;
						case 3: special_Mjolnir += 1; break;
						case 4: special_Whisperer += 1; break;
					}
				}
			}
		
			image_index = 5;
		
			var again = choose(1,0);
			bshockwave_create_layer(x+random_range(-85,10),y+random_range(-70,40),"FX_Layer",view_camera[0],12,32,1,4,-25,1,fontcolor1,1);
			if (again)
				bshockwave_create_layer(x+random_range(-85,10),y+random_range(-70,40),"FX_Layer",view_camera[0],12,32,1,4,-25,1,fontcolor1,1);
			again = choose(1,0);
			bshockwave_create_layer(x+random_range(15,85),y+random_range(-70,40),"FX_Layer",view_camera[0],12,32,1,4,-25,1,fontcolor2,1);
			if (again)
				bshockwave_create_layer(x+random_range(15,85),y+random_range(-70,40),"FX_Layer",view_camera[0],12,32,1,4,-25,1,fontcolor2,1);
		}
	
		if (image_index > 9) image_index = 4; //--> animation loop
	}

	if (image_index > 4) && (image_index < 10)
	{
		var cc_spawnX = obj_Manticore.x + random_range(-70,70);
		var spawn_layer = choose("Back_FX_Layer","Info_Layer");
		var chispa = instance_create_layer(cc_spawnX,obj_Manticore.bbox_bottom,spawn_layer,obj_charge_chispa);
		chispa.angl = choose(45);
		if (global.RELIKS[24,2]) chispa.image_blend = choose(fontcolor1,fontcolor2);
		var FX = irandom(6);
		if (FX == 1)
		{
			audio_sound_pitch(snd_SPCharge,random_range(0.7,1.4));
			audio_play_sound(snd_SPCharge,10,0);
		}
	}

	if (image_index > 12) state = move_Manticore;
}
