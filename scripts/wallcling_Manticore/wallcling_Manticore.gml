function wallcling_Manticore() {
	state = wallcling_Manticore;

	//--> Gamepad Axis Release code
	var WCgpAxisL = false;
	var WCgpAxisR = false;

	if (gamepad_axis_value(global.controller_type,gp_axislh) < 0.4) && (gamepad_axis_value(global.controller_type,gp_axislh) > -0.4)
	{
		WCgpAxisL = true;
		WCgpAxisR = true;
	}
	else
	{
		if (gamepad_axis_value(global.controller_type,gp_axislh) > 0.4) WCgpAxisR = false;
		if (gamepad_axis_value(global.controller_type,gp_axislh) < -0.4) WCgpAxisL = false;
	}

	//--> Movement Capture
	cling_key_left_released = max(keyboard_check_released(vk_left),gamepad_button_check_released(global.controller_type,gp_padl),0);
	cling_key_right_released = max(keyboard_check_released(vk_right),gamepad_button_check_released(global.controller_type,gp_padr),0);
	cling_walljump = max(keyboard_check_pressed(vk_space),gamepad_button_check_pressed(global.controller_type,gp_face1),0);
	cling_key_lshift = max(keyboard_check(vk_lshift),gamepad_button_check(global.controller_type,gp_shoulderlb),0);
	cling_key_a = max(keyboard_check(ord("A")),gamepad_button_check(global.controller_type,gp_shoulderrb),0);
	cling_key_q = max(keyboard_check(ord("Q")),gamepad_button_check(global.controller_type,gp_face4),0);
	cling_key_d = max(keyboard_check(ord("D")),gamepad_button_check(global.controller_type,gp_face3),0);
	cling_key_s = max(keyboard_check(ord("S")),gamepad_button_check(global.controller_type,gp_face2),0);

	if (global.RELIKS[49,2]) cling_key_q = false;

	//--> If gamepad is connected, wallclings with axis
	if (gamepad_is_connected(global.controller_type))
	{
		cling_key_left_released = WCgpAxisL;
		cling_key_right_released = WCgpAxisR;
	}

	if (cling_key_q) spCharge_air();

	if (!walljump_used) //--> Asigns Wallcling sprite
	{
		sprite_index = spr_Manticore_wallcling;
		image_index = 0;
	}

	if !walljump_used //--> Reverse Sprite Near Wall obj when clings
	{
		var nearWall = instance_nearest(x,y,obj_Wall);
		if (nearWall == x) direc = 0;
		else 
		{
			if (nearWall.x > x)
			{
				image_xscale = -1;
				x = nearWall.bbox_left-30;
			}
			else
			{
				x = nearWall.bbox_right+30;
				image_xscale = 1;
			}
		}
	}

	if ((!cling_walljump)&&(!walljump_used)) && ((cling_key_a) || (cling_key_d) || (cling_key_s)) && //-->> Performs Wall Attack
	   (alarm[4] <= 0)
	{
		alarm[0] = 25;
		alarm[4] = 2;
		image_speed = 1;
		state = wallattack_Manticore;
		x += 10 * image_xscale;
		exit;
	}

	if (alarm[3] >= 60) //--> Grab Wall FX
	{
		audio_sound_pitch(snd_WallGrab,1);
		audio_play_sound(snd_WallGrab,25,0);
	}

	//--> Release Wallcling
	if (cling_key_left_released or cling_key_right_released) and (!walljump_used)
	{
		if (image_xscale == 1) x += 20;
		if (image_xscale == -1) x += -20; //--> Prevents getting stuck on wall after cling
		vsp = 0;
		wallcling_cooldown = true;
		alarm[2] = 20;
		state = move_Manticore;
	}

	//--> Walljump
	if cling_walljump or walljump_used
	{
		if !walljump_used 
		{
			alarm[1] = 10;
			if (sprite_index != spr_Manticore_jump)&&(sprite_index != spr_Manticore_spinning)
				sprite_index = choose(spr_Manticore_jump,spr_Manticore_spinning);
			else sprite_index = (sprite_index == spr_Manticore_jump) ? spr_Manticore_jump : spr_Manticore_spinning;
			image_speed = (sprite_index == spr_Manticore_spinning) ? 1 : 0;
			if (sprite_index == spr_Manticore_jump) image_index = 0;
			walljump_used = true; //--> Inicialize alarm for walljump once in the "loop"
		}
		if (alarm[1] > 3) and (alarm[1] mod 2 == 0) //--> Walljump afterimage
		{
			var afterimage = instance_create_layer(x,y,"Player_Layer",dash_afterimage);
			afterimage.sprite_index = sprite_index;
			afterimage.image_index = image_index;
			afterimage.image_xscale = image_xscale;
			afterimage.glowFX = true;
			if (instance_exists(obj_GraniteShield)) afterimage.image_blend = c_yellow;
		}
		//--> Prevents getting stuck on walls after walljump
		if !place_meeting(x+(16*image_xscale),y,obj_Wall) x += 16 * image_xscale;
		else hsp = 0;
		if !place_meeting(x,y-12,obj_Wall) y -= 12;
		else hsp = 0;
	
		if (alarm[1] >= 10)
		{
			var wjFX = instance_create_layer(x+(50*-image_xscale),y,"FX_Layer",obj_hit_FX);
			wjFX.hit_FX = spr_hit_FX;
			wjFX.hit_FX_angle = random(360);
		
			if (!audio_is_playing(snd_Dash))
			{
				audio_sound_gain(snd_Dash,0.2,0);
				audio_sound_pitch(snd_Dash,1.1)
				audio_play_sound(snd_Dash,20,0);
			}
			else
			{
				audio_sound_pitch(snd_Dash,1.1)
				audio_play_sound(snd_Dash,20,0);
			}
		}
	}
}
