/// @description Manticore Automatic Movement
// Player Manticore automatic behavior when entering a Stage

function auto_Manticore() {
	atk_type = 0;
	attack_number = 1;
	if (!auto_move) keyinput_Stop();

	//--> Movement Calcs
	if (!key_right) and (!key_left) direc = 0;
	else
	{
		if (key_right) and (!key_left) direc = 1;
		if (key_left) and (!key_right) direc = -1;
	}

	if (direc == 0) speed = 0; //--> Prevents Shaking

	if (artist.freezeFX) walksp = 7; //--> If freeze stat, slows the walk speed
	else walksp = 9;
	hsp = direc * walksp; //--> Horizontal Speed

	if grv != 1 grv = 1; //--> Returns Gravity to normal

	vsp = vsp + grv; //--> Vertical Speed
	vsp = clamp(vsp,-20,25); //--> Prevents ridiculous speeds when player falls
	
	onFloor = place_meeting(x,y+1,obj_Wall);

	onWall = place_meeting(x+sign(hsp),y,obj_Wall);

	//--> Platform Collision Check
	if (!platfDrop)
	{
		if place_meeting(x,y+vsp,obj_Platform)
		{
			if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
			{
				y = instance_nearest(x,y,obj_Platform).bbox_top-56;
				vsp = 0;
			}
		}
	}
	
		//--> onPlatform Variable check
	if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
	   (place_meeting(x,y+1,obj_Platform)) onPlatform = true;
	else onPlatform = false;

	//--> Switches platfDrop variable back to false, in case it was set true in "crouch defense" script
	platfDrop = false;

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	x = x + hsp; //--> Horizontal Movement

	//--> Vertical Collision (Wall)
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}

	y = y + vsp; //--> Vertical Movement

	//--> Sprites
	if (sprite_index != spr_Manticore_wallcling)
	{
		if (onFloor) sprite_index = spr_Manticore;
		else
		{
			sprite_index = spr_Manticore_jump;
			image_index = (vsp > 0) ? 0 : 1;
		}
	}

	//--> Destroys Hitbox is exits
	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);


}
