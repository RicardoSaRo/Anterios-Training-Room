//--> Function to perform Dash at 4 directions. Plus any effects and relik influences.

function dash_Manticore() {
	snd_Stop_Manticore();

	var dashSpd = (global.RELIKS[27,2]) ? 25 : 15;

	if (ExpUpDash) //--> Explosive Dash
	{
		if (!audio_is_playing(snd_ExplosionUpDash)) audio_play_sound(snd_ExplosionUpDash,12,0);
		with (obj_Manticore)
		{
			if !(alarm[11] > 3)
			{
				alarm[11] = 3;
				image_blend = c_yellow;
			}
		}
	}

	if (global.RELIKS[28,2]) //--> Invisibility if Feather Cloak is Equipped
	{
		with (obj_Manticore)
		{
			if !(alarm[11] > 3)
			{
				alarm[11] = 3;
				image_blend = c_yellow;
			}
		}
	
		var rndmFFX = irandom(10);
		if (rndmFFX)
		{
			var FFX = instance_create_layer(x,y-random(20),"FX_Layer",obj_GrassLeaf);
			FFX.sprite_index = spr_wFeatherFX;
			FFX.raise = choose(2,3,4,5);
			FFX.alarm[0] = irandom_range(20,30);
		}
	}

	var DshonFloor = place_meeting(x,y+1,obj_Wall); //--> Checks the player is Dashing on the floor

	if (global.RELIKS[40,2]) //--> Creates Shadow Clones if Shadow Pearl is equipped
	{
		var ini = (global.RELIKS[28,2]) ? 20 : 15;
		if (alarm[0] >= ini)
		{
			//--> onPlatform Variable check
			if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
			   (place_meeting(x,y+1,obj_Platform)) var DshonPlat = true;
			else var DshonPlat = false;
		
			var shadowImg = instance_create_layer(x,y,"FX_Layer",obj_ShadowImage);
			var currentState = choose(0,1);
			if (!currentState)
			{
				switch(equiped_sword)
				{
					case 1: shadowImg.sprite_index = (DshonFloor)||(DshonPlat) ? 
							choose(spr_Argenta,spr_ArgentaMix) : choose(spr_AirArgenta,spr_AirArgentaMix); break;
					case 2: shadowImg.sprite_index = (DshonFloor)||(DshonPlat) ? 
							choose(spr_Dyrnwyn,spr_DyrnwynMix) : choose(spr_AirDyrnwyn,spr_AirDyrnwynMix); break;
					case 3: shadowImg.sprite_index = choose(spr_GirTab,spr_GirTabMix); break;
					case 4: shadowImg.sprite_index = choose(spr_Frostbiter,spr_FrostbiterMix); break;
				}
			}
			else
			{
				switch(equipped_axe)
				{
					case 1: shadowImg.sprite_index = choose(spr_Korunax); break;
					case 2: shadowImg.sprite_index = choose(spr_Mirage); break;
					case 3: shadowImg.sprite_index = choose(spr_Mjolnir); break;
					case 4: shadowImg.sprite_index = choose(spr_Whisperer); break;
				}
			}
			if ((image_xscale > 0) and (direc == -1)) or ((image_xscale < 0) and (direc == 1))
				shadowImg.image_xscale = image_xscale;
			else shadowImg.image_xscale = -image_xscale;
		}
	}

	//--> Prevents Getting stuck on walls using Explosive Dash
	if (sprite_index != spr_Manticore_Updash) or (sprite_index != spr_Manticore_UpD_prep) or
	   (sprite_index != spr_Manticore_ExpUpdash) or (sprite_index != spr_Manticore_ExpUpD_prep) or
	   (y < instance_nearest(x,y,obj_Wall.bbox_bottom))
	{
		if place_meeting(x+(sign(hsp)*dashSpd),y,obj_Wall)
		{
			y = clamp(y,Boundary_minY,Boundary_maxY-1);
			x = clamp(x,Boundary_minX+1,Boundary_maxX-1);
			if (y != Boundary_maxY-1)
			{
				alarm[0] = 0;
				if (!global.RELIKS[15,2]) alarm[3] = 60;
				state = wallcling_Manticore;
			}
		
			atk_type = 0;
			attack_number = 1;
			exit;
		}
	}

	//--> Boundaries check
	if (x < Boundary_minX) or (x > Boundary_maxX)
	{
		atk_type = 0;
		attack_number = 1;
		exit;
	}

	state = dash_Manticore;

	//--> Jump Cancel Check
	var jumpcancel = max(keyboard_check(vk_space),gamepad_button_check(global.controller_type,gp_face1),0);

	//--> Check Vertical Collision
	var movUP = (ExpUpDash) ? 30 : ((key_down) ? 25 : dashSpd);

	var dashTouchRoofFloor = place_meeting(x,y+movUP,obj_Wall);

	//--> Dash into the floor effects
	if ((sprite_index != spr_Manticore_Updash) or (sprite_index != spr_Manticore_UpD_prep) or
	   (sprite_index != spr_Manticore_ExpUpdash) or (sprite_index != spr_Manticore_ExpUpD_prep)) &&
	   (!DshonFloor)
	{
		if dashTouchRoofFloor
		{
			var onepixel = movUP;
			while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
			movUP = 0;
		
			if (sprite_index == spr_Manticore_spinning)
			{
				alarm[0] = 0;
				alarm[4] = 10;
				state = move_Manticore;
				//vsp = 0;
				//sprite_index = spr_Manticore_landing;
				//image_index = 0;
	
				var windash = instance_create_layer(x,y+70,"FX_Layer",obj_centerXYfollow);
				windash.sprite_index = spr_windash;
				//windash.obj_2_follow = self;
				windash.anim_end = 4;
				//windash.brightness = true;
				windash.image_angle = 90;
				windash.image_alpha = 0.6;
				windash.image_xscale = 0.8;
				windash.image_yscale = 3;
				windash.image_speed = choose(1,1.5);
	
				//--> WIND LANDING SOUND HERE!!!!!!
				audio_play_sound(snd_Windlanding,25,0);
	
				exit;
			}
		}
	}

	image_speed = (sprite_index == spr_Manticore_spinning) ? 1.5 : 0.7;

	vsp = 0;

	if (key_up) //--> Dash Upwards Preparation
	{
		if (sprite_index != spr_Manticore_Updash)&&(sprite_index != spr_Manticore_ExpUpdash)
			sprite_index = (ExpUpDash) ? spr_Manticore_ExpUpD_prep : spr_Manticore_UpD_prep;
	}
	else
	{
		if (key_down) sprite_index = spr_Manticore_spinning;
		else
		{
			//--> Checks image direction and player's input to asign fdash or bdash
			if ((image_xscale > 0) and (direc == -1)) or ((image_xscale < 0) and (direc == 1))
			{
				sprite_index = (lghtDash_used) ? spr_Manticore_bLgtDash : spr_Manticore_bdash;
				if (!audio_is_playing(snd_LightningDash))&&(lghtDash_used) audio_play_sound(snd_LightningDash,14,0);
			}
			else
			{
				if ((sprite_index != spr_Manticore_UpD_prep)&&(sprite_index != spr_Manticore_ExpUpdash)&&
				   (sprite_index != spr_Manticore_Updash)&&(sprite_index != spr_Manticore_ExpUpD_prep)&&
				   (sprite_index != spr_Manticore_spinning))
					sprite_index = (lghtDash_used) ? spr_Manticore_fLgtDash : spr_Manticore_fdash;
					if (!audio_is_playing(snd_LightningDash))&&(lghtDash_used) audio_play_sound(snd_LightningDash,14,0);
			}
		}
	}

	//--> Dash Duration and Wind Effects
	var duration = (global.RELIKS[28,2]) ? 20 : 15;
	var moreWD = (ExpUpDash)&&(alarm[0] mod 10 == 0) ? true : false;
	if (alarm[0] >= duration) || (moreWD)
	{
		for(var i=1; i<=1; i++)
		{
			var windash = instance_create_layer(x+(50*image_xscale),y,"FX_Layer",obj_centerXYfollow);
			windash.sprite_index = spr_windash;
			windash.obj_2_follow = self;
			windash.anim_end = 4;
			windash.brightness = ((sprite_index == spr_Manticore_fLgtDash)||(sprite_index == spr_Manticore_bLgtDash)||(global.RELIKS[40,2]))&&(!global.RELIKS[27,2]) ? false : true;
			var wdcolor = (ExpUpDash) ? c_orange : ((global.RELIKS[40,2]) ? c_purple : c_aqua);
			if (sprite_index == spr_Manticore_fLgtDash)||(sprite_index == spr_Manticore_bLgtDash) wdcolor = c_white;
			if (global.RELIKS[27,2]) wdcolor = choose(c_yellow,c_red,c_aqua,c_lime,c_fuchsia,c_teal,c_orange);
			windash.image_blend = (instance_exists(obj_GraniteShield))||(global.RELIKS[28,2]) ? c_yellow : wdcolor;
			windash.image_angle = (key_up) ? 90 * image_xscale : ((key_down) ? 90 * -image_xscale : 0);
			windash.image_alpha = 0.6;
			windash.image_xscale = (sprite_index == spr_Manticore_bdash) ? -1.5 * image_xscale : 1.5 * image_xscale;
			var stretch = (i==2) ? 0.5 * image_xscale : 0;
			windash.image_yscale = (key_up) ? -1.3 + stretch: 1.3 + stretch;
			windash.image_speed = 2;
		}
	}

	//--> Up Dash
	switch(sprite_index)
	{
		case spr_Manticore_UpD_prep: case spr_Manticore_ExpUpD_prep: 
			if (alarm[0] < 15)
			 {
				 sprite_index = (sprite_index==spr_Manticore_ExpUpD_prep) ? spr_Manticore_ExpUpdash : spr_Manticore_Updash;
				 alarm[0] = 15;
			 }
			 break;
		case spr_Manticore_Updash: case spr_Manticore_ExpUpdash: y -= movUP; break;
		case spr_Manticore_bdash: case spr_Manticore_bLgtDash: x -= sign(hsp) * -dashSpd; break;
		case spr_Manticore_fdash: case spr_Manticore_fLgtDash: x += sign(hsp) * dashSpd; break;
		case spr_Manticore_spinning: y += movUP; vsp = movUP; break;
	}

	//---> LightningFX
	if (sprite_index == spr_Manticore_fLgtDash)||(sprite_index == spr_Manticore_bLgtDash)
	{
		if (irandom(10))
		{
			var spwn = instance_create_layer(x,y,"Back_FX_Layer",obj_centerXYfollow);
			spwn.sprite_index = spr_relampago2;
			spwn.obj_2_follow = self;
			spwn.anim_end = 2;
			spwn.brightness = 3;
			spwn.image_alpha = 0.3;
			spwn.image_angle = random_range(115,245);
			spwn.image_xscale = image_xscale * (image_index/3.5);
		}
	}

	//--> Explosion FX
	if (alarm[0] >= duration)&&(ExpUpDash)
	{
		var LE = instance_create_layer(x,y+50,"Back_FX_Layer",obj_LaunchExp);
		LE.image_xscale = 1.5;
		LE.image_yscale = 1.5;
	}

	//--> Afterimage animation
	if (alarm[0] > 1)  and (alarm[0] mod 2 == 1)
	{
			var xExp = (ExpUpDash) ? 50 : 0;
			var afterimage = instance_create_layer(x,y+xExp,"Player_Layer",dash_afterimage);
			afterimage.sprite_index = (sprite_index==spr_Manticore_ExpUpdash) ? spr_Explosion_hmmgF : sprite_index;
			afterimage.image_index = image_index;
			var ExpXYscl = random_range(0.4,1);
			afterimage.image_xscale = (ExpUpDash) ? -1 * ExpXYscl : image_xscale;
			afterimage.image_yscale = (ExpUpDash) ? -1 * ExpXYscl : image_yscale;
			afterimage.glowFX = true;
			afterimage.image_alpha = (global.RELIKS[40,2]) ? 1 : 0.6;
			var aiColor = (sprite_index==spr_Manticore_ExpUpdash) ? c_orange : ((global.RELIKS[40,2]) ? c_purple : c_aqua);
			if (sprite_index == spr_Manticore_fLgtDash)||(sprite_index == spr_Manticore_bLgtDash)
			{
				afterimage.glowFX = false;
				aiColor = c_white;
			}
			afterimage.image_blend = (global.RELIKS[27,2]) ? choose(c_yellow,c_red,c_aqua,c_lime,c_fuchsia,c_teal,c_orange) : aiColor;
			if (instance_exists(obj_GraniteShield)) afterimage.image_blend = c_yellow;
			if (!audio_is_playing(snd_Dash))
			{
				audio_sound_gain(snd_Dash,0.2,0);
				audio_sound_pitch(snd_Dash,1.1)
				audio_play_sound(snd_Dash,20,0);
			}
	}

	//--> Refreshes Air Dash when touches wall object
	if (!place_meeting(x,y+vsp,obj_Wall)) airDash_used = true;

	//--> Refreshes Attack Variables
	atk_type = 0;
	attack_number = 1;

	//--> Destroys Hitbox is exits
	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
}
