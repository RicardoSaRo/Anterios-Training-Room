// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Kicks(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Kicks : spr_VioletP_Kicks;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		switch_var = true;
	}
	
	if ((image_index >= 4)&&(!switch_var2))
	{
		var wave = instance_create_layer(x+(75*image_xscale),y+(110*image_yscale),"FX_Layer",obj_EnemyBullet)
		wave.priority = 10;
		wave.NoDamage = true;
		wave.switch_var = true;
		wave.sprite_index = spr_StunWave;
		wave.image_xscale = abs(image_xscale);
		wave.image_yscale = image_yscale;
		wave.image_angle = (sign(image_xscale)) ? 290 : 250;
		wave.direction = (sign(image_xscale)) ? 310 : 230;
		
		switch_var2 = true;
	}
	
	if ((image_index >= 8)&&(!switch_var3))
	{
		var wave = instance_create_layer(x+(82*image_xscale),y+(82*image_yscale),"FX_Layer",obj_EnemyBullet)
		wave.priority = 10;
		wave.NoDamage = true;
		wave.sprite_index = spr_StunWave;
		wave.image_xscale = image_xscale;
		wave.image_yscale = image_yscale;
		wave.image_angle = 0;
		wave.direction = (sign(image_xscale)) ? 0 : 180;
		
		switch_var3 = true;
	}
	
	if (image_index >= 11.6)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}