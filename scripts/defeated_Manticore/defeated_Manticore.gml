function defeated_Manticore() {
	sprite_index = spr_damage_Manticore;

	image_speed = 0;
	image_index = 1;

	Manticore_hp = 0;

	if (!audio_is_playing(snd_ManticoreDefeat)) audio_play_sound(snd_ManticoreDefeat,2,0);

	alarm[5] = clamp(alarm[5],1,alarm[5]);
	alarm[10] = 10;
	alarm[11] = 100;
}
