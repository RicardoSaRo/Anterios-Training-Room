/// @description Destroy_Bullets(spr_index)
/// @param spr_index
function Destroy_Bullets(argument0) {

	var dstry_spr = argument0;

	switch(dstry_spr)
	{
		case spr_RockAxe_G_destroy: case spr_RockAxe_B_destroy:
			if (image_index >= 3) instance_destroy();
			var rockFX = instance_create_layer(x + random(40) * choose(-1,1), y + random(40) * choose(-1,1),"FX_Layer",obj_rockice_FX);
			if (destroy_sprite == spr_RockAxe_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
			break;
		case spr_Spike_B_destroy: case spr_Spike_G_destroy:
			if (image_index >= 3) instance_destroy();
			if (image_yscale == 1)
			{
				repeat(6)
				{
					var rockFX = instance_create_layer(x + random(10) * choose(-1,1), y + random_range(0,400),"FX_Layer",obj_rockice_FX);
					if (destroy_sprite == spr_Spike_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
					var rockFX = instance_create_layer(x + random(10) * choose(-1,1), y + random_range(200,400),"FX_Layer",obj_rockice_FX);
					if (destroy_sprite == spr_Spike_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
				}
			}
			else
			{
				repeat(6)
				{
					var rockFX = instance_create_layer(x + random(10) * choose(-1,1), y + random_range(0,-450),"FX_Layer",obj_rockice_FX);
					if (destroy_sprite == spr_Spike_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
					var rockFX = instance_create_layer(x + random(10) * choose(-1,1), y + random_range(200,-450),"FX_Layer",obj_rockice_FX);
					if (destroy_sprite == spr_Spike_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
				}
			}
			break;
		case spr_Boulder_B_destroy: case spr_Boulder_G_destroy:
			if (image_index >= 3)
			{
				audio_play_sound(snd_Golem_rockSpread,17,0);
				instance_destroy();
			}
			repeat(3)
			{
				var rockFX = instance_create_layer(x + random(100) * choose(-1,1), y + random(100)* choose(-1,1),"FX_Layer",obj_rockice_FX);
				if (destroy_sprite == spr_Boulder_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
			}
			var StoneBullet = instance_create_layer(x,y,"FX_Layer",obj_EnemyBullet);
			if (obj_BossEnemy.enemy_name = "GolemB") StoneBullet.sprite_index = spr_RockBullet_B;
			else StoneBullet.sprite_index = spr_RockBullet_G;
			StoneBullet.destroy_sprite = spr_poof;
			StoneBullet.image_index = image_index;
			StoneBullet.vsp = random_range(-15,15);
			StoneBullet.hsp = -15 * image_xscale;
			StoneBullet.priority = 3;
			break;
		case spr_Pike_B_destroy: case spr_Pike_G_destroy:
			x += hsp/3;
			if (image_index >= 5) instance_destroy();
			repeat(3)
			{
				var rockFX = instance_create_layer(x + random(100) * choose(-1,1), y + random(20)* choose(-1,1),"FX_Layer",obj_rockice_FX);
				if (destroy_sprite == spr_Pike_G_destroy) rockFX.sprite_index = spr_rocks_FX_G;
			}
			break;
		case spr_WW_groundSlash_destroy: case spr_NeonBullet_destroy: case spr_NeonSmallBullet_destroy: case spr_NeonBigBullet_destroy:
		case spr_UpgradeFlame_Destroy: case spr_Wstpd_Needle_destroy: case spr_Wstpd_Ball_destroy: case spr_Wstpd_BallSP_destroy:
		case spr_StaticBallBlue_destroy: case spr_StaticBomb_destroy: case spr_DArrow_Fire_destroy: case spr_DArrow_Ice_destroy:
			if (image_index >= 3) instance_destroy();
			break;
		case spr_WW_miniSlash_destroy:
			if (image_index >= 3) instance_destroy();
			break;
		case spr_SerialThorns_destroy: case spr_SerialThorns2_destroy:
			if (image_index >= 2) instance_destroy();
			break;
		case spr_Explosion_grnd: case spr_Explosion_blue: case spr_Explosion_green:
			if (!bounce)
			{
				if (sprite_index == spr_Explosion_grnd) soundGain_by_distance(obj_Manticore,50,0.8,snd_smallExplosion,22,0);
				if (sprite_index == spr_Explosion_blue) soundGain_by_distance(obj_Manticore,50,0.8,snd_M2Nak_staticExplosion,22,0);
				bounce = true;
			}
			speed = 0;
			image_angle = 0;
			image_xscale = 1;
			image_yscale = 1;
			if (image_index >= 12)
			{
				audio_stop_sound(snd_Dragiankin_spiralFlame);
				instance_destroy();
			}
			break;
		case spr_poof: case spr_StaticWeb_destroy:
			if (obj_BossEnemy.enemy_name = "GolemB") or (obj_BossEnemy.enemy_name = "GolemG")
			{
				var rockFX = instance_create_layer(x + random(20) * choose(-1,1), y + random(20)* choose(-1,1),"FX_Layer",obj_rockice_FX);
				if (destroy_sprite == spr_poof) rockFX.sprite_index = spr_rocks_FX_G;
			}
			if (image_index >= 4) instance_destroy();
			break;
		case spr_VenomSpikeball_destroy:
			{
				speed = 0;
				image_alpha -= 0.01;
				image_xscale += 0.02;
				image_yscale += 0.02;
				if (image_index >= 6.6) instance_destroy();
			}
			break;
		case spr_FMarbleExp: case spr_IMarbleExp:
			if (instance_exists(obj_AshSnow))
			{
				with(obj_AshSnow) beingAbsorbed = false;
			}
			if (image_index >= 7.8) && (switch_var)
			{
				repeat(8)
				{
					switch(counter)
					{
						case 0: var yPos = y - 200; var xPos = 0; break;
						case 1: var yPos = y - 100; var xPos = x + 100; break;
						case 2: var yPos = 0; var xPos = x + 200; break;
						case 3: var yPos = y + 100; var xPos = x + 100; break;
						case 4: var yPos = y + 200; var xPos = 0; break;
						case 5: var yPos = y + 100; var xPos = x - 100; break;
						case 6: var yPos = 0; var xPos = x - 200; break;
						case 7: var yPos = y - 100; var xPos = x - 100; break;
					}
					var cluster = instance_create_layer(xPos,yPos,"FX_Layer",obj_EnemyBullet);
					if (sprite_index == spr_FMarbleExp)	cluster.sprite_index = spr_FMarbleExp;
					else cluster.sprite_index = spr_IMarbleExp;
					cluster.destroy_sprite = sprite_index;
					cluster.priority = 10;
					cluster.image_xscale = image_xscale;
					cluster.image_yscale = 2;
					cluster.image_angle = 45;
					cluster.NoDamage = true;
					counter += 1;
				}
				switch_var = false;
			}
			if (image_index >= 6) && (playerCollision)//--> ONE HIT KILL!!!! + FXs
			{
				consecutive_dmg = -1;
				repeat(global.player_max_health - 1) 
				{
					consecutive_dmg += 1;
					var lifeorb = instance_find(obj_Life_Orb,consecutive_dmg);
					lifeorb.sprite_index = spr_Life_Orb_empty;
					lifeorb.dmg = consecutive_dmg+1;
				}
				obj_Manticore.Manticore_hp -= global.player_max_health;
				obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
			}
			if (image_index >= 14) instance_destroy();
			break;
		case spr_Flarestar_exp: case spr_FrostBulwark_breaks:
			if (!switch_var3) //--> SndFX
			{
				var desSnd = (sprite_index == spr_Flarestar_exp) ? snd_FlareStar_destroy : snd_FrostBulwark_destroy;
				audio_play_sound(desSnd,20,0);
				switch_var3 = true;
			}
			speed = 0;
			if (place_meeting(x,y,obj_Manticore))
			{
				if (room == rm_DragonLord) && (obj_Manticore.state != crouchDEF_Manticore) && (obj_Manticore.state != spCharge_Manticore)
				{
					obj_Manticore.x += 40 * -image_xscale;
					obj_Manticore.y -= 10;
				}
				if (obj_Manticore.alarm[11] <= 1)
				{
					if (switch_var)
					{
						if (obj_Manticore.alarm[10] <= 0)
						{
							obj_Manticore.state = damage_Manticore;
							obj_Manticore.alarm[10] = 10;
							if (sprite_index == spr_Flarestar_exp) obj_Manticore.Manticore_hp -= 3;
							else obj_Manticore.Manticore_hp -= 2;
							obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp, 0, global.player_max_health);
					
							switch_var = false;
						}
						//--> Burn Stat
						if (instance_exists(obj_DragonLordRoom_manager))
						{
							with (obj_DragonLordRoom_manager)
							{
								if (other.sprite_index == spr_Flarestar_exp) ManticoreBurn = true;
								else ManticoreFreeze = true;
								switch(global.difficulty)
								{
									case "casual":	if (other.sprite_index == spr_Flarestar_exp) alarm[4] = 240;
													else alarm[3] = 300;
													break;
									case "normal":	if (other.sprite_index == spr_Flarestar_exp) alarm[4] = 240;
													else alarm[3] = 360;
													break;
									case "hard":	if (other.sprite_index == spr_Flarestar_exp) alarm[4] = 240;
													else alarm[3] = 420;
													break;
									case "lunatic":	if (other.sprite_index == spr_Flarestar_exp) alarm[4] = 240;
													else alarm[3] = 480;
													break;
								}
							}
						}
					}
				}
			}
			if (sprite_index == spr_Flarestar_exp) 
			{
				if (image_index >= 7) instance_destroy();
			}
			else if (image_index >= 5) instance_destroy();
			break;
		
		case spr_PoisonBlob_exp: case spr_VenomBlob_explsn:
			if (distance_to_object(obj_Manticore) <= 1400)
			{
				if (!audio_is_playing(snd_PoisonExp1)) soundGain_by_distance(obj_Manticore,100,0.2,snd_PoisonExp1,irandom_range(40,80),0);
				else
				{
					if (!audio_is_playing(snd_PoisonExp2)) soundGain_by_distance(obj_Manticore,100,0.2,snd_PoisonExp2,irandom_range(40,80),0);
					else
					{
						if (!switch_var3)
						{
							var rndmPExpl = choose(0,0,0,0,0,1,2);
							if (rndmPExpl == 1) soundGain_by_distance(obj_Manticore,100,0.2,snd_PoisonExp1,irandom_range(40,80),0);
							if (rndmPExpl == 2) soundGain_by_distance(obj_Manticore,100,0.2,snd_PoisonExp2,irandom_range(40,80),0);
							switch_var3 = true;
						}
					}
				}
			}
			if (playerCollision) && (obj_Manticore.state != crouchDEF_Manticore) && (bounce)
			{
				var manager = global.currentManager;
				if (obj_Manticore.alarm[11] <= 1)
				{
					if (!counter)
					{
						if (obj_Manticore.alarm[10] <= 0)
						{
							var MHP = obj_Manticore.Manticore_hp;
							obj_Manticore.state = damage_Manticore;
							obj_Manticore.alarm[10] = 10;
							MHP -= (manager.ManticorePoisoned) ? 2 : 1;
							MHP = clamp(MHP, 0, global.player_max_health);
							obj_Manticore.Manticore_hp = MHP;
							counter = true;
						}
					}
					else counter = false;
				}
				if (!place_meeting(x,y,obj_CrouchDEF))
				{
					artist.poisonFX = true;
					with (manager)
					{
						ManticorePoisoned = true;
						switch(global.difficulty)
						{
							case "casual":	alarm[2] = 240; break;
							case "normal":	alarm[2] = 300; break;
							case "hard":	alarm[2] = 360; break;
							case "lunatic":	alarm[2] = 420; break;
						}
					}
				}
			}
			if (image_index >= 5.6) instance_destroy();
			break;
	
		case spr_PoisonBlobB_exp:
		 
			if (!switch_var2)
			{
				audio_play_sound(snd_shortExplosion,12,0);
				audio_play_sound(snd_PoisonSP,25,0);
				switch_var2 = true;
			}
			image_blend = (image_blend == c_white) ? c_red : c_white;
			if (playerCollision)//--> ONE HIT KILL!!!! + FXs
			{
				consecutive_dmg = -1;
				repeat(global.player_max_health - 1) 
				{
					consecutive_dmg += 1;
					var lifeorb = instance_find(obj_Life_Orb,consecutive_dmg);
					lifeorb.sprite_index = spr_Life_Orb_empty;
					lifeorb.dmg = consecutive_dmg+1;
				}
				obj_Manticore.Manticore_hp -= global.player_max_health;
				obj_Manticore.Manticore_hp = clamp(obj_Manticore.Manticore_hp,0,global.player_max_health);
			}
			if (image_index >= 5.6) instance_destroy();
			break;
			
		case spr_DragianVenom_destroy:
			//-->INFLICTS DRAGIAN VENOM STATUS AILMENT
			image_xscale += 0.01;
			image_yscale += 0.01;
			speed = 1;
			if (image_index >= 5.6) instance_destroy();
			break;
		
		case spr_PiranhaPlant_destroy:
			var rndmPExpl = choose(0,0,0,1,2);
			if (rndmPExpl == 1) audio_play_sound(snd_PoisonExp1,25,0);
			if (rndmPExpl == 2) audio_play_sound(snd_PoisonExp2,25,0);
			var explosions = instance_create_layer(x,y,"Info_Layer",obj_fadeAway);
			explosions.sprite_index = spr_PoisonBlob_exp;
			explosions.image_angle = irandom(360);
			var rndm = choose(1,1.5,2);
			explosions.image_xscale = rndm;
			explosions.image_yscale = rndm;
			explosions.Xmovement = irandom_range(-5,5);
			explosions.Ymovement = irandom_range(-5,5);
			explosions.fadeSpd = 0.01;
			explosions.bright = true;
			if (image_index >= 3)
			{
				var ORB = instance_create_layer(x,y,"Life_n_Bars",obj_AntaresOrb)
				ORB.manager = obj_AlrauneRoom_manager;
				ORB.image_xscale = 2;
				ORB.image_yscale = 2;
				switch(owner.enemy_name)
				{
					case "Rose":	owner.state = Alraune_Defeated; break;
					default:		owner.state = Alraune_Defeated; break;
				}
				instance_destroy();
			}
			break;
			
		case spr_RainbowExplosion:
			if (playerCollision)
			{
				if (global.BossVariant == 0) bulletCollidesPlayer(2,2,global.currentManager,0,1,0,0);
				else bulletCollidesPlayer(2,4,global.currentManager,1,1,0,0);
			}
			if (image_index >= 8.6) instance_destroy();
			break;
			
		case spr_StoneBrth_destroy:
			if (playerCollision)
			{
				obj_Manticore.state = stun_Manticore;
				
				if (!instance_exists(obj_petrification))
				{
					var petrify = instance_create_layer(obj_Manticore.x,obj_Manticore.y,"Info_Layer",obj_petrification);
				}
			}
			x += hsp;
			y += vsp;
			if (image_index >= 3.6) instance_destroy();
			break;
	}


}
