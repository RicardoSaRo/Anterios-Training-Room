// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_LancesSP(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawRaise : spr_VioletP_ClawRaise;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		alarm[2] = 50;
		
		switch_var = true;
	}
	
	switch(sprite_index)
	{
		case spr_RadiantP_ClawRaise: case spr_VioletP_ClawRaise:
			if (image_index >= 3.6)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawUP : spr_VioletP_ClawUP;
			}
			break;
		
		case spr_RadiantP_ClawUP: case spr_VioletP_ClawUP:
			
			if (interact_instance == self)
			{
				interact_instance = instance_create_layer(x,y-(120*image_yscale),"FX_Layer",obj_EnemyBullet);
				interact_instance.sprite_index = spr_RainbowLance_ball;
				interact_instance.image_xscale = 0.1;
				interact_instance.image_yscale = 0.1;
				interact_instance.max_h = x; //--> X pos for orbit
				interact_instance.min_h = y-(120*image_yscale); //--> Y pos for orbit
				interact_instance.ownership = global.currentManager;
				interact_instance.priority = 10;
				
				switch(global.difficulty)
				{
					case "casual":	interact_instance.counter = choose(3,3,4); break;
					case "normal":	interact_instance.counter = choose(4,5,6); break;
					case "hard":	interact_instance.counter = choose(5,6,7); break;
					case "lunatic":	interact_instance.counter = choose(6,7,8,8); break;
				}
			}
			
			if (switch_var2)
			{
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_ClawExtend : spr_VioletP_ClawExtend;
			}
			break;
			
		case spr_RadiantP_ClawExtend: case spr_VioletP_ClawExtend:
			if (image_index >= 5.6)	switch_var3 = true;
			break;
	}
	
	if (switch_var3)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 160; break;
			case "normal": alarm[1] = 140; break;
			case "hard": alarm[1] = 130; break;
			case "lunatic": alarm[1] = 120; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		switch_var3 = false;
		interact_instance = self;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}