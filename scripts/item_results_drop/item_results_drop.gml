function item_results_drop() {
	//--> Item Results Drop
	//--> Chooses the item to drop based on enemy defeated info.

	switch(global.HuntRoom)
	{
		case rm_Werewolf1:	if (rareness == "mandatory") item_name = "HOWLING\nFANGS"; 
							if (rareness == "normal") item_name = choose("BEAST\nSKIN","HOWLING\nFANGS");
							if (rareness == "rare") item_name = "SPIRITUAL\nFORCE";
							if (rareness == "superrare") item_name = "MOONSTONE";
							if (rareness == "ultrarare") item_name = "CRIMSON\nCLAWS"; 
							if (rareness == "variantrare") item_name = "GOLDEN\nMANE";
							break;
						
		case rm_Wastipede:	if (rareness == "mandatory") item_name = "WASTIPEDE\nPOLLEN"; 
							if (rareness == "normal") item_name = choose("INSECT\nPLATING","WASTIPEDE\nPOLLEN");
							if (rareness == "rare") item_name = "EXO CAMO";
							if (rareness == "superrare") item_name = "CRIMSON\nSCYTHE";
							if (rareness == "ultrarare") item_name = "CRIMSON\nSTING"; 
							if (rareness == "variantrare") item_name = "POISON\nCROWN";
							break;
						
		case rm_Meetonak:	if (rareness == "mandatory") item_name = "TELEPORT\nDEVICE"; 
							if (rareness == "normal") item_name = choose("TELEPORT\nDEVICE","TELEPORT\nDEVICE","BIOTECH\nLINKS","BIOTECH\nLINKS","NEON FLUID");
							if (rareness == "rare") item_name = choose("ANCIENT TECH\nPARTS","ANCIENT TECH\nPARTS","VALARI\nPARTS");
							if (rareness == "superrare") item_name = "STATIC\nCORE";
							if (rareness == "ultrarare") item_name = "ANCIENT\nGENERATOR"; 
							if (rareness == "variantrare") item_name = "NOISE\nDYNAMO";
							break;
						
		case rm_Dragonkin:	if (rareness == "mandatory") item_name = "DRAGIAN\nOIL"; 
							if (rareness == "normal") item_name = choose("DRAGIAN\nOIL","DRAGIANKIN\nFANG");
							if (rareness == "rare") item_name = "VALARI\nPARTS";
							if (rareness == "superrare") item_name = "GAUNTLET\nPARTS";
							if (rareness == "ultrarare") item_name = "DRAGIAN\nENHACER"; 
							if (rareness == "variantrare")
							{
								switch(global.results[results.enemy,enemyInfo.foename])
								{
									case "BDragonkin":	item_name = "FIRE\nSCALE"; break;
									case "IDragonkin":	item_name = "ICE\nSCALE"; break;
									case "VDragonkin":	item_name = "VENOM\nSCALE"; break;
									case "PDragonkin":	item_name = "RADIANT\nSCALE"; break;		
								}
							}
							break;
						
		case rm_Crab:		if (rareness == "mandatory") item_name = "NEON FLUID"; 
							if (rareness == "normal") item_name = choose("NEON FLUID","NEON SALT\nCRYSTAL");
							if (rareness == "rare") item_name = "CARAPACE\nPART";
							if (rareness == "superrare") item_name = "SEA\nQUARTZ";
							if (rareness == "ultrarare") item_name = "KARKINOS\nPINCERS";
							if (rareness == "variantrare") item_name = "TAMBANOKANO\nPINCERS";
							break;

						
		case rm_Golem:		if (rareness == "mandatory") item_name = "KORUNITE\nORE"; 
							if (rareness == "normal") item_name = choose("SILVER","GOLD","STEEL");
							if (rareness == "rare") item_name = choose("SAPPHIRE","RUBY","EMERALD");
							if (rareness == "superrare")
							{
								if (global.results[results.enemy,enemyInfo.foename] == "GolemB")
									item_name = choose("DIAMOND","TOPAZ","TOPAZ","TOPAZ","PURE\nKORUNITE");
								if (global.results[results.enemy,enemyInfo.foename] == "GolemG")
									item_name = choose("DIAMOND","TOPAZ","PURE\nKORUNITE","PURE\nKORUNITE","PURE\nKORUNITE");
								if (global.results[results.enemy,enemyInfo.foename] == "GolemD")
									item_name = choose("DIAMOND","DIAMOND","DIAMOND","TOPAZ","PURE\nKORUNITE");
							}
							if (rareness == "ultrarare") item_name = "ORICHALCUM"; 
							if (rareness == "variantrare")
							{
								if (global.results[results.enemy,enemyInfo.foename] == "GolemG") item_name = "MITHRIL";
								else item_name = "ADAMANTINE";
							}
							break;
						
		case rm_Alraune:	if (rareness == "mandatory") item_name = "ALRAUNE\nSPORES"; 
							if (rareness == "normal") item_name = choose("ARAUNE\nSPORES","RAZOR\nPETALS");
							if (rareness == "rare") item_name = "VENOMOUS\nTHORNS";
							if (rareness == "superrare") item_name = "MANDRAGORA\nROOT";
							if (rareness == "ultrarare") item_name = "PREMIUM\nSEED"; 
							if (rareness == "variantrare") item_name = "MAKHANA";
							break;


	/*	case rm_Revenant:	if (rareness == "mandatory") item_name = "VOID FRAGMENT"; 
							if (rareness == "normal") item_name = choose("VOID FRAGMENT","EERIE EMBERS");
							if (rareness == "rare") item_name = "ILLUSION PRISM";
							if (rareness == "superrare") item_name = "AETHER ORB";
							if (rareness == "ultrarare") item_name = "VOID NEXUS"; 
							if (rareness == "variantrare") item_name = "DARK MATTER";
							break;
	*/

	/*	case rm_Gryffon:	if (rareness == "mandatory") item_name = "GRYFFON PLUME"; 
							if (rareness == "normal") item_name = choose("GRYFFON PLUME","FIERCE BEAK");
							if (rareness == "rare") item_name = "GRYFFON MANE";
							if (rareness == "superrare") item_name = "GRYPHON CLAW";
							if (rareness == "ultrarare") item_name = "WIND TALONS"; 
							if (rareness == "variantrare") item_name = "FIERCE PINION";
							break;
	*/

	/*	case rm_Axtol:		if (rareness == "mandatory") item_name = "GAS GLANDS"; 
							if (rareness == "normal") item_name = choose("GAS GLANDS","ELECTRIC FINS");
							if (rareness == "rare") item_name = "ELECTRIC GUILLS";
							if (rareness == "superrare") item_name = "FULGURITE PIECES";
							if (rareness == "ultrarare") item_name = "AXTOL TAIL"; 
							if (rareness == "variantrare") item_name = "ELECTROCYTE CORE";
							break;
	*/

		case rm_DragonLord:	var isFirelord = (global.DragonLordRoom == 0) ? true : false;
							if (rareness == "mandatory") item_name = "LORD'S\nNAIL";
							if (rareness == "normal") item_name = (isFirelord) ? "FIRE\nFANG" : "ICE\nFANG";
							if (rareness == "rare") item_name = (isFirelord) ? "FLAMERUBY" : "SAPPHIRICE";
							if (rareness == "superrare") item_name = (isFirelord) ? "SCORCHING\nCORE" : "FREEZING\nCORE";
							if (rareness == "ultrarare") item_name = "LORD'S\nHORN"; 
							if (rareness == "variantrare") item_name = "FROZEN\nFLAME";
							break;
							
		case rm_VenomLord:	if (rareness == "mandatory") item_name = "VENOMLORD'S\nSCALES";
							if (rareness == "normal") item_name = "DRAGIAN VENOM\nGLANDS";
							if (rareness == "rare") item_name = "VIPER TAIL'S\nSTING";
							if (rareness == "superrare") item_name = "LORD VIPER'S\nFANG";
							if (rareness == "ultrarare") item_name = "TOXIMMAND";
							if (rareness == "variantrare") item_name = "FROST\nTOXIMMAND";
							break;
	}


}
