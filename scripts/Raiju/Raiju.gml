function Raiju() {
	//--> Thunder HoloFamiliar - Raiju

	if (image_alpha < 1) image_alpha += 0.05;
	
		if (sprite_index == spr_ThunderFamiliar_Standing1) || (sprite_index == spr_ThunderFamiliar_Standing2)
		{
			var ManticoreX = obj_Manticore.x + (45 * obj_Manticore.image_xscale);
			var ManticoreY = obj_Manticore.y - 150;
	
			//--> Speed to chatch up player
			var shieldSPD = hsp;
			var max_acc = 0.010;
			hsp += 0.0025;
			hsp = clamp(hsp,0,max_acc);
			shieldSPD = hsp;
		
			x = lerp(x,ManticoreX,shieldSPD);
			y = lerp(y,ManticoreY,0.025);
		
			if (image_index >= 6.6)
			{
				sprite_index = choose(spr_ThunderFamiliar_Standing1,spr_ThunderFamiliar_Standing2);
				image_index = 0;
			
				if (obj_BossEnemy.x >= x)
				{
					if (image_xscale == -1.5)
					{
						image_xscale = 1.5;
						image_alpha = 0;
					}
				}
				else
				{
					if (image_xscale == 1.5)
					{
						image_xscale = -1.5;
						image_alpha = 0;
					}
				}
			}
		}
	
		if ((sprite_index == spr_ThunderFamiliar_Attack) && (image_index >= 6.6)) ||
		   ((sprite_index == spr_ThunderFamiliar_Create) && (image_index >= 13.6))
		{
			sprite_index = choose(spr_ThunderFamiliar_Standing1,spr_ThunderFamiliar_Standing2);
			image_index = 0;
		}
	
		if (alarm[0] <= 0) && (!switch_var)
		{
			var distance = distance_to_object(obj_BossEnemy);
		
			var FamAttk = (distance<200) ? 0 : ((distance<480) ? 1 : 2);
	
			switch(FamAttk)
			{
				case 0:
					repeat(10)
					{
						var glare = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
						glare.sprite_index = choose(spr_spark,spr_starShine);
						var sz = choose(1,1.5);
						glare.image_xscale = sz;
						glare.image_yscale = sz;
						glare.image_blend = c_white;
						glare.Xmovement = random_range(0,3) * choose(1,-1) * orbLvl;
						glare.Ymovement = random_range(0,3) * choose(1,-1) * orbLvl;
						glare.bright = true;
						glare.fadeSpd = choose (0.01,0.02);
					}
		
					var TW = instance_create_layer(x,y,"Back_FX_Layer",obj_TWave);
					TW.owner = self;
					TW.image_alpha = 0.5;
					TW.priority = 9;
					TW.max_size += 5;
					break;
				
				case 1:
					var nearBoss = instance_nearest(x,y,obj_BossEnemy)
					repeat(2)
					{
						var relampago = instance_create_layer(x,y,"Back_FX_Layer",obj_relampago);
						relampago.image_angle = point_direction(x,y,nearBoss.x,nearBoss.y)
						relampago.sprite_index = spr_relampago4;
						relampago.maxRangeBolt = true;
						relampago.radius = 2;
					}
					break;
				
				case 2:
					for(var i=1; i<=2; i++)
					{
						var WL = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
						WL.sprite_index = spr_wavelightning;
						WL.Consum = false;
						WL.image_xscale = image_xscale * 2;
						WL.image_blend = (i==2) ? merge_color(c_navy,c_aqua,0.5) : c_white;
						WL.image_yscale = (i==2) ? random_range(1.5,3) : random_range(0.5,1.5);
						WL.image_index = (i==2) ? 2 : 0;
						WL.alarm[0] = 3;
						WL.alarm[1] = 240;
					}
					break;
			}
		
			alarm[0] = 120;
			sprite_index = spr_ThunderFamiliar_Attack;
			image_index = 0;
		}
	
		if (alarm[1] <= 0) && (!switch_var)
		{
			sprite_index = spr_ThunderFamiliar_Crumbles;
			image_index = 0;
			switch_var = true;
		}
	
		if (place_meeting(x,y,obj_EnemyBullet))
		{
			var instnear = instance_nearest(x,y,obj_EnemyBullet);
			if (instnear.priority < 10) && (instnear.sprite_index != instnear.destroy_sprite)
				alarm[1] -= 30;
		}
	
		if (sprite_index == spr_ThunderFamiliar_Crumbles) && (image_index >= 7.6) instance_destroy();


}
