function spCharge_air() {
	if (global.RELIKS[49,2]) || (global.HuntRoom != noone) exit;

	switch(equiped_sword)
	{
		case 1: current_sp_sword = special_Argenta;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_silver : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 2: current_sp_sword = special_Dyrnwyn;
				var fontcolor1 = (!global.RELIKS[24,2]) ? make_color_rgb(241,100,31) : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 3: current_sp_sword = special_GirTab;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_red : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
		case 4: current_sp_sword = special_Frostbiter;
				var fontcolor1 = (!global.RELIKS[24,2]) ? c_aqua : choose(c_silver,c_red,c_aqua,make_color_rgb(241,100,31));
				break;
	}

	switch(equipped_axe)
	{
		case 1: current_sp_axe = special_Korunax;
				var fontcolor2 = (!global.RELIKS[24,2]) ? make_color_rgb(211,151,65) : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 2: current_sp_axe = special_Mirage;
				var fontcolor2 = (!global.RELIKS[24,2]) ? c_fuchsia : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 3: current_sp_axe = special_Mjolnir;
				var fontcolor2 = (!global.RELIKS[24,2]) ? make_color_rgb(39,100,205) : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
		case 4: current_sp_axe = special_Whisperer;
				var fontcolor2 = (!global.RELIKS[24,2]) ? c_lime : choose(make_color_rgb(211,151,65),c_fuchsia,make_color_rgb(39,100,205),c_lime);
				break;
	}

	if (current_sp_sword == 100) && (current_sp_axe == 100) //--> if both bars are filled, exits the script
		exit;

	if (current_sp_sword < 100)
	{
		if (alarm[9] mod spCharge_spd == 0)
		{
			with (instance_create_layer(x+random_range(-10,-50),y,"FX_Layer",obj_show_damage))
			{
				font_color = fontcolor1;
				damage_display = 1;
				plus_or_minus = "+";
				y_speed = 1;
			}
		
			audio_sound_pitch(snd_SPCharge,random_range(0.7,1.4));
			audio_play_sound(snd_SPCharge,10,0);
		
			if (global.RELIKS[24,2])
				{
					special_Argenta += 1;
					special_Dyrnwyn += 1;
					special_GirTab += 1;
					special_Frostbiter += 1;
				}
				else
				{
					switch(equiped_sword)
					{
						case 1: special_Argenta += 1; break;
						case 2: special_Dyrnwyn += 1; break;
						case 3: special_GirTab += 1; break;
						case 4: special_Frostbiter += 1; break;
					}
				}
		
			for (var i = 1; i < 3; i++)
			{
				var cc_spawnX = obj_Manticore.x + random_range(-70,70);
				var spawn_layer = choose("Back_FX_Layer","Info_Layer");
				var chispa = instance_create_layer(cc_spawnX,obj_Manticore.bbox_bottom,spawn_layer,obj_charge_chispa);
				chispa.angl = choose(45);
				if (global.RELIKS[24,2]) chispa.image_blend = choose(fontcolor1,fontcolor2);
			}
		}
	}
	
	if (current_sp_axe < 100)
	{
		if (alarm[8] mod spCharge_spd == 0)
		{
			with (instance_create_layer(x+random_range(10,50),y,"FX_Layer",obj_show_damage))
			{
				font_color = fontcolor2;
				damage_display = 1;
				plus_or_minus = "+";
				y_speed = 1;
			}
		
			audio_sound_pitch(snd_SPCharge,random_range(0.7,1.8));
			audio_play_sound(snd_SPCharge,10,0);
				
			if (global.RELIKS[24,2])
				{
					special_Korunax += 1;
					special_Mirage += 1;
					special_Mjolnir += 1;
					special_Whisperer += 1;
				}
				else
				{
					switch(equipped_axe)
					{
						case 1: special_Korunax += 1; break;
						case 2: special_Mirage += 1; break;
						case 3: special_Mjolnir += 1; break;
						case 4: special_Whisperer += 1; break;
					}
				}
		
			for (var i = 1; i < 3; i++)
			{
				var cc_spawnX = obj_Manticore.x + random_range(-70,70);
				var spawn_layer = choose("Back_FX_Layer","Info_Layer");
				var chispa = instance_create_layer(cc_spawnX,obj_Manticore.bbox_bottom,spawn_layer,obj_charge_chispa);
				chispa.angl = choose(45);
				if (global.RELIKS[24,2]) chispa.image_blend = choose(fontcolor1,fontcolor2);
			}
		}			
	}

	exit;


}
