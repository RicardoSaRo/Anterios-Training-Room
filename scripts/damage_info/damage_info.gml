/// @description damage_info(obj,wpn_sprite,hitbox_spr)
/// @param obj
/// @param wpn_sprite
/// @param hitbox_spr
function damage_info(argument0, argument1, argument2) {

	var enemy = argument0;
	var wpn_spr = argument1;
	var dmg = 0;
	var hit_spr = spr_hit_FX;
	var hit_angle_slash = choose(random_range(30,150),random_range(210,330));
	var hit_angle = 0;

	switch (wpn_spr)
	{
		case spr_Argenta: case spr_ArgentaMix: case spr_Argenta_DashStab:
			dmg = 10 * (global.SWORDS[1,3] + 9); hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
		case spr_AirArgenta: case spr_AirArgentaMix:
			dmg = 10 * ((global.SWORDS[1,3]/2) + 4); hit_spr = spr_slash; hit_angle = random(360); break;
		
		case spr_Dyrnwyn: case spr_DyrnwynMix: case spr_Dyrnwyn_DashStab:
			dmg = 10 * ((global.SWORDS[2,3]/2) + 2); hit_spr = spr_burn_red; hit_angle = 0; break;
		case spr_AirDyrnwyn: case spr_AirDyrnwynMix:
			dmg = 10 * ((global.SWORDS[2,3]/2) + 2); hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_GirTab: case spr_GirTabMix: case spr_AirGirTab: case spr_AirGirTabMix:
			dmg = (global.SWORDS[3,3]==0) ? 10 * choose(2,3) : ((global.SWORDS[3,3]==1) ? 10 * choose(2,3.5) : 10 * choose(3,3.5));
			hit_spr = spr_spear_hit; hit_angle = 0; break;
		case spr_GirTabUP: case spr_AirGirTabUP:
			dmg = (global.SWORDS[3,3]==0) ? 10 * choose(2,3) : ((global.SWORDS[3,3]==1) ? 10 * choose(2,3.5) : 10 * choose(3,3.5));
			hit_spr = spr_spear_hit; hit_angle = 270; break;
	
		case spr_GirTrident: case spr_AirGirTrident: case spr_GirTridentMix: case spr_AirGirTridentMix:
			var Crit = irandom(50);
			dmg = (Crit == 7) ? 500 : 40;
			hit_spr = (Crit == 7) ? spr_V_lightning : spr_redspark; hit_angle = random(360); break;
		case spr_GirTridentUP: case spr_AirGirTridentUP:
			var Crit = irandom(50);
			dmg = (Crit == 7) ? 500 : 40;
			hit_spr = (Crit == 7) ? spr_V_lightning : spr_redspark; hit_angle = random(360); break;
		
		case spr_Frostbiter: case spr_FrostbiterMix: case spr_AirFrostbiter: case spr_AirFrostbiterMix:
		case spr_FrostBulwark:
			dmg = (global.SWORDS[4,3]==0) ? 30 : ((global.SWORDS[4,3]==1) ? 10 * choose(3,3.5) : ((global.SWORDS[4,3]==2) ? 10 * choose(3.5,4) : 40));
			hit_spr = spr_spear_hit; hit_angle = 0; break;
		
		case spr_FrostbiterUP: case spr_AirFrostbiterUP:
			dmg = (global.SWORDS[4,3]==0) ? 30 : ((global.SWORDS[4,3]==1) ? 10 * choose(3,3.5) : ((global.SWORDS[4,3]==2) ? 10 * choose(3.5,4) : 40));
			hit_spr = spr_spear_hit; hit_angle = 270; break;
		
		case spr_wallattack_hb:
			dmg = (instance_exists(obj_Wallattack)) ? obj_Wallattack.WAdmg : 1;	hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_Chakram:
			dmg = (global.TWs[1,3]==0) ? 10 : ((global.TWs[1,3]==1) ? 10 * choose(1,1,1,2) : ((global.TWs[1,3]==2) ? 10 * choose(1,1,2) : 10 * choose(1,2)));
			hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_Triblader: case spr_TribladerSP:
			dmg = (global.TWs[7,3]==0) ? 10 : ((global.TWs[7,3]==1) ? 10 * choose(1,1.5,2) : ((global.TWs[7,3]==2) ? 10 * choose(1.5,2) : 20));
			hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_Kunai:
			dmg = 10 * (4 + global.TWs[2,3]); hit_spr = spr_spear_hit; hit_angle = 0; break;
		
		case spr_Korunax: case spr_AirKorunax: 
			dmg = 10 * (global.AXES[1,3] + 12); hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
		
		case spr_StrAtk_Korunax1:
			dmg = (global.AXES[1,3] * 100) + 700; hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_StrAtk_KorunaxFrostbiter:
			dmg = (global.AXES[1,3] * 250) + (global.SWORDS[4,3] * 250); hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_StrAtk_Korunax2:
			dmg = (global.AXES[1,3] * 200) + 600; hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_StrAtk_Mirage1:
			dmg = (global.AXES[2,3] * 100) + 700; hit_spr = spr_burn_green; hit_angle = 0; break;
		
		case spr_StrAtk_Mirage2:
			dmg = (global.AXES[2,3] * 200) + 600; hit_spr = spr_burn_green; hit_angle = 0; break;	
		
		case spr_StrAtk_Mjolnir1:
			dmg = (global.AXES[3,3] * 10) + choose(30,35,40,45,50); hit_spr = spr_relampago; hit_angle = random(360); break;
		
		case spr_StrAtk_Mjolnir2:
			dmg = (global.AXES[3,3] * 20) + choose(40,45,50); hit_spr = spr_V_lightning; hit_angle = random(360); break;
		
		case spr_StrAtk_Whisperer1:
			dmg = (global.AXES[4,3] * 5) + 25; hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_StrAtk_Whisperer2:
			dmg = (global.AXES[4,3] * 10) + 30; hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_StrAtk_WhispererDyrnwyn:
			dmg = (global.AXES[4,3] * 10) + (global.AXES[4,3] * 10); hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_Mirage: case spr_AirMirage:
			dmg = 10 * (global.AXES[2,3] + 9); hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
		
		case spr_willowisp:
			dmg = 10; hit_spr = spr_burn_green; hit_angle = 0; break;
		
		case spr_Mjolnir: case spr_AirMjolnir:	
			dmg = 10 * irandom_range(1,4); hit_spr = spr_spark; hit_angle = random(360); break;
		
		case spr_Whisperer: case spr_AirWhisperer:
			dmg = 10 * ((global.AXES[4,3]) + 7); hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_TornadoINNER_back: case spr_TornadoINNER_front:
		case spr_TornadoINVERSE_front: case spr_TornadoINVERSE_back:
			dmg = (global.AXES[4,3]>=2) ? 10 * choose(1,2) : 10; hit_spr = spr_half_slash; hit_angle = hit_angle_slash; break;

		case spr_Starblades: case spr_StarbladesSP:
			dmg = (global.TWs[8,3]==0) ? 10 : ((global.TWs[8,3]==1) ? 10 * choose(1,1,2) : ((global.TWs[8,3]==2) ? 10 * choose(1,2) : 20));
			hit_spr = spr_half_slash; hit_angle = hit_angle_slash; break;
		
		case spr_Flarestar: case spr_Flarestar_create: 
			dmg = 10; hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_Flarestar_exp:
			dmg = 10 * ((global.SWORDS[2,3]*40)+100); hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_relampago: case spr_relampago2: case spr_relampago3: case spr_relampago4:
			dmg = 10 * irandom_range(1,global.AXES[3,3]+6); hit_spr = spr_spark; hit_angle = random(360); break;
		
		case spr_Lightball:
			dmg = 10; hit_spr = spr_spark; hit_angle = random(360); break;
		
		case spr_wow_bomb:
			dmg = 10; hit_spr = spr_burn_green; hit_angle = 0; break;
		
		case spr_Explosion_grnd:
			var upDmg = (global.TWs[3,3]*3);
			dmg = 10 * irandom_range(15,20) + upDmg; hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_Explosion_hmmgF:
			dmg = 10 * irandom_range(15,20); hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_Explosion_bmb:
			var minDmg = (global.TWs[4,3]==0) ? 150 : ((global.TWs[4,3]==1) ? 165 : ((global.TWs[4,3]==2) ? 180 : 200));
			minDmg *= 10; dmg = irandom_range(minDmg,2500); hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_Guillotine:
			dmg = 10 * irandom_range(1,3+(global.TWs[5,3]/2)); hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
		
		case spr_Shuriken:
			dmg = (global.TWs[6,3]==0) ? 20 : ((global.TWs[6,3]==1) ? 10 * choose(2,2,3) : ((global.TWs[6,3]==2) ? 10 * choose(2,3) : 30));;
			hit_spr = spr_half_slash; hit_angle = hit_angle_slash; break;
		
		case spr_windslash: case spr_Whirlwind:
			dmg = 20; hit_spr = spr_half_slash; hit_angle = hit_angle_slash; break;
		
		case spr_Assegai:
			var assegai = instance_place(x,y,obj_Throwing_Wpn);
			dmg = 10 * (1 + assegai.starblade_angle * ((global.TWs[9,3]/7)+1));
			dmg = clamp(dmg,0,3000);
			hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_TWave:
			var TOPlus = (global.TWs[10,3]>=2) ? global.TWs[10,3] - 1 : 0;
			dmg = 10 * irandom_range(1,3+TOPlus); hit_spr = spr_spark; hit_angle = random(360); break;
		
		case spr_Coin_Launcher: case spr_BigCoin: case spr_BigTripleCoin:
			dmg = 10 * (20 + (global.TWs[15,3] * 10)); hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_Iceball:
			dmg = 10 * obj_Iceball.ib_hsp; hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_Explosion_RExp1: case spr_Explosion_RExp2:
			dmg = 10 * obj_Throwing_Wpn.bounce_count; hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_Argenta_wave2:
			dmg = 10 * ((global.SWORDS[1,3]*10)+ 150 + obj_Argenta_wave.aw_plus_dmg);
			hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
		
		case spr_Special_GirTab:
			dmg = 10 * (obj_Manticore.current_sp_sword * 30); hit_spr = spr_burn_slash; hit_angle = hit_angle_slash; break;
	
		case spr_ShurikenSP:
			dmg = 45 + (global.TWs[6,3]*15); hit_spr = spr_slash; hit_angle = random(360); break;
	
		case spr_ChakramSP:
			dmg = 80 + (global.TWs[1,3] * 20); hit_spr = spr_hit_FX; hit_angle = random(360); break;
			
		case spr_KunaiSP:
			dmg = 250 + (global.TWs[2,3]*50); hit_spr = spr_hit_FX; hit_angle = random(360); break;
		
		case spr_BombSP:
			dmg = (instance_exists(obj_TWSpecial)) ? 500 * obj_TWSpecial.ammoConsum : 500;
			hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_AssegaiSP:
			var assegai = instance_place(x,y,obj_TWSpecial);
			dmg = 10 * (10 + assegai.angle * ((global.TWs[9,3])+1));
			dmg = clamp(dmg,0,2000 + (global.TWs[9,3]*500));
			hit_spr = spr_Smack; hit_angle = random(360); break;
		
		case spr_GuillotineSP:
			dmg = 15 * irandom_range(1,3+(global.TWs[5,3]/2)); hit_spr = spr_slash; hit_angle = hit_angle_slash; break;
	
		case spr_IceSpikeball:
			dmg = 100 + (global.TWs[13,3] * 50); hit_spr = spr_hit_FX; hit_angle = random(360); break;
	
		case spr_flametongues:
			dmg = 40 + (global.SWORDS[2,3] * 15); hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_wavelightning:
			dmg = irandom(20) + (global.AXES[3,3] * 5); hit_spr = spr_spark; hit_angle = random(360); break;
		
		case spr_FireFamiliar_Attack: case spr_FireFamiliar_Onfire:
			dmg = 1 + global.AXES[2,3]; hit_spr = spr_burn_red; hit_angle = 0; break;
		
		case spr_NeonLaser:
			dmg = 10 + random(global.SWORDS[4,3]); hit_spr = spr_snowflake; hit_angle = random(360); break;
		
		case spr_BigAquaExplosion:
			dmg = 100; hit_spr = spr_snowflake; hit_angle = random(360); break;
		
		case spr_ShadowHitbox:
			dmg = random_range(100,200); hit_spr = spr_PoisonDMG_FX; hit_angle = random(360); break;
		
		case spr_windjmp_fx:
			dmg = irandom_range(1,4); hit_spr = spr_half_slash; hit_angle = hit_angle_slash; break;
	}

	switch (argument2)
	{
		case spr_Argenta_hitbox3: dmg *= 3; break;
		case spr_ArgentaSTR_hitbox: dmg *= 4; break;
		case spr_DyrnwynSTR_hitbox: dmg *= 2; break;
		case spr_Argenta_hitbox2: case spr_Korunax_hitbox6: dmg += 10; break;
		case spr_Mjolnir_hitbox4: case spr_Mjolnir_hitbox5:
		case spr_Mjolnir_hitbox6: dmg += 1; break;
		case spr_GirTab_hitbox2: switch(wpn_spr)
								 {
									case spr_GirTab: case spr_AirGirTab: case spr_GirTabUP: case spr_AirGirTabUP:
									case spr_GirTrident: case spr_GirTridentUP: case spr_AirGirTrident:
									case spr_AirGirTridentUP: dmg = (global.SWORDS[3,3]>=2) ? dmg * 1.5 : dmg + 1;
									break;
								
									case spr_Frostbiter: case spr_FrostbiterUP: case spr_AirFrostbiter:
									case spr_AirFrostbiterUP: dmg = (global.SWORDS[4,3]>=2) ? dmg * 1.5 : dmg + 1;
									break;
								 }
								 break;
							 
	}

	var dmg_info_array = array_create(3,0);
	dmg_info_array[1] = dmg;
	dmg_info_array[2] = hit_spr;
	dmg_info_array[3] = hit_angle;

	return dmg_info_array;


}
