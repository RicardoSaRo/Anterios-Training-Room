// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_Tail(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Tail : spr_VioletP_Tail;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index > 4) && (!switch_var2)
	{
		var htbx = instance_create_layer(x+(225*image_xscale),y+(155*image_yscale),"FX_Layer",obj_Enemy_hitbox);
		htbx.sprite_index = spr_PhoeniskTail_htbx;
		htbx.image_xscale = image_xscale;
		htbx.image_yscale = image_yscale;
		
		var tailexpl = instance_create_layer(x+(335*image_xscale),y+(135*image_yscale),"FX_Layer",obj_EnemyBullet);
		tailexpl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_grnd : spr_Explosion_blue;
		if (global.BossVariant == 1) tailexpl.image_blend = c_fuchsia;
		tailexpl.image_angle = (sign(image_xscale)) ? 270 : 90;
		tailexpl.NoDamage = true;
		/*
		var vSmallB = instance_create_layer(x+(335*image_xscale),y+(135*image_yscale),"FX_Layer",obj_EnemyBullet);
		vSmallB.sprite_index = spr_Rainbow_Fireball;
		vSmallB.destroy_sprite = spr_RainbowExplosion;
		vSmallB.NoDamage = true;
		vSmallB.image_angle = (sign(image_xscale)) ? 0 : 180;
		vSmallB.direction = (sign(image_xscale)) ? 0 : 180;
		vSmallB.alarm[0] = random_range(12,25);
		vSmallB.alarm[1] = 3;
		vSmallB.speed = 14;
		vSmallB.priority = 10;
		*/
		switch_var2 = true;
	}
	
	if (image_index >= 5) if (instance_exists(obj_Enemy_hitbox)) instance_destroy(obj_Enemy_hitbox);
	
	if (image_index >= 8.6)
	{
		//--> Ends State and goes to Standing
		switch(global.difficulty)
		{
			case "casual": alarm[1] = 90; break;
			case "normal": alarm[1] = 80; break;
			case "hard": alarm[1] = 70; break;
			case "lunatic": alarm[1] = 60; break;
		}
	
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
		image_index = 0;
		switch_var = false;
		switch_var2 = false;
		state = Phoenisk_Standing;
	
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
		AtkTurn += 1;
	}
}