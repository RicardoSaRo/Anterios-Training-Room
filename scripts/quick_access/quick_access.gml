function quick_access() {
	key_0 = max(keyboard_check_pressed(vk_numpad0),gamepad_axis_value(global.controller_type,gp_axisrv*-1),0);
	key_w = max(keyboard_check_pressed(ord("W")),gamepad_axis_value(global.controller_type,gp_axisrh*-1),0);
	key_e = max(keyboard_check_pressed(ord("E")),gamepad_axis_value(global.controller_type,gp_axisrh),0);

	if (!max(key_0,key_w,key_e,0)) || (obj_Manticore.state == specialTW_Manticore) exit; //--> Exits if none of the above keys are pressed

	if (key_0) //--> Quick Throwing Weapon change key
	{
		var foundtw = false;
		var twcount = obj_Manticore.equipped_tw;
		while (foundtw == false)
		{
			twcount += 1;
			if (twcount > 16) twcount = 1;
			if (global.TWs[twcount,1]) && (global.TWs[twcount,2])
			{
				foundtw = true;
				global.equippedTW = twcount;
			}
		}
		audio_sound_pitch(snd_QuickAccess,0.6);
		audio_play_sound(snd_QuickAccess,5,0);
	}

	if (key_w) //--> Quick Axe/Mace Weapon change key
	{
		var foundaxe = false;
		var axecount = obj_Manticore.equipped_axe;
		while (foundaxe == false)
		{
			axecount += 1;
			if (axecount > 4) axecount = 1;
			if (global.AXES[axecount,1]) && (global.AXES[axecount,2])
			{
				foundaxe = true;
				global.equippedAXE = axecount;
			}
		}
		audio_sound_pitch(snd_QuickAccess,0.8);
		audio_play_sound(snd_QuickAccess,5,0);
	}

	if (key_e) //--> Quick Sword/Lance Weapon change key
	{
		var foundswrd = false;
		var swrdcount = obj_Manticore.equiped_sword;
		while (foundswrd == false)
		{
			swrdcount += 1;
			if (swrdcount > 4) swrdcount = 1;
			if (global.SWORDS[swrdcount,1]) && (global.SWORDS[swrdcount,2])
			{
				foundswrd = true;
				global.equippedSWORD = swrdcount;
			}
		}
		audio_sound_pitch(snd_QuickAccess,1);
		audio_play_sound(snd_QuickAccess,5,0);
	}


}
