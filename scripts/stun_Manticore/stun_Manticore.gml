function stun_Manticore() {
	sprite_index = spr_damage_Manticore;
	image_index = 1;

	//--> REINICIALIZE ALL VARIABLES
	airDash_used = false;
	lghtDash_used = false;
	walljump_used = false;
	wallcling_cooldown = false;
	doublejump_used = false;
	windjump_used = false;
	air_attack = false;
	atk_type = 0;
	attack_number = 1;
	image_blend = c_white;
	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);

	vsp = vsp + grv;

	//--> Platform Collision Check
	 if place_meeting(x,y+vsp,obj_Platform)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Platform )) y += onepixel;
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) vsp = 0;
	}

	//--> Horizontal Collision (Wall)
	if place_meeting(x+hsp,y,obj_Wall)
	{
		var onepixel = sign(hsp);
		while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
		hsp = 0;
	}

	x = x + hsp; //--> Horizontal Movement
	
	//--> Vertical Collision Check
	if place_meeting(x,y+vsp,obj_Wall)
	{
		var onepixel = sign(vsp);
		while (!place_meeting(x,y+onepixel,obj_Wall )) y += onepixel;
		vsp = 0;
	}
	y = y + vsp; //--> Vertical Movement
}
