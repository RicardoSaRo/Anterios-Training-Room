// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Tbot_Standing(){
	//--> bounce var is for ON/OFF
	//--> AtkTurn var is for AtkSpd
	//--> rightvar is for MovSpd
	//--> leftvar is for StunTouch
	
	if (bounce > 1)
	{
		if (sprite_index == spr_TrainingBot_Shot)
		{
			if (image_index >= 8) && (!switch_var2)
			{
				var spBllt = instance_create_layer(x+(40*image_xscale),y-10,"FX_Layer",obj_specialBullet);
				spBllt.image_xscale = -image_xscale;
			
				switch_var2 = true;
			}
			
			if (image_index >= 11.6)
			{
				image_index = 0;
				sprite_index = spr_TrainingBot_Standing;
			}
		}
		else switch_var2 = false;
		
		if (sprite_index == spr_TrainingBot_UTurn)
		{
			if (image_index >= 3.6)
			{
				image_index = 0;
				sprite_index = spr_TrainingBot_Standing;
			}
		}
		
		if (sprite_index == spr_TrainingBot_ON)
		{
			image_speed = 1;
			if (image_index >= 7.6)
			{
				image_index = 0;
				sprite_index = spr_TrainingBot_Standing;
			}
		}
		
		if (sprite_index == spr_TrainingBot_Standing)
		{
			if ((image_xscale==1)&&(x <= 850)) || ((image_xscale==-1)&&(x >= 1580))
			{
				image_xscale *= -1;
				image_index = 0;
				sprite_index = spr_TrainingBot_UTurn;
			}
			
			if (right_var > 1)
			{
				x += right_var * -image_xscale;
			}
		}
		
		if (AtkTurn > 1)
		{
			if (alarm[1] <= 0)
			{
				switch(AtkTurn)
				{
					case 2: alarm[1] = 360; break;
					case 3: alarm[1] = 300; break;
					case 4: alarm[1] = 240; break;
					case 5: alarm[1] = 180; break;
				}
				
				image_index = 0;
				sprite_index = spr_TrainingBot_Shot;
			}
		}
		
		if (left_var > 1)
		{
			defeated_spr = spr_poof;
			if (place_meeting(x,y,obj_Manticore))
			{
				obj_Manticore.Manticore_hp = global.player_max_health;
			}
		}
		else defeated_spr = sprite_index;
	}
	else
	{
		defeated_spr = sprite_index;
		if (sprite_index != spr_TrainingBot_ON)
		{
			if (!switch_var)
			{
				image_index = 0;
				sprite_index = spr_TrainingBot_OFF;
				switch_var = true;
			}
			
			if (image_index > 5.6)
			{
				image_index = 0;
				sprite_index = spr_TrainingBot_ON;
				image_speed = 0;
				switch_var = false;
			}
		}
	}
}