// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_ComboSP(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Kicks : spr_VioletP_Kicks;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		alarm[2] = 50; //--> GUI Special Attack Alarm 
		
		switch_var = true;
	}
	
	if ((image_index >= 4)&&(!switch_var2))
	{
		var wave = instance_create_layer(x+(75*image_xscale),y+(110*image_yscale),"FX_Layer",obj_EnemyBullet)
		wave.priority = 10;
		wave.NoDamage = true;
		wave.switch_var = true;
		wave.sprite_index = spr_StunWave;
		wave.image_xscale = abs(image_xscale);
		wave.image_yscale = image_yscale;
		wave.image_angle = (sign(image_xscale)) ? 290 : 250;
		wave.direction = (sign(image_xscale)) ? 310 : 230;
		
		switch_var2 = true;
	}
	
	if ((image_index >= 8)&&(!switch_var3))
	{
		var wave = instance_create_layer(x+(82*image_xscale),y+(82*image_yscale),"FX_Layer",obj_EnemyBullet)
		wave.priority = 10;
		wave.NoDamage = true;
		wave.switch_var = true;
		wave.direc = 1.5;
		wave.sprite_index = spr_StunWave;
		wave.image_xscale = image_xscale;
		wave.image_yscale = image_yscale;
		wave.image_angle = 0;
		wave.direction = (sign(image_xscale)) ? 0 : 180;
		
		switch_var3 = true;
	}
	
	if (image_index >= 11.6) && ((sprite_index == spr_RadiantP_Kicks)||(sprite_index== spr_VioletP_Kicks))
	{
		if ((obj_Manticore.x>x-400)&&(obj_Manticore.x<x+400))
		{
			image_index = 0;
			
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Swordup : spr_VioletP_Swordup;
			//audio_play_sound(snd_hiss3,10,0);
			
			repeat(30)
			{
				var rage = instance_create_layer(x+random_range(-100,100),y+random_range(-100,100),"FX_Layer",obj_charge_chispa)
				rage.coloroption1 = (global.BossVariant == 0) ? c_red : c_fuchsia;
				rage.coloroption2 = (global.BossVariant == 0) ? c_orange : c_fuchsia;
			}
		
			if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
			else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		}
		else
		{
			//--> Ends State and goes to Standing
			switch(global.difficulty)
			{
				case "casual": alarm[1] = 90; break;
				case "normal": alarm[1] = 80; break;
				case "hard": alarm[1] = 70; break;
				case "lunatic": alarm[1] = 60; break;
			}
	
			if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
			else image_xscale = (global.BossVariant == 2) ? -1 : 2;
			
			image_index = 0;
			switch_var = false;
			switch_var2 = false;
			switch_var3 = false;
			counter = false;
			state = Phoenisk_Standing;
	
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
			AtkTurn += 1;
		}
	}
	
	if ((sprite_index == spr_RadiantP_Swordup)||(sprite_index== spr_VioletP_Swordup))
	{
		image_blend = (global.BossVariant == 0) ? merge_color(c_red,c_orange,0.4) : c_fuchsia;
		
		if (image_index >= 2) && (!instance_exists(obj_Enemy_hitbox))
		{
			var htbx = instance_create_layer(x+(125*image_xscale),y-(60*image_yscale),"FX_Layer",obj_Enemy_hitbox);
			htbx.sprite_index = spr_PhoeniskSwd4;
			htbx.image_xscale = image_xscale;
			htbx.image_yscale = image_yscale;
		}
		
		if (image_index >= 4)
		{
			if (instance_exists(obj_Enemy_hitbox)) instance_destroy(obj_Enemy_hitbox);
		
			if (!counter)
			{
				audio_play_sound(snd_Bomb,10,0);
				
				var expl = instance_create_layer(x+(257*image_xscale),y-(183*image_yscale),"FX_Layer",obj_EnemyBullet);
				expl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_bmb : spr_BigAquaExplosion;
				if (global.BossVariant == 1) expl.image_blend = c_fuchsia;
				expl.image_xscale = image_xscale;
				expl.image_yscale = image_yscale;
				expl.priority = 10;
				expl.NoDamage = true;
				expl.bounce = true;
				
				counter = true;
			}
		}
		
		if (image_index >= 5.6)
		{
			//--> Ends State and goes to Standing
			switch(global.difficulty)
			{
				case "casual": alarm[1] = 90; break;
				case "normal": alarm[1] = 80; break;
				case "hard": alarm[1] = 70; break;
				case "lunatic": alarm[1] = 60; break;
			}
	
			if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
			else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
			image_blend = c_white;
			image_index = 0;
			switch_var = false;
			switch_var2 = false;
			switch_var3 = false;
			counter = false;
			state = Phoenisk_Standing;
	
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
			AtkTurn += 1;
		}
	}
}