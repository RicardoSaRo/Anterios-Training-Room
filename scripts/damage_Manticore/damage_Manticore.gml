function damage_Manticore() {
	if (!global.RELIKS[44,2])
	{
		state = damage_Manticore;

		//--> REINICIALIZE ALL VARIABLES
		airDash_used = false;
		lghtDash_used = false;
		walljump_used = false;
		wallcling_cooldown = false;
		doublejump_used = false;
		windjump_used = false;
		air_attack = false;
		atk_type = 0;
		attack_number = 1;
		if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);

		hsp = (direc * walksp) * -1; //--> Horizontal Speed

		if grv != 1 grv = 1; //--> Returns Gravity to normal

		vsp = vsp + grv; //--> Vertical Speed
	
		onFloor = place_meeting(x,y+1,obj_Wall);

		onWall = place_meeting(x+sign(hsp),y,obj_Wall);

		var sprite_bbox_top = sprite_get_bbox_top(sprite_index) - sprite_get_yoffset(sprite_index);
		var sprite_bbox_bottom = sprite_get_bbox_bottom(sprite_index) - sprite_get_yoffset(sprite_index);

		//--> onPlatform Variable check
		if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
		   (place_meeting(x,y+1,obj_Platform)) dmg_onPlatform = true;
		else dmg_onPlatform = false;

		if (alarm[10] > 0) //--> Duration of damage state
		{
			image_speed = 0;
			sprite_index = spr_damage_Manticore;
			if (alarm[10] >= 9) image_index = irandom(1); //--> Random damage image
		}

		if place_meeting(x,y+vsp,obj_Platform)
		{
			if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top)
			{
				y = instance_nearest(x,y,obj_Platform).bbox_top-56;
				vsp = 0;
			}
		}

		if place_meeting(x+hsp,y,obj_Wall)
		{
			var onepixel = sign(hsp);
			while (!place_meeting(x+onepixel,y,obj_Wall )) x += onepixel;
			hsp = -onepixel * 2; //--> Air Attack pushes further
		}

		if (!onFloor && !dmg_onPlatform) && (vsp == 0) //--> Prevents getting stuck on ceiling while dashing
			while (place_meeting(x,y-1,obj_Wall )) y += 1;
 
		y = y + vsp; //--> Vertical Movement

		//--> BBOX "Y" Collision Check
		if place_meeting(x,y,obj_Wall)
		{
		    var wall = instance_place(x,y,obj_Wall);
		    if (vsp > 0) y = (wall.bbox_top-1) - sprite_bbox_bottom;
		    else if (vsp < 0)
			{
		        y = (wall.bbox_bottom+1) - sprite_bbox_top;
		    }
		    vsp = 0;
		}
	}
	else //-->> if Venomlord Scalemail is equipped
	{
		state = move_Manticore();
		alarm[10] = 0;
	}
	
	if (alarm[10] <= 1)
	{
		switch(global.difficulty)
		{
			case "casual":	alarm[11] = (global.RELIKS[33,2]) ? 200 : 100; //--> Invincibility
							state = move_Manticore;
							break;
			case "normal":	alarm[11] = (global.RELIKS[33,2]) ? 160 : 80; //--> Invincibility
							state = move_Manticore;
							break;
			case "hard":	alarm[11] = (global.RELIKS[33,2]) ? 120 : 60; //--> Invincibility
							state = move_Manticore;
							break;
			case "lunatic": alarm[11] = (global.RELIKS[33,2]) ? 80 : 40; //--> Invincibility
							state = move_Manticore;
							break;
		}
	}

	if (global.RELIKS[48,2]) global.player_max_health = Manticore_hp;

	//-->> Heavy Cooldown for Regen when hit
	if (Manticore_hp != global.player_max_health) && (!regen_stop)
	{
		var mReg = 500;
		if (instance_exists(obj_RegenBubble)) if (!obj_RegenBubble.permanent) mReg = 60;
		if (mReg == 500)
		{
			if (global.RELIKS[30,2]) mReg = (global.RELIKS[5,2]) ? 250 : 280;
			else if (global.RELIKS[5,2]) mReg = 385;
		}
		var mnger = global.currentManager;
		if (!mnger.ManticoreVenomed)
		{
			alarm[5] = mReg;
			artist.max_Regen = mReg;
		}
		else
		{
			var plus1 = (global.RELIKS[30,2]) ? 50 : 0;
			var plus2 = (global.RELIKS[5,2]) ? 50 : 0;
			alarm[5] = mReg + plus1 + plus2;
			artist.max_Regen = mReg + plus1 + plus2;
		}
	}
}
