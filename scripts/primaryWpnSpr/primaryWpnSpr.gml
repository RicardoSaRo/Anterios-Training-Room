/// @description primaryWpnSpr(sprite_index)
/// @param spr
function primaryWpnSpr(argument0) {

	var spr = argument0;

	switch(spr)
	{
		case spr_Argenta: case spr_ArgentaMix: case spr_AirArgenta: case spr_AirArgentaMix: case spr_Dyrnwyn:
		case spr_DyrnwynMix: case spr_AirDyrnwyn: case spr_AirDyrnwynMix: case spr_GirTab: case spr_GirTabMix:
		case spr_GirTabUP: case spr_AirGirTab: case spr_AirGirTabMix: case spr_AirGirTabUP: case spr_GirTrident:
		case spr_GirTridentMix: case spr_GirTridentUP: case spr_AirGirTrident: case spr_AirGirTridentMix:
		case spr_AirGirTridentUP: case spr_Frostbiter: case spr_FrostbiterMix: case spr_FrostbiterUP:
		case spr_AirFrostbiter: case spr_AirFrostbiterMix: case spr_AirFrostbiterUP: case spr_Korunax:
		case spr_AirKorunax: case spr_Mirage: case spr_AirMirage: case spr_Mjolnir: case spr_AirMjolnir:
		case spr_Whisperer: case spr_AirWhisperer: case spr_Dyrnwyn_DashStab: case spr_Argenta_DashStab:
			return true;
	
		default: return false;
	}


}
