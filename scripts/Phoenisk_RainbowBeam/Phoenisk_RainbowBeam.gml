// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_RainbowBeam(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathStart : spr_VioletP_Breath_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		image_index = 0;
		counter = 0;
		switch(global.difficulty)
		{
			case "casual":	bounce = choose(2,2,3); break;
			case "normal":	bounce = choose(2,3,3,4); break;
			case "hard":	bounce = choose(3,4,4,5); break;
			case "lunatic": bounce = choose(4,5,5,6); break;
			default:		bounce = 2; break;
		}
		
		switch_var = true;
	}
	
	if (image_index >= 3.6) && ((sprite_index == spr_RadiantP_BreathStart)||(sprite_index == spr_VioletP_Breath_start))
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Breath : spr_VioletP_Breath;
	}
	
	if (image_index >= 2.6) && ((sprite_index == spr_RadiantP_Breath)||(sprite_index == spr_VioletP_Breath))
	{
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathSP : spr_VioletP_BreathSP;
	}
	
	if (sprite_index == spr_RadiantP_BreathSP) || (sprite_index == spr_VioletP_BreathSP)
	{
		if (!switch_var2)
		{
			var RB = instance_create_layer(x+(80*image_xscale),y+(35*image_yscale),"FX_Layer",obj_EnemyBullet);
			RB.sprite_index = spr_RainbowBeam_standing;
			RB.image_xscale = 0.1;
			RB.image_yscale = 0.1;
			RB.bounce = choose(3,3,4);
			RB.priority = 10;
			RB.ownership = self;
			RB.NoDamage = true;
			RB.amount = global.currentManager; //--> For RainbowBlend
			
			counter ++;
			alarm[4] = 60;
			switch_var2 = true;
		}
		
		if (counter >= bounce) && (alarm[4] <= 0)
		{
			image_index = 0;
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_BreathEnd : spr_VioletP_Breath_end;
		}
	}
	
	if (sprite_index == spr_RadiantP_BreathEnd) || (sprite_index == spr_VioletP_Breath_end)
	{
		if (image_index >= 3.6)
		{
			//--> Ends State and goes to Standing
			switch(global.difficulty)
			{
				case "casual": alarm[1] = 220; break;
				case "normal": alarm[1] = 200; break;
				case "hard": alarm[1] = 180; break;
				case "lunatic": alarm[1] = 140; break;
			}
	
			if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
			else image_xscale = (global.BossVariant == 2) ? -1 : 2;
	
			image_blend = c_white;
			image_index = 0;
			counter = 0;
			bounce = 0;
			switch_var = false;
			switch_var2 = false;
			switch_var3 = false;
			state = Phoenisk_Standing;
	
			sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Standing : spr_VioletP_Standing;
	
			AtkTurn += 1;
		}
	}
}