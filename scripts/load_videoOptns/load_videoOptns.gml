/// @description load_videoOptns(WinMode,Vsync,AlfaBlend,ResW,ResH)
/// @param DWinMode
/// @param DVsync
/// @param DAlfaBlend
/// @param DResW
/// @param DResH
function load_videoOptns() {

	if (file_exists("Options.ini"))
	{
		ini_open("Options.ini");
		var DWinMode =	ini_read_real("VideoOptions","WindowMode",1);
		var DVsync =		ini_read_real("VideoOptions","Vsync",0);
		var DAlfaBlend = ini_read_real("VideoOptions","AlfaBlend",1);
		var DResW =		ini_read_real("VideoOptions","ResWidth",960);
		var DResH =		ini_read_real("VideoOptions","ResHeight",540);
		ini_close();
	}

	var video_info_array = array_create(5,0);
	video_info_array[1] = DWinMode;
	video_info_array[2] = DVsync;
	video_info_array[3] = DAlfaBlend;
	video_info_array[4] = DResW;
	video_info_array[5] = DResH;

	return video_info_array;


}
