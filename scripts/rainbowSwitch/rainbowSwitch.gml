// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function rainbowSwitch(){
	if (gradient == 1)
	{
		switch(nextColor)
		{
			case c_red: prevColor = c_red; nextColor = c_purple; break;
			case c_orange: prevColor = c_orange; nextColor = c_yellow; break;
			case c_yellow: prevColor = c_yellow; nextColor = c_lime; break;
			case c_lime: prevColor = c_lime; nextColor = c_aqua; break;
			case c_purple: prevColor = c_purple; nextColor = c_green; break;
			case c_green: prevColor = c_green; nextColor = c_blue; break;
			case c_blue: prevColor = c_blue; nextColor = c_red; break;
			case c_aqua: prevColor = c_aqua; nextColor = c_fuchsia; break;
			case c_fuchsia: prevColor = c_fuchsia; nextColor = c_orange; break;
		}
		
		gradient = 0;
	}
	
	gradient = gradient + grdntSpd;
	gradient = clamp(gradient,0,1);
	rainbow_blend = merge_color(prevColor,nextColor,gradient);
}