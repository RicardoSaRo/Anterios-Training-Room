function Icelord() {
	//--> Ice HoloFamiliar - Icelord

	if (image_alpha < 1) image_alpha += 0.05;
	
	if (sprite_index == spr_IceFamiliar_Standing)
	{
		var ManticoreX = obj_Manticore.x + (145 * obj_Manticore.image_xscale);
		var ManticoreY = obj_Manticore.y - 150;

		//--> Speed to chatch up player
		var shieldSPD = hsp;
		var max_acc = 0.015;
		hsp += 0.0020;
		hsp = clamp(hsp,0,max_acc);
		shieldSPD = hsp;
	
		x = lerp(x,ManticoreX,shieldSPD);
		y = lerp(y,ManticoreY,0.025);
	
		if (image_index >= 6.6)
		{
			image_index = 0;
		
			if (obj_BossEnemy.x >= x)
			{
				if (image_xscale == -1.5)
				{
					image_xscale = 1.5;
					image_alpha = 0;
				}
			}
			else
			{
				if (image_xscale == 1.5)
				{
					image_xscale = -1.5;
					image_alpha = 0;
				}
			}
		}
	}

	if ((sprite_index == spr_IceFamiliar_Attack) && (image_index >= 5.6)) ||
	   ((sprite_index == spr_IceFamiliar_Create) && (image_index >= 15.6))
	{
		sprite_index = spr_IceFamiliar_Standing;
		image_index = 0;
	}

	if (alarm[0] <= 0) && (!switch_var)
	{
		var distance = distance_to_object(obj_BossEnemy);
		var FamAttk = (distance<300) ? 0 : ((distance<600) ? 1 : 2);

		switch(FamAttk)
		{
			case 1:
				amount = choose(1,0);
				if (amount)
				{
					for(var i = 1; i <= 6; i++)
					{
						var ispkball = instance_create_layer(x,y,"Info_Layer",obj_TWSpecial);
						ispkball.sprite_index = spr_IceSpikeball;
						ispkball.Consum = false;
						ispkball.image_xscale = 0.5;
						ispkball.image_yscale = 0.5;
						ispkball.alarm[1] = 120;
						ispkball.vsp = -27;
						ispkball.hsp = -10 + (i * 3);
					}
				}
				else
				{
					if (instance_exists(obj_Iceball)) with(obj_Iceball) ib_hitpoints = 0;
					for(var i = 1; i <= 2; i++)
					{
						var iceball = instance_create_layer(x,y,"Info_Layer",obj_Iceball);
						iceball.sprite_index = spr_Iceball;
						iceball.ib_hsp = (i == 1) ? 15 : -15;
						iceball.ib_vsp = -25;
						iceball.ib_hitpoints = 100;
						iceball.owner = self;
					}
				}
				break;
			
			case 2:
				amount = choose(1,0);
				if (amount)
				{
					var var icebeam = instance_create_layer(x+(30*image_xscale),y-160,"Info_Layer",obj_TWSpecial);
					icebeam.sprite_index = spr_NeonLaser;
					icebeam.Consum = false;
					icebeam.image_xscale = image_xscale;
					icebeam.image_yscale = 2;
					icebeam.image_angle = (image_xscale) ? 225 : 135;
					icebeam.image_blend = c_aqua;
					icebeam.image_speed = 0.5;
					icebeam.instnc = self;
				}
				else
				{
					var xscl = (image_xscale == 1.5) ? 1 : -1;
					var implosion = instance_create_layer(x+(30*image_xscale),y-160,"Info_Layer",obj_TWSpecial);
					implosion.sprite_index = spr_IceImplosion;
					implosion.Consum = false;
					implosion.image_xscale = 0.5 * xscl;
					implosion.image_yscale = 0.5;
					implosion.image_angle = random(360);
					implosion.amount = 8 + global.SWORDS[4,3];
				}
				break;
			
			case 0:
				var hurricane = instance_create_layer(x,y,"FX_layer",obj_TWSpecial);
				hurricane.sprite_index = spr_FrostBulwark;
				hurricane.Consum = false;
				hurricane.image_alpha = 0;
				hurricane.image_yscale = -1;
				hurricane.alarm[0] = 6 - global.SWORDS[4,3];
				break;
		}
	
		alarm[0] = 140;
		sprite_index = spr_IceFamiliar_Attack;
		image_index = 0;
	}

	if (alarm[1] <= 0) && (!switch_var)
	{
		sprite_index = spr_IceFamiliar_Crumbles;
		image_index = 0;
		switch_var = true;
	}

	if (place_meeting(x,y,obj_EnemyBullet))
	{
		var instnear = instance_nearest(x,y,obj_EnemyBullet);
		if (instnear.priority < 10) && (instnear.sprite_index != instnear.destroy_sprite)
			alarm[1] -= 30;
	}

	if (sprite_index == spr_IceFamiliar_Crumbles) && (image_index >= 6.6) instance_destroy();


}
