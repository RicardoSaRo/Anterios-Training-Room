function wallattack_Manticore() {
	//Wallattack Manticore

	if (sprite_index == spr_Manticore_wallcling) //-->> inicialize stuff for Wall Attack state
	{
		snd_Stop_Manticore();
	
		sprite_index = spr_Manticore_wallattack;
		var WallAType = 1;
		var WAcolor = c_white;
	
		if (cling_key_d) || (cling_key_a)
		{
			switch(equiped_sword)
			{
				case 1:	WallAType = 1; WAcolor = c_white; break;
				case 2:	WallAType = 2; WAcolor = c_orange; break;
				case 3:	WallAType = 3; WAcolor = c_red; break;
				case 4:	WallAType = 4; WAcolor = c_aqua; break;
			}
		
			airDash_used = false; //--> Refreshes Air Dash
			lghtDash_used = false;
			doublejump_used = false; //--> Refreshes Doublejump
			windjump_used = false;
			prevVSP = false;
		}
		else
		{
			if (cling_key_s)
			{
				switch(equipped_axe)
				{
					case 1:	WallAType = 5; WAcolor = make_color_rgb(211,151,65); break;
					case 2:	WallAType = 6; WAcolor = c_fuchsia; break;
					case 3:	WallAType = 7; WAcolor = c_blue; break;
					case 4:	WallAType = 8; WAcolor = c_lime; break;
				}
			}
		}
	
		var objWallAtk = instance_create_layer(x,y,"Info_Layer",obj_Wallattack); //-->> Creates Wall Attack
		objWallAtk.WAtype = WallAType;
		objWallAtk.owner = self;
		objWallAtk.image_blend = WAcolor;
		objWallAtk.image_xscale = image_xscale;
	
		for(var i=1; i<=1; i++) //-->> Wind FX when dashing
		{
			var windash = instance_create_layer(x+(50*image_xscale),y,"FX_Layer",obj_centerXYfollow);
			windash.sprite_index = spr_windash;
			windash.obj_2_follow = self;
			windash.anim_end = 4;
			windash.brightness = true;
			windash.image_blend = c_yellow;
			windash.image_alpha = 0.6;
			windash.image_xscale = 1.5 * image_xscale;
			var stretch = (i==2) ? 0.5 * image_xscale : 0;
			windash.image_yscale = 1.3 + stretch;
			windash.image_speed = 2;
		}
	}

		if place_meeting(x+(image_xscale*25),y,obj_Wall)
		{
			y = clamp(y,Boundary_minY,Boundary_maxY-1);
			x = clamp(x,Boundary_minX+1,Boundary_maxX-1);
			if (y != Boundary_maxY-1)
			{
				state = wallcling_Manticore;
				alarm[0] = 0;
				if (!global.RELIKS[15,2]) alarm[3] = 60;
			}

			atk_type = 0;
			attack_number = 1;
			exit;
		}

	//--> Invulnerability if hit Bosses
	var hitBoss = place_meeting(x+(image_xscale*25),y,obj_BossEnemy);
	if (hitBoss)
	{
		if (alarm[11] <= 20) alarm[11] = 20;
		image_blend = c_yellow;
	}

	if (x < Boundary_minX) or (x > Boundary_maxX)
	{
		atk_type = 0;
		attack_number = 1;
		exit;
	}

	state = wallattack_Manticore;

	vsp = 0;

	//-->> Dash Movement
	x += image_xscale * 25;
	
	//--> Afterimage animation
	if (alarm[0] > 1)  and (alarm[0] mod 2 == 1)
	{
		var afterimage = instance_create_layer(x,y,"Player_Layer",dash_afterimage);
		afterimage.sprite_index = sprite_index;
		afterimage.image_index = image_index;
		afterimage.image_xscale = image_xscale;
		afterimage.image_yscale = image_yscale;
		afterimage.glowFX = true;
		afterimage.image_alpha = 0.6;
		afterimage.image_blend = (hitBoss) ? c_yellow : c_aqua;
		if (!audio_is_playing(snd_Dash))
		{
			audio_sound_gain(snd_Dash,0.2,0);
			audio_sound_pitch(snd_Dash,1.1);
			audio_play_sound(snd_Dash,20,0);
		}
	}

	//--> Refreshes Air Dash when touches wall object
	if (!place_meeting(x,y+vsp,obj_Wall)) airDash_used = false;

	//--> Refreshes Attack Variables
	atk_type = 0;
	attack_number = 1;

	//--> Destroys Hitbox is exits
	if (instance_exists(obj_Manticore_hitbox)) instance_destroy(obj_Manticore_hitbox);
}
