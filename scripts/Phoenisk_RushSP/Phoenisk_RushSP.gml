// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Phoenisk_RushSP(){
	if (!switch_var)
	{
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Rush_start : spr_VioletP_Rush_start;
		//audio_play_sound(snd_hiss3,10,0);
		
		if (obj_Manticore.x <= x) image_xscale = (global.BossVariant == 2) ? -1 : -2;
		else image_xscale = (global.BossVariant == 2) ? -1 : 2;
		
		alarm[2] = 50; //--> GUI Special Attack Alarm 
		
		repeat(30)
		{
			var rage = instance_create_layer(x+random_range(-100,100),y+random_range(-100,100),"FX_Layer",obj_charge_chispa)
			rage.coloroption1 = (global.BossVariant == 0) ? c_red : c_fuchsia;
			rage.coloroption2 = (global.BossVariant == 0) ? c_orange : c_fuchsia;
		}
		
		image_blend = (global.BossVariant == 0) ? merge_color(c_red,c_orange,0.4) : c_fuchsia;
		image_index = 0;
		switch_var = true;
	}
	
	if (image_index >= 3.6) && (!switch_var2)
	{
		audio_play_sound(snd_ExplosionUpDash,10,0);
				
		var expl = instance_create_layer(x-(205*image_xscale),y+(65*image_yscale),"FX_Layer",obj_EnemyBullet);
		expl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_bmb : spr_BigAquaExplosion;
		if (global.BossVariant == 1) expl.image_blend = c_fuchsia;
		expl.image_xscale = image_xscale;
		expl.image_yscale = image_yscale;
		expl.priority = 10;
		expl.NoDamage = true;
		expl.bounce = true;
		
		var wave = instance_create_layer(x+(60*image_xscale),y+(65*image_yscale),"FX_Layer",obj_fadeAway);
			wave.sprite_index = spr_StunWave;
			wave.image_alpha = choose(0.5,0.6,0.7);
			wave.image_xscale = image_xscale * 4;
			wave.image_yscale = image_yscale * 2;
			wave.fadeSpd = choose(0.05,0.1,0.2);
			wave.Ymovement = 0;
			wave.Xmovement = 20 * image_xscale;
		
		image_index = 0;
		sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Rush : spr_VioletP_Rush;
		
		switch(global.difficulty)
		{
			case "casual":	counter = (global.ruins_tier <= 3) ? choose(1,1,1,2) : choose(1,2); break;
			case "normal":	counter = (global.ruins_tier <= 3) ? choose(1,1,2,2,3) : choose(1,2,3,3); break;
			case "hard":	counter = (global.ruins_tier <= 3) ? choose(2,3) : choose(2,3,4); break;
			case "lunatic":	counter = (global.ruins_tier <= 3) ? choose(3,4) : choose(3,4,5); break;
		}
		switch_var2 = true;
	}
	
	if (sprite_index == spr_RadiantP_Rush) || (sprite_index == spr_VioletP_Rush)
	{
		if (image_index >= 2)
		{
			var wave = instance_create_layer(x+(6*image_xscale),y+(65*image_yscale),"FX_Layer",obj_fadeAway);
			wave.sprite_index = spr_StunWave;
			wave.image_alpha = choose(0.5,0.6,0.7);
			wave.image_xscale = image_xscale * 4;
			wave.image_yscale = image_yscale * 2;
			wave.fadeSpd = choose(0.05,0.1,0.2);
			wave.Ymovement = 0;
			wave.Xmovement = 20 * image_xscale;
		}
		
		if (place_meeting(x,y,obj_Manticore))
		{
			//obj_Manticore.y = y+(65*image_yscale);
			obj_Manticore.hsp = 30 * image_xscale;
			if ((obj_Manticore.state != stun_Manticore)&&(obj_Manticore.state != dash_Manticore))&&
			   (obj_Manticore.alarm[11]<= 0)
			{
				audio_play_sound(snd_Dragiankin_clap,6,0);
				
				obj_Manticore.state = stun_Manticore;
				obj_Manticore.alarm[6] = 15;
				
				var rainbowCircle2 = instance_create_layer(x,y,"FX_Layer",obj_fadeAway);
				rainbowCircle2.sprite_index = spr_Smack;
				rainbowCircle2.image_index = irandom(4);
				rainbowCircle2.image_alpha = random_range(0.8,1);
				rainbowCircle2.image_speed = 0;
				rainbowCircle2.fadeSpd = random_range(0.03,0.05);
				rainbowCircle2.Ymovement = 0;
				rainbowCircle2.Xmovement = 0;
				rainbowCircle2.sizeMod = random_range(0.01,0.05);
				var circSiz = random_range(0.5,1.5);
				rainbowCircle2.image_xscale = circSiz;
				rainbowCircle2.image_yscale = circSiz;
			}
			
			artist.burnFX = true;
		
			with(global.currentManager)
			{
				ManticoreBurn = true;
				switch(global.difficulty)
				{
					case "casual":	alarm[4] = 240;	break;
					case "normal":	alarm[4] = 300; break;
					case "hard":	alarm[4] = 360; break;
					case "lunatic":	alarm[4] = 420; break;
				}
			}
		}
		
		if (place_meeting(x+(20*image_xscale),y,obj_Wall))
		{
			bounce++;
			
			audio_play_sound(snd_Bomb,10,0);
			
			//--> Explosion when collides with walls
			var expl = instance_create_layer(x+(95*image_xscale),y+(65*image_yscale),"FX_Layer",obj_EnemyBullet);
			expl.sprite_index = (global.BossVariant == 0) ? spr_Explosion_bmb : spr_BigAquaExplosion;
			if (global.BossVariant == 1) expl.image_blend = c_fuchsia;
			expl.image_xscale = image_xscale;
			expl.image_yscale = image_yscale;
			expl.priority = 10;
			expl.NoDamage = true;
			expl.bounce = true;
			
			//--> Shake camera FX
			global.camera_shake = true;
			global.cam_shake_amount = 5;
			var mng = global.currentManager;
			mng.alarm[0] = 20;
			
			with(obj_Manticore) //--> Stuns Manticore if is on Floor or Platform
			{
				//--> onPlatform Variable check
				var onPlat = false;
				if (bbox_bottom < instance_nearest(x,y,obj_Platform).bbox_top) &&
				   (place_meeting(x,y+1,obj_Platform)) onPlat = true;
				else onPlat = false;
				
				if (place_meeting(x,y+1,obj_Wall))||(onPlat)//--> Stun
				{
					switch(global.difficulty)
					{
						case "casual": var stun = 100-12; break;
						case "normal": var stun = 100-16; break;
						case "hard": var stun = 100-20; break;
						case "lunatic": var stun = 100-24; break;
					}
					if (alarm[11] <= stun)
					{
						state = stun_Manticore;
						switch(global.difficulty)
						{
							case "casual": alarm[6] = 12; break;
							case "normal": alarm[6] = 16; break;
							case "hard": alarm[6] = 20; break;
							case "lunatic": alarm[6] = 24; break;
						}
					}
				}
			}
				
			if (bounce < counter)
			{
				image_xscale *= -1;
				x += 100 * image_xscale;
			}
			else
			{
				image_xscale *= -1;
				switch_var2 = false;
				counter = 0;
				bounce = 0;
				image_blend = c_white;
				image_index = 0;
				sprite_index = (global.BossVariant == 0) ? spr_RadiantP_Dash : spr_VioletP_Dash;
				state = Phoenisk_Dash;
			}
		}
		else
		{
			if (!place_meeting(x,y,dash_afterimage))
			{
				var afterimage = instance_create_layer(x,y,"Back_FX_Layer",dash_afterimage);
				afterimage.sprite_index = sprite_index;
				afterimage.image_xscale = image_xscale;
				afterimage.image_yscale = image_yscale;
				afterimage.glowFX = true;
				afterimage.image_alpha = 0.6;
				afterimage.image_blend = (global.BossVariant == 0) ? merge_color(c_red,c_orange,0.4) : c_fuchsia;
			}
				
			x += (20 * image_xscale);
		}
	}
}