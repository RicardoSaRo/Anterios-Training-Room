/// @description weakness_check(obj,wpn_sprite)
/// @param obj
/// @param wpn_sprite
/// @param dmg
function weakness_check(argument0, argument1) {

	var enemy = argument0;
	var weapon = argument1;
	var weakness = 0;

	switch (enemy.enemy_name)
	{
		case "Test_Enemy":	
			if (weapon == spr_Frostbiter) weakness = -1; 
			if (weapon == spr_Korunax) weakness = 1;
			if (weapon == spr_Argenta)or(weapon == spr_AirArgenta) weakness = noone;
			if (weapon == spr_Dyrnwyn)or(weapon == spr_AirDyrnwyn) weakness = 0;
			break;
		case "GolemB": case "GolemG":	
			switch(weapon)
			{
				//--> Golem Strengths
				case spr_Argenta: case spr_AirArgenta: case spr_IceImplosion: case spr_willowisp:
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_Argenta_wave2: case spr_StrAtk_WhispererDyrnwyn:
				case spr_Triblader:	case spr_Guillotine: case spr_AirArgentaMix: case spr_Dyrnwyn_DashStab:
				case spr_ArgentaMix: case spr_AirDyrnwynMix: case spr_DyrnwynMix: case spr_StrAtk_Mirage2:
				case spr_ShurikenSP: case spr_GuillotineSP: case spr_StarbladesSP: case spr_TribladerSP:
				case spr_flametongues: case spr_Mirage: case spr_AirMirage: case spr_Argenta_DashStab:
				case spr_StrAtk_Mirage1: weakness = -1; break;
				//--> Golem Weaknesses
				case spr_Explosion_bmb: case spr_Explosion_grnd: case spr_Explosion_RExp1: case spr_BombSP:
				case spr_Explosion_RExp2: case spr_Korunax: case spr_AirKorunax: case spr_Mjolnir:
				case spr_AirMjolnir: case spr_Whisperer: case spr_AirWhisperer: case spr_relampago:
				case spr_Lightball: case spr_TWave: case spr_Explosion_hmmgF: case spr_StrAtk_Korunax1:
				case spr_RemoteExpSP: case spr_wavelightning: case spr_StrAtk_Korunax2:
				case spr_StrAtk_Mjolnir1: case spr_StrAtk_Mjolnir2: weakness = 1; break;
				//--> Golem Immunities
				case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp: case spr_FrostbiterMix: 
				case spr_windslash: case spr_Chakram: case spr_Kunai: case spr_Assegai: case spr_FrostBulwark:
				case spr_Shuriken: case spr_Starblades: case spr_Iceball: case spr_TornadoINNER_back:
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_FrostbiterUP: case spr_KunaiSP:
				case spr_AirFrostbiterUP: case spr_TornadoINNER_front: case spr_AirFrostbiterMix: case spr_ChakramSP:
				case spr_AssegaiSP: case spr_Whirlwind: case spr_windjmp_fx: case spr_IceSpikeball: case spr_NeonLaser:
					weakness = noone; break;
			}
			break;
		case "Werewolf_Brown": case "Werewolf_Pink": case "Werewolf_Gray": case "Werewolf_Foxy":
			switch(weapon)
			{
				//--> Werewolf Strengths
				case spr_windslash: case spr_Frostbiter: case spr_AirFrostbiter: case spr_FrostBulwark:
				case spr_FrostbiterMix: case spr_AirFrostbiterMix: case spr_FrostbiterUP: case spr_AirFrostbiterUP:
				case spr_Iceball: case spr_IceSpikeball: case spr_NeonLaser: case spr_IceImplosion:
				case spr_windjmp_fx: case spr_StrAtk_Whisperer1: case spr_StrAtk_Whisperer2:
				case spr_Whirlwind: case spr_StrAtk_KorunaxFrostbiter: weakness = -1; break;
				//--> Werewolf Weaknesses
				case spr_Argenta: case spr_AirArgenta: case spr_Argenta_wave2: case spr_Triblader: 
				case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp: case spr_ArgentaMix:
				case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_AirDyrnwyn: case spr_TribladerSP:
				case spr_Dyrnwyn: case spr_Assegai: case spr_AirArgentaMix: case spr_DyrnwynMix:
				case spr_AirDyrnwynMix: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2: case spr_Argenta_DashStab:
				case spr_Dyrnwyn_DashStab: case spr_StrAtk_WhispererDyrnwyn: case spr_AssegaiSP: weakness = 1; break;
			}
			break;
		case "Werewolf_Gold":
			switch(weapon)
			{
				//--> Werewolf Strengths
				case spr_windslash: case spr_Frostbiter: case spr_AirFrostbiter: case spr_FrostBulwark:
				case spr_FrostbiterMix: case spr_AirFrostbiterMix: case spr_FrostbiterUP: case spr_AirFrostbiterUP:
				case spr_Iceball: case spr_IceSpikeball: case spr_NeonLaser: case spr_IceImplosion:
				case spr_windjmp_fx: case spr_StrAtk_Whisperer1: case spr_StrAtk_Whisperer2: case spr_Argenta_wave2:
				case spr_Whirlwind: case spr_StrAtk_KorunaxFrostbiter: weakness = -1; break;
				//--> Werewolf Weaknesses
				case spr_Argenta: case spr_AirArgenta: case spr_Triblader:
				case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp: case spr_ArgentaMix:
				case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_AirDyrnwyn: case spr_TribladerSP:
				case spr_Dyrnwyn: case spr_Assegai: case spr_AirArgentaMix: case spr_DyrnwynMix:
				case spr_AirDyrnwynMix: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2: case spr_Argenta_DashStab:
				case spr_Dyrnwyn_DashStab: case spr_StrAtk_WhispererDyrnwyn: case spr_AssegaiSP: weakness = 1; break;
			}
			break;
		case "OCrab":
			switch(weapon)
			{
				//--> Crab Strengths
				case spr_Assegai: case spr_AssegaiSP: case spr_Kunai: case spr_KunaiSP: case spr_flametongues:
				case spr_Mirage: case spr_AirMirage: case spr_StrAtk_Whisperer1: case spr_StrAtk_Whisperer2:
				case spr_hmmng_flame: case spr_Dyrnwyn: case spr_DyrnwynMix: case spr_AirDyrnwyn: 
				case spr_AirDyrnwynMix: case spr_Dyrnwyn_DashStab: case spr_Explosion_RExp2:
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_FrostBulwark: case spr_FrostbiterMix:
				case spr_AirFrostbiterMix: case spr_FrostbiterUP: case spr_AirFrostbiterUP:
				case spr_Iceball: case spr_IceSpikeball: case spr_NeonLaser: case spr_IceImplosion:
				case spr_BombSP: case spr_Explosion_bmb: weakness = -1; break;
				//--> Crab Weaknesses
				case spr_Mjolnir: case spr_AirMjolnir: case spr_TWave: case spr_wavelightning:
				case spr_relampago: case spr_relampago2: case spr_relampago3: case spr_relampago4:
				case spr_StrAtk_Mjolnir1: case spr_StrAtk_Mjolnir2: case spr_Korunax: case spr_AirKorunax:
				case spr_StrAtk_Korunax1: case spr_StrAtk_Korunax2: case spr_StrAtk_KorunaxFrostbiter:
				case spr_Lightball: weakness = 1; break;
				//--> Crab Immunities
				case spr_willowisp: case spr_windslash: case spr_Explosion_hmmgF: case spr_Explosion_RExp1:
				case spr_Explosion_grnd: case spr_StrAtk_WhispererDyrnwyn: weakness = noone; break;
			}
			break;
		case "GCrab":
			switch(weapon)
			{
				//--> Crab Strengths
				case spr_Assegai: case spr_AssegaiSP: case spr_Kunai: case spr_KunaiSP: case spr_flametongues:
				case spr_Mirage: case spr_AirMirage: case spr_StrAtk_Whisperer2: case spr_StrAtk_Mirage2:
				case spr_hmmng_flame: case spr_Dyrnwyn: case spr_DyrnwynMix: case spr_AirDyrnwyn: 
				case spr_AirDyrnwynMix: case spr_Dyrnwyn_DashStab: case spr_TWave: case spr_wavelightning:
				case spr_relampago3: case spr_relampago4: case spr_StrAtk_Mirage1: weakness = -1; break;
				//--> Crab Weaknesses
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_FrostBulwark: case spr_FrostbiterMix:
				case spr_AirFrostbiterMix: case spr_FrostbiterUP: case spr_AirFrostbiterUP:
				case spr_Iceball: case spr_IceSpikeball: case spr_NeonLaser: case spr_IceImplosion:
				case spr_Korunax: case spr_AirKorunax: case spr_StrAtk_Korunax1:
				case spr_StrAtk_Korunax2: case spr_StrAtk_KorunaxFrostbiter: weakness = 1; break;
				//--> Crab Immunities
				case spr_willowisp: case spr_windslash: case spr_Explosion_hmmgF: case spr_Explosion_RExp1:
				case spr_Explosion_grnd: case spr_StrAtk_WhispererDyrnwyn: case spr_Explosion_RExp2:
				case spr_relampago: case spr_relampago2: case spr_Mjolnir: case spr_AirMjolnir:
				case spr_Lightball:	case spr_BombSP: case spr_Explosion_bmb: case spr_StrAtk_Mjolnir1: 
				case spr_StrAtk_Whisperer1: case spr_StrAtk_Mjolnir2: weakness = noone; break;
			}
			break;
		case "BDragonkin":
			switch(weapon)
			{
				//--> Werewolf Strengths
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_hmmng_flame: case spr_Flarestar: case spr_DyrnwynMix:
				case spr_Flarestar_create: case spr_Flarestar_exp: case spr_Explosion_hmmgF: case spr_AirDyrnwynMix:
				case spr_Dyrnwyn_DashStab: case spr_flametongues: weakness = -1; break;
				//--> Werewolf Weaknesses
				case spr_Whisperer: case spr_AirWhisperer: case spr_windslash: case spr_TornadoINNER_back:
				case spr_TornadoINNER_front: case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front:
				case spr_Shuriken: case spr_Kunai: case spr_Starblades: case spr_Frostbiter: case spr_AirFrostbiter:
				case spr_FrostBulwark: case spr_FrostbiterUP: case spr_AirFrostbiterUP: case spr_FrostbiterMix:
				case spr_AirFrostbiterMix: case spr_Iceball: weakness = 1; break;
			}
			break;
		case "Wastipede":
			switch(weapon)
			{
				//--> Wastipede Strengths
				case spr_Argenta: case spr_AirArgenta: case spr_Chakram: case spr_AirArgentaMix: case spr_KunaiSP:
				case spr_Argenta_wave2: case spr_Assegai: case spr_Kunai: case spr_ChakramSP: case spr_FrostBulwark: 
				case spr_Triblader: case spr_TribladerSP: case spr_Argenta_DashStab: case spr_ArgentaMix:
				case spr_AssegaiSP: weakness = -1; break;
				//--> Wastipede Weaknesses
				case spr_Whisperer: case spr_AirWhisperer: case spr_windslash: case spr_TornadoINNER_back:
				case spr_TornadoINNER_front: case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front:
				case spr_Shuriken: case spr_Starblades: case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_ShurikenSP:
				case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp:
				case spr_Mirage: case spr_AirMirage: case spr_willowisp:  case spr_Guillotine: case spr_Explosion_hmmgF:
				case spr_Korunax: case spr_AirKorunax: case spr_TWave: case spr_StrAtk_Mirage1:
				case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_StarbladesSP: case spr_GuillotineSP:
				case spr_StrAtk_Mirage2: case spr_StrAtk_Korunax1: case spr_StrAtk_Korunax2: case spr_Whirlwind:
				case spr_StrAtk_KorunaxFrostbiter: case spr_StrAtk_Whisperer2: case spr_StrAtk_Whisperer1:
				case spr_StrAtk_WhispererDyrnwyn: case spr_windjmp_fx: weakness = 1; break;
			
			}
			break;
		case "Meetonak":	
			switch(weapon)
			{
				//--> Meetonak Strengths
				case spr_Korunax: case spr_AirKorunax: case spr_Mjolnir: case spr_AirMjolnir: case spr_Whisperer:
				case spr_AirWhisperer: case spr_windslash: case spr_TornadoINNER_back: case spr_TornadoINNER_front:
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_relampago: case spr_TWave:
				case spr_relampago2: case spr_relampago3: case spr_relampago4: case spr_StrAtk_Mjolnir1:
				case spr_StrAtk_Mjolnir2: case spr_StrAtk_Korunax1: case spr_StrAtk_Korunax2: case spr_Whirlwind:
				case spr_StrAtk_Whisperer1: case spr_StrAtk_Whisperer2: case spr_windjmp_fx: case spr_wavelightning:
				weakness = -1; break;
				//--> Meetonak Weaknesses
				case spr_Explosion_grnd: case spr_Explosion_bmb: case spr_Explosion_RExp1: case spr_Explosion_RExp2:
				case spr_Argenta: case spr_AirArgenta: case spr_Frostbiter: case spr_AirFrostbiter: case spr_Iceball:
				case spr_IceSpikeball: case spr_NeonLaser: case spr_IceImplosion: case spr_flametongues:
				case spr_Shuriken: case spr_Starblades: case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_Argenta_wave2:
				case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create: case spr_Flarestar_exp:
				case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_Guillotine: case spr_FrostbiterUP:
				case spr_AirFrostbiterUP: case spr_Explosion_hmmgF: case spr_AirArgentaMix: case spr_ArgentaMix:
				case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_Argenta_DashStab: case spr_Dyrnwyn_DashStab:
				case spr_FrostbiterMix: case spr_AirFrostbiterMix: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2:
				case spr_FrostBulwark: case spr_ShurikenSP: case spr_StarbladesSP: weakness = 1; break;
			}
			break;
						
		case "DragonLordF":
			switch(weapon)
			{
				//--> FireLord Strengths
				case spr_Argenta: case spr_AirArgenta: case spr_Argenta_wave2: case spr_Mjolnir: case spr_AirMjolnir: 
				case spr_relampago: case spr_TWave: case spr_Kunai: case spr_Shuriken: case spr_Starblades:
				case spr_AirArgentaMix: case spr_ArgentaMix: case spr_windslash: case spr_Chakram: case spr_ChakramSP:
				case spr_Argenta_DashStab: case spr_StrAtk_Mjolnir1: case spr_StrAtk_Mirage2: case spr_StarbladesSP:
				case spr_relampago2: case spr_relampago3: case spr_relampago4: case spr_wavelightning:
				case spr_Lightball: case spr_ShurikenSP: case spr_KunaiSP: case spr_windjmp_fx:
				case spr_Whirlwind: weakness = -1; break;
				//--> FireLord Weaknesses
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_Iceball: case spr_Whisperer:
				case spr_AirWhisperer: case spr_TornadoINNER_back: case spr_TornadoINNER_front: case spr_NeonLaser:
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_FrostBulwark: case spr_TribladerSP:
				case spr_Guillotine: case spr_Triblader: case spr_FrostbiterUP: case spr_AirFrostbiterUP:
				case spr_FrostbiterMix: case spr_AirFrostbiterMix: case spr_IceSpikeball: case spr_IceImplosion:
				case spr_StrAtk_KorunaxFrostbiter: case spr_GuillotineSP: case spr_StrAtk_Whisperer1:
				case spr_StrAtk_Whisperer2: weakness = 1; break;
				//--> FireLord Immunities
				case spr_Explosion_grnd: case spr_Explosion_bmb: case spr_Explosion_RExp1: case spr_Explosion_RExp2:
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create:
				case spr_Flarestar_exp: case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_Explosion_hmmgF:
				case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_StrAtk_WhispererDyrnwyn: case spr_flametongues:
				case spr_Dyrnwyn_DashStab: case spr_BombSP: weakness = noone; break;
			}
			break;
						
		case "DragonLordI":
			switch(weapon)
			{
				//--> FireLord Strengths
				case spr_Argenta: case spr_AirArgenta: case spr_Argenta_wave2: case spr_Mjolnir: case spr_AirMjolnir: 
				case spr_relampago: case spr_TWave: case spr_Kunai: case spr_Shuriken: case spr_Starblades:
				case spr_AirArgentaMix: case spr_ArgentaMix: case spr_Chakram: case spr_ChakramSP: case spr_KunaiSP: 
				case spr_Argenta_DashStab: case spr_StrAtk_Mjolnir1: case spr_StarbladesSP:
				case spr_relampago2: case spr_relampago3: case spr_relampago4: case spr_wavelightning:
				case spr_Lightball: case spr_ShurikenSP: weakness = -1; break;
				//--> FireLord Weaknesses
				case spr_Explosion_grnd: case spr_Explosion_bmb: case spr_Explosion_RExp1: case spr_Explosion_RExp2:
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create:
				case spr_Flarestar_exp: case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_Guillotine:
				case spr_Explosion_hmmgF: case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_Dyrnwyn_DashStab:
				case spr_StrAtk_WhispererDyrnwyn: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2: case spr_BombSP:
				case spr_GuillotineSP: weakness = 1; break;
				//--> FireLord Immunities
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_Iceball: case spr_Whisperer: case spr_windjmp_fx:
				case spr_AirWhisperer: case spr_windslash: case spr_TornadoINNER_back: case spr_TornadoINNER_front:
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_FrostBulwark: case spr_Whirlwind:
				case spr_Triblader: case spr_StrAtk_Whisperer1: case spr_StrAtk_KorunaxFrostbiter:
				case spr_IceSpikeball: case spr_IceImplosion: case spr_NeonLaser: case spr_FrostbiterMix:
				case spr_AirFrostbiterMix: weakness = noone; break;
			}
			break;
						
		case "normalVine":
			switch(weapon)
			{
				//--> Strenghts
				case spr_Mjolnir: case spr_AirMjolnir: case spr_relampago: case spr_TWave: case spr_Kunai:
				case spr_Frostbiter: case spr_AirFrostbiter: case spr_Iceball: case spr_IceSpikeball:
				case spr_relampago2: case spr_relampago3: case spr_relampago4: case spr_FrostbiterMix:
				case spr_AirFrostbiterMix: case spr_FrostbiterUP: case spr_AirFrostbiterUP: case spr_FrostBulwark:
				case spr_IceImplosion: case spr_NeonLaser: weakness = -1; break;
				//--> Weaknesses
				case spr_Argenta: case spr_AirArgenta: case spr_Argenta_wave2: case spr_Shuriken: case spr_Starblades:
				case spr_Explosion_grnd: case spr_Explosion_bmb: case spr_Explosion_RExp1: case spr_Explosion_RExp2:
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create:
				case spr_Flarestar_exp: case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_Guillotine:
				case spr_Explosion_hmmgF: case spr_windslash: case spr_AirArgentaMix: case spr_ArgentaMix:
				case spr_DyrnwynMix: case spr_AirDyrnwynMix: case spr_TornadoINNER_back: case spr_TornadoINNER_front:
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_ShurikenSP: case spr_StarbladesSP:
				case spr_BombSP: case spr_flametongues: case spr_Dyrnwyn_DashStab: case spr_Argenta_DashStab:
				case spr_Whirlwind: case spr_windjmp_fx: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2: 
				case spr_GuillotineSP: case spr_Korunax: case spr_AirKorunax: weakness = 1; break;
				//--> Immunities
				case spr_Triblader:	weakness = noone; break;
			}
			break;
						
		case "Rose": case "Orchid": case "Aster": case "Ixia": case "Dahlia": case "Lotus":
			switch(weapon)
			{
				//--> Strenghts
				case spr_Kunai: case spr_Frostbiter: case spr_AirFrostbiter: case spr_Iceball: case spr_windslash:
				case spr_FrostbiterUP: case spr_AirFrostbiterUP: case spr_FrostbiterMix: case spr_AirFrostbiterMix:
				case spr_KunaiSP: case spr_Whirlwind: case spr_windjmp_fx: case spr_IceSpikeball: case spr_IceImplosion:
				case spr_NeonLaser: case spr_StrAtk_Whisperer1: case spr_StrAtk_KorunaxFrostbiter: weakness = -1; break;
				//--> Weaknesses
				case spr_Argenta: case spr_AirArgenta: case spr_Argenta_wave2: case spr_Shuriken: case spr_Starblades:
				case spr_Explosion_grnd: case spr_Explosion_bmb: case spr_Explosion_RExp1: case spr_Explosion_RExp2:
				case spr_Dyrnwyn: case spr_AirDyrnwyn: case spr_hmmng_flame: case spr_Flarestar: case spr_Flarestar_create:
				case spr_Flarestar_exp: case spr_Mirage: case spr_AirMirage: case spr_willowisp: case spr_Guillotine:
				case spr_Explosion_hmmgF:  case spr_AirArgentaMix: case spr_ArgentaMix: case spr_DyrnwynMix:
				case spr_AirDyrnwynMix: case spr_Argenta_DashStab: case spr_ShurikenSP: case spr_StarbladesSP:
				case spr_BombSP: case spr_Dyrnwyn_DashStab: case spr_StrAtk_Mirage1: case spr_StrAtk_Mirage2:
				case spr_GuillotineSP: weakness = 1; break;
				//--> Immunities
				case spr_TornadoINVERSE_back: case spr_TornadoINVERSE_front: case spr_FrostBulwark:
				case spr_Triblader: case spr_Mjolnir: case spr_AirMjolnir: case spr_relampago: case spr_TWave:
				case spr_relampago2: case spr_relampago3: case spr_relampago4: case spr_wavelightning:
				case spr_Lightball: case spr_StrAtk_Mjolnir1: case spr_StrAtk_Mjolnir2: case spr_TribladerSP:
				case spr_TornadoINNER_back: case spr_TornadoINNER_front: weakness = noone; break;
			}
			break;
						
	}
	
	/* WEAKNESS FOR DIFFERENT ENEMIES. NOT INCLUDED IN THIS VERSION
	switch(enemy.sprite_index)
	{
		case spr_Rose_Close: case spr_Rose_CloseSpin: case spr_Rose_BulbAttack: case spr_Rose_BulbDOWN:
		case spr_Rose_BulbStanding: case spr_Rose_BulbUP: case spr_Rose_CloseSP: case spr_Rose_Open: case spr_Rose_OpenSP:
		case spr_Orchid_Close: case spr_Orchid_CloseSpin: case spr_Orchid_BulbAttack: case spr_Orchid_BulbDOWN:
		case spr_Orchid_BulbStanding: case spr_Orchid_BulbUP: case spr_Orchid_CloseSP: case spr_Orchid_Open: case spr_Orchid_OpenSP:
		case spr_Ixia_Close: case spr_Ixia_CloseSpin: case spr_Ixia_BulbAttack: case spr_Ixia_BulbDOWN:
		case spr_Ixia_BulbStanding: case spr_Ixia_BulbUP: case spr_Ixia_CloseSP: case spr_Ixia_Open: case spr_Ixia_OpenSP:
		case spr_Dahlia_Close: case spr_Dahlia_CloseSpin: case spr_Dahlia_BulbAttack: case spr_Dahlia_BulbDOWN:
		case spr_Dahlia_BulbStanding: case spr_Dahlia_BulbUP: case spr_Dahlia_CloseSP: case spr_Dahlia_Open: case spr_Dahlia_OpenSP:
			weakness = -1; break;
	
		case spr_Aster_Close: case spr_Aster_CloseSpin: case spr_Aster_BulbAttack: case spr_Aster_BulbDOWN:
		case spr_Aster_BulbStanding: case spr_Aster_BulbUP: case spr_Aster_CloseSP: case spr_Aster_Open: case spr_Aster_OpenSP:
		case spr_Lotus_Close: case spr_Lotus_CloseSpin: case spr_Lotus_BulbAttack: case spr_Lotus_BulbDOWN:
		case spr_Lotus_BulbStanding: case spr_Lotus_BulbUP: case spr_Lotus_CloseSP: case spr_Lotus_Open: case spr_Lotus_OpenSP:
			weakness = noone; break;
	}*/

	//if (instance_exists(obj_timedInvin)) weakness = noone;
	
	if (artist.burnFX) if (weakness != noone) weakness = -1;

	if (weapon == spr_GirTrident) or (weapon == spr_GirTridentUP) or
	   (weapon == spr_AirGirTrident) or (weapon == spr_AirGirTridentUP)
	{
		var tridentCrit = irandom(15);
		if (tridentCrit == 7) weakness = 1;
	}

	return weakness;


}
