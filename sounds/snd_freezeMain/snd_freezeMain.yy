{
  "conversionMode": 0,
  "compression": 2,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_freezeMain",
  "duration": 1.374796,
  "parent": {
    "name": "Swords",
    "path": "folders/Sounds/SndFX/Manticore/Swords.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_freezeMain",
  "tags": [],
  "resourceType": "GMSound",
}