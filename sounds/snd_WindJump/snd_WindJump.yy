{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.9,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_WindJump",
  "duration": 1.300805,
  "parent": {
    "name": "Movement",
    "path": "folders/Sounds/SndFX/Manticore/Movement.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_WindJump",
  "tags": [],
  "resourceType": "GMSound",
}