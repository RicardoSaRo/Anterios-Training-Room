{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.4,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_MjolnirClap",
  "duration": 0.614116,
  "parent": {
    "name": "Axes",
    "path": "folders/Sounds/SndFX/Manticore/Axes.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_MjolnirClap",
  "tags": [],
  "resourceType": "GMSound",
}