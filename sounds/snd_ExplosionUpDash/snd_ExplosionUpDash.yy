{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.8,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_ExplosionUpDash",
  "duration": 1.070374,
  "parent": {
    "name": "Movement",
    "path": "folders/Sounds/SndFX/Manticore/Movement.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_ExplosionUpDash",
  "tags": [],
  "resourceType": "GMSound",
}