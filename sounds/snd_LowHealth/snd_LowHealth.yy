{
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 1,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_LowHealth",
  "duration": 2.516701,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Manticore/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_LowHealth",
  "tags": [],
  "resourceType": "GMSound",
}