{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.8,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_LightningDash",
  "duration": 1.026383,
  "parent": {
    "name": "Movement",
    "path": "folders/Sounds/SndFX/Manticore/Movement.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_LightningDash",
  "tags": [],
  "resourceType": "GMSound",
}