{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.33,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_WhispererSwing1",
  "duration": 1.183957,
  "parent": {
    "name": "Axes",
    "path": "folders/Sounds/SndFX/Manticore/Axes.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_WhispererSwing1",
  "tags": [],
  "resourceType": "GMSound",
}