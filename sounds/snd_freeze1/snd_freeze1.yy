{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_freeze1",
  "duration": 1.32915,
  "parent": {
    "name": "Swords",
    "path": "folders/Sounds/SndFX/Manticore/Swords.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_freeze1",
  "tags": [],
  "resourceType": "GMSound",
}