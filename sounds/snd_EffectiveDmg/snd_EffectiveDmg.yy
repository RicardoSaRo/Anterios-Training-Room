{
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 1,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_EffectiveDmg",
  "duration": 0.630578,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Manticore/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_EffectiveDmg",
  "tags": [],
  "resourceType": "GMSound",
}