{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_GirTabSP1",
  "duration": 2.400034,
  "parent": {
    "name": "Specials",
    "path": "folders/Sounds/SndFX/Manticore/Specials.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_GirTabSP1",
  "tags": [],
  "resourceType": "GMSound",
}