{
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 1,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_ManticoreDmg1",
  "duration": 0.318719,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Manticore/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_ManticoreDmg1",
  "tags": [],
  "resourceType": "GMSound",
}