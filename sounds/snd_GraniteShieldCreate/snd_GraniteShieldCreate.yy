{
  "conversionMode": 0,
  "compression": 3,
  "volume": 1.0,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Golem_audio",
    "path": "audiogroups/Golem_audio",
  },
  "soundFile": "snd_GraniteShieldCreate",
  "duration": 1.301417,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Manticore/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_GraniteShieldCreate",
  "tags": [],
  "resourceType": "GMSound",
}