{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.5,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_EnergyShot",
  "duration": 0.79339,
  "parent": {
    "name": "TWs",
    "path": "folders/Sounds/SndFX/Manticore/TWs.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_EnergyShot",
  "tags": [],
  "resourceType": "GMSound",
}