{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.3,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_DyrnwynSwing3",
  "duration": 0.666104,
  "parent": {
    "name": "Swords",
    "path": "folders/Sounds/SndFX/Manticore/Swords.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_DyrnwynSwing3",
  "tags": [],
  "resourceType": "GMSound",
}