{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.1,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_Guillotine1",
  "duration": 0.418311,
  "parent": {
    "name": "TWs",
    "path": "folders/Sounds/SndFX/Manticore/TWs.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Guillotine1",
  "tags": [],
  "resourceType": "GMSound",
}