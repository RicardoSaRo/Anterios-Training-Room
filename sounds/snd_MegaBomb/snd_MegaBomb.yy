{
  "conversionMode": 0,
  "compression": 0,
  "volume": 0.5,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_MegaBomb",
  "duration": 1.8250229,
  "parent": {
    "name": "Specials",
    "path": "folders/Sounds/SndFX/Manticore/Specials.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_MegaBomb",
  "tags": [],
  "resourceType": "GMSound",
}