{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_Golem_unearth",
  "duration": 1.148175,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Bosses/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Golem_unearth",
  "tags": [],
  "resourceType": "GMSound",
}