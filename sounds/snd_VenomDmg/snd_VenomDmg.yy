{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.8,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_VenomDmg.wav",
  "duration": 1.000283,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Manticore/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_VenomDmg",
  "tags": [],
  "resourceType": "GMSound",
}