{
  "conversionMode": 0,
  "compression": 2,
  "volume": 0.5,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "snd_freeze4",
  "duration": 1.443299,
  "parent": {
    "name": "Swords",
    "path": "folders/Sounds/SndFX/Manticore/Swords.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_freeze4",
  "tags": [],
  "resourceType": "GMSound",
}