{
  "conversionMode": 0,
  "compression": 3,
  "volume": 0.1,
  "preload": false,
  "bitRate": 56,
  "sampleRate": 22050,
  "type": 0,
  "bitDepth": 0,
  "audioGroupId": {
    "name": "Manticore_audio",
    "path": "audiogroups/Manticore_audio",
  },
  "soundFile": "snd_MjolnirSwing3",
  "duration": 1.149535,
  "parent": {
    "name": "Axes",
    "path": "folders/Sounds/SndFX/Manticore/Axes.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_MjolnirSwing3",
  "tags": [],
  "resourceType": "GMSound",
}