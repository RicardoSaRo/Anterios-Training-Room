{
  "conversionMode": 0,
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "Golem_audio",
    "path": "audiogroups/Golem_audio",
  },
  "soundFile": "snd_Golem_stomp",
  "duration": 0.764932,
  "parent": {
    "name": "Misc",
    "path": "folders/Sounds/SndFX/Bosses/Misc.yy",
  },
  "resourceVersion": "1.0",
  "name": "snd_Golem_stomp",
  "tags": [],
  "resourceType": "GMSound",
}