SHOCKWAVE!

This asset is for you to use to create a "shockwave" effect. It uses surfaces and textured
primitives to make an expanding wave of distortion around a single point.

When importing this asset into your games, you can delete all the demo objects and sprites
and maintain only the Shockwave Scripts and the object "obj_ShockWave", and simply use 
one of the two "shockwave_create_*" scripts to create the effect.

Before using the asset in a project, please take a moment to open each of the scripts supplied 
and read through them to get an idea of how they work and what they do. Note that the asset
has been created to be as versatile as possible, and so includes features like an option to use 
additive blending. These things can be eddited out of the draw script to help make it more
efficient if required, but make sure you understand the code before attempting this.

There is also a folder marked "Extras" in the Script Resources. This folder contains additional 
draw scripts that can be "swapped" with the regular draw script to get different effects. In 
general, the inside/outside scripts will give you a shockwave with a "hard" edge, and the 
inverted scripts will simply invert the way the alpha and texture mapping is done. How you use
these extras is up to you, but I would recommend smply making a duplicate of the base 
"obj_Shockwave" object and changing the draw script used, then duplicate the appropriate 
shockwave_create_* script and edit the object that it creates to the new one.

Thank you for buying the asset and supporting my Studio, and I hope that you can learn from 
it and have fun using it!

Nocturne
(Mark Alexander)

 