{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 178,
  "bbox_top": 0,
  "bbox_bottom": 37,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 179,
  "height": 38,
  "textureGroupId": {
    "name": "Labels",
    "path": "texturegroups/Labels",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"611d92f4-84e7-41a4-b9d9-c15f89484ad0","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"611d92f4-84e7-41a4-b9d9-c15f89484ad0","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},"LayerId":{"name":"250dd540-34a0-49bf-9268-cdf7d17276e3","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Inventory_lbl","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},"resourceVersion":"1.0","name":"611d92f4-84e7-41a4-b9d9-c15f89484ad0","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Inventory_lbl","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2a68a04f-8714-435b-9b86-2323a6024740","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"611d92f4-84e7-41a4-b9d9-c15f89484ad0","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 89,
    "yorigin": 19,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Inventory_lbl","path":"sprites/spr_Inventory_lbl/spr_Inventory_lbl.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"250dd540-34a0-49bf-9268-cdf7d17276e3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Main_Menu",
    "path": "folders/Sprites/Menus/Main_Menu.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Inventory_lbl",
  "tags": [],
  "resourceType": "GMSprite",
}