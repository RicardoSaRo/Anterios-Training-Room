{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 13,
  "bbox_top": 0,
  "bbox_bottom": 13,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 14,
  "height": 14,
  "textureGroupId": {
    "name": "GUI",
    "path": "texturegroups/GUI",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b3c3e918-f801-4742-b677-d9254e4f6fc9","path":"sprites/spr_Times/spr_Times.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b3c3e918-f801-4742-b677-d9254e4f6fc9","path":"sprites/spr_Times/spr_Times.yy",},"LayerId":{"name":"1e403441-bbbc-46f6-b550-2c9c6a32fc6c","path":"sprites/spr_Times/spr_Times.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Times","path":"sprites/spr_Times/spr_Times.yy",},"resourceVersion":"1.0","name":"b3c3e918-f801-4742-b677-d9254e4f6fc9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Times","path":"sprites/spr_Times/spr_Times.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d97698de-f323-4129-9461-a7419c8e8ea5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b3c3e918-f801-4742-b677-d9254e4f6fc9","path":"sprites/spr_Times/spr_Times.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 7,
    "yorigin": 7,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Times","path":"sprites/spr_Times/spr_Times.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1e403441-bbbc-46f6-b550-2c9c6a32fc6c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Health_Bars",
    "path": "folders/Sprites/Health_Bars.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Times",
  "tags": [],
  "resourceType": "GMSprite",
}