{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 20,
  "bbox_right": 76,
  "bbox_top": 5,
  "bbox_bottom": 111,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 96,
  "height": 112,
  "textureGroupId": {
    "name": "Manticore",
    "path": "texturegroups/Manticore",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cd4a3360-edb9-4408-b977-b550e50291f4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd4a3360-edb9-4408-b977-b550e50291f4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"cd4a3360-edb9-4408-b977-b550e50291f4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e513473-3009-41a3-ae53-c865ed2d1278","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e513473-3009-41a3-ae53-c865ed2d1278","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"4e513473-3009-41a3-ae53-c865ed2d1278","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2c0f3ab5-5c4a-47af-9f85-1d93ffa37c87","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2c0f3ab5-5c4a-47af-9f85-1d93ffa37c87","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"2c0f3ab5-5c4a-47af-9f85-1d93ffa37c87","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96c932ea-a1e2-401c-8324-e49591f3cfa4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96c932ea-a1e2-401c-8324-e49591f3cfa4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"96c932ea-a1e2-401c-8324-e49591f3cfa4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"583a6d34-51dd-4b86-bb99-768677b610fa","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"583a6d34-51dd-4b86-bb99-768677b610fa","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"583a6d34-51dd-4b86-bb99-768677b610fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2a92fca-913e-45f1-8a29-b374c3b990e4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2a92fca-913e-45f1-8a29-b374c3b990e4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"LayerId":{"name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","name":"e2a92fca-913e-45f1-8a29-b374c3b990e4","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a306ea48-c779-4063-bc3a-0e7096c71c16","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd4a3360-edb9-4408-b977-b550e50291f4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c0c99c4f-10e9-484a-aeee-0ecbd860c359","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e513473-3009-41a3-ae53-c865ed2d1278","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e8d7da1-2582-4919-8028-abf1affa92d6","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2c0f3ab5-5c4a-47af-9f85-1d93ffa37c87","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dab968e4-6be7-4aa9-9eac-63db9a2932fa","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96c932ea-a1e2-401c-8324-e49591f3cfa4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d19d14c-9712-46b9-ab33-5d5c8146e950","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"583a6d34-51dd-4b86-bb99-768677b610fa","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1fcf43c6-4024-4490-ad10-5a7987950002","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2a92fca-913e-45f1-8a29-b374c3b990e4","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 48,
    "yorigin": 56,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Manticore_doublejmp","path":"sprites/spr_Manticore_doublejmp/spr_Manticore_doublejmp.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"dd4f8f0a-982a-43ef-bd26-6608c4aa185c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Manticore_doublejmp",
  "tags": [],
  "resourceType": "GMSprite",
}