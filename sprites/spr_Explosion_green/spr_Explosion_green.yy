{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 7,
  "bbox_right": 174,
  "bbox_top": 9,
  "bbox_bottom": 180,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 184,
  "height": 184,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bc68c495-adb8-48c9-a5e3-ba84cea337cd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bc68c495-adb8-48c9-a5e3-ba84cea337cd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"bc68c495-adb8-48c9-a5e3-ba84cea337cd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd04e656-fb6b-4fc8-ab73-5e3ddc380fcd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd04e656-fb6b-4fc8-ab73-5e3ddc380fcd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"fd04e656-fb6b-4fc8-ab73-5e3ddc380fcd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"22f43611-bfed-4800-ab3f-44ee26c5514c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"22f43611-bfed-4800-ab3f-44ee26c5514c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"22f43611-bfed-4800-ab3f-44ee26c5514c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2afced35-c8bd-4cf0-915a-6b2a3df8330c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2afced35-c8bd-4cf0-915a-6b2a3df8330c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"2afced35-c8bd-4cf0-915a-6b2a3df8330c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4011c80-3c7b-492e-9e8c-24c3591a49f2","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4011c80-3c7b-492e-9e8c-24c3591a49f2","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"d4011c80-3c7b-492e-9e8c-24c3591a49f2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"30b1b2ca-bad4-4e35-9a59-89c80187964b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30b1b2ca-bad4-4e35-9a59-89c80187964b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"30b1b2ca-bad4-4e35-9a59-89c80187964b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea98a51e-355b-41db-9ebe-6c37783cc046","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea98a51e-355b-41db-9ebe-6c37783cc046","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"ea98a51e-355b-41db-9ebe-6c37783cc046","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"78a21ba5-8111-47a5-bc2d-3c6c8bcec8a5","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"78a21ba5-8111-47a5-bc2d-3c6c8bcec8a5","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"78a21ba5-8111-47a5-bc2d-3c6c8bcec8a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8ecd4875-34c1-4b90-bdf6-9e2d5be7d69a","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8ecd4875-34c1-4b90-bdf6-9e2d5be7d69a","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"8ecd4875-34c1-4b90-bdf6-9e2d5be7d69a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9674a912-8de2-4309-ad85-0fa584d6ea59","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9674a912-8de2-4309-ad85-0fa584d6ea59","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"9674a912-8de2-4309-ad85-0fa584d6ea59","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4ee48c5b-ccb3-4b58-8e1a-d2ba6059fde9","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4ee48c5b-ccb3-4b58-8e1a-d2ba6059fde9","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"4ee48c5b-ccb3-4b58-8e1a-d2ba6059fde9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"060762b3-6f0f-4846-983b-67656ee0a19b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"060762b3-6f0f-4846-983b-67656ee0a19b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"060762b3-6f0f-4846-983b-67656ee0a19b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"21280a44-0154-4784-8e0b-5df836b59ec4","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"21280a44-0154-4784-8e0b-5df836b59ec4","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"LayerId":{"name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","name":"21280a44-0154-4784-8e0b-5df836b59ec4","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 13.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b768b492-ab69-4a49-a5d0-f9c81af5532c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bc68c495-adb8-48c9-a5e3-ba84cea337cd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6fcd7e24-cb75-41ac-a338-f413c8c2a1be","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd04e656-fb6b-4fc8-ab73-5e3ddc380fcd","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0b7168ae-8a88-49b9-9883-c5e63796b143","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"22f43611-bfed-4800-ab3f-44ee26c5514c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8ca7b2db-77e7-403d-aaae-343760095594","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2afced35-c8bd-4cf0-915a-6b2a3df8330c","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9398c538-ede7-4cb4-84e6-3c1e9b587f43","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4011c80-3c7b-492e-9e8c-24c3591a49f2","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2322dee1-6fa2-4bb7-b221-3ba73856f36e","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30b1b2ca-bad4-4e35-9a59-89c80187964b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"721132db-276b-4150-8b98-802a5ffcbb94","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea98a51e-355b-41db-9ebe-6c37783cc046","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c5a2e230-4792-4496-902b-8416e69c38ab","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"78a21ba5-8111-47a5-bc2d-3c6c8bcec8a5","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ebb500b9-957f-47d0-bdff-8634701e801e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8ecd4875-34c1-4b90-bdf6-9e2d5be7d69a","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e6e2443-15a1-406e-88bc-19014616753f","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9674a912-8de2-4309-ad85-0fa584d6ea59","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6ed7f322-badf-4b2f-8c25-afc8478ceb36","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4ee48c5b-ccb3-4b58-8e1a-d2ba6059fde9","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d2de15d-85d2-4b3f-a02e-cd4b22bbb60d","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"060762b3-6f0f-4846-983b-67656ee0a19b","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eb3e31e8-27ad-4b5c-b3a4-381b94bcc004","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21280a44-0154-4784-8e0b-5df836b59ec4","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 92,
    "yorigin": 164,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Explosion_green","path":"sprites/spr_Explosion_green/spr_Explosion_green.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"25ebab8d-5ac3-4cd9-86f4-f34a0400a854","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Explosion_green",
  "tags": [],
  "resourceType": "GMSprite",
}