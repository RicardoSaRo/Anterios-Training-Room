{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 345,
  "bbox_top": 0,
  "bbox_bottom": 337,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 346,
  "height": 338,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"db3a6b56-8185-4762-9367-588a58251193","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"db3a6b56-8185-4762-9367-588a58251193","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"db3a6b56-8185-4762-9367-588a58251193","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b24ae563-788b-4c67-9732-abcdc98b1b48","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b24ae563-788b-4c67-9732-abcdc98b1b48","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"b24ae563-788b-4c67-9732-abcdc98b1b48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"47f6383b-e61d-4c72-8dc7-9352679e35b5","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"47f6383b-e61d-4c72-8dc7-9352679e35b5","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"47f6383b-e61d-4c72-8dc7-9352679e35b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2b94bc84-5cb6-495a-b5b7-9218a090eedc","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2b94bc84-5cb6-495a-b5b7-9218a090eedc","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"2b94bc84-5cb6-495a-b5b7-9218a090eedc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3bdac67-b734-46f8-95b9-28d0bf47fcf3","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3bdac67-b734-46f8-95b9-28d0bf47fcf3","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"e3bdac67-b734-46f8-95b9-28d0bf47fcf3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"87919278-5c7b-48c1-9e15-6b2d787d2578","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"87919278-5c7b-48c1-9e15-6b2d787d2578","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"87919278-5c7b-48c1-9e15-6b2d787d2578","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"595f678b-7fdb-4ec6-8736-a5da76e73f9e","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"595f678b-7fdb-4ec6-8736-a5da76e73f9e","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"595f678b-7fdb-4ec6-8736-a5da76e73f9e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5b37d182-bcc9-4320-8b96-5a2af81c4d70","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5b37d182-bcc9-4320-8b96-5a2af81c4d70","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"5b37d182-bcc9-4320-8b96-5a2af81c4d70","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f6c58217-0201-4bb5-a27e-c4e02c5cae2d","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f6c58217-0201-4bb5-a27e-c4e02c5cae2d","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"f6c58217-0201-4bb5-a27e-c4e02c5cae2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca6af688-f330-40e2-96fc-673656c4aec4","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca6af688-f330-40e2-96fc-673656c4aec4","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"ca6af688-f330-40e2-96fc-673656c4aec4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f1b916f7-0c42-4e9e-b509-458562be4828","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f1b916f7-0c42-4e9e-b509-458562be4828","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"f1b916f7-0c42-4e9e-b509-458562be4828","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0fca0d36-b6fe-4d72-bbe5-774d803d79d9","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0fca0d36-b6fe-4d72-bbe5-774d803d79d9","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"0fca0d36-b6fe-4d72-bbe5-774d803d79d9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d7131f47-4baf-4534-b872-108fed999a24","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d7131f47-4baf-4534-b872-108fed999a24","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"d7131f47-4baf-4534-b872-108fed999a24","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"796e4963-4550-4924-9b1c-919467436319","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"796e4963-4550-4924-9b1c-919467436319","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"796e4963-4550-4924-9b1c-919467436319","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"378641fb-8cfb-416d-8c7b-c392595760b2","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"378641fb-8cfb-416d-8c7b-c392595760b2","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"LayerId":{"name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","name":"378641fb-8cfb-416d-8c7b-c392595760b2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 15.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cc32c0c8-b6fb-4ac7-9f5e-7c472eb26e1a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"db3a6b56-8185-4762-9367-588a58251193","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a05c4b54-41f0-4786-9895-24e2aab8b644","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b24ae563-788b-4c67-9732-abcdc98b1b48","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e2e98f9b-c8d8-4d3a-bac4-60a18d7686c7","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"47f6383b-e61d-4c72-8dc7-9352679e35b5","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"44d1982c-6141-4683-a26e-ce5179ac118f","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2b94bc84-5cb6-495a-b5b7-9218a090eedc","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1a67688b-fddd-4ec8-b6db-50620d0b0619","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3bdac67-b734-46f8-95b9-28d0bf47fcf3","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2e1b6d7c-4f45-4323-ad12-85b16e98eaa7","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"87919278-5c7b-48c1-9e15-6b2d787d2578","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"74fcc766-7ab2-455c-be75-6c7d7377fe0a","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"595f678b-7fdb-4ec6-8736-a5da76e73f9e","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"eed5e58a-5ab1-4445-9728-babbb4b519d7","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5b37d182-bcc9-4320-8b96-5a2af81c4d70","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"79fea5f0-988e-4cdc-bf7c-1b2c8b0b96ec","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f6c58217-0201-4bb5-a27e-c4e02c5cae2d","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c9740112-4e67-4d34-85ef-c292bf183ed3","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca6af688-f330-40e2-96fc-673656c4aec4","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bb959e6d-ee6c-45b7-94b3-8522a4e18247","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f1b916f7-0c42-4e9e-b509-458562be4828","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cbbb38c8-2b84-47c9-9065-995f259e3815","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0fca0d36-b6fe-4d72-bbe5-774d803d79d9","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"551009e3-ee7a-42b3-b093-a86f9b9b4c83","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7131f47-4baf-4534-b872-108fed999a24","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"591b8681-69a6-4d49-9e01-6060beaa0576","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"796e4963-4550-4924-9b1c-919467436319","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2e5bac14-9238-4268-a1d6-b7a9ceadd3a8","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"378641fb-8cfb-416d-8c7b-c392595760b2","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 173,
    "yorigin": 169,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Explosion_bmb","path":"sprites/spr_Explosion_bmb/spr_Explosion_bmb.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a1bc9e76-22d6-4990-883a-00baa0242fc7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Explosion_bmb",
  "tags": [],
  "resourceType": "GMSprite",
}