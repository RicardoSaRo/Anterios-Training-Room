{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 115,
  "bbox_top": 0,
  "bbox_bottom": 109,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 116,
  "height": 110,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3c70c573-1825-4264-ab22-379eb26a6987","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c70c573-1825-4264-ab22-379eb26a6987","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"3c70c573-1825-4264-ab22-379eb26a6987","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a77b143-50fd-4710-bb64-6dba13c07747","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a77b143-50fd-4710-bb64-6dba13c07747","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"7a77b143-50fd-4710-bb64-6dba13c07747","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"717cf997-7692-481d-a1e8-d228366d1671","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"717cf997-7692-481d-a1e8-d228366d1671","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"717cf997-7692-481d-a1e8-d228366d1671","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a4274403-a9ae-427d-8932-49b5c3be402c","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a4274403-a9ae-427d-8932-49b5c3be402c","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"a4274403-a9ae-427d-8932-49b5c3be402c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"01717765-2162-47c6-97f0-eee8c9111689","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"01717765-2162-47c6-97f0-eee8c9111689","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"01717765-2162-47c6-97f0-eee8c9111689","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ac205c28-100a-4d7a-ad40-5f70a8f703ce","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ac205c28-100a-4d7a-ad40-5f70a8f703ce","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"LayerId":{"name":"f987e085-2128-4b64-b374-87587f8be9a5","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","name":"ac205c28-100a-4d7a-ad40-5f70a8f703ce","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"bf613d4e-2de7-4440-8f52-bf1dc383054d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c70c573-1825-4264-ab22-379eb26a6987","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8426e5ee-cde0-4fb4-8143-ab988c491efb","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a77b143-50fd-4710-bb64-6dba13c07747","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"577445e7-3585-44e6-9073-6138fb7cde9d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"717cf997-7692-481d-a1e8-d228366d1671","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0ff2de17-e469-4367-a1ab-43149ed244af","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a4274403-a9ae-427d-8932-49b5c3be402c","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0df741c7-3655-4c7d-9008-b1342d1c3dad","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"01717765-2162-47c6-97f0-eee8c9111689","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"131aa90a-4e61-436e-a06a-d94ff0e6f298","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ac205c28-100a-4d7a-ad40-5f70a8f703ce","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 87,
    "yorigin": 65,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_runFeatherCloak","path":"sprites/spr_runFeatherCloak/spr_runFeatherCloak.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f987e085-2128-4b64-b374-87587f8be9a5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_runFeatherCloak",
  "tags": [],
  "resourceType": "GMSprite",
}