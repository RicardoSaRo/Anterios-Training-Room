{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 47,
  "bbox_right": 201,
  "bbox_top": 60,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 202,
  "height": 162,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4cea37d5-4124-4566-a792-e9a2c47175ed","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4cea37d5-4124-4566-a792-e9a2c47175ed","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},"LayerId":{"name":"8e5d9de1-2552-4d07-b440-f36957a899ee","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Dyrnwyn_hitbox1","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},"resourceVersion":"1.0","name":"4cea37d5-4124-4566-a792-e9a2c47175ed","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Dyrnwyn_hitbox1","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f123b1a3-2f03-4415-bf95-773772cdbbb0","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4cea37d5-4124-4566-a792-e9a2c47175ed","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 57,
    "yorigin": 106,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Dyrnwyn_hitbox1","path":"sprites/spr_Dyrnwyn_hitbox1/spr_Dyrnwyn_hitbox1.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8e5d9de1-2552-4d07-b440-f36957a899ee","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Dyrnwyn_hitbox1",
  "tags": [],
  "resourceType": "GMSprite",
}