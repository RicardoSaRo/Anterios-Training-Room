{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 14,
  "bbox_right": 305,
  "bbox_top": 14,
  "bbox_bottom": 305,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 320,
  "height": 320,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d36f25be-503c-40a3-90fb-9d168a88bd6f","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d36f25be-503c-40a3-90fb-9d168a88bd6f","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"d36f25be-503c-40a3-90fb-9d168a88bd6f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c622fd2f-068d-431f-96e8-8d240e24e7fd","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c622fd2f-068d-431f-96e8-8d240e24e7fd","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"c622fd2f-068d-431f-96e8-8d240e24e7fd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f9c13be-f72e-4b6a-8358-37be3982b8b3","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f9c13be-f72e-4b6a-8358-37be3982b8b3","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"4f9c13be-f72e-4b6a-8358-37be3982b8b3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"af6a5c8a-9a6c-4ab9-9df4-c8da4b19e686","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"af6a5c8a-9a6c-4ab9-9df4-c8da4b19e686","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"af6a5c8a-9a6c-4ab9-9df4-c8da4b19e686","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09423eef-1cc5-4c19-bcd5-a06ac3dc27e8","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09423eef-1cc5-4c19-bcd5-a06ac3dc27e8","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"09423eef-1cc5-4c19-bcd5-a06ac3dc27e8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa8097b1-5983-4c88-a895-f572c74a2764","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa8097b1-5983-4c88-a895-f572c74a2764","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"aa8097b1-5983-4c88-a895-f572c74a2764","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c4478e6c-88b2-4ffd-aa68-1f04b6e0e4c1","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c4478e6c-88b2-4ffd-aa68-1f04b6e0e4c1","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"c4478e6c-88b2-4ffd-aa68-1f04b6e0e4c1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"aa1db069-06d1-426b-9e11-7852f54a861a","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa1db069-06d1-426b-9e11-7852f54a861a","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"LayerId":{"name":"36d736dd-7e63-4381-b70a-5a6f27a12093","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","name":"aa1db069-06d1-426b-9e11-7852f54a861a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a0aa4e78-916f-4849-bd5c-125f36c39774","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d36f25be-503c-40a3-90fb-9d168a88bd6f","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"907fda97-723e-4028-a8d3-23c245efb1ee","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c622fd2f-068d-431f-96e8-8d240e24e7fd","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"54096339-fe07-483b-99d6-aa2eb31afd8d","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f9c13be-f72e-4b6a-8358-37be3982b8b3","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7d3a2256-df19-41b0-aab0-3803bcb45c5e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"af6a5c8a-9a6c-4ab9-9df4-c8da4b19e686","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c2ffbb24-4fd5-4e38-826c-8b8f2944391b","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09423eef-1cc5-4c19-bcd5-a06ac3dc27e8","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"417451bc-a453-4d11-b2cd-9df3d60549c9","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa8097b1-5983-4c88-a895-f572c74a2764","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1eb29bd-e31a-4822-a220-b6d8936c19f3","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c4478e6c-88b2-4ffd-aa68-1f04b6e0e4c1","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dd682042-87d3-44af-8a3c-3aa3fcc4b041","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa1db069-06d1-426b-9e11-7852f54a861a","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 160,
    "yorigin": 160,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Flarestar_exp","path":"sprites/spr_Flarestar_exp/spr_Flarestar_exp.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"36d736dd-7e63-4381-b70a-5a6f27a12093","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Flarestar_exp",
  "tags": [],
  "resourceType": "GMSprite",
}