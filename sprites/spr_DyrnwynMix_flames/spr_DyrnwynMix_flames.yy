{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 201,
  "bbox_top": 0,
  "bbox_bottom": 161,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 202,
  "height": 162,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f5b625bb-3ba1-4ad8-9d41-789f4cc89554","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f5b625bb-3ba1-4ad8-9d41-789f4cc89554","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"f5b625bb-3ba1-4ad8-9d41-789f4cc89554","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"400d78d1-e538-4d5e-a8c4-18c3b0b6c28e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"400d78d1-e538-4d5e-a8c4-18c3b0b6c28e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"400d78d1-e538-4d5e-a8c4-18c3b0b6c28e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4d482a33-a1dc-42e7-934e-8fdf8c47e6f1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4d482a33-a1dc-42e7-934e-8fdf8c47e6f1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"4d482a33-a1dc-42e7-934e-8fdf8c47e6f1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4e4ce690-4c01-45de-8b1e-a84de9817ae1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4e4ce690-4c01-45de-8b1e-a84de9817ae1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"4e4ce690-4c01-45de-8b1e-a84de9817ae1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57614c48-318e-4a14-b436-5df375f2c562","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57614c48-318e-4a14-b436-5df375f2c562","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"57614c48-318e-4a14-b436-5df375f2c562","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3b8487c-3289-4fdc-ac80-130f477a62a1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3b8487c-3289-4fdc-ac80-130f477a62a1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"e3b8487c-3289-4fdc-ac80-130f477a62a1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6ad4401d-7d85-4f8c-8dc2-099446635c98","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6ad4401d-7d85-4f8c-8dc2-099446635c98","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"6ad4401d-7d85-4f8c-8dc2-099446635c98","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"adb56d7a-32af-4348-bd3c-0de33238ec60","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"adb56d7a-32af-4348-bd3c-0de33238ec60","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"adb56d7a-32af-4348-bd3c-0de33238ec60","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"332bb4b1-0bae-4696-8b22-227911ac1ea5","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"332bb4b1-0bae-4696-8b22-227911ac1ea5","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"332bb4b1-0bae-4696-8b22-227911ac1ea5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1f8505db-c176-4b9c-83aa-67330f5b7720","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1f8505db-c176-4b9c-83aa-67330f5b7720","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"1f8505db-c176-4b9c-83aa-67330f5b7720","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bba2a4eb-cc35-4c57-abbd-28b211a07890","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bba2a4eb-cc35-4c57-abbd-28b211a07890","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"bba2a4eb-cc35-4c57-abbd-28b211a07890","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"23dc3ac3-8af7-4302-9179-0ea1fa8e8b15","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"23dc3ac3-8af7-4302-9179-0ea1fa8e8b15","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"23dc3ac3-8af7-4302-9179-0ea1fa8e8b15","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"88da350d-06a3-4b91-801a-79151ea6c5aa","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"88da350d-06a3-4b91-801a-79151ea6c5aa","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"88da350d-06a3-4b91-801a-79151ea6c5aa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"843a8e1f-0a03-4b3c-9d88-f142ff163a2b","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"843a8e1f-0a03-4b3c-9d88-f142ff163a2b","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"843a8e1f-0a03-4b3c-9d88-f142ff163a2b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e942fdf5-3869-40d9-a2af-e8a0f5a85875","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e942fdf5-3869-40d9-a2af-e8a0f5a85875","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"e942fdf5-3869-40d9-a2af-e8a0f5a85875","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"01dd5a04-5953-45bb-95e0-f5bcc3e759db","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"01dd5a04-5953-45bb-95e0-f5bcc3e759db","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"01dd5a04-5953-45bb-95e0-f5bcc3e759db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cbdfb4dc-dcba-4cba-a5ca-5742bf00c52e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cbdfb4dc-dcba-4cba-a5ca-5742bf00c52e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"cbdfb4dc-dcba-4cba-a5ca-5742bf00c52e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"169eeefe-b823-4370-9a3f-ff29a37ac371","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"169eeefe-b823-4370-9a3f-ff29a37ac371","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"LayerId":{"name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","name":"169eeefe-b823-4370-9a3f-ff29a37ac371","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 18.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a4926d1d-b3c7-44e2-980d-4d228a360445","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f5b625bb-3ba1-4ad8-9d41-789f4cc89554","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"85484053-6651-48be-846e-3e228959a7fe","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"400d78d1-e538-4d5e-a8c4-18c3b0b6c28e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5334225d-fa81-4dde-82d9-ff56e204ac28","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4d482a33-a1dc-42e7-934e-8fdf8c47e6f1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e5192e5-ab13-493f-bd83-826935e11599","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4e4ce690-4c01-45de-8b1e-a84de9817ae1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ead8fd2a-23c0-4226-9f8b-f8df5f993de6","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57614c48-318e-4a14-b436-5df375f2c562","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6120e0fd-4fde-4e9b-b18d-bab3a52f0b8b","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3b8487c-3289-4fdc-ac80-130f477a62a1","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1bcfca35-e35d-4fdd-8671-73c1a002c793","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6ad4401d-7d85-4f8c-8dc2-099446635c98","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"655a85ab-e1ae-4375-97bf-3c4f33221fc5","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"adb56d7a-32af-4348-bd3c-0de33238ec60","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4f95f678-01ad-4e6c-95ae-3c747f5c7148","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"332bb4b1-0bae-4696-8b22-227911ac1ea5","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd132538-84ea-4f22-bd63-cf811dadb784","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1f8505db-c176-4b9c-83aa-67330f5b7720","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"23769d9a-acca-4434-a17e-bfb9972a496d","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bba2a4eb-cc35-4c57-abbd-28b211a07890","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e06b34b1-5a12-43ed-93d3-768437645880","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"23dc3ac3-8af7-4302-9179-0ea1fa8e8b15","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63db9089-01ca-4d78-a161-265dba4ba951","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"88da350d-06a3-4b91-801a-79151ea6c5aa","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3fef21ca-948b-4547-a18f-4decf646c906","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"843a8e1f-0a03-4b3c-9d88-f142ff163a2b","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b68f6bf4-5981-450c-84ec-ca7f3040a8ef","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e942fdf5-3869-40d9-a2af-e8a0f5a85875","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e7091da9-2372-40a3-9b1d-6eaef063a437","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"01dd5a04-5953-45bb-95e0-f5bcc3e759db","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"05a363c2-593a-436c-8bc8-3b188d8f5099","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cbdfb4dc-dcba-4cba-a5ca-5742bf00c52e","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"467ec146-4b37-4450-ace4-4350b845dc48","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"169eeefe-b823-4370-9a3f-ff29a37ac371","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 57,
    "yorigin": 106,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DyrnwynMix_flames","path":"sprites/spr_DyrnwynMix_flames/spr_DyrnwynMix_flames.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5003d0a2-8fd4-4cd3-a08d-91290661e94d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DyrnwynMix_flames",
  "tags": [],
  "resourceType": "GMSprite",
}