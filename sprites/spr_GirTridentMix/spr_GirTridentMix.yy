{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 34,
  "bbox_right": 89,
  "bbox_top": 15,
  "bbox_bottom": 125,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 234,
  "height": 128,
  "textureGroupId": {
    "name": "Manticore",
    "path": "texturegroups/Manticore",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bfeba0fa-dbcd-41f9-b6be-58447820aff9","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bfeba0fa-dbcd-41f9-b6be-58447820aff9","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"bfeba0fa-dbcd-41f9-b6be-58447820aff9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5392851e-16ee-49bf-8d16-856e614885d7","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5392851e-16ee-49bf-8d16-856e614885d7","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"5392851e-16ee-49bf-8d16-856e614885d7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d9d2388e-f3af-40b5-aa43-978942984b67","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d9d2388e-f3af-40b5-aa43-978942984b67","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"d9d2388e-f3af-40b5-aa43-978942984b67","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cfc4ea6c-5251-4731-9b78-f8e10b05bad2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cfc4ea6c-5251-4731-9b78-f8e10b05bad2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"cfc4ea6c-5251-4731-9b78-f8e10b05bad2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14582444-a10d-41d8-aa46-3f390b273e94","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14582444-a10d-41d8-aa46-3f390b273e94","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"14582444-a10d-41d8-aa46-3f390b273e94","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74450b51-ca52-4da5-a327-f0e5997566cb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74450b51-ca52-4da5-a327-f0e5997566cb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"74450b51-ca52-4da5-a327-f0e5997566cb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"09a4dc84-bf00-4239-ba19-61dbb125d702","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"09a4dc84-bf00-4239-ba19-61dbb125d702","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"09a4dc84-bf00-4239-ba19-61dbb125d702","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e873f4c3-767a-4fa8-8138-dfac518c9fbb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e873f4c3-767a-4fa8-8138-dfac518c9fbb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"e873f4c3-767a-4fa8-8138-dfac518c9fbb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3be40563-83d4-4988-921c-462e47f9da0e","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3be40563-83d4-4988-921c-462e47f9da0e","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"3be40563-83d4-4988-921c-462e47f9da0e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"952910d3-bf2f-49d8-8d9c-70c3141d61ad","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"952910d3-bf2f-49d8-8d9c-70c3141d61ad","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"952910d3-bf2f-49d8-8d9c-70c3141d61ad","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"52c6932b-28fd-4660-a056-3ff53da819dd","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"52c6932b-28fd-4660-a056-3ff53da819dd","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"LayerId":{"name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","name":"52c6932b-28fd-4660-a056-3ff53da819dd","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 11.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7464e752-1d45-4999-8e10-9dfed236f8ee","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bfeba0fa-dbcd-41f9-b6be-58447820aff9","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c6d451a0-d392-4ccd-a848-ab1d36c1bb5a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5392851e-16ee-49bf-8d16-856e614885d7","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"69e83c85-5add-40cb-b7be-69d9edfd4682","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d9d2388e-f3af-40b5-aa43-978942984b67","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68f3d1d4-8170-4461-91b4-859cce06106e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cfc4ea6c-5251-4731-9b78-f8e10b05bad2","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3283f33-a019-4ff8-9d4a-8ac8bb2362d1","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14582444-a10d-41d8-aa46-3f390b273e94","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2b19a9b3-44bb-41aa-860c-97105a32896c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74450b51-ca52-4da5-a327-f0e5997566cb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"768b8ee3-4697-406f-bcfd-4c3fee048f86","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"09a4dc84-bf00-4239-ba19-61dbb125d702","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f88849fb-3acd-4006-9765-bace1768a124","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e873f4c3-767a-4fa8-8138-dfac518c9fbb","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8ea4ca4-0dae-4dc4-9999-8b8e22fb1066","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3be40563-83d4-4988-921c-462e47f9da0e","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"57017146-1553-4073-8730-cb46cc36a08c","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"952910d3-bf2f-49d8-8d9c-70c3141d61ad","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf8cec66-902a-4d95-b528-7796df06a207","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"52c6932b-28fd-4660-a056-3ff53da819dd","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 61,
    "yorigin": 70,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GirTridentMix","path":"sprites/spr_GirTridentMix/spr_GirTridentMix.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"14eb9530-a28c-4995-a5cf-1fdf8d5754a2","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GirTridentMix",
  "tags": [],
  "resourceType": "GMSprite",
}