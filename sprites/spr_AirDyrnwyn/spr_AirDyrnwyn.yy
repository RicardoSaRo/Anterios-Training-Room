{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 90,
  "bbox_right": 146,
  "bbox_top": 69,
  "bbox_bottom": 169,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 230,
  "height": 230,
  "textureGroupId": {
    "name": "Manticore",
    "path": "texturegroups/Manticore",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"05f62236-e4ae-4073-b5c8-d89583c2c648","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05f62236-e4ae-4073-b5c8-d89583c2c648","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"05f62236-e4ae-4073-b5c8-d89583c2c648","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63bd89d5-e917-4bf6-a687-c7f5aa6cc22d","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63bd89d5-e917-4bf6-a687-c7f5aa6cc22d","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"63bd89d5-e917-4bf6-a687-c7f5aa6cc22d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"301b4cf3-76ae-49ce-b41a-92f460778c48","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"301b4cf3-76ae-49ce-b41a-92f460778c48","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"301b4cf3-76ae-49ce-b41a-92f460778c48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"14d2f118-2bed-47bb-945e-74cdde7713c6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"14d2f118-2bed-47bb-945e-74cdde7713c6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"14d2f118-2bed-47bb-945e-74cdde7713c6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c280f022-546b-4bed-bea9-df20d7f8e5de","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c280f022-546b-4bed-bea9-df20d7f8e5de","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"c280f022-546b-4bed-bea9-df20d7f8e5de","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37a3f9ec-293a-44ac-bf0e-0604ca8b4471","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37a3f9ec-293a-44ac-bf0e-0604ca8b4471","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"37a3f9ec-293a-44ac-bf0e-0604ca8b4471","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0826f1b5-128f-4df7-befb-96a1811adfc6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0826f1b5-128f-4df7-befb-96a1811adfc6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"0826f1b5-128f-4df7-befb-96a1811adfc6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2f1875d9-888f-49e2-a603-15170a9012c7","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2f1875d9-888f-49e2-a603-15170a9012c7","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"LayerId":{"name":"57080ade-caab-4ea4-956f-df1f61f50488","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","name":"2f1875d9-888f-49e2-a603-15170a9012c7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 25.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 8.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7414a4e7-0dc7-44e3-96dc-61a79c9db2a8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05f62236-e4ae-4073-b5c8-d89583c2c648","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5c46277a-67e5-4981-8be0-56d9be05312a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63bd89d5-e917-4bf6-a687-c7f5aa6cc22d","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1f98a761-659e-4a25-b349-0555e5c95290","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"301b4cf3-76ae-49ce-b41a-92f460778c48","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"22376c65-a83c-4a9f-b8a7-db252d3ca69e","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"14d2f118-2bed-47bb-945e-74cdde7713c6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e3e7611e-c5ea-4b95-873a-e614f5bccdf3","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c280f022-546b-4bed-bea9-df20d7f8e5de","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"849bf1dc-7fb9-45b6-9228-2f36e0890cff","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37a3f9ec-293a-44ac-bf0e-0604ca8b4471","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"14fdc2fb-19b6-40a3-bf26-cfdb7988e25b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0826f1b5-128f-4df7-befb-96a1811adfc6","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e1b4b558-e9cc-431e-9260-141a20440d1f","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2f1875d9-888f-49e2-a603-15170a9012c7","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 118,
    "yorigin": 114,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AirDyrnwyn","path":"sprites/spr_AirDyrnwyn/spr_AirDyrnwyn.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"57080ade-caab-4ea4-956f-df1f61f50488","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": {
    "left": 0,
    "top": 0,
    "right": 0,
    "bottom": 0,
    "guideColour": [
      4294902015,
      4294902015,
      4294902015,
      4294902015,
    ],
    "highlightColour": 1728023040,
    "highlightStyle": 0,
    "enabled": false,
    "tileMode": [
      0,
      0,
      0,
      0,
      0,
    ],
    "resourceVersion": "1.0",
    "loadedVersion": null,
    "resourceType": "GMNineSliceData",
  },
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AirDyrnwyn",
  "tags": [],
  "resourceType": "GMSprite",
}