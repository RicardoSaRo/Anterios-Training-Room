{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 183,
  "bbox_top": 0,
  "bbox_bottom": 205,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 184,
  "height": 206,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ecbdd7f3-eb65-4f7d-8d50-f1fbaa799a1e","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ecbdd7f3-eb65-4f7d-8d50-f1fbaa799a1e","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":{"name":"00b611cf-b9f6-47ab-8d83-387b96e4ee7f","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"ecbdd7f3-eb65-4f7d-8d50-f1fbaa799a1e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1de5f646-3fb5-414e-96d6-67dea070573c","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1de5f646-3fb5-414e-96d6-67dea070573c","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":{"name":"00b611cf-b9f6-47ab-8d83-387b96e4ee7f","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"1de5f646-3fb5-414e-96d6-67dea070573c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0d7f1789-b3ca-4425-9cca-a504bb08e003","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0d7f1789-b3ca-4425-9cca-a504bb08e003","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":{"name":"00b611cf-b9f6-47ab-8d83-387b96e4ee7f","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"0d7f1789-b3ca-4425-9cca-a504bb08e003","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3f42181-4e3f-433e-9f30-0b5bc29fc23a","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3f42181-4e3f-433e-9f30-0b5bc29fc23a","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"LayerId":{"name":"00b611cf-b9f6-47ab-8d83-387b96e4ee7f","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","name":"e3f42181-4e3f-433e-9f30-0b5bc29fc23a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"b7efd5ad-48b6-4343-a54c-f560d655456c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ecbdd7f3-eb65-4f7d-8d50-f1fbaa799a1e","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"68838519-25ee-4ece-9d73-581a83bdcb69","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1de5f646-3fb5-414e-96d6-67dea070573c","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"75dd8c8d-fb6f-4683-89ed-be50423c168f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0d7f1789-b3ca-4425-9cca-a504bb08e003","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7a18345e-ffcc-42d0-bcf2-74dd47ee2041","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3f42181-4e3f-433e-9f30-0b5bc29fc23a","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 92,
    "yorigin": 103,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_dmgFX_Manticore","path":"sprites/spr_dmgFX_Manticore/spr_dmgFX_Manticore.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"00b611cf-b9f6-47ab-8d83-387b96e4ee7f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_dmgFX_Manticore",
  "tags": [],
  "resourceType": "GMSprite",
}