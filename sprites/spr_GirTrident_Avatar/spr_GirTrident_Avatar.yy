{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 17,
  "bbox_right": 210,
  "bbox_top": 6,
  "bbox_bottom": 209,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 220,
  "height": 217,
  "textureGroupId": {
    "name": "Weapons_Menu",
    "path": "texturegroups/Weapons_Menu",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"218fb40d-662c-40ae-a7de-13b897a8cfbf","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"218fb40d-662c-40ae-a7de-13b897a8cfbf","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},"LayerId":{"name":"266fc67d-555d-478a-8007-5141f4f621fe","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GirTrident_Avatar","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},"resourceVersion":"1.0","name":"218fb40d-662c-40ae-a7de-13b897a8cfbf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GirTrident_Avatar","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"590bb627-b739-4c63-b6c1-e1acf8a49678","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"218fb40d-662c-40ae-a7de-13b897a8cfbf","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 110,
    "yorigin": 108,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GirTrident_Avatar","path":"sprites/spr_GirTrident_Avatar/spr_GirTrident_Avatar.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"266fc67d-555d-478a-8007-5141f4f621fe","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Wpn_Avatars",
    "path": "folders/Sprites/Menus/Weapons/Wpn_Avatars.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GirTrident_Avatar",
  "tags": [],
  "resourceType": "GMSprite",
}