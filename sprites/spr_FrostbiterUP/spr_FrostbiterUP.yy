{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 34,
  "bbox_right": 89,
  "bbox_top": 135,
  "bbox_bottom": 245,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 190,
  "height": 248,
  "textureGroupId": {
    "name": "Manticore",
    "path": "texturegroups/Manticore",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"60b4162f-d681-4bde-af72-d928a8e58826","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"60b4162f-d681-4bde-af72-d928a8e58826","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"60b4162f-d681-4bde-af72-d928a8e58826","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"13c3ac75-9502-4459-91e6-6beefb9937aa","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"13c3ac75-9502-4459-91e6-6beefb9937aa","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"13c3ac75-9502-4459-91e6-6beefb9937aa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d50769c2-5eeb-4afe-bac2-fc1cc0aa64b5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d50769c2-5eeb-4afe-bac2-fc1cc0aa64b5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"d50769c2-5eeb-4afe-bac2-fc1cc0aa64b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ce5c800-d2e8-454f-a525-f860cc223ec2","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ce5c800-d2e8-454f-a525-f860cc223ec2","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"2ce5c800-d2e8-454f-a525-f860cc223ec2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e035dc30-4217-446c-a6a2-8e5c8446d41d","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e035dc30-4217-446c-a6a2-8e5c8446d41d","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"e035dc30-4217-446c-a6a2-8e5c8446d41d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"43a3fc33-763b-41bc-adaf-796a8bd98f74","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"43a3fc33-763b-41bc-adaf-796a8bd98f74","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"43a3fc33-763b-41bc-adaf-796a8bd98f74","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6dfe2ca8-fd19-484b-9982-1ca475ab91a5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6dfe2ca8-fd19-484b-9982-1ca475ab91a5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"6dfe2ca8-fd19-484b-9982-1ca475ab91a5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"46082532-8206-4db4-a469-0b1288697008","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"46082532-8206-4db4-a469-0b1288697008","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"46082532-8206-4db4-a469-0b1288697008","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e90b7bda-5607-40ad-8771-64323058ee37","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e90b7bda-5607-40ad-8771-64323058ee37","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"e90b7bda-5607-40ad-8771-64323058ee37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8a151001-7418-42bc-9eea-866b95a8f369","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8a151001-7418-42bc-9eea-866b95a8f369","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"8a151001-7418-42bc-9eea-866b95a8f369","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"97c4bf8d-d90f-4e63-8c50-86c98860b562","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"97c4bf8d-d90f-4e63-8c50-86c98860b562","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"LayerId":{"name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","name":"97c4bf8d-d90f-4e63-8c50-86c98860b562","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 11.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"43c4308d-718a-4870-8e27-d2b5e6c4de1c","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"60b4162f-d681-4bde-af72-d928a8e58826","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1b20da9-4881-4aeb-96ed-61689fd57178","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13c3ac75-9502-4459-91e6-6beefb9937aa","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b18e9ef4-e393-461a-801f-46af1ef7987a","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d50769c2-5eeb-4afe-bac2-fc1cc0aa64b5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"230dd063-6bcf-472e-ab4c-d22eab45e316","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ce5c800-d2e8-454f-a525-f860cc223ec2","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"057fc3de-cfd9-4cbd-a7a1-74060c469454","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e035dc30-4217-446c-a6a2-8e5c8446d41d","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b3187fd9-aa19-4f02-8f60-4eda6727a1a8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"43a3fc33-763b-41bc-adaf-796a8bd98f74","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6691f7c2-304d-45e7-beb7-6e177c92158b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6dfe2ca8-fd19-484b-9982-1ca475ab91a5","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0cdf05d-d10a-4bb3-9513-f6e6d0e49f3e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"46082532-8206-4db4-a469-0b1288697008","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28f7d157-d5c4-4204-a113-8da6f310b4a5","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e90b7bda-5607-40ad-8771-64323058ee37","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1deac0f0-ba6e-485c-9dc9-f5b54c3fb3d3","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8a151001-7418-42bc-9eea-866b95a8f369","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"87749631-e5bd-4a4b-b4eb-4f0264279473","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"97c4bf8d-d90f-4e63-8c50-86c98860b562","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 61,
    "yorigin": 190,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_FrostbiterUP","path":"sprites/spr_FrostbiterUP/spr_FrostbiterUP.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"7e31439a-8508-43e8-87a5-fbc2fb817f81","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_FrostbiterUP",
  "tags": [],
  "resourceType": "GMSprite",
}