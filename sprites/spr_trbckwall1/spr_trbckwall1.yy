{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 255,
  "bbox_top": 0,
  "bbox_bottom": 511,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 256,
  "height": 512,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3333c0ef-3577-488b-8a80-b7d39baf02e2","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3333c0ef-3577-488b-8a80-b7d39baf02e2","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},"LayerId":{"name":"f5e67b65-6619-4dc0-a76f-f6efd9874522","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_trbckwall1","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},"resourceVersion":"1.0","name":"3333c0ef-3577-488b-8a80-b7d39baf02e2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_trbckwall1","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 0.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"640f0401-b4c4-4ba5-9a73-1debd2715183","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3333c0ef-3577-488b-8a80-b7d39baf02e2","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_trbckwall1","path":"sprites/spr_trbckwall1/spr_trbckwall1.yy",},
    "resourceVersion": "1.4",
    "name": "spr_trbckwall1",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f5e67b65-6619-4dc0-a76f-f6efd9874522","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "TestRoom",
    "path": "folders/Sprites/Backgrounds_Misc/TestRoom.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_trbckwall1",
  "tags": [],
  "resourceType": "GMSprite",
}