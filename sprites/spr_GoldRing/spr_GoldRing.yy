{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 109,
  "bbox_top": 0,
  "bbox_bottom": 109,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 110,
  "height": 110,
  "textureGroupId": {
    "name": "Reliks",
    "path": "texturegroups/Reliks",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"b91a02e7-1441-4424-beaf-48507e56ea06","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b91a02e7-1441-4424-beaf-48507e56ea06","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},"LayerId":{"name":"b22cd3ff-af87-4710-880a-9ecb7364ad57","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_GoldRing","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},"resourceVersion":"1.0","name":"b91a02e7-1441-4424-beaf-48507e56ea06","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_GoldRing","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a36988d2-b2d4-4f5e-a271-11d9cbedb98a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b91a02e7-1441-4424-beaf-48507e56ea06","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 55,
    "yorigin": 55,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_GoldRing","path":"sprites/spr_GoldRing/spr_GoldRing.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b22cd3ff-af87-4710-880a-9ecb7364ad57","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "RelikAvatar",
    "path": "folders/Sprites/Menus/Reliks/RelikAvatar.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_GoldRing",
  "tags": [],
  "resourceType": "GMSprite",
}