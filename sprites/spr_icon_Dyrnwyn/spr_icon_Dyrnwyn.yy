{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 47,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 48,
  "textureGroupId": {
    "name": "GUI",
    "path": "texturegroups/GUI",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5c6ad069-f2fc-4fda-b4bc-2a9442f99b57","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5c6ad069-f2fc-4fda-b4bc-2a9442f99b57","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},"LayerId":{"name":"36aba582-375a-46bd-9f0f-e18c887aa94d","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Dyrnwyn","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},"resourceVersion":"1.0","name":"5c6ad069-f2fc-4fda-b4bc-2a9442f99b57","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_icon_Dyrnwyn","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"81f3997d-4a4e-4f4d-883f-f48b3eb652ba","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5c6ad069-f2fc-4fda-b4bc-2a9442f99b57","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_icon_Dyrnwyn","path":"sprites/spr_icon_Dyrnwyn/spr_icon_Dyrnwyn.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"36aba582-375a-46bd-9f0f-e18c887aa94d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Icons",
    "path": "folders/Sprites/Health_Bars/Icons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_icon_Dyrnwyn",
  "tags": [],
  "resourceType": "GMSprite",
}