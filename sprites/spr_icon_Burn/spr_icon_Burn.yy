{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 81,
  "bbox_top": 0,
  "bbox_bottom": 86,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 82,
  "height": 87,
  "textureGroupId": {
    "name": "GUI",
    "path": "texturegroups/GUI",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"79a2f321-0772-4c24-be2d-719e231f0075","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"79a2f321-0772-4c24-be2d-719e231f0075","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"79a2f321-0772-4c24-be2d-719e231f0075","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc4cbc4c-5037-4428-b792-4d48443ed319","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc4cbc4c-5037-4428-b792-4d48443ed319","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"fc4cbc4c-5037-4428-b792-4d48443ed319","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"70c26ffc-2477-4322-a7df-e5e81c8aa4b7","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70c26ffc-2477-4322-a7df-e5e81c8aa4b7","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"70c26ffc-2477-4322-a7df-e5e81c8aa4b7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e73a067d-b937-4576-a66d-a2279be84610","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e73a067d-b937-4576-a66d-a2279be84610","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"e73a067d-b937-4576-a66d-a2279be84610","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"16d14eaa-d41e-4434-9866-ab240437af19","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"16d14eaa-d41e-4434-9866-ab240437af19","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"16d14eaa-d41e-4434-9866-ab240437af19","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6073a019-5f0c-4933-96a0-f3244c018b5f","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6073a019-5f0c-4933-96a0-f3244c018b5f","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"6073a019-5f0c-4933-96a0-f3244c018b5f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9a609ff-4e64-49ab-8f78-4037d069faca","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9a609ff-4e64-49ab-8f78-4037d069faca","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"b9a609ff-4e64-49ab-8f78-4037d069faca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a09c5879-1c84-4019-bcea-05e1876529d4","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a09c5879-1c84-4019-bcea-05e1876529d4","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"a09c5879-1c84-4019-bcea-05e1876529d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"afa40f64-5fce-461c-992f-a56938ab7000","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"afa40f64-5fce-461c-992f-a56938ab7000","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"afa40f64-5fce-461c-992f-a56938ab7000","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c881b6b1-9b1a-4a51-a4e0-60bfe91a68db","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c881b6b1-9b1a-4a51-a4e0-60bfe91a68db","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"c881b6b1-9b1a-4a51-a4e0-60bfe91a68db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"53d461fa-578a-4565-bd5b-b30ea5c98035","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"53d461fa-578a-4565-bd5b-b30ea5c98035","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"53d461fa-578a-4565-bd5b-b30ea5c98035","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"745f42d9-940a-4f37-b040-4126ca9a69a9","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"745f42d9-940a-4f37-b040-4126ca9a69a9","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"LayerId":{"name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","name":"745f42d9-940a-4f37-b040-4126ca9a69a9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 12.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cba99e34-a643-4d25-beb6-32570f4ff576","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"79a2f321-0772-4c24-be2d-719e231f0075","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"30a8cab0-4260-4a6c-99dd-65cdb72794d5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc4cbc4c-5037-4428-b792-4d48443ed319","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef2392a5-c7af-4970-9112-fbd7b1b52704","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70c26ffc-2477-4322-a7df-e5e81c8aa4b7","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0021f25a-7f4b-4578-aab3-129a1c5d5834","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e73a067d-b937-4576-a66d-a2279be84610","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"36a6a5f5-188d-4351-a2be-c4312436d064","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"16d14eaa-d41e-4434-9866-ab240437af19","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"430d3899-27f7-4980-9920-ab015e3f07c8","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6073a019-5f0c-4933-96a0-f3244c018b5f","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"26d21109-b2bb-4ecf-8198-4fd389d4f691","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9a609ff-4e64-49ab-8f78-4037d069faca","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6fdf4333-71b2-4238-813a-80cd90591948","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a09c5879-1c84-4019-bcea-05e1876529d4","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"889e3ecd-4d65-469f-83fd-52cda844cdad","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"afa40f64-5fce-461c-992f-a56938ab7000","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7feab03f-3057-417b-af06-a65b94538217","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c881b6b1-9b1a-4a51-a4e0-60bfe91a68db","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac7db797-70a7-4c59-8b87-2b627f6048c0","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"53d461fa-578a-4565-bd5b-b30ea5c98035","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0380e3f-a9f1-488f-90da-c793558c51ce","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"745f42d9-940a-4f37-b040-4126ca9a69a9","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 41,
    "yorigin": 43,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_icon_Burn","path":"sprites/spr_icon_Burn/spr_icon_Burn.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d94b80cc-7974-4b9c-8151-d4987aa9b762","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Icons",
    "path": "folders/Sprites/Health_Bars/Icons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_icon_Burn",
  "tags": [],
  "resourceType": "GMSprite",
}