{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 109,
  "bbox_top": 0,
  "bbox_bottom": 109,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 110,
  "height": 110,
  "textureGroupId": {
    "name": "Reliks",
    "path": "texturegroups/Reliks",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"aa1cac01-7af0-4276-8476-52a9f119e0c1","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"aa1cac01-7af0-4276-8476-52a9f119e0c1","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},"LayerId":{"name":"0cd10201-4094-4038-a8f9-c6a2b52bb2d5","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Gnome_Figurine","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},"resourceVersion":"1.0","name":"aa1cac01-7af0-4276-8476-52a9f119e0c1","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Gnome_Figurine","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6b253607-61e2-4b81-8473-101414bb7ea1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"aa1cac01-7af0-4276-8476-52a9f119e0c1","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 55,
    "yorigin": 55,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Gnome_Figurine","path":"sprites/spr_Gnome_Figurine/spr_Gnome_Figurine.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"0cd10201-4094-4038-a8f9-c6a2b52bb2d5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "RelikAvatar",
    "path": "folders/Sprites/Menus/Reliks/RelikAvatar.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Gnome_Figurine",
  "tags": [],
  "resourceType": "GMSprite",
}