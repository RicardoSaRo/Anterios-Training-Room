{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 18,
  "bbox_right": 295,
  "bbox_top": 2,
  "bbox_bottom": 159,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 320,
  "height": 160,
  "textureGroupId": {
    "name": "Manticore_Specials",
    "path": "texturegroups/Manticore_Specials",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5ef999c5-33a8-4896-90bc-12bca6046345","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ef999c5-33a8-4896-90bc-12bca6046345","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"5ef999c5-33a8-4896-90bc-12bca6046345","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b3f5b249-f3ba-4ce3-be88-36f980550b33","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b3f5b249-f3ba-4ce3-be88-36f980550b33","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"b3f5b249-f3ba-4ce3-be88-36f980550b33","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ebe1d5a0-f20a-431f-ae3a-a8bdfc7c197d","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ebe1d5a0-f20a-431f-ae3a-a8bdfc7c197d","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"ebe1d5a0-f20a-431f-ae3a-a8bdfc7c197d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1574d61f-74f4-44d7-9de4-cde4d5d39682","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1574d61f-74f4-44d7-9de4-cde4d5d39682","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"1574d61f-74f4-44d7-9de4-cde4d5d39682","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f7c0b29-c003-49c2-b097-147143584958","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f7c0b29-c003-49c2-b097-147143584958","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"4f7c0b29-c003-49c2-b097-147143584958","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b40a4a4b-387e-473b-9f8e-182a0bb073bb","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b40a4a4b-387e-473b-9f8e-182a0bb073bb","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"b40a4a4b-387e-473b-9f8e-182a0bb073bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"40c691be-b900-45d5-b30f-bf6adbccba04","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"40c691be-b900-45d5-b30f-bf6adbccba04","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"40c691be-b900-45d5-b30f-bf6adbccba04","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e09e0001-7ebb-4741-a89c-600e4486be36","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e09e0001-7ebb-4741-a89c-600e4486be36","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"e09e0001-7ebb-4741-a89c-600e4486be36","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"13bb6755-61f5-4d22-90b8-81a8cec8f5f4","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"13bb6755-61f5-4d22-90b8-81a8cec8f5f4","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"13bb6755-61f5-4d22-90b8-81a8cec8f5f4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b08cdaa2-76b4-43b4-8702-a4ac1b6eae63","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b08cdaa2-76b4-43b4-8702-a4ac1b6eae63","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"b08cdaa2-76b4-43b4-8702-a4ac1b6eae63","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"89cb5dbf-e854-4dc4-a9e4-58c39e6b5e37","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"89cb5dbf-e854-4dc4-a9e4-58c39e6b5e37","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"89cb5dbf-e854-4dc4-a9e4-58c39e6b5e37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f0394681-d764-402b-b20a-3c57b092d637","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0394681-d764-402b-b20a-3c57b092d637","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"f0394681-d764-402b-b20a-3c57b092d637","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"59eeb5a5-c3d1-4ec0-a276-eb8c383e226f","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"59eeb5a5-c3d1-4ec0-a276-eb8c383e226f","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"59eeb5a5-c3d1-4ec0-a276-eb8c383e226f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2ca786b9-a5f8-430b-af2b-1d58837cb75e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2ca786b9-a5f8-430b-af2b-1d58837cb75e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"2ca786b9-a5f8-430b-af2b-1d58837cb75e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"894e3d1e-ae42-4903-acaa-629ed31f73a8","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"894e3d1e-ae42-4903-acaa-629ed31f73a8","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"894e3d1e-ae42-4903-acaa-629ed31f73a8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"30e8457d-0986-4268-8e4d-e794008db249","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"30e8457d-0986-4268-8e4d-e794008db249","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"30e8457d-0986-4268-8e4d-e794008db249","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc788564-4b88-4471-a560-3132e031231e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc788564-4b88-4471-a560-3132e031231e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"cc788564-4b88-4471-a560-3132e031231e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6ea9dac6-129b-43a6-956e-451093248ca2","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6ea9dac6-129b-43a6-956e-451093248ca2","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"LayerId":{"name":"e9d78541-a6da-45e8-9e72-f8419d36de32","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","name":"6ea9dac6-129b-43a6-956e-451093248ca2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 18.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4a5be4da-2fde-4515-8b0a-44628fff2dd5","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ef999c5-33a8-4896-90bc-12bca6046345","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ccfc0478-929f-46f9-8df1-89726bd5839b","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b3f5b249-f3ba-4ce3-be88-36f980550b33","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"16e7b231-59fd-45eb-99f1-4be87be7ad66","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ebe1d5a0-f20a-431f-ae3a-a8bdfc7c197d","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1efca107-3ad0-41c2-9e80-e25687f818af","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1574d61f-74f4-44d7-9de4-cde4d5d39682","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ccb284bd-8ee6-4495-a3ea-a6c1933dc899","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f7c0b29-c003-49c2-b097-147143584958","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d7746808-4c59-41ae-af2c-cdc9fc6d700d","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b40a4a4b-387e-473b-9f8e-182a0bb073bb","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fc674036-e2df-4da0-9ad5-cd1931f92546","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"40c691be-b900-45d5-b30f-bf6adbccba04","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"771bdd1c-3200-4198-85e7-a1c958ad816d","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e09e0001-7ebb-4741-a89c-600e4486be36","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"24daf331-cd50-4771-81e8-a2c275a6f2ae","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"13bb6755-61f5-4d22-90b8-81a8cec8f5f4","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ef3026b3-1ebe-4965-9fae-c1afc61e0ca2","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b08cdaa2-76b4-43b4-8702-a4ac1b6eae63","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9664e979-5a5d-4477-a8b1-25a7b12a2e00","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"89cb5dbf-e854-4dc4-a9e4-58c39e6b5e37","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2ada1db9-4e82-4183-8743-e2e4d9764bcc","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0394681-d764-402b-b20a-3c57b092d637","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"33e877c1-2d62-4233-b81c-0522846aa46f","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"59eeb5a5-c3d1-4ec0-a276-eb8c383e226f","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ab605176-ffc6-4c91-8001-3e6683f4768e","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2ca786b9-a5f8-430b-af2b-1d58837cb75e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4dd87adf-134f-4a4e-b30d-7dd576acd5af","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"894e3d1e-ae42-4903-acaa-629ed31f73a8","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"825bd771-bbc3-40a5-977d-2cf535bd8698","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"30e8457d-0986-4268-8e4d-e794008db249","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"164478fe-2c18-4569-a703-c5360edb62e4","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc788564-4b88-4471-a560-3132e031231e","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2e14e616-244b-42b7-896b-f315b7da72cf","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6ea9dac6-129b-43a6-956e-451093248ca2","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 160,
    "yorigin": 159,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_TornadoINNER_back","path":"sprites/spr_TornadoINNER_back/spr_TornadoINNER_back.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e9d78541-a6da-45e8-9e72-f8419d36de32","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Proyectiles/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_TornadoINNER_back",
  "tags": [],
  "resourceType": "GMSprite",
}