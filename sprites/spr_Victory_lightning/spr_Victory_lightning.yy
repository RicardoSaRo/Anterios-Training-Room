{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 40,
  "bbox_right": 161,
  "bbox_top": 16,
  "bbox_bottom": 325,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 190,
  "height": 326,
  "textureGroupId": {
    "name": "FX",
    "path": "texturegroups/FX",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1c4828b8-dd7b-4f79-952e-00cf229ab1b1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c4828b8-dd7b-4f79-952e-00cf229ab1b1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"1c4828b8-dd7b-4f79-952e-00cf229ab1b1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a2236c4d-4061-4d62-80a9-77793d54f078","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a2236c4d-4061-4d62-80a9-77793d54f078","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"a2236c4d-4061-4d62-80a9-77793d54f078","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"17c7c0d4-e8aa-4ced-882d-7e3a96770727","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"17c7c0d4-e8aa-4ced-882d-7e3a96770727","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"17c7c0d4-e8aa-4ced-882d-7e3a96770727","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74255e94-0541-4c14-889a-ce3a57df8254","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74255e94-0541-4c14-889a-ce3a57df8254","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"74255e94-0541-4c14-889a-ce3a57df8254","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ec9aa303-ce53-4abf-97a6-a666ad41fce1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ec9aa303-ce53-4abf-97a6-a666ad41fce1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"ec9aa303-ce53-4abf-97a6-a666ad41fce1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"37e44a74-626c-4d52-9403-9b2dcde0ff48","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"37e44a74-626c-4d52-9403-9b2dcde0ff48","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"37e44a74-626c-4d52-9403-9b2dcde0ff48","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d8cd73a3-446f-4bd5-a9eb-923c3e1a05f5","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8cd73a3-446f-4bd5-a9eb-923c3e1a05f5","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"d8cd73a3-446f-4bd5-a9eb-923c3e1a05f5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1c77dd98-ebe3-42ca-9b05-f0927099a59a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1c77dd98-ebe3-42ca-9b05-f0927099a59a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"1c77dd98-ebe3-42ca-9b05-f0927099a59a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"29685780-4b01-4347-b4ec-f9026810894a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"29685780-4b01-4347-b4ec-f9026810894a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"29685780-4b01-4347-b4ec-f9026810894a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"044d3657-1193-48ff-a3c2-56e74f9ba95c","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"044d3657-1193-48ff-a3c2-56e74f9ba95c","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"044d3657-1193-48ff-a3c2-56e74f9ba95c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"feeddd6c-ae18-4a60-9e3b-e620e7006e9e","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"feeddd6c-ae18-4a60-9e3b-e620e7006e9e","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"feeddd6c-ae18-4a60-9e3b-e620e7006e9e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ef5b51d9-e051-461b-826a-d4ef750e4bf8","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ef5b51d9-e051-461b-826a-d4ef750e4bf8","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"ef5b51d9-e051-461b-826a-d4ef750e4bf8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92a8c49b-d1d8-454a-9d98-34b9a8eca317","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92a8c49b-d1d8-454a-9d98-34b9a8eca317","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"92a8c49b-d1d8-454a-9d98-34b9a8eca317","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"21763fb0-34e1-46c9-b160-cff886f6e49a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"21763fb0-34e1-46c9-b160-cff886f6e49a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"21763fb0-34e1-46c9-b160-cff886f6e49a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72c70c5a-378a-46fc-9d37-8ff286934f35","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72c70c5a-378a-46fc-9d37-8ff286934f35","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"72c70c5a-378a-46fc-9d37-8ff286934f35","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a5f198d0-8fbc-44c1-8ce6-d01f7150fb74","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a5f198d0-8fbc-44c1-8ce6-d01f7150fb74","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"a5f198d0-8fbc-44c1-8ce6-d01f7150fb74","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a46da37a-b036-44a5-9f39-a55436d28ce2","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a46da37a-b036-44a5-9f39-a55436d28ce2","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"LayerId":{"name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","name":"a46da37a-b036-44a5-9f39-a55436d28ce2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 7.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 17.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a033a9c4-36ee-4b39-ab62-de83b2d29504","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c4828b8-dd7b-4f79-952e-00cf229ab1b1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fa7d62dd-8b57-4f0c-a94b-4394ae8340ff","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a2236c4d-4061-4d62-80a9-77793d54f078","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6b5e68b6-58b0-44bc-a5d9-356db74844bf","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"17c7c0d4-e8aa-4ced-882d-7e3a96770727","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ccb2d436-c731-41af-8b5b-d9507bdbb0a1","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74255e94-0541-4c14-889a-ce3a57df8254","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8bb505a1-65d4-4c5b-9634-f2f3cf1e7e11","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ec9aa303-ce53-4abf-97a6-a666ad41fce1","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"59c9a2a5-dc4e-4034-bb18-a17761db4164","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"37e44a74-626c-4d52-9403-9b2dcde0ff48","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e944eef0-340b-44ea-bc16-6a8cdc7f2d7b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8cd73a3-446f-4bd5-a9eb-923c3e1a05f5","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc97be6e-c7d9-43b2-a92d-8693ac747985","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1c77dd98-ebe3-42ca-9b05-f0927099a59a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6387df88-7495-4486-b327-68c040f35668","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29685780-4b01-4347-b4ec-f9026810894a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ff4896f6-8f13-4f9d-83a9-5bffd9f25bcc","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"044d3657-1193-48ff-a3c2-56e74f9ba95c","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"316ed1d4-85b3-4bcc-b3f8-15cb16ce9bf7","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"feeddd6c-ae18-4a60-9e3b-e620e7006e9e","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ff4cfb3b-ef0e-4481-8a57-068c40b8fa6f","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ef5b51d9-e051-461b-826a-d4ef750e4bf8","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d24aeb81-5dcc-4019-85f9-a82bcfbea8ce","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92a8c49b-d1d8-454a-9d98-34b9a8eca317","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"08c5ef87-0f3b-42da-b98e-8dd025c858d2","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"21763fb0-34e1-46c9-b160-cff886f6e49a","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"28668d2f-f6e4-458e-a2e4-865f0f7a34c2","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72c70c5a-378a-46fc-9d37-8ff286934f35","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0c47f8e-edaa-4c51-b3ea-3ef32e5f7782","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a5f198d0-8fbc-44c1-8ce6-d01f7150fb74","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5fbbe943-269b-4906-9734-637e7a633968","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a46da37a-b036-44a5-9f39-a55436d28ce2","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 107,
    "yorigin": 270,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Victory_lightning","path":"sprites/spr_Victory_lightning/spr_Victory_lightning.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"3d5993df-28b8-410b-b3b2-7e3ea820f0ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "FX",
    "path": "folders/Sprites/FX.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Victory_lightning",
  "tags": [],
  "resourceType": "GMSprite",
}