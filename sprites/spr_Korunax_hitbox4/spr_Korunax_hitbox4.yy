{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 89,
  "bbox_right": 181,
  "bbox_top": 17,
  "bbox_bottom": 183,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 182,
  "height": 184,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"0fb6f15b-a549-46b5-8a87-fbb72d2fba5e","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0fb6f15b-a549-46b5-8a87-fbb72d2fba5e","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},"LayerId":{"name":"2533d75a-05a7-4617-98c4-93c8bab92c17","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Korunax_hitbox4","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},"resourceVersion":"1.0","name":"0fb6f15b-a549-46b5-8a87-fbb72d2fba5e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Korunax_hitbox4","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"94cdfcfe-7e87-4a10-81fb-b784140dd7f1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0fb6f15b-a549-46b5-8a87-fbb72d2fba5e","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 71,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Korunax_hitbox4","path":"sprites/spr_Korunax_hitbox4/spr_Korunax_hitbox4.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2533d75a-05a7-4617-98c4-93c8bab92c17","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Korunax_hitbox4",
  "tags": [],
  "resourceType": "GMSprite",
}