{
  "bboxMode": 1,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 307,
  "bbox_top": 0,
  "bbox_bottom": 371,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 308,
  "height": 372,
  "textureGroupId": {
    "name": "Throwing_Weapons",
    "path": "texturegroups/Throwing_Weapons",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e0640368-c304-4ac2-8ee8-09b8979f4a42","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e0640368-c304-4ac2-8ee8-09b8979f4a42","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"e0640368-c304-4ac2-8ee8-09b8979f4a42","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"24471973-fce8-4f05-a909-fee82e743369","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24471973-fce8-4f05-a909-fee82e743369","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"24471973-fce8-4f05-a909-fee82e743369","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"10ef7b73-6ed4-425a-8385-863677a23b8c","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"10ef7b73-6ed4-425a-8385-863677a23b8c","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"10ef7b73-6ed4-425a-8385-863677a23b8c","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3c9b769e-df1f-43fe-9e1f-4788e7022299","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3c9b769e-df1f-43fe-9e1f-4788e7022299","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"3c9b769e-df1f-43fe-9e1f-4788e7022299","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b25ff44d-0bad-4753-ba4c-5caee7d944e4","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b25ff44d-0bad-4753-ba4c-5caee7d944e4","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"b25ff44d-0bad-4753-ba4c-5caee7d944e4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"456d1cff-5aef-4410-af00-7ea52ca5a489","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"456d1cff-5aef-4410-af00-7ea52ca5a489","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"456d1cff-5aef-4410-af00-7ea52ca5a489","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3635ea88-d67e-421d-928d-e38c4ca1484b","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3635ea88-d67e-421d-928d-e38c4ca1484b","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"3635ea88-d67e-421d-928d-e38c4ca1484b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"27ca1db3-2052-4e7d-b8ec-29ad1289a310","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"27ca1db3-2052-4e7d-b8ec-29ad1289a310","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"27ca1db3-2052-4e7d-b8ec-29ad1289a310","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1b92dbf4-0b34-40fa-bbe6-be6a005f4df5","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b92dbf4-0b34-40fa-bbe6-be6a005f4df5","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"1b92dbf4-0b34-40fa-bbe6-be6a005f4df5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b8990a36-6d81-4a90-b6e4-af670c7993f7","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8990a36-6d81-4a90-b6e4-af670c7993f7","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"b8990a36-6d81-4a90-b6e4-af670c7993f7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"bf788f2d-a203-4386-9a4e-5211da7eb799","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bf788f2d-a203-4386-9a4e-5211da7eb799","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"LayerId":{"name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","name":"bf788f2d-a203-4386-9a4e-5211da7eb799","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 11.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5608cde0-cdf6-49ac-b140-071b3324727b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e0640368-c304-4ac2-8ee8-09b8979f4a42","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fff10282-0f44-4399-a1ce-313ad866eb9c","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24471973-fce8-4f05-a909-fee82e743369","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4d352cc9-6623-4c6c-ac4a-cbaaa1390f00","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"10ef7b73-6ed4-425a-8385-863677a23b8c","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d233fa8d-c36a-43f8-aaa5-31ab104b7738","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3c9b769e-df1f-43fe-9e1f-4788e7022299","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"db994332-070f-4686-9df4-e3708db5e3c7","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b25ff44d-0bad-4753-ba4c-5caee7d944e4","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6fc3cf41-66e7-41ca-bec5-2862df5ea351","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"456d1cff-5aef-4410-af00-7ea52ca5a489","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dd7c391d-3a37-46ae-b723-28a7888c37c6","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3635ea88-d67e-421d-928d-e38c4ca1484b","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13dd8a91-8b70-40a2-b6f9-2bbafa5d1daa","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"27ca1db3-2052-4e7d-b8ec-29ad1289a310","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6776f033-71f0-40e5-95a4-b102c117ee60","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b92dbf4-0b34-40fa-bbe6-be6a005f4df5","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"81ebeb3e-5320-4e8c-82a0-400216084f47","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8990a36-6d81-4a90-b6e4-af670c7993f7","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ebb3d48-eb7f-4e4f-9ae6-1c0beaf11c7c","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bf788f2d-a203-4386-9a4e-5211da7eb799","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 71,
    "yorigin": 316,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_StrAtk_Mjolnir2","path":"sprites/spr_StrAtk_Mjolnir2/spr_StrAtk_Mjolnir2.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"66fb7e51-64b5-4a72-9f00-2dc6b6909d8d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "StrongAtks",
    "path": "folders/Sprites/StrongAtks.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_StrAtk_Mjolnir2",
  "tags": [],
  "resourceType": "GMSprite",
}