{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 89,
  "bbox_right": 193,
  "bbox_top": 38,
  "bbox_bottom": 215,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 218,
  "height": 216,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c6b6b2c6-e224-4242-b326-d6b85a911fc2","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6b6b2c6-e224-4242-b326-d6b85a911fc2","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},"LayerId":{"name":"d6c45a2e-3fc8-40b1-84ab-2ca8de3c3672","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Whisperer_hitbox3","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},"resourceVersion":"1.0","name":"c6b6b2c6-e224-4242-b326-d6b85a911fc2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Whisperer_hitbox3","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ec23ce72-4589-4267-826e-95a729dbe90a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6b6b2c6-e224-4242-b326-d6b85a911fc2","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 75,
    "yorigin": 160,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Whisperer_hitbox3","path":"sprites/spr_Whisperer_hitbox3/spr_Whisperer_hitbox3.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d6c45a2e-3fc8-40b1-84ab-2ca8de3c3672","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Whisperer_hitbox3",
  "tags": [],
  "resourceType": "GMSprite",
}