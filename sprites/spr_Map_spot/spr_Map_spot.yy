{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 18,
  "bbox_top": 0,
  "bbox_bottom": 18,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 19,
  "height": 19,
  "textureGroupId": {
    "name": "Map",
    "path": "texturegroups/Map",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5ccb68d4-0deb-4f18-afb3-ed84bfdf8440","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5ccb68d4-0deb-4f18-afb3-ed84bfdf8440","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"LayerId":{"name":"e7bdf92d-cee0-4265-8f99-fb22888cf9db","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Map_spot","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","name":"5ccb68d4-0deb-4f18-afb3-ed84bfdf8440","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d4685c4b-4877-4813-8da1-9627f6917138","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d4685c4b-4877-4813-8da1-9627f6917138","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"LayerId":{"name":"e7bdf92d-cee0-4265-8f99-fb22888cf9db","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Map_spot","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","name":"d4685c4b-4877-4813-8da1-9627f6917138","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Map_spot","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MessageEventKeyframe",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"MomentsEventKeyframe",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f4324a6a-45dd-4749-853c-7f697910a3f2","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5ccb68d4-0deb-4f18-afb3-ed84bfdf8440","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
            {"id":"198ba546-88e1-4843-b113-757d45ad627e","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d4685c4b-4877-4813-8da1-9627f6917138","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe`1","elementType":"SpriteFrameKeyframe",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore`1","elementType":"SpriteFrameKeyframe",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 9,
    "yorigin": 9,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Map_spot","path":"sprites/spr_Map_spot/spr_Map_spot.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e7bdf92d-cee0-4265-8f99-fb22888cf9db","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Signs",
    "path": "folders/Sprites/Signs.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Map_spot",
  "tags": [],
  "resourceType": "GMSprite",
}