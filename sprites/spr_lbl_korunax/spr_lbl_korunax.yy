{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 157,
  "bbox_top": 0,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 158,
  "height": 30,
  "textureGroupId": {
    "name": "Labels",
    "path": "texturegroups/Labels",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"99aa288c-83db-4ce5-bcf2-38db2123a2eb","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"99aa288c-83db-4ce5-bcf2-38db2123a2eb","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},"LayerId":{"name":"f208edf9-8260-4e8f-a706-0dd0c618c6b5","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lbl_korunax","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},"resourceVersion":"1.0","name":"99aa288c-83db-4ce5-bcf2-38db2123a2eb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_lbl_korunax","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"090ba473-54e0-44f6-9c0b-a2f62affca6e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"99aa288c-83db-4ce5-bcf2-38db2123a2eb","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 79,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_lbl_korunax","path":"sprites/spr_lbl_korunax/spr_lbl_korunax.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"f208edf9-8260-4e8f-a706-0dd0c618c6b5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "WpnMLabels",
    "path": "folders/Sprites/Menus/Weapons/WpnMLabels.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_lbl_korunax",
  "tags": [],
  "resourceType": "GMSprite",
}