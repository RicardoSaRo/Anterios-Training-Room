{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 0,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 47,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 48,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"ba96b6d9-715a-4b7e-abcb-118a19099fed","path":"sprites/spr_stringShot/spr_stringShot.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ba96b6d9-715a-4b7e-abcb-118a19099fed","path":"sprites/spr_stringShot/spr_stringShot.yy",},"LayerId":{"name":"40896173-01dd-4677-b282-8c68d30ad51a","path":"sprites/spr_stringShot/spr_stringShot.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_stringShot","path":"sprites/spr_stringShot/spr_stringShot.yy",},"resourceVersion":"1.0","name":"ba96b6d9-715a-4b7e-abcb-118a19099fed","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_stringShot","path":"sprites/spr_stringShot/spr_stringShot.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"651749f1-d58a-48f8-935e-8acd6217c16d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ba96b6d9-715a-4b7e-abcb-118a19099fed","path":"sprites/spr_stringShot/spr_stringShot.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 0,
    "yorigin": 0,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_stringShot","path":"sprites/spr_stringShot/spr_stringShot.yy",},
    "resourceVersion": "1.4",
    "name": "spr_stringShot",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"40896173-01dd-4677-b282-8c68d30ad51a","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "TestBot",
    "path": "folders/Sprites/Bosses/TestBot.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_stringShot",
  "tags": [],
  "resourceType": "GMSprite",
}