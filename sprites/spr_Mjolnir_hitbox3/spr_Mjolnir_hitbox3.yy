{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 187,
  "bbox_top": 18,
  "bbox_bottom": 183,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 188,
  "height": 184,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"76faa576-1291-4cc2-a27e-a7cde7d9cd5f","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"76faa576-1291-4cc2-a27e-a7cde7d9cd5f","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},"LayerId":{"name":"bd103c08-5015-4b5f-9c41-21acbf31f087","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Mjolnir_hitbox3","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},"resourceVersion":"1.0","name":"76faa576-1291-4cc2-a27e-a7cde7d9cd5f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Mjolnir_hitbox3","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6ca67419-17cc-4c23-85b7-3f7fbcd7c813","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"76faa576-1291-4cc2-a27e-a7cde7d9cd5f","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 73,
    "yorigin": 128,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Mjolnir_hitbox3","path":"sprites/spr_Mjolnir_hitbox3/spr_Mjolnir_hitbox3.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"bd103c08-5015-4b5f-9c41-21acbf31f087","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Mjolnir_hitbox3",
  "tags": [],
  "resourceType": "GMSprite",
}