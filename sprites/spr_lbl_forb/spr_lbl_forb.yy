{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 154,
  "bbox_top": 0,
  "bbox_bottom": 29,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 155,
  "height": 30,
  "textureGroupId": {
    "name": "Labels",
    "path": "texturegroups/Labels",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cd4b8bc6-5bbd-4d52-a9cd-2157dae8d605","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cd4b8bc6-5bbd-4d52-a9cd-2157dae8d605","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},"LayerId":{"name":"c7cc07a0-50e4-4d9b-9a01-e08276e46c0b","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_lbl_forb","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},"resourceVersion":"1.0","name":"cd4b8bc6-5bbd-4d52-a9cd-2157dae8d605","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_lbl_forb","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"c4706a1a-8fa3-49cc-9f4d-007cab5cb967","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cd4b8bc6-5bbd-4d52-a9cd-2157dae8d605","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 77,
    "yorigin": 15,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_lbl_forb","path":"sprites/spr_lbl_forb/spr_lbl_forb.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"c7cc07a0-50e4-4d9b-9a01-e08276e46c0b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "WpnMLabels",
    "path": "folders/Sprites/Menus/Weapons/WpnMLabels.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_lbl_forb",
  "tags": [],
  "resourceType": "GMSprite",
}