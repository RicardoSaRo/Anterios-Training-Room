{
  "bboxMode": 1,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 163,
  "bbox_top": 0,
  "bbox_bottom": 163,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 164,
  "height": 164,
  "textureGroupId": {
    "name": "Manticore_Specials",
    "path": "texturegroups/Manticore_Specials",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a076a8d9-b1bb-4e00-a439-5c065f629c88","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a076a8d9-b1bb-4e00-a439-5c065f629c88","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":{"name":"47b5f98a-9e95-42be-bd4a-1c244817ddf0","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"a076a8d9-b1bb-4e00-a439-5c065f629c88","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8735cdbf-d526-46c8-80bd-eeb432192dd4","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8735cdbf-d526-46c8-80bd-eeb432192dd4","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":{"name":"47b5f98a-9e95-42be-bd4a-1c244817ddf0","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"8735cdbf-d526-46c8-80bd-eeb432192dd4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d280bd6f-ee7c-4d02-909d-dcb024651f37","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d280bd6f-ee7c-4d02-909d-dcb024651f37","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":{"name":"47b5f98a-9e95-42be-bd4a-1c244817ddf0","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"d280bd6f-ee7c-4d02-909d-dcb024651f37","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"231e2300-bfce-44e5-b49a-9ca04eb11841","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"231e2300-bfce-44e5-b49a-9ca04eb11841","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"LayerId":{"name":"47b5f98a-9e95-42be-bd4a-1c244817ddf0","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","name":"231e2300-bfce-44e5-b49a-9ca04eb11841","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"933f2b67-b24a-413a-bf23-61011c2a4697","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a076a8d9-b1bb-4e00-a439-5c065f629c88","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6f514f10-a186-4f59-8aa7-6e063ccb2bba","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8735cdbf-d526-46c8-80bd-eeb432192dd4","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"13a6e2a3-61d4-4e4e-9675-b8332ea9c108","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d280bd6f-ee7c-4d02-909d-dcb024651f37","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3be10b78-0181-4e4a-b9c7-2ca1b0be73fe","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"231e2300-bfce-44e5-b49a-9ca04eb11841","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 82,
    "yorigin": 82,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_wow_bomb","path":"sprites/spr_wow_bomb/spr_wow_bomb.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"47b5f98a-9e95-42be-bd4a-1c244817ddf0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Proyectiles/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_wow_bomb",
  "tags": [],
  "resourceType": "GMSprite",
}