{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 47,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 48,
  "textureGroupId": {
    "name": "GUI",
    "path": "texturegroups/GUI",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"90252b4f-3c59-43d6-b63a-1c58e242db82","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"90252b4f-3c59-43d6-b63a-1c58e242db82","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},"LayerId":{"name":"2c1788c7-188c-4129-a3af-734375962e79","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_icon_GirTab","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},"resourceVersion":"1.0","name":"90252b4f-3c59-43d6-b63a-1c58e242db82","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_icon_GirTab","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4703b348-5221-4d2c-9e1b-14b2b2687cbb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"90252b4f-3c59-43d6-b63a-1c58e242db82","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_icon_GirTab","path":"sprites/spr_icon_GirTab/spr_icon_GirTab.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2c1788c7-188c-4129-a3af-734375962e79","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Icons",
    "path": "folders/Sprites/Health_Bars/Icons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_icon_GirTab",
  "tags": [],
  "resourceType": "GMSprite",
}