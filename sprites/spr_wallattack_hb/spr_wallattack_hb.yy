{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 22,
  "bbox_right": 155,
  "bbox_top": 0,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 156,
  "height": 144,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"195fd250-fae1-46dd-99d4-646acc91bd51","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"195fd250-fae1-46dd-99d4-646acc91bd51","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":{"name":"5f58e911-807f-4584-a0aa-be8c12dc74ea","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"195fd250-fae1-46dd-99d4-646acc91bd51","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"42be7a9e-5de8-47b4-917a-d11accf7eaec","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42be7a9e-5de8-47b4-917a-d11accf7eaec","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":{"name":"5f58e911-807f-4584-a0aa-be8c12dc74ea","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"42be7a9e-5de8-47b4-917a-d11accf7eaec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b6d58ecc-bf70-4ebc-9d27-99ed7e05dea6","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b6d58ecc-bf70-4ebc-9d27-99ed7e05dea6","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":{"name":"5f58e911-807f-4584-a0aa-be8c12dc74ea","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"b6d58ecc-bf70-4ebc-9d27-99ed7e05dea6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"62ac8eec-0546-450f-9da0-1cd46bd7860f","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"62ac8eec-0546-450f-9da0-1cd46bd7860f","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"LayerId":{"name":"5f58e911-807f-4584-a0aa-be8c12dc74ea","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","name":"62ac8eec-0546-450f-9da0-1cd46bd7860f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 12.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"74f43dcf-d12e-4d43-9a3b-8ba853a25760","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"195fd250-fae1-46dd-99d4-646acc91bd51","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1e9de6f9-3128-46db-9542-8f300d8e28d9","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42be7a9e-5de8-47b4-917a-d11accf7eaec","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b31d7ae3-fffb-47f4-b49c-0d4710d4d080","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b6d58ecc-bf70-4ebc-9d27-99ed7e05dea6","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e41089ca-6e56-4959-aa66-ce539e1ccaf8","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"62ac8eec-0546-450f-9da0-1cd46bd7860f","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 64,
    "yorigin": 72,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_wallattack_hb","path":"sprites/spr_wallattack_hb/spr_wallattack_hb.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5f58e911-807f-4584-a0aa-be8c12dc74ea","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_wallattack_hb",
  "tags": [],
  "resourceType": "GMSprite",
}