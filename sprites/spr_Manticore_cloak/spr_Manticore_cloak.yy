{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 123,
  "bbox_top": 0,
  "bbox_bottom": 131,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 136,
  "height": 132,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9d740771-e699-47d2-ac2f-96c7dda32b63","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9d740771-e699-47d2-ac2f-96c7dda32b63","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"9d740771-e699-47d2-ac2f-96c7dda32b63","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7b6fd467-4fec-44c2-87f3-2006c7fd1a0a","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7b6fd467-4fec-44c2-87f3-2006c7fd1a0a","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"7b6fd467-4fec-44c2-87f3-2006c7fd1a0a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f60bcf4-f594-48ff-9e62-778e2bb7a5db","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f60bcf4-f594-48ff-9e62-778e2bb7a5db","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"3f60bcf4-f594-48ff-9e62-778e2bb7a5db","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"3f22e984-296d-4863-a451-2eef2f0c310e","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3f22e984-296d-4863-a451-2eef2f0c310e","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"3f22e984-296d-4863-a451-2eef2f0c310e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a945a8dc-c2d1-4614-b7ec-6c1d77866ec8","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a945a8dc-c2d1-4614-b7ec-6c1d77866ec8","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"a945a8dc-c2d1-4614-b7ec-6c1d77866ec8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"26120929-7d46-4383-a954-b830d833408f","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"26120929-7d46-4383-a954-b830d833408f","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"26120929-7d46-4383-a954-b830d833408f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b9516541-6b9f-4bb7-b1ab-7cbe3e3d0f14","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b9516541-6b9f-4bb7-b1ab-7cbe3e3d0f14","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"b9516541-6b9f-4bb7-b1ab-7cbe3e3d0f14","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4f6b970c-7513-499c-890f-e107d518d6a3","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f6b970c-7513-499c-890f-e107d518d6a3","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"4f6b970c-7513-499c-890f-e107d518d6a3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"16446a57-e98d-4a44-b00c-751c5f9ce82d","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"16446a57-e98d-4a44-b00c-751c5f9ce82d","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"LayerId":{"name":"edca8976-099e-4c57-a673-94a2ee6b5b48","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","name":"16446a57-e98d-4a44-b00c-751c5f9ce82d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 8.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 9.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"006cf602-9a43-47eb-bfc9-c2ef10532c30","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9d740771-e699-47d2-ac2f-96c7dda32b63","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0cac2832-8581-4c0c-97ff-8f117e039a23","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7b6fd467-4fec-44c2-87f3-2006c7fd1a0a","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bd305211-13de-4d25-9bc6-320aa7c99f10","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f60bcf4-f594-48ff-9e62-778e2bb7a5db","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7a745cdd-3083-46ef-8f14-bf46858724ef","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3f22e984-296d-4863-a451-2eef2f0c310e","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3ced6fc6-05a3-4de8-950a-ba1b1f2ca8b7","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a945a8dc-c2d1-4614-b7ec-6c1d77866ec8","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"63c232fe-cb1f-499e-9446-e72dfc62944a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"26120929-7d46-4383-a954-b830d833408f","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"32d58079-10d9-4f6c-8080-46ec87977466","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b9516541-6b9f-4bb7-b1ab-7cbe3e3d0f14","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8b3cf8db-2d06-47ad-96ff-d85767bca813","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f6b970c-7513-499c-890f-e107d518d6a3","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"36f18eb7-5115-41a3-8e74-c29f6c36bfdb","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"16446a57-e98d-4a44-b00c-751c5f9ce82d","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 99,
    "yorigin": 76,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Manticore_cloak","path":"sprites/spr_Manticore_cloak/spr_Manticore_cloak.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"edca8976-099e-4c57-a673-94a2ee6b5b48","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "Manticore",
    "path": "folders/Sprites/Manticore.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Manticore_cloak",
  "tags": [],
  "resourceType": "GMSprite",
}