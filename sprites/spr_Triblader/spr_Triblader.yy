{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 35,
  "bbox_top": 0,
  "bbox_bottom": 35,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 36,
  "height": 36,
  "textureGroupId": {
    "name": "Throwing_Weapons",
    "path": "texturegroups/Throwing_Weapons",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c9b04d53-9c75-422a-a7ba-e64f53e6f849","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c9b04d53-9c75-422a-a7ba-e64f53e6f849","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":{"name":"aafc7d23-ea1f-46fa-bdba-62200813b753","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"c9b04d53-9c75-422a-a7ba-e64f53e6f849","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6e7521e-c2ba-4b5b-9481-3256e1dbf5d0","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6e7521e-c2ba-4b5b-9481-3256e1dbf5d0","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":{"name":"aafc7d23-ea1f-46fa-bdba-62200813b753","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"e6e7521e-c2ba-4b5b-9481-3256e1dbf5d0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0379d401-a8d0-4789-bf18-ae6bf2687e08","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0379d401-a8d0-4789-bf18-ae6bf2687e08","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":{"name":"aafc7d23-ea1f-46fa-bdba-62200813b753","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"0379d401-a8d0-4789-bf18-ae6bf2687e08","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"05800151-bad3-447c-8d1e-81f88f9d7b41","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"05800151-bad3-447c-8d1e-81f88f9d7b41","path":"sprites/spr_Triblader/spr_Triblader.yy",},"LayerId":{"name":"aafc7d23-ea1f-46fa-bdba-62200813b753","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","name":"05800151-bad3-447c-8d1e-81f88f9d7b41","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6ff65753-3f08-47b5-b95d-70565d0dd484","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c9b04d53-9c75-422a-a7ba-e64f53e6f849","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9204fa15-7888-40bc-a08a-3abb5e0bcaa5","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6e7521e-c2ba-4b5b-9481-3256e1dbf5d0","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fad43603-4008-4067-bc1d-a365e2534e75","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0379d401-a8d0-4789-bf18-ae6bf2687e08","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"285e83c6-9697-4471-9d13-82da0884ffe5","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"05800151-bad3-447c-8d1e-81f88f9d7b41","path":"sprites/spr_Triblader/spr_Triblader.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 18,
    "yorigin": 18,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Triblader","path":"sprites/spr_Triblader/spr_Triblader.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"aafc7d23-ea1f-46fa-bdba-62200813b753","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "ThrowingWeapons",
    "path": "folders/Sprites/ThrowingWeapons.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Triblader",
  "tags": [],
  "resourceType": "GMSprite",
}