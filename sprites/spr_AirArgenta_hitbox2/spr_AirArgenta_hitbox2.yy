{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 98,
  "bbox_right": 195,
  "bbox_top": 68,
  "bbox_bottom": 195,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 196,
  "height": 196,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"18a6d31a-bd28-4db0-890c-82a6df041be8","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"18a6d31a-bd28-4db0-890c-82a6df041be8","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},"LayerId":{"name":"015f39e5-ab7d-4b2d-b683-713eeca5863f","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_AirArgenta_hitbox2","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},"resourceVersion":"1.0","name":"18a6d31a-bd28-4db0-890c-82a6df041be8","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_AirArgenta_hitbox2","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 25.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"26215684-3e05-4be6-88c9-11379ee93c12","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"18a6d31a-bd28-4db0-890c-82a6df041be8","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 98,
    "yorigin": 98,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_AirArgenta_hitbox2","path":"sprites/spr_AirArgenta_hitbox2/spr_AirArgenta_hitbox2.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"015f39e5-ab7d-4b2d-b683-713eeca5863f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_AirArgenta_hitbox2",
  "tags": [],
  "resourceType": "GMSprite",
}