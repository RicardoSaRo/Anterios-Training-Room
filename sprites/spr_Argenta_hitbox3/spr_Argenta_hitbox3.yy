{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 49,
  "bbox_right": 201,
  "bbox_top": 0,
  "bbox_bottom": 156,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 202,
  "height": 162,
  "textureGroupId": {
    "name": "Manticore_Hitboxes",
    "path": "texturegroups/Manticore_Hitboxes",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"c8569c61-d206-44dc-a9a0-1a12f592cc8f","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c8569c61-d206-44dc-a9a0-1a12f592cc8f","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},"LayerId":{"name":"8cd0a6b2-4d26-4647-9fb6-244a5dab69fe","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Argenta_hitbox3","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},"resourceVersion":"1.0","name":"c8569c61-d206-44dc-a9a0-1a12f592cc8f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Argenta_hitbox3","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6ffe1dbd-eaa7-4892-a1e6-7683bf2e3e47","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c8569c61-d206-44dc-a9a0-1a12f592cc8f","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 57,
    "yorigin": 106,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Argenta_hitbox3","path":"sprites/spr_Argenta_hitbox3/spr_Argenta_hitbox3.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8cd0a6b2-4d26-4647-9fb6-244a5dab69fe","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "HitBoxes",
    "path": "folders/Sprites/HitBoxes.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Argenta_hitbox3",
  "tags": [],
  "resourceType": "GMSprite",
}