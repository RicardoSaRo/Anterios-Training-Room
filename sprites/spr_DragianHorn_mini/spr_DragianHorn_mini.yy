{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 82,
  "bbox_top": 0,
  "bbox_bottom": 82,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 83,
  "height": 83,
  "textureGroupId": {
    "name": "Reliks",
    "path": "texturegroups/Reliks",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"1b858ad6-624c-4279-a129-ea06c466896d","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},"LayerId":null,"resourceVersion":"1.0","name":"composite","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1b858ad6-624c-4279-a129-ea06c466896d","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},"LayerId":{"name":"b3e09aa2-8a92-41bf-9601-9d667815ee49","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_DragianHorn_mini","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},"resourceVersion":"1.0","name":"1b858ad6-624c-4279-a129-ea06c466896d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_DragianHorn_mini","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"9cf80c28-4998-4332-9750-0bca7bd78754","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1b858ad6-624c-4279-a129-ea06c466896d","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack","modifiers":[],},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 41,
    "yorigin": 41,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_DragianHorn_mini","path":"sprites/spr_DragianHorn_mini/spr_DragianHorn_mini.yy",},
    "resourceVersion": "1.4",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b3e09aa2-8a92-41bf-9601-9d667815ee49","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "RelikMini",
    "path": "folders/Sprites/Menus/Reliks/RelikMini.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_DragianHorn_mini",
  "tags": [],
  "resourceType": "GMSprite",
}