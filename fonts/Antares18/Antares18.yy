{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "applyKerning": 0,
  "fontName": "Antares",
  "styleName": "Normal",
  "size": 18.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 0,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "ascender": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":12,"h":32,"character":32,"shift":12,"offset":0,},
    "33": {"x":191,"y":104,"w":9,"h":32,"character":33,"shift":9,"offset":0,},
    "34": {"x":180,"y":104,"w":9,"h":32,"character":34,"shift":12,"offset":3,},
    "35": {"x":163,"y":104,"w":15,"h":32,"character":35,"shift":18,"offset":3,},
    "36": {"x":143,"y":104,"w":18,"h":32,"character":36,"shift":21,"offset":3,},
    "37": {"x":126,"y":104,"w":15,"h":32,"character":37,"shift":18,"offset":3,},
    "38": {"x":109,"y":104,"w":15,"h":32,"character":38,"shift":18,"offset":3,},
    "39": {"x":104,"y":104,"w":3,"h":32,"character":39,"shift":6,"offset":3,},
    "40": {"x":96,"y":104,"w":6,"h":32,"character":40,"shift":9,"offset":3,},
    "41": {"x":88,"y":104,"w":6,"h":32,"character":41,"shift":9,"offset":3,},
    "42": {"x":202,"y":104,"w":15,"h":32,"character":42,"shift":18,"offset":3,},
    "43": {"x":71,"y":104,"w":15,"h":32,"character":43,"shift":18,"offset":3,},
    "44": {"x":46,"y":104,"w":6,"h":32,"character":44,"shift":9,"offset":3,},
    "45": {"x":35,"y":104,"w":9,"h":32,"character":45,"shift":12,"offset":3,},
    "46": {"x":30,"y":104,"w":3,"h":32,"character":46,"shift":6,"offset":3,},
    "47": {"x":19,"y":104,"w":9,"h":32,"character":47,"shift":12,"offset":3,},
    "48": {"x":2,"y":104,"w":15,"h":32,"character":48,"shift":18,"offset":3,},
    "49": {"x":243,"y":70,"w":9,"h":32,"character":49,"shift":12,"offset":3,},
    "50": {"x":226,"y":70,"w":15,"h":32,"character":50,"shift":18,"offset":3,},
    "51": {"x":209,"y":70,"w":15,"h":32,"character":51,"shift":18,"offset":3,},
    "52": {"x":192,"y":70,"w":15,"h":32,"character":52,"shift":18,"offset":3,},
    "53": {"x":54,"y":104,"w":15,"h":32,"character":53,"shift":18,"offset":3,},
    "54": {"x":2,"y":138,"w":15,"h":32,"character":54,"shift":18,"offset":3,},
    "55": {"x":174,"y":138,"w":15,"h":32,"character":55,"shift":18,"offset":3,},
    "56": {"x":19,"y":138,"w":15,"h":32,"character":56,"shift":18,"offset":3,},
    "57": {"x":71,"y":172,"w":15,"h":32,"character":57,"shift":18,"offset":3,},
    "58": {"x":66,"y":172,"w":3,"h":32,"character":58,"shift":6,"offset":3,},
    "59": {"x":58,"y":172,"w":6,"h":32,"character":59,"shift":9,"offset":3,},
    "60": {"x":44,"y":172,"w":12,"h":32,"character":60,"shift":15,"offset":3,},
    "61": {"x":30,"y":172,"w":12,"h":32,"character":61,"shift":15,"offset":3,},
    "62": {"x":16,"y":172,"w":12,"h":32,"character":62,"shift":15,"offset":3,},
    "63": {"x":2,"y":172,"w":12,"h":32,"character":63,"shift":15,"offset":3,},
    "64": {"x":225,"y":138,"w":24,"h":32,"character":64,"shift":27,"offset":3,},
    "65": {"x":208,"y":138,"w":15,"h":32,"character":65,"shift":18,"offset":3,},
    "66": {"x":88,"y":172,"w":15,"h":32,"character":66,"shift":18,"offset":3,},
    "67": {"x":191,"y":138,"w":15,"h":32,"character":67,"shift":18,"offset":3,},
    "68": {"x":157,"y":138,"w":15,"h":32,"character":68,"shift":18,"offset":3,},
    "69": {"x":140,"y":138,"w":15,"h":32,"character":69,"shift":18,"offset":3,},
    "70": {"x":123,"y":138,"w":15,"h":32,"character":70,"shift":18,"offset":3,},
    "71": {"x":106,"y":138,"w":15,"h":32,"character":71,"shift":18,"offset":3,},
    "72": {"x":89,"y":138,"w":15,"h":32,"character":72,"shift":18,"offset":3,},
    "73": {"x":78,"y":138,"w":9,"h":32,"character":73,"shift":12,"offset":3,},
    "74": {"x":67,"y":138,"w":9,"h":32,"character":74,"shift":12,"offset":3,},
    "75": {"x":50,"y":138,"w":15,"h":32,"character":75,"shift":18,"offset":3,},
    "76": {"x":36,"y":138,"w":12,"h":32,"character":76,"shift":15,"offset":3,},
    "77": {"x":169,"y":70,"w":21,"h":32,"character":77,"shift":24,"offset":3,},
    "78": {"x":219,"y":104,"w":18,"h":32,"character":78,"shift":21,"offset":3,},
    "79": {"x":152,"y":70,"w":15,"h":32,"character":79,"shift":18,"offset":3,},
    "80": {"x":84,"y":36,"w":12,"h":32,"character":80,"shift":15,"offset":3,},
    "81": {"x":50,"y":36,"w":15,"h":32,"character":81,"shift":18,"offset":3,},
    "82": {"x":36,"y":36,"w":12,"h":32,"character":82,"shift":15,"offset":3,},
    "83": {"x":19,"y":36,"w":15,"h":32,"character":83,"shift":18,"offset":3,},
    "84": {"x":2,"y":36,"w":15,"h":32,"character":84,"shift":18,"offset":3,},
    "85": {"x":232,"y":2,"w":15,"h":32,"character":85,"shift":18,"offset":3,},
    "86": {"x":215,"y":2,"w":15,"h":32,"character":86,"shift":18,"offset":3,},
    "87": {"x":192,"y":2,"w":21,"h":32,"character":87,"shift":24,"offset":3,},
    "88": {"x":175,"y":2,"w":15,"h":32,"character":88,"shift":18,"offset":3,},
    "89": {"x":158,"y":2,"w":15,"h":32,"character":89,"shift":18,"offset":3,},
    "90": {"x":67,"y":36,"w":15,"h":32,"character":90,"shift":18,"offset":3,},
    "91": {"x":150,"y":2,"w":6,"h":32,"character":91,"shift":9,"offset":3,},
    "92": {"x":125,"y":2,"w":9,"h":32,"character":92,"shift":12,"offset":3,},
    "93": {"x":117,"y":2,"w":6,"h":32,"character":93,"shift":9,"offset":3,},
    "94": {"x":100,"y":2,"w":15,"h":32,"character":94,"shift":18,"offset":3,},
    "95": {"x":83,"y":2,"w":15,"h":32,"character":95,"shift":14,"offset":0,},
    "96": {"x":75,"y":2,"w":6,"h":32,"character":96,"shift":6,"offset":0,},
    "97": {"x":58,"y":2,"w":15,"h":32,"character":97,"shift":18,"offset":3,},
    "98": {"x":44,"y":2,"w":12,"h":32,"character":98,"shift":15,"offset":3,},
    "99": {"x":30,"y":2,"w":12,"h":32,"character":99,"shift":15,"offset":3,},
    "100": {"x":16,"y":2,"w":12,"h":32,"character":100,"shift":15,"offset":3,},
    "101": {"x":136,"y":2,"w":12,"h":32,"character":101,"shift":15,"offset":3,},
    "102": {"x":98,"y":36,"w":9,"h":32,"character":102,"shift":12,"offset":3,},
    "103": {"x":237,"y":36,"w":12,"h":32,"character":103,"shift":15,"offset":3,},
    "104": {"x":109,"y":36,"w":12,"h":32,"character":104,"shift":15,"offset":3,},
    "105": {"x":125,"y":70,"w":3,"h":32,"character":105,"shift":6,"offset":3,},
    "106": {"x":117,"y":70,"w":6,"h":32,"character":106,"shift":9,"offset":3,},
    "107": {"x":103,"y":70,"w":12,"h":32,"character":107,"shift":15,"offset":3,},
    "108": {"x":98,"y":70,"w":3,"h":32,"character":108,"shift":6,"offset":3,},
    "109": {"x":78,"y":70,"w":18,"h":32,"character":109,"shift":21,"offset":3,},
    "110": {"x":64,"y":70,"w":12,"h":32,"character":110,"shift":15,"offset":3,},
    "111": {"x":47,"y":70,"w":15,"h":32,"character":111,"shift":18,"offset":3,},
    "112": {"x":33,"y":70,"w":12,"h":32,"character":112,"shift":15,"offset":3,},
    "113": {"x":19,"y":70,"w":12,"h":32,"character":113,"shift":15,"offset":3,},
    "114": {"x":130,"y":70,"w":9,"h":32,"character":114,"shift":12,"offset":3,},
    "115": {"x":2,"y":70,"w":15,"h":32,"character":115,"shift":18,"offset":3,},
    "116": {"x":226,"y":36,"w":9,"h":32,"character":116,"shift":12,"offset":3,},
    "117": {"x":212,"y":36,"w":12,"h":32,"character":117,"shift":15,"offset":3,},
    "118": {"x":198,"y":36,"w":12,"h":32,"character":118,"shift":15,"offset":3,},
    "119": {"x":181,"y":36,"w":15,"h":32,"character":119,"shift":18,"offset":3,},
    "120": {"x":167,"y":36,"w":12,"h":32,"character":120,"shift":15,"offset":3,},
    "121": {"x":153,"y":36,"w":12,"h":32,"character":121,"shift":15,"offset":3,},
    "122": {"x":139,"y":36,"w":12,"h":32,"character":122,"shift":15,"offset":3,},
    "123": {"x":128,"y":36,"w":9,"h":32,"character":123,"shift":12,"offset":3,},
    "124": {"x":123,"y":36,"w":3,"h":32,"character":124,"shift":9,"offset":3,},
    "125": {"x":141,"y":70,"w":9,"h":32,"character":125,"shift":12,"offset":3,},
    "126": {"x":105,"y":172,"w":18,"h":32,"character":126,"shift":21,"offset":3,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":32,"upper":127,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "Antares18",
  "tags": [],
  "resourceType": "GMFont",
}